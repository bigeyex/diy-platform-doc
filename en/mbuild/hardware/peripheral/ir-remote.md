#  IR Remote

The IR Remote works with the IR Transceiver.

<img src="../../../../zh/mbuild/hardware/peripheral/images/ir-remote.png" style="padding:3px 0px 12px 3px;width:300px;">

### Parameters

- Size: 90×40×12mm
- Frequency: 940nm
- Protocol: NEC
- Transmission distance: 8m
- Powering method: CR2025 button battery
- Button life span: > 200,000times