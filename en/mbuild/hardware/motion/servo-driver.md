# Servo Driver

The Servo Driver can drive all kinds of 180° servos, controlling the degree of rotation. 

<img src="../../../../zh/mbuild/hardware/motion/images/servo-driver.png" style="padding:3px 0px 12px 3px;width:200px;">

### Parameters

- Size: 24×24mm
- Operating current: < 1A