# Sensors
  
* [Light Sensor](sensors/light-sensor.md)
* [Dual Color Sensor](sensors/dual-color-sensor.md)
* [Sound Sensor](sensors/sound-sensor.md)
* [Thermal Motion Sensor](sensors/thermal-motion-sensor.md)
* [Ultrasonic Sensor](sensors/ultrasonic-sensor.md)
* [Ranging Sensor](sensors/ranging-sensor.md)
* [Motion Sensor](sensors/motion-sensor.md)
* [Soil Moisture Sensor](sensors/soil-moisture-sensor.md)
* [Temperature Sensor](sensors/temperature-sensor.md)
* [Humiture Sensor](sensors/humiture-sensor.md)
* [MQ2 Gas Sensor](sensors/gas-sensor.md)
* [Flame Sensor](sensors/flame-sensor.md)  