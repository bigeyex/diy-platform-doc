# M4 Adapter

The M4 Adapter is used to connect mbuild modules to parts with M4 holes, or connect different mbuild modules.

<img src="../../../../zh/mbuild/hardware/accessories/images/connect-1.png" style="padding:3px 0px 12px 3px;width:300px;">

### Examples

**Fixation**

<img src="../../../../zh/mbuild/hardware/accessories/images/connect-2.png" style="padding:3px 0px 12px 3px;width:300px;">

**Mounting**

<img src="../../../../zh/mbuild/hardware/accessories/images/connect-3.png" style="padding:3px 0px 12px 3px;width:300px;">
