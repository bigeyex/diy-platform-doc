# Extend Module

The Extend Module can connect to two pieces of 5V Universal Wire, so as to extend the length.

<img src="../../../../zh/mbuild/hardware/accessories/images/extend.png" style="padding:6px 0px 12px 3px;width:200px;">

### Connection Example

<img src="../../../../zh/mbuild/hardware/accessories/images/extend-1.png" style="padding:6px 0px 12px 3px;width:300px;">