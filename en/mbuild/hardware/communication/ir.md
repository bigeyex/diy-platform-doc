# IR Transceiver

The IR Transceiver can transmit and receive infrared signals in the 940mm band.

<img src="../../../../zh/mbuild/hardware/communication/images/ir.png" style="padding:3px 0px 12px 3px;width:200px;">

### Real-Life Examples

- Air conditioner and the ir remote control<br>
<img src="../../../../zh/mbuild/hardware/communication/images/ir-1.jpg" style="padding:8px 0px 12px 3px;width:300px;">
- TV and the ir remote control<br>
<img src="../../../../zh/mbuild/hardware/communication/images/ir-2.jpg" style="padding:8px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×24mm
- Wave length: 940nm
- Protocol: NEC
- Transmission distance: 6m
- Receiver range: 6m
- Operating current: 40mA