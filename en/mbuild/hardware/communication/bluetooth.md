# Bluetooth

The Bluetooth Block works with Makeblock Bluetooth Adapter or devices supporting Bluetooth 4.0. You can make creation wirelessly.

<img src="../../../../zh/mbuild/hardware/communication/images/bluetooth.png" style="padding:3px 0px 12px 3px;width:300px;">

Bluetooth connection guide: please visit [Connect device via Bluetooth 4.0](http://www.mblock.cc/doc/en/part-one-basics/connect-devices.html)

### LED Indicator

- Blinking: Bluetooth not connected
- Solid on: Bluetooth connected and working normally
- Off: system off, or error

### Parameters

- Size: 24×24mm
- Bluetooth version: BT4.0
- Band width: 2402~2480MHz
- Antenna gain: 1.5dBi
- Energy consumption: ≤4dBm
- Operating current: 15mA