# RGB LED

The RGB led can emit light with different color or lightness, according to received signal or programming command.

<img src="../../../../zh/mbuild/hardware/light/images/rgb-led.png" style="padding:3px 0px 12px 3px;width:200px;">

### Real-Life Examples

- Light torch has a single-color LED<br>
<img src="../../../../zh/mbuild/hardware/light/images/rgb-led-1.jpg" style="padding:8px 0px 12px 3px;width:300px;">
- The colorful keyboard of gaming laptops also uses RGB LEDs<br>
<img src="../../../../zh/mbuild/hardware/light/images/rgb-led-2.jpg" style="padding:8px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×20mm
- Operating current: 15~73mA