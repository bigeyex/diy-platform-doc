# LED Driver

The LED Driver works with LED, LED strip, LED ring, and the like.

<img src="../../../../zh/mbuild/hardware/light/images/led-driver.png" style="padding:3px 0px 12px 3px;width:200px;">

---

### LED Strip

<img src="../../../../zh/mbuild/hardware/light/images/led-strip.png" style="padding:3px 0px 12px 3px;width:600px;">

The LED Strip can be used in various scenarios, such as ambient lighting, light effect texts, and so on.

<img src="../../../../zh/mbuild/hardware/light/images/led-strip-1.jpg" style="padding:3px 0px 12px 3px;width:200px;"><img src="../../../../zh/mbuild/hardware/light/images/led-strip-2.jpg" style="padding:3px 0px 12px 3px;width:200px;">

**Parameters**

- Size: 8×112mm

---

### 12 RGB LED Ring

<img src="../../../../zh/mbuild/hardware/light/images/led-ring-1.png" style="padding:3px 0px 12px 3px;width:200px;">
<img src="../../../../zh/mbuild/hardware/light/images/led-ring-2.png" style="padding:3px 0px 12px 3px;width:200px;">

The 12 LEDs are as follow:

<img src="../../../../zh/mbuild/hardware/light/images/led-ring-3.png" style="padding:3px 0px 12px 3px;width:300px;">

**Parameters**

- Size: 45mm(diameter)

---


