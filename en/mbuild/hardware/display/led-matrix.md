# 8×16 Blue LED Matrix

The 8×16 Blue LED Matrix can display animation, emoji, text, or number. Each dot on the matrix contains a LED, which can be controlled individually.

<img src="../../../../zh/mbuild/hardware/display/images/led-matrix.png" style="padding:3px 0px 12px 3px;width:300px;">

### Real-Life Examples

- Airport flight schedule screen is a huge LED matrix<br>
<img src="../../../../zh/mbuild/hardware/display/images/led-matrix-1.jpg" style="padding:8px 0px 12px 3px;width:300px;">

### Parameters

- Size: 64×44mm
- Operating current: 150mA

