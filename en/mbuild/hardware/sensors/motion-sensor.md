# Motion Sensor

The motion sensor consists of a 3-axis gyroscope, and a 3-axis accelerometer, to detects the motion, acceleration, and vibration of the object.

<img src="../../../../zh/mbuild/hardware/sensors/images/motion-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### The Axes and Angles

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">Axis</th>
<th style="border: 1px solid black;">Angle</th>
<th style="border: 1px solid black;">Range</th>
</tr>

</td>
<td style="border: 1px solid black;">X</td>
<td style="border: 1px solid black;">Pitch</td>
<td style="border: 1px solid black;">-180~180°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Y</td>
<td style="border: 1px solid black;">Roll</td>
<td style="border: 1px solid black;">-90~90°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Z</td>
<td style="border: 1px solid black;">Yaw</td>
<td style="border: 1px solid black;">-180~180°</td>
</tr>
</table>

### Real-Life Examples

- Nintendo Switch has embedded gyro that enables to play somatosensory games.<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/motion-1.jpg" style="padding:10px 0px 12px 3px;width:400px;">
- The level feature of iPhone also uses gyro<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/motion-2.png" style="padding:10px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×20mm
- Precision: ±1°
- Acceleration range: ±8g
- Operating current: 18mA