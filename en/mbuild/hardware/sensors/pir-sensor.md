# PIR Sensor

The Passive Infrared (PIR) sensor detects motion from humans or warm-blooded animals.

<img src="../../../../zh/mbuild/hardware/sensors/images/pir-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### Real-Life Examples

- The automatic door of shopping malls uses PIR sensor in the opening or closing door<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/pir-1.jpg" style="padding:8px 0px 12px 3px;width:360px;">

### Parameters

- Size: 24×20mm
- Detecting range: 3~5m
- Detecting angle of the X axis: 80°
- Detecting angle of the Y axis:55°
- Duration after triggering: 3s
- Operating current: 15mA