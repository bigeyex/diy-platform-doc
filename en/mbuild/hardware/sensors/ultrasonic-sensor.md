# Ultrasonic Sensor

The ultrasonic sensor block can be used to measure the distance between the block and an obstacle. The left probe of the ultrasonic module is responsible for transmitting ultrasonic waves, while the right probe is responsible for receiving ultrasonic waves.

<img src="../../../../zh/mbuild/hardware/sensors/images/ultrasonic-01.png" style="padding:3px 0px 12px 3px;width:250px;">

<img src="../../../../zh/mbuild/hardware/sensors/images/ultrasonic.png" style="padding:3px 0px 12px 3px;width:250px;">


### Real-Life Examples

- Bats use ultrasonic wave to locate an object<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/ultrasonic-2.png" style="padding:15px 0px 12px 3px;width:300px;">

### Parameters

- Measuring range: 5~300cm
- Precision: ±5%
- Operating current: 26mA