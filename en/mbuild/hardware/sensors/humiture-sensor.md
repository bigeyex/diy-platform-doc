# Humiture Sensor

The Humiture Sensor detects the temperature and humidity of the surrounding area. You can use it to make a smart fan, a smart humidifier, and the like.

<img src="../../../../zh/mbuild/hardware/sensors/images/humiture-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### Real-Life Examples

- Smarthome devices use humiture sensor to adjust indoor environment<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/humiture-sensor-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×20mm
- Temperature range: -40~125℃
- Temperature reading precision: ±1℃
- Humidity range: 0~100%
- Humidity reading precision: ±3%
- Operating current: 15mA