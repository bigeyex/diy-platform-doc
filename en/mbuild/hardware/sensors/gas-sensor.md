# MQ2 Gas Sensor

The MQ2 Gas Sensor can detect all kinds of gases in the air, such as smoke, liquefied gas, butane, propane, methane, alcohol, hydrogen, and others.

<img src="../../../../zh/mbuild/hardware/sensors/images/gas-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### Real-Life Examples

- Smoke alarms detect smoke and other flammable gases in the environment.<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/gas-sensor-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×36mm
- Operating current: 160mA
- Heating impedance: 33Ω
- Pre-heat energy consumption: &lt; 800mW

### Notes

5-minute pre-heat for better performance.