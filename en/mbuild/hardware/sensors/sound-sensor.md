# Sound Sensor

The Sound Sensor detects sound intensity. 

<img src="../../../../zh/mbuild/hardware/sensors/images/sound-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### Real-Life Examples

- Voice control light in everyday life uses sound sensor to control the light

### Parameters

- Size: 24×20mm
- Operating current: 40mA
- Microphone sensitivity: -45~-39dB
- Microphone impedance: 2.2KΩ
- Microphone S/N ratio: 55dB