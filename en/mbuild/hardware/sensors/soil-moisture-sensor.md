# Soil Moisture Sensor

The Soil Moisture Sensor detects the level of moisture in the soil.

<img src="../../../../zh/mbuild/hardware/sensors/images/soil-moisture.png" style="padding:3px 0px 12px 3px;width:200px;">

### Parameters

- Size: 24×72mm
- Range value: 0~100
- Precision: ±5%
- Operating current: 14mA
