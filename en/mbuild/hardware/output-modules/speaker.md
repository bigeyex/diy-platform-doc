# Speaker

The Speaker can play a sound, or record your voice.

<img src="../../../../zh/mbuild/hardware/output-modules/images/speaker.png" style="padding:3px 0px 12px 3px;width:200px;">

### Parameters

- Size: 24×36mm
- Storage: 16M
- File type: mp3
- Interface: Micro-USB
- Rated operating current: 400mA