# Multi Touch

The Multi Touch Block consists of several touch sensors, each of which can detect whether the corresponding position is being touched. When a specific position is touched, the LED indicator of the specific touch sensor will light up.

You can also extend the touchable position through alligator clips or wire to apply the Multi Touch in more scenarios. For example, you can use a piece of wire to connect the Multi Touch to a fruit.

<img src="../../../../zh/mbuild/hardware/interaction/images/multi-touch.png" style="padding:3px 0px 12px 3px;width:200px;">

The Multi Touch can adjust the threshold values intelligently. Each time the system starts up, all threshold values will be reset. Furthermore, the threshold value is programmable. You can customize the threshold values to make fun creations.

### Real-Life Examples

- Some robots have touch sensor on its back to detect if being touched<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/touch-1.png" style="padding:8px 0px 12px 3px;width:400px;">
- The touch screen of smart phones is a more complicated touch sensor<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/touch-2.jpg" style="padding:8px 0px 12px 3px;width:400px;">

### Parameters

- Size: 24×72mm
- Operating current: 18mA