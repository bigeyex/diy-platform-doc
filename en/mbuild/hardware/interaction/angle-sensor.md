# Angle Sensor

The Angle Sensor Block is made of a magnetic encoder, which detects the position of rotation with accuracy. Unlike the knob, the angle sensor rotates continuously, detecting the degree of rotation, and the angular velocity in real time. What's more, the Angle Sensor comes with a variety of plates to connect to various structural components, so as to be applied in different scenarios.

<img src="../../../../zh/mbuild/hardware/interaction/images/angle-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

- Connect to Makeblock mechanical parts
- Connect to LEGO cross stitches

### Real-Life Examples

- The magnetic encoder used in some servo to detect angles<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/angle-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">
- The Digital Crown of Apple Watch can be used to control sound volume. The intricate system shows itself in miniature size.<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/angle-2.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×36mm
- Precision: ±1°
- Operating current: 22mA