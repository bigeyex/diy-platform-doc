# Joystick

Joystick is used to control the direction of a moving object. The Joystick Block of mbuild supports customized x, y axes of the joystick.

<img src="../../../../zh/mbuild/hardware/interaction/images/joystick.png" style="padding:3px 0px 12px 3px;width:200px;">

<br>

<img src="../../../../zh/mbuild/hardware/interaction/images/joystick-00.png" style="padding:13px 0px 12px 3px;width:400px;">

### Real-Life Examples

- Game controllers use joystick to control character's direction of movement or field of view.<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/joystick-1.jpg" style="padding:8px 0px 12px 3px;width:400px;">
- Drone remote control uses joystick to control drone's rotation direction and speed.<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/joystick-2.jpg" style="padding:8px 0px 12px 3px;width:400px;">

### Parameters

- Size: 24×36mm
- Life span: 500,000 times
- Range of the X axis: -100~100
- Range of the Y axis: -100~100
- Operating current: 15mA
- Reversion precision: ±0.2mm