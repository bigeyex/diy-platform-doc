# Slider

The Slider Block is used to control input value.

<img src="../../../../zh/mbuild/hardware/interaction/images/slide-potentiometer.png" style="padding:3px 0px 12px 3px;width:200px;">


### Real-Life Examples

- The sound volume of smart phone is controlled by slider<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/slide-3.png" style="padding:8px 0px 12px 3px;width:300px;">
- The progress bar of videos is also a slider<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/slide-2.jpg" style="padding:8px 0px 12px 3px;width:500px;">

### Parameters

- Size: 24×72mm
- Life span: 15000次
- Range value: 0~100
- Precision: ±2%
- Operating current: 15mA