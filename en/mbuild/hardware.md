# Hardware Guide

* [Power Modules](hardware/power.md)
* [Communication](hardware/communication.md)
* [Interaction Modules](hardware/interaction.md)
* [Sensors](hardware/sensors.md)
* [Display](hardware/display.md)
* [Light](hardware/light.md)
* [Play](hardware/sound.md)
* [Motion](hardware/motion.md)
* [Peripheral Modules](hardware/peripheral.md)
* [Accessories](hardware/accessories.md)