# MG995 Standard Servo

![](images/mg995-standard-servo_MG995-Standard-Servo.jpg)

<img src="images/mg995-standard-servo_微信截图_20160128132425-300x238.png" alt="微信截图_20160128132425" width="300" style="padding:5px 5px 12px 0px;">

### Description

MG995 servo is a simple, commonly used standard servo for your mechanical needs such as robotic head, robotic arm. It comes with a standard 3-pin power and control cable for easy using and metal gears for high torque.  A Me RJ25 Adapter also help you to connect the servo with Me Baseboard or Makeblock Orion easily.

### SizeChart

<img src="images/mg995-standard-servo_微信截图_20160128132755-297x300.png" alt="微信截图_20160128132755" width="391" style="padding:5px 5px 12px 0px;">

### Mechanical Specification

- Size: 40.4\*19.9\*37.5mm      
- Weight: 58g
- Gear type: 5 metal gear     
- Limit angle: 180°±5°   
- Bearing: DUAL BB   
- Horn gear spline 
- Horn type: Metal     
- Case: Engineering plastics(Polyamide)
- Connector wire: FP: 240mm±5mm; JR: 300mm±5mm  
- Motor: DC motor  
- Splash water resistance: No 

### Electrical Specification

- Operating voltage: 4.8V
- Idle current: 5mA 
- No load speed: 0.17sec/60°
- Running current: 350mA      
- Peak stall torque: 9.0kg/cm   
- Stall current: 1500mA     

### Control Specification


- Command signal: Pulse width modification 
- Amplifier type: Digital controller
- Pulse width range: 500-2500usec      
- Neutral position: 1500usec   
- Running degree: 180°±3°(500～2500usec)   
- Dead bandwidth: 4 usec     
- Rotating direction: Counterclockwise (500～2500usec)

 
