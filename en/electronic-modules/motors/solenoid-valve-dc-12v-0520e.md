# Solenoid Valve DC 12V-0520E


![](images/solenoid-valve-dc-12v-0520e_Solenoid-Valve-DC-12V-0520E.jpg)

<img src="images/solenoid-valve-dc-12v-0520e_微信截图_20160128154549.png" alt="微信截图_20160128154549" width="366" style="padding:5px 5px 12px 0px;">

### Description

Makeblock solenoid valve DC 12V/0520E has a mini body, and is widely
used in industry device and DIY projects.

### Specification

- Rated Voltage: DC 12V
- Load: Air
- Current(With load): Less than 240mA
- Pattern: Two position, three -way
- Total Size : 34 x 21mm
- Maximum pressure : More than 300mmHg
- Insulation class: A

### Size Charts(mm)

<img src="images/solenoid-valve-dc-12v-0520e_微信截图_20160128154708.png" alt="微信截图_20160128154708" width="729" style="padding:5px 5px 12px 0px;">
