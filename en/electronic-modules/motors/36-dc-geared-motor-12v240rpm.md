# 36 DC Geared Motor 12V240RPM


![](images/36-dc-geared-motor-12v240rpm_36-DC-Geared-Motor-12V240RPM.jpg)

<img src="images/36-dc-geared-motor-12v240rpm_微信截图_20160128122512-300x281.png" alt="微信截图_20160128122512" width="300" style="padding:5px 5px 12px 0px;">

### Description

36mm planetary gear reducing motor have better performance, higher torque. It can be used as the power motor of carts chassis or some high-performance small cars.

### Specification

1.  Voltage: 12V
2.  No Load RPM: 240rpm
3.  Rated RPM: 182rpm
4.  Rated torque: 4kg.cm
5.  Current: 1.2A

### Size Charts(mm)

<img src="images/36-dc-geared-motor-12v240rpm_微信截图_20160128122745-300x121.png" alt="微信截图_20160128122745" width="540" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/36-dc-geared-motor-12v240rpm_微信截图_20160128122822-300x133.png" alt="微信截图_20160128122822" width="528" style="padding:5px 5px 12px 0px;">
