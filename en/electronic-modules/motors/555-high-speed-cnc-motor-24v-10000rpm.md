# 555 High-speed CNC Motor 24V-10000RPM


![](images/555-high-speed-cnc-motor-24v-10000rpm_555-High-speed-CNC-Motor-24V-10000RPM.jpg)

<img src="images/555-high-speed-cnc-motor-24v-10000rpm_微信截图_20160128115154.png" alt="微信截图_20160128115154" width="281" style="padding:5px 5px 12px 0px;">

### Description

Makeblock 555 High-speed CNC Motor 24V/10000RPM is assembled by high performance 555 motors and high-precision JTO drill chuck. It is usually used to be a mini cnc spindle, making a mini bench drill, a mini table saw and so on.

### Specification

- Weight: 290g
- JTO drill chuck clamping range: 0.3-4mm
- Rated voltage: 24V
- Speed: 10000rpm

### Size Charts(mm)

<img src="images/555-high-speed-cnc-motor-24v-10000rpm_微信截图_20160128115210-300x145.png" alt="微信截图_20160128115210" width="559" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/555-high-speed-cnc-motor-24v-10000rpm_微信截图_20160128115219-300x249.png" alt="微信截图_20160128115219" width="501" style="padding:5px 5px 12px 0px;">

 
