# MEDS15 Servo Motor


![](images/meds15-servo-motor_MEDS15-Servo-Motor.jpg)

<img src="images/meds15-servo-motor_微信截图_20160128145039.png" alt="微信截图_20160128145039" width="306" style="padding:5px 5px 12px 0px;">

### Mechanical Specification


- Size: 40×20×41mm
- Weight: 75g                                 
- Gear type: 5 metal gear                        
- Limit angle: 210°±5°                             
- Bearing" 2\*Bearing                          
- Horn gear spline: Φ8mm35T                             
- Horn type: Plastic, POM                        
- Case: Aluminum alloy + Engineering plastics 
- Connector wire: 230mm±5mm                           
- Motor: Coreless Motor                      
- Splash water resistance: No

### Electrical Specification

- Operating voltage: 6.0V        
- Idle current: 8mA         
- No load speed: 0.19sec/60° 
- Running current: 450mA       
- Peak stall torque: 16.5kg/cm   
- Stall current: 2500mA      

### Control Specification

- Command signal: Pulse width modification                 
- Amplifier type: Digital controller                       
- Pulse width range: 500-2500usec
- Neutral position: 1500usec                                 
- Running degree: 180°±3°(when 1000～2000usec)           
- Dead bandwidth : 5 usec                                   
- Rotating direction: Counterclockwise (when 1000～1500usec) 

### Size Charts(mm)

<img src="images/meds15-servo-motor_微信截图_20160128145325.png" alt="微信截图_20160128145325" width="620" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/meds15-servo-motor_微信截图_20160128145357.png" alt="微信截图_20160128145357" width="615" style="padding:5px 5px 12px 0px;">
