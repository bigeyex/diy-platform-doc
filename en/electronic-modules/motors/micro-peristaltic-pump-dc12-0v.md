# Micro Peristaltic Pump DC12.0V


![](images/micro-peristaltic-pump-dc12-0v_Micro-Peristaltic-Pump-DC12.0V.jpg)

<img src="images/micro-peristaltic-pump-dc12-0v_微信截图_20160128111459-300x288.png" alt="微信截图_20160128111459" width="300" style="padding:5px 5px 12px 0px;">

### Description

Micro Peristaltic Pump (DC12.0V) is a new type of micro liquefied pump that has mini size and powerful function. With easy-to-use feature, it can be used in a DIY project or an industrial project. The pump body comes with triple-roller and environmental-friendly silicone hosepipe. Having no valve and sealing, the silicone hosepipe can be easily replaced. The liquid won’t be in direct contact with the pump body, hence more environment-friendly and pollution-free. The Micro Peristaltic Pump can also be used to pump sensitive liquid and organic solvent.

### Internal Structure

<img src="images/micro-peristaltic-pump-dc12-0v_微信截图_20160128112231-300x152.png" alt="微信截图_20160128112231" width="421" style="padding:5px 5px 12px 0px;">

### Features

- Support for positive and negative transfer.
- High accurateness in flow control.
- D(1.5-4)mm\*0.5m silicone tube is included for using.
- Environmental protection silicone tube for food and medical industry
- Simple structure and low maintenance.

### Specifications

- Voltage: DC 12.0V
- Flow Velocity: (6\~24)ml/min
- Operating Temperature: 0\~40℃
- Relative Humidity &lt; 80%

### Size Chart(mm)

<img src="images/micro-peristaltic-pump-dc12-0v_微信截图_20160128113231-300x127.png" alt="微信截图_20160128113231" width="619" style="padding:5px 5px 12px 0px;">

### Demo 

<img src="images/micro-peristaltic-pump-dc12-0v_微信截图_20160128113246-300x293.png" alt="微信截图_20160128113246" width="547" style="padding:5px 5px 12px 0px;">

 
