# 42BYG Geared Stepper Motor


![](images/42byg-geared-stepper-motor_42BYG-Geared-Stepper-Motor.jpg)

<img src="images/42byg-geared-stepper-motor_微信截图_20160128150831.png" alt="微信截图_20160128150831" width="332" style="padding:5px 5px 12px 0px;">

### Description

Makeblock 42BYG geared stepper motor is a simple, but very powerful stepper motor with high output torque and response speed but low noise and energy consumption.

It comes with higher torque than 42BYG stepper motor. It can be used as the power motor of some high-performance machine.

### Specification

- Step Angle (degrees) :1.8
- Phase Current: 2.8A
- Quantity of wires:4
- Motor Length: 56mm
- Ratio: 5.18:1
- Output Shaft: D shaft 8mm

### Size Charts(mm)

<img src="images/42byg-geared-stepper-motor_微信截图_20160128151208.png" alt="微信截图_20160128151208" width="567" >

### Demo

<img src="images/42byg-geared-stepper-motor_微信截图_20160128151237.png" alt="微信截图_20160128151237" width="464" >

The Makeblock versatile motor bracket is not included.

<img src="images/42byg-geared-stepper-motor_微信截图_20160128151348.png" alt="微信截图_20160128151348" width="626" >

### Connection Type

May you are confusing about which color cable on the stepper motor should be connected to which port on the driver (A+,A-, B+,B-), here is a guide of it

1. Our stepper motor has two phase. So it doesn’t matter you change the connection order between phase A and phase B. Which I mean Black and Green could be connected to A or B, Red and Blue could be connected to B or A. And yes, it would change the motor direction.
2. There is only one rule you need to follow, which is you can’t mix them together.

- A+: Red  
- A-: Black  
- B+: Blue  
- B-: Green
