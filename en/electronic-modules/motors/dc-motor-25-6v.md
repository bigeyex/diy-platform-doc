# DC Motor-25 6V


![](images/dc-motor-25-6v_DC-Motor-25-6V.jpg)

<img src="images/dc-motor-25-6v_微信截图_20160128160459.png" alt="微信截图_20160128160459" width="353" style="padding:5px 5px 12px 0px;">

### Description

DC motors are the most commonly used motors in Makeblock Platform. With Makeblock DC Motor-25 Brackets, they may be easy to connect to Makeblock structural components.

### Specification

<img src="images/dc-motor-25-6v_微信截图_20160128160649.png" alt="微信截图_20160128160649" width="625" style="padding:5px 5px 12px 0px;">

### Size Chart (mm) 

<img src="images/dc-motor-25-6v_微信截图_20160128160612.png" alt="微信截图_20160128160612" width="732" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/dc-motor-25-6v_微信截图_20160128160724.png" alt="微信截图_20160128160724" width="703" style="padding:5px 5px 12px 0px;">

It can be connected with Makeblock Me Orion or Makeblock Me Dual Motor Driver V1 by adding the plug 3.96-2P on the stripped-end to control the motors.

<img src="images/dc-motor-25-6v_微信截图_20160128160812.png" alt="微信截图_20160128160812" width="442" style="padding:5px 5px 12px 0px;">

 
