# TT Geared Motor DC 6V-200RPM


![](images/tt-geared-motor-dc-6v-200rpm_TT-Geared-Motor-DC-6V-200RPM.jpg)

<img src="images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128120940-300x250.png" alt="微信截图_20160128120940" width="300" style="padding:5px 5px 12px 0px;">

### Description

Makeblock TT Geared Motor DC 6V/200RPM is the new power source with
plastic gears in Makeblock platform. This TT Geared Motor fits perfectly with Makeblock Plastic Timing Pulley 62T and Plastic Timing Pulley 90T for the wheels system of DIY items. It can be used in Makeblock mBot as the power source.

### Features

- Support for positive and negative transfer.
- High accurateness in flow control.
- D(1.5-4)mm\*0.5m silicone tube is included for using.
- Environmental protection silicone tube for food and medical industry.
- Simple structure and low maintenance.

### Specifications

- Rated Voltage: DC 6V
- No Load Speed: 200RPM±10%
- Gear Ratio 1:48

### Size Chart(mm)

<img src="images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128121258-300x204.png" alt="微信截图_20160128121258" width="565" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128121358-300x210.png" alt="微信截图_20160128121358" width="300" style="padding:5px 5px 12px 0px;">
<img src="images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128121404-300x295.png" alt="微信截图_20160128121404" width="300" style="padding:5px 5px 12px 0px;">

<img src="images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128121409-300x300.png" alt="微信截图_20160128121409" width="300" style="padding:5px 5px 12px 0px;">
<img src="images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128121414-300x290.png" alt="微信截图_20160128121414" width="300" style="padding:5px 5px 12px 0px;">
