# 9g Micro Servo Pack

![](images/9g-micro-servo_9g-Micro-Servo-Pack.jpg)

<img src="images/9g-micro-servo_微信截图_20160128144154-300x243.png" alt="微信截图_20160128144154" width="300" style="padding:5px 5px 12px 0px;">

### Description

Makeblock 9g Micro Servo Pack is a servo pack for beginners who like to make stuff move , it contains a 9g servo, a servo hub, a servo bracket and hardwares. The 9g Micro Servo can rotate approximately 180 degrees, it works like standard servos but of course not as strong as a standard servo.

With the servo hub and servo bracket, it may be convenient to connect the servo with other parts. A Me RJ25 Adapter also help you to connect the servo with Me Orion easily.

### Features

- Small volume and light weight
- Compatible with Me RJ25 Adapter
- A servo hub and a servo bracket are included.

### Specification

<img src="images/9g-micro-servo_微信截图_20160128144519.png" alt="微信截图_20160128144519" width="634" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/9g-micro-servo_微信截图_20160128144637.png" alt="微信截图_20160128144637" width="411" style="padding:5px 5px 12px 0px;">
