# Air Pump Motor – DC 12V-370-02PM


![](images/air-pump-motor-dc-12v-370-02pm_Air-Pump-Motor---DC-12V-370-02PM.jpg)

<img src="images/air-pump-motor-dc-12v-370-02pm_微信截图_20160128114507-300x291.png" alt="微信截图_20160128114507" width="300" style="padding:5px 5px 12px 0px;">

### Description

Makeblock air pump motor – DC 12V/370-02PM is widely used for aquarium tank oxygen circulate, DIY projects.

### Specification

- Rated voltage: DC 12V
- Load: Air
- Current(With load): Less than 250mA
- Flow : 3.0LPM
- Size : D27 x 65mm
- Maximum pressure : More than 600mmHg
- Noise: Less than 60dB

### Size Charts(mm)

<img src="images/air-pump-motor-dc-12v-370-02pm_微信截图_20160128114823-300x103.png" alt="微信截图_20160128114823" width="530" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/air-pump-motor-dc-12v-370-02pm_微信截图_20160128114931-300x134.png" alt="微信截图_20160128114931" style="padding:5px 5px 12px 0px;">

<img src="images/air-pump-motor-dc-12v-370-02pm_微信截图_20160128115008-300x259.png" alt="微信截图_20160128115008"  style="padding:5px 5px 12px 0px;">

<img src="images/air-pump-motor-dc-12v-370-02pm_微信截图_20160128115016-300x252.png" alt="微信截图_20160128115016"  style="padding:5px 5px 12px 0px;">

 
