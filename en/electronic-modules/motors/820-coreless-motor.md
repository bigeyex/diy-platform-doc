# 820 Coreless Motor


![](images/820-coreless-motor_820-Coreless-Motor.jpg)

<img src="images/820-coreless-motor_微信截图_20160128145747.png" alt="微信截图_20160128145747" width="315" style="padding:5px 5px 12px 0px;">

### Description

Makeblock 820 Coreless Motor has both the positive pitch and the negative pitch, it is usually used in the quadcopter.

### Features

- High torque;
- Low weight and small volume;
- Low energy consumption;
- High speed.

### Specification

- Motor diameter: 8.5 mm
- Motor Length: 20 mm
- Output shaft: 1.0 mm
- Output shaft length: 5 mm
- Pigtail cable: 5cm
- Weight: 6.5 g
- Voltage: 3 V-5V
- Current: 0.15 A
- Speed:   
3V: 35000-37000 RPM   
5V: about 50000 RPM

### Size Charts(mm)

<img src="images/820-coreless-motor_微信截图_20160128150106.png" alt="微信截图_20160128150106" width="313" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/820-coreless-motor_微信截图_20160128150137.png" alt="微信截图_20160128150137" width="661" style="padding:5px 5px 12px 0px;">
