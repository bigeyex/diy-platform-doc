# 42BYG Stepper Motor


![](images/42byg-stepper-motor_42BYG-Stepper-Motor.jpg)

<img src="images/42byg-stepper-motor_微信截图_20160128152924.png" alt="微信截图_20160128152924" width="334" style="padding:5px 5px 12px 0px;">

### Specifications

- PHASE : 2PHASE
- STEP ANGLE : 1.8+-5%°/STEP
- RATED VOLTAGE : 12V
- CURRENT : 1.7A/PHASE
- RESISRANCE : 1.5+-10%/PHASE
- INDUCRANCE : 2.8+-20%mH/PHASE
- HOLDING TORQUE : 40N.cm Min
- DETENT TORQUE : 2.2N.cm Max
- INSULATION CLASS : B
- LEAD STYLE : AWG26 UL1007
- ROTOR TORQUE : 54G.cm2

### Size Charts

<img src="images/42byg-stepper-motor_微信截图_20160128153028.png" alt="微信截图_20160128153028" width="587" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/42byg-stepper-motor_微信截图_20160128153108.png" alt="微信截图_20160128153108" width="298" style="padding:5px 5px 12px 0px;">

The Stepper Motor Bracket is not included.

<img src="images/42byg-stepper-motor_微信截图_20160128153143.png" alt="微信截图_20160128153143" width="510" style="padding:5px 5px 12px 0px;">

### Connection Type

Maybe you are confusing about which color cable on the stepper motor should be connected to which port on the driver (A+,A-, B+,B-), Here is a guidance of it

- Our stepper motor has two phase. So it doesn’t matter you change the connection order between phase A and phase B. Which I mean Black and Green could be connected to A or B, Red and Blue could be connected to B or A. And yes, it would change the motor direction.
