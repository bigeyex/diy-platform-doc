# Me audio player

<img src="images/me-audio-player-1.jpg" width="300px;" style="padding: 5px 5px 12px 0px;">

### 1. Overview

Me Audio Player module, compatible with the entire series of MakeBlock control boards, is able to play music and records with a built-in voice decoding chip. This module’s connector is marked in white, meaning that it is controlled by I2C signals, and must be connected to main board’s white-marked port. Insert the TF memory card to feel the joy of music and it is very convenient to use.

### 2. Technological Specifications:

- Working voltage: 5V DC  
- Microphone sensitivity (1Khz): 50-54dB  
- Microphone impedance: 2.2 kΩ  
- Microphone signal to noise ratio: 58 db  
- Speaker rated power: 1W  
- Speaker rated impedance: 8±15%Ω  
- Communication method: I2C  
- Largest electric current: 500mA  
- Module size: 56 x 41 x 28 mm (LxWxH)

### 3. Characteristics

- Onboard blue LED solid on means music playback mode, flashing means recording mode;  
- High sensitivity to sound;  
- The module’s metal hole area is the reference area in contact with the metal beam;  
- With reverse polarity protection, reverse power supply will not damage the IC;  
- Supports mBlock graphical programming, suitable for all ages users;  
- Convenient connection by using the RJ25 port;  
- Modular installation, compatible with LEGO series;  
- Module supports Micro USB to copy audio files directly without the need for a card reader;  
- Module directly supports MP3, WMA, and WAV files.

### 4. Connection Method

● **RJ25 connection**

Since the Me Audio Player module’s connector port is marked in white, when using the RJ25 port, it needs to be connected to the port marked in white on the main control board as well. Taking MakeBlock Orion as an example, you can connect it to ports 3, 4, 6, 7 and 8, as shown.

<img src="images/me-audio-player_20184171.jpg" alt="微信截图_20160129151012" width="350" style="padding: 5px 5px 12px 0px;">

### 5. Programming Guide

● **Arduino programming** 

If you are using Arduino programming, you must call the
``Makeblock-Library-master`` to control the audio playback module.  
This program controls Me Audio Player module through buttons through Arduino programming, achieving the playback, pausing, start recording, and stop recording.  

<img src="images/me-audio-player_20184172.png" alt="微信截图_20160129151123" width="569" style="padding: 5px 5px 12px 0px;">

**Me Audio Player module-function list**

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<td style="border: 1px solid black;width:40%;">Function</td>
<td style="border: 1px solid black;width:30%;">Instruction</td>
<td style="border: 1px solid black;width:30%;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">MeAudioPlayer(uint8_t port)</td>
<td style="border: 1px solid black;">Choose a port</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">playMusicFileIndex(uint16_t music_index)</td>
<td style="border: 1px solid black;">Specify audio file index playback</td>
<td style="border: 1px solid black;">Numbers：1.2.3……</td>
</tr>

<tr>
<td style="border: 1px solid black;">pauseMusic()</td>
<td style="border: 1px solid black;">Pause playback</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">stopMusic()</td>
<td style="border: 1px solid black;">stop playback</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">playNextMusic()</td>
<td style="border: 1px solid black;">Next</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">playPrevMusic()</td>
<td style="border: 1px solid black;">Previous</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">setMusicVolume(uint8_t vol)</td>
<td style="border: 1px solid black;">Set the volume</td>
<td style="border: 1px solid black;">Range0~100</td>
</tr>

<tr>
<td style="border: 1px solid black;">setMusicPlayMode(uint8_t mode)</td>
<td style="border: 1px solid black;">Set playback mode</td>
<td style="border: 1px solid black;">0.Single play<br>
1.Single cycle<br>
2.Playlist loop<br>
3.Random play</td>
</tr>

<tr>
<td style="border: 1px solid black;">startRecordingFileName(char *str)</td>
<td style="border: 1px solid black;">Select a file name to begin</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">stopRecording()</td>
<td style="border: 1px solid black;">Stop recording</td>
<td style="border: 1px solid black;"></td>
</tr>
</table>

<br>

● **mBlock programming**

Me Audio Player module supports the mBlock programming, and the
following is an introduction to the module instructions.

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:50%">Code</th>
<th style="border: 1px solid black;width:50%">Description</th>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling1.png" alt="微信截图_20160129151218" width="368" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;"> Choose a port</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling2.png" alt="微信截图_20160129151218" width="178" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;"> Specify audio file index playback</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling3.png" alt="微信截图_20160129151218" width="190" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;"> Specify the audio file name playback</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling4.png" alt="微信截图_20160129151218" width="210" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;"> Set playback mode: Single play, single cycle, playlist loop, random play</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling5.png" alt="微信截图_20160129151218" width="150" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Play previous audio</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling6.png" alt="微信截图_20160129151218" width="120" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;"> Play next audio</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling7.png" alt="微信截图_20160129151218" width="130" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Pause/resume playback</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling8.png" alt="微信截图_20160129151218" width="95" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;"> Stop playback</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling9.png" alt="微信截图_20160129151218" width="150" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;"> Volume setting</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling10.png" alt="微信截图_20160129151218" width="95" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;"> Increase volume</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling11.png" alt="微信截图_20160129151218" width="106" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;"> Decrease volume</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling12.png" alt="微信截图_20160129151218" width="310" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Begin recording with the name “T001”</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling13.png" alt="微信截图_20160129151218" width="220" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;"> Stop recording</td>
</tr>
</table>

<br>

The following is an example of how to use mBlock to control the Me Audio Player module：

This program can control Me Audio Player through the [Me 4 button module](http://learn.makeblock.com/en/me-4-button/). To achieve playback, pause, start recording, and stopping recording audio files, the following are the results:

<img src="images/me-audio-player_20184201111.png" alt="微信截图_20160129151218" width="1108" style="padding: 5px 5px 12px 0px;">

### 6. Audio file format instructions:

- Please add an appropriate delay after After the volume setting blocks, and wait for it to take effect;  
- Using external memory TF card to store audio files, support MP3, WAV, WMA high quality audio format files;  
- Using FAT and FAT32 file systems;  
- The audio file naming format supports English naming
(case-insensitive). English letters and numbers are mixed and the length of the naming is recommended to be less than 8 characters. For example: Hello.MP3, T002.MP3, and R000001.MP3. (It is not recommended to use pure number naming);  
- Sorting of audio files in TF card: It is recommended to sort by file name;  
- This module does not support Chinese naming of audio files;  
- Do not use special character names such as: v1.0″, o\_o0, … ( not support).
