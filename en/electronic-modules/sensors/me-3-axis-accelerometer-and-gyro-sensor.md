# Me 3-Axis Accelerometer and Gyro Sensor


![](images/me-3-axis-accelerometer-and-gyro-sensor_Me-3-Axis-Accelerometer-and-Gyro-Sensor.jpg)

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203143634.png" alt="微信截图_20160203143634" width="262" style="padding:5px 5px 12px 0px;">

### Overview

The Me 3-Axis Accelerometer and Gyro Sensor is an ideal module for
motion and posture detection of robot. It includes a 3-axis
accelerometer, a 3-axis angular velocity sensor, and a motion processor,
and provides I2C port for communication. It can be applied on the
self-balancing cart, 4-axis aircraft, robot, and mobile devices. It has
the advantage of high-dynamic measurement range and low current
consumption. Its white ID means that it’s in I2C communication mode and
should be connected to the port with white ID on Makeblock Orion.

### Technical specifications

- Input voltage: 5V DC  
- Signal mode: I²C communication  
- Module size: 51 x 24 x 18 mm (L x W x H)

### Functional characteristics

- White area of module is the reference area to contact metal beams  
- Digital output of 6-axis or 9-axis synthetical operation data of such formats as rotation matrix, quaternion, and Euler angle  
- 3-axis angular velocity sensor controlled measurement range: ±250, ±500, ±1000, ±2000°/s (dps)  
- 3-axis accelerometer controlled measurement range: ±2g, ±4g, ±8g, and ±16g  
- Digital Motion Processing (DMP) engine is provided to reduce the load of complex motion integration, sensor synchronization, and posture detection  
- Eliminate the axial sensitivity between accelerometer and gyro; reduce the influence of settings and drifting of sensor 
- Embedded calibration algorithm of operation time deviation and magnetic force sensor  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type of port to support most development boards including Arduino series

### Pin definition

The port of Me 3-Axis Accelerometer and Gyro Sensor has 4 pins, and their functions are as follows:

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144123.png" alt="微信截图_20160203144123" width="719" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25** 

Since the port of Me 3-Axis Accelerometer and Gyro Sensor has white ID, you need to connect the port with white ID on Makeblock Orion when using RJ25 port. Taking Makeblock Orion as example, you can connect it to
ports No. 3, 4, 6, 7, and 8 as follows:

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144150.png" alt="微信截图_20160203144150" width="332" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its SCL and SDA pins should be connected to I2C port, that is, the port A5 and A4 respectively as follows:

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144159.png" alt="微信截图_20160203144159" width="340" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me 3-Axis Accelerometer and Gyro Sensor. This is a routine wrote in Arduino. When any value is input through the serial monitor, the module receives it and returns the current value of X, Y, and Z, so that we can identify the posture of current module. We can see the value of X, Y, and Z on
the serial monitor.

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144211.png" alt="微信截图_20160203144211" width="433" style="padding:5px 5px 12px 0px;">

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144515.png" alt="微信截图_20160203144515" width="723" style="padding:5px 5px 12px 0px;">

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144541.png" alt="微信截图_20160203144541" width="572" style="padding:5px 5px 12px 0px;">

● **mBlock programming**  

Me 3-Axis Accelerometer and Gyro Sensor supports the mBlock programming environment and its instructions are introduced as follows:

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144620.png" alt="微信截图_20160203144620" width="723" style="padding:5px 5px 12px 0px;">

The following is the effect to make the panda speaking out the value of X/Y/Z of Me 3-Axis Accelerometer and
Gyro Sensor.

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144657.png" alt="微信截图_20160203144657" width="753" style="padding:5px 5px 12px 0px;">

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144708.png" alt="微信截图_20160203144708" width="753" style="padding:5px 5px 12px 0px;">

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144716.png" alt="微信截图_20160203144716" width="751" style="padding:5px 5px 12px 0px;">

### Principle analysis

The Me 3-Axis Accelerometer and Gyro Sensor integrates a 3-axis accelerometer, a 3-axis angular velocity sensor (gyro), and a Digital Motion Processing engine (DMP), and outputs integrated 9-axis synthetical data to application client in the form of single data stream from the I2C port. 

The 3-axis accelerometer can measure the change of acceleration in X, Y, and Z axes. By perceiving the total inertial force in a specific direction, the accelerometer can measure the acceleration and gravity.

The 3-axis accelerometer means it can detect the motion or gravity of an object in 3D space. The accelerometer can be used to measure the gravity g. If the module is still and completely motionless, the force that the earth gravity exerts on it is about 1 g. If it is placed vertically, it will detect the force exerting on it along Y-axis is about 1 g. If it is placed at a certain angle, it will detect that the force of 1 g distributes on different axis. When it is in motion or vibration in 3D space, the Me 3-Axis accelerometer and Gyro Sensor will detect the force greater than 1 g in one or more axes, and acceleration as well. Velocity and displacement can be obtained by integrated the acceleration. When an object rotates around an axis, the angular velocity is generated. The 3-axis angular velocity sensor (gyro) can detect the change of angular velocity on X, Y, and Z axes. The motion processing engine can directly output data through I2C port, which reduces the burden of peripheral microprocessor and avoids complicated
filtering and data synthesization.

### Schematic

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_Me-3-Axis-Accelerometer-and-Gyro-Sensor-V1.1.png" alt="Me 3-Axis Accelerometer and Gyro Sensor V1.1" width="959" style="padding:5px 5px 12px 0px;">
