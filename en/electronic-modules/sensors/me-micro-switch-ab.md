# Me Micro Switch A


![](images/me-micro-switch-ab_Me-Micro-Switch-A.jpg)

<img src="images/me-micro-switch-ab_微信截图_20160126151-1.png" alt="微信截图_20160126151" width="237" style="padding:5px 5px 12px 0px;">

### Overview

Me Micro Switch B is a physical switch and can be used with Makeblock parts perfectly. When it is triggered, it will send a signal to the control side. It can be connected to Orion development board through the wire and RJ25 adapter module, and connected to Makeblock mechanical parts through the self-contained brackets.

### Technical specifications

- Rated voltage: 250V AC  
- Rated current: 1A  
- Contact resistance: ≤50 mΩ  
- Insulation resistance: ≥1 MΩ  
- Switch life: 10,000 times  
- Dimension: 20 x 6 x 24 mm (L x W x H) excluding cable

### Functional characteristics

- Small and easy to install  
- Provide connecting wires to avoid user rewelding  
- Fully match with Arduino library functions to enable users programming
easily  
- Control the machinery equipment, and use position-limited protection
and switching circuit components  
- Long mechanical and electrical life of switch

### Diagram of bracket dimension (unit: mm)

<img src="images/me-micro-switch-ab_微信截图_20160129121434.png" alt="微信截图_20160129121434" width="343" style="padding:5px 5px 12px 0px;">

### Connection diagram

<img src="images/me-micro-switch-ab_微信截图_20160129121515.png" alt="微信截图_20160129121515" width="484" style="padding:5px 5px 12px 0px;">

<img src="images/me-micro-switch-ab_微信截图_20160129121530.png" alt="微信截图_20160129121530" width="325" style="padding:5px 5px 12px 0px;">
