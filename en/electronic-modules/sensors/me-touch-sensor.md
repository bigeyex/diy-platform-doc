# Me Touch Sensor


![](images/me-touch-sensor_Me-Touch-Sensor.jpg)

<img src="images/me-touch-sensor_微信截图_20160129142421.png" alt="微信截图_20160129142421" width="204" style="padding:5px 5px 12px 0px;">

### Overview

The main component of Me Touch Sensor is a capacitive touch IC. Touch detection is designed to replace traditional button with variable area of region. When it is touched, the blue LED on the plate will make a response. It can be used with other components to make a table lamp with touch control. The blue ID on the port of this module indicates it is controlled by double-digital port, and to be connected to the port with yellow ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Response time: 60-220ms  
- Maximum operating current of chip: 7uA  
- Module size: 51 x 24 x 18 mm (L x W x H)

### Functional characteristics

- Make the power-on self-calibrating by taking the 4 seconds before power-on as a reference point. Do not touch the module within 4 seconds after power-on in order to reduce its judgement.  
- Sensitive and short time delay  
- White area of module is the reference area to contact metal beams  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type port to support most development boards including Arduino series

### Pin definition

The port of Me Touch Sensor has three pins, and their functions are as follows:

<img src="images/me-touch-sensor_微信截图_20160129142600.png" alt="微信截图_20160129142600" width="575" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**  

Since the port of Me Touch Sensor has blue ID, you need to connect the port with blue ID on Makeblock Orion when using RJ25 port. Taking Makeblock Orion as example, you can connect to ports No. 3, 4, 5, and 6 as
follows:

<img src="images/me-touch-sensor_微信截图_20160129142638.png" alt="微信截图_20160129142638" width="338" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire** 

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its OUT pin should be connected to digital port, and its Touch pin can be connected with a metal sheet as touch input as follows:

<img src="images/me-touch-sensor_微信截图_20160129143127.png" alt="微信截图_20160129143127" width="315" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming** 

If you use Arduino to write a program, the library
Makeblock-Librarymaster should be invoked to control the Me Touch Sensor. This is a routine to find out whether it is touched or not through Arduino programming.

<img src="images/me-touch-sensor_微信截图_20160129143212.png" alt="微信截图_20160129143212" width="538" style="padding:5px 5px 12px 0px;"><img src="images/me-touch-sensor_微信截图_20160129143233.png" alt="微信截图_20160129143233" width="743" style="padding:5px 5px 12px 0px;">

The function of the code segment is: When the Me Touch Sensor is touched, the serial port outputs “State: DOWN.” When it is not touched, the serial port outputs “State: UP.”

● **mBlock programming**  

Me Touch Sensor supports the mBlock programming environment and its instructions are introduced as follows: Connect Me Touch Sensor to the Port 4 of Me Orion. When the Me Touch Sensor is pressed, the panda says: “Hello!” When it is loosened, the panda says: “Bye!”

<img src="images/me-touch-sensor_微信截图_20160129143330.png" alt="微信截图_20160129143330" width="609" style="padding:5px 5px 12px 0px;">

### Principle analysis

Me Touch Sensor is a module based on capacitive sensing. Users can use RJ25 port to set its operating mode. The TOG pin of RJ25 port is used to control its operating mode. When the TOG is in high level, it is set as trigger mode; when the TOG is in low level, it is set as direct mode. The direct touch of human body or metal with the metal surface of
sensor will be sensed. In addition to the direct touch with metal surface, the contact with a certain thickness of materials such as plastic and glass can also be sensed, and its sensitivity is related with the size of contact surface and thickness of covered material.

### Schematic

<img src="images/me-touch-sensor_Me-touch-sensor.png" alt="Me touch sensor" width="870" style="padding:5px 5px 12px 0px;">
