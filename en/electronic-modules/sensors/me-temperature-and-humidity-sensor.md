# Me Temperature and Humidity Sensor


![](images/me-temperature-and-humidity-sensor_Me-Temperature-and-Humidity-Sensor.jpg)

 

<img src="images/me-temperature-and-humidity-sensor_微信截图_20160129134216.png" alt="微信截图_20160129134216" width="290" style="padding:5px 5px 12px 0px;">

### Overview

Me Temperature and Humidity Sensor is a sensor that contains a
calibrated digital signal output. It adopts specific digital module collecting technology and temperature-humidity sensing technology to ensure high reliability and excellent long-term stability. This module can measure the temperature from 0℃ to 50℃. Users can use it to build a cost-effective temperature and humidity monitoring system. Its yellow ID means that it has a single-digital port and needs to be connected to the port with yellow ID on Makeblock Orion.

### Technical specifications

- Power supply voltage: 5V DC  
- Control mode: Single bus digital signal  
- Power supply current: max 2.5mA  
- Temperature range: 0-50℃ ± 2℃  
- Humidity range: 20-90% RH ± 5% RH  
- Precision: 1% RH, 1℃  
- Module dimension: 51 x 24 x 18mm(L x W x H)

### Functional characteristics

- Small size and low power consumption  
- Strong anti-interference ability  
- Total calibrated digital output  
- White area of module is the reference area to contact metal beams  
- Support Arduino IDE programming and provide runtime library to simplify programming  
- Adopt RJ25 port for easy connection  
- Provide pins to support most Arduino Baseboards

### Pin definition

The port of Me Temperature and Humidity Sensor has three pins, and their functions are as follows:

<img src="images/me-temperature-and-humidity-sensor_微信截图_20160129134529.png" alt="微信截图_20160129134529" width="790" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**  

Since the port of Me Temperature and Humidity Sensor has yellow ID, you need to connect the port with yellow ID on Makeblock Orion when using RJ25 port. Taking Makeblock Orion as example, you can connect to ports No. 3, 4, 5, 6, 7, and 8 as follows:

<img src="images/me-temperature-and-humidity-sensor_微信截图_20160129134631.png" alt="微信截图_20160129134631" width="327" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its DATA pin should be connected to digital port as follows:

<img src="images/me-temperature-and-humidity-sensor_微信截图_20160129134707.png" alt="微信截图_20160129134707" width="322" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me
Temperature and Humidity Sensor. This is a routine to read the
current temperature and humidity through Arduino
programming.

<img src="images/me-temperature-and-humidity-sensor_微信截图_20160129134748.png" alt="微信截图_20160129134748" width="467" style="padding:5px 5px 12px 0px;">

<img src="images/me-temperature-and-humidity-sensor_微信截图_20160129134813.png" alt="微信截图_20160129134813" width="898" style="padding:5px 5px 12px 0px;">

The function of the code segment is: to read the result measured by Me Temperature and Humidity Sensor, and output the result to the serial monitor in Arduino IDE. Upload the code segment to the Makeblock Orion and click the Arduino serial monitor, and then you will see the running
result as follows:

<img src="images/me-temperature-and-humidity-sensor_微信截图_20160129134851.png" alt="微信截图_20160129134851" width="738" style="padding:5px 5px 12px 0px;">

### Principle analysis

Me Temperature and Humidity Sensor is composed of sensor DHT11, which comprises a resistive humidity-sensing component and an NTC temperature-measuring component. Calibration coefficients are stored in OTP memory in the form of program, and invoked by the sensor during processing the detection signal. Adopting single bus serial port, the sensor can implement two-way communication with the microprocessor by a
bus only, and the time period of one communication is about 4ms. The system integration becomes quick and easy.

### Schematic

<img src="images/me-temperature-and-humidity-sensor_Huminity-and-temp.png" alt="Huminity and temp" width="1264" style="padding:5px 5px 12px 0px;">
