# Me Sound Sensor


![](images/me-sound-sensor_Me-Sound-Sensor.jpg)

<img src="images/me-sound-sensor_微信截图_20160129145124.png" alt="微信截图_20160129145124" width="200" style="padding:5px 5px 12px 0px;">

### Overview

Based on a microphone, the Me Sound Sensor can be used to detect the ambient sound intensity. Its main component is LM2904 low-power amplifier. The module can be used to build some interactive projects, such as the voice operated switch, and the dance-following robot. Its black ID means that it has an analog port and should be connected to the port with black ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Microphone sensitivity (1 Khz): 50-54dB  
- Microphone impedance: 2.2 KΩ  
- Microphone frequency: 16-20 KHz  
- Microphone SNR: 54 db  
- Control mode: Single analog port output  
- Maximum current: 0.5mA  
- Type of power amplifier: LM2906  
- Module size: 51 x 24 x 18 mm (L x W x H)

### Functional characteristics

- Brightness of onboard LED indicates the intensity of sound  
- High sensitivity to the sound  
- White area of module is the reference area to contact metal beams  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type port to support most development boards including Arduino series

<img src="images/me-sound-sensor_微信截图_20160129145250.png" alt="微信截图_20160129145250" width="798" style="padding:5px 5px 12px 0px;">

### Pin definition

The port of Me Sound Sensor has three pins, and their functions are as follows:

<img src="images/me-sound-sensor_微信截图_20160129145353.png" alt="微信截图_20160129145353" width="797" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**  

Since the port of Me Sound Sensor has black ID, you need to connect the port with black ID on Makeblock Orion when using RJ25 port. 

Taking Makeblock Orion as example, you can connect it to ports No. 6, 7, and 8 as follows:

<img src="images/me-sound-sensor_微信截图_20160129145447.png" alt="微信截图_20160129145447" width="364" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its AO pin should be connected to analog pin as follows:

<img src="images/me-sound-sensor_微信截图_20160129145519.png" alt="微信截图_20160129145519" width="369" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me Sound Sensor. This is a routine to instruct the module to detect the intensity of ambient sound through
Arduino programming.

<img src="images/me-sound-sensor_微信截图_20160129145554.png" alt="微信截图_20160129145554" width="387" style="padding:5px 5px 12px 0px;">

<img src="images/me-sound-sensor_微信截图_20160129145613.png" alt="微信截图_20160129145613" width="799" style="padding:5px 5px 12px 0px;">

Read the detected result from the Me Sound Sensor, and output the result to serial monitor in Arduino IDE in the cycle of 100ms. Upload the code segment to the Makeblock Baseboard and click the Arduino serial monitor, and then you will see the running result as follows:

<img src="images/me-sound-sensor_微信截图_20160129145642.png" alt="微信截图_20160129145642" width="474" style="padding:5px 5px 12px 0px;">

### Schematic

<img src="images/me-sound-sensor_Me-Sound-Sensor.png" alt="Me Sound Sensor" width="1507" style="padding:5px 5px 12px 0px;">
