# Me Gas Sensor


![](images/me-gas-sensormq2_Me-Gas-Sensor.jpg)

<img src="images/me-gas-sensormq2_微信截图_20160129122027.png" alt="微信截图_20160129122027" width="222" style="padding:5px 5px 12px 0px;">

### Overview

Me Gas Sensor includes a smoke sensor of type MQ2 which has good repeatability, long-term stability, short response time, and durable working performance. It is often used as a gas leakage monitoring device in family and factory, and suitable for detecting Liquefied Natural Gas (LNG), butane, propane, methane, alcohol, hydrogen, smoke, etc. Its black ID means that it has an analog port and needs to be connected to
the port with black ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5.0V ± 0.1 V  
- Heating voltage: 5.0V ± 0.1 V  
- Heating resistor: 33Ω ± 5% (at room temperature)  
- Heating power: &lt;800mw  
- Warm up time: &gt;24h  
- Detection range: 100-10000 ppm  
- Operating temperature: 20 ± 2℃ (at standard condition)  
- Operating temperature: -20℃-50℃  
- Relative humidity: &lt; 95%RH  
- Oxygen concentration: 21% (at standard condition)  
- Module dimension: 51 x 24 x 18mm(L x W x H)

### Functional characteristics

- White area of this module is the reference area to contact metal beams  
- Provide adjustable resistor to adjust sensitivity  
- Preheat for a while before use to keep it warm after preheating  
- Blue indicator will light on when a certain amount of flammable gas is
accumulated and detected  
- Provide output port for digital and analog signal  
- Strong stability and fast detection  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type port to support most development boards including Arduino series

### Wiring mode

● **Connecting with RJ25**  

Since the port of Me Gas Sensor has black ID, you need to connect the port with black ID on Makeblock Orion when using RJ25 port. 

Taking Makeblock Orion as example, you can connect to ports No. 6, 7, and 8 as follows:

<img src="images/me-gas-sensormq2_微信截图_20160129122204.png" alt="微信截图_20160129122204" width="297" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its DO pin should be connected to digital port as follows:

<img src="images/me-gas-sensormq2_微信截图_20160129122244.png" alt="微信截图_20160129122244" width="331" style="padding:5px 5px 12px 0px;">

### Pin definition

The port of Me Gas Sensor has three pins, and their functions are as follows:

<img src="images/me-gas-sensormq2_微信截图_20160129122322.png" alt="微信截图_20160129122322" width="328" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the
library ``Makeblock-Library-master`` should be invoked to control the Me Gas Sensor. This program reads the digital and analog output values of sensor through Arduino programming, and output them to the serial port
for display.

<img src="images/me-gas-sensormq2_微信截图_20160129122416.png" alt="微信截图_20160129122416" width="456" style="padding:5px 5px 12px 0px;">

<img src="images/me-gas-sensormq2_微信截图_20160129122530.png" alt="微信截图_20160129122530" width="723" style="padding:5px 5px 12px 0px;">

The function of the code segment is: If smoke is detected by Me Gas Sensor, the output is “Gas”; if no smoke is detected, the output is “No Gas”. Then the detected smoke analog value is output, and the results are displayed on the serial monitor in Arduino IDE. Upload the code segment to the Makeblock Orion and click the Arduino serial monitor, and
you will see the results as follows:

<img src="images/me-gas-sensormq2_微信截图_20160129122614.png" alt="微信截图_20160129122614" width="620" style="padding:5px 5px 12px 0px;">

### Principle analysis

Me Gas Sensor has an adjustable resistor to adjust the sensitivity of sensor for the smoke. The smoke concentration varies from the distance between the sensor and smoke source. In the same environment, the closer the distance is, the greater the smoke concentration is; the farther the distance is, the lower the smoke concentration is. Therefore, it is necessary to set a voltage value corresponding to the appropriate smoke concentration threshold.

Detection principle: MQ-2 type of smoke sensor adopts stannic oxide as the semiconductor gas sensitive material which is the surface ionic N-type semiconductor. When the temperature is in the range of 200\~300℃, the stannic oxide adsorbs the oxygen in the air to form into negative oxygen ions, so that the electron density of the semiconductor is decreased and its resistance is increased. When in contact with smoke, if the potential energy barrier at the grain boundary is
modulated by the smoke, its change will cause the change of conductance. This can be used to obtain the information whether the smoke exists or not. The greater the smoke concentration is, the greater the conductance
is, and the lower the output resistance is. The decrease of bulk resistance of the sensor will cause its output voltage to the ground increasing. Comparing the output voltage and the threshold voltage by using the comparator on the module, the concentration of detected smoke can be identified.

### Schematic

<img src="images/me-gas-sensormq2_ME-GAS-SENORMQ2.png" alt="ME GAS SENOR(MQ2)" width="1179" style="padding:5px 5px 12px 0px;">
