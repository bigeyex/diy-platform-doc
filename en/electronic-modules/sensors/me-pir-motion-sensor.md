# Me PIR Motion Sensor


![](images/me-pir-motion-sensor_Me-PIR-Motion-Sensor.jpg)

<img src="images/me-pir-motion-sensor_微信截图_20160129145803.png" alt="微信截图_20160129145803" width="171" style="padding:5px 5px 12px 0px;">

### Overview

Me PIR Motion Sensor is a module to detect the infrared radiation emitted from human or animal body, and its maximum induction range is 6m. If somebody is moving in the range, its DO pin will output an effective signal to light on the blue LED on the board. Its blue ID means that it has a double-digital port and needs to be connected to
the port with blue ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Output voltage: 5 V/high level, 0 V/low level  
- Trigger signal: 5 V/high level  
- Retention time: 2 second  
- Detection angle: 120°  
- Distance detected: max. 6 m  
- Dimension: 51 x 24 x 18 mm (L x W x H)

### Functional characteristics

- Provide a potentiometer which sensitivity is adjustable  
- Built-in two-way amplitude discriminator for interference
suppression  
- Operate in two modes: repeatable trigger (default) and non- repeatable trigger  
- White area of module is the reference area to contact metal beams  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type port to support most development boards including Arduino series

### Pin definition

The port of Me PIR Motion Sensor has four pins, and their functions are as follows:

<img src="images/me-pir-motion-sensor_微信截图_20160129145932.png" alt="微信截图_20160129145932" width="788" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25** 

Since the port of Me PIR Motion Sensor has blue ID, you need to connect the port with blue ID on Makeblock Orion when using RJ25 port. 

Taking Makeblock Orion as example, you can connect to ports No. 3, 4, 5, and 6 as follows:

<img src="images/me-pir-motion-sensor_微信截图_20160129150011.png" alt="微信截图_20160129150011" width="381" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire** 

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its Mode and DOP pins should be connected to digital ports as follows:

<img src="images/me-pir-motion-sensor_微信截图_20160129150042.png" alt="微信截图_20160129150042" width="414" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming** 

If you use Arduino to write a program, the library `Makeblock-Library-master` should be invoked to control the Me PIR Motion Sensor. This is a routine to identify if there is anyone or anything moving nearby by this module through Arduino programming.

<img src="images/me-pir-motion-sensor_pir2.png" alt="微信截图_20160129150137" width="439" style="padding:5px 5px 12px 0px;">

<img src="images/me-pir-motion-sensor_pir3.png" alt="微信截图_20160129150245" width="806" style="padding:5px 5px 12px 0px;">

The function of the code segment is: to read the result detected from the DO pin to identify if there is someone nearby, and output 1 if yes or 0 if no. The result is output to the serial monitor in Arduino IDE every 100ms. Upload the code segment to Makeblock Orion and click the
Arduino serial monitor, and you will see the running result
as follows:

<img src="images/me-pir-motion-sensor_微信截图_20160129150321.png" alt="微信截图_20160129150321" width="602" style="padding:5px 5px 12px 0px;">

● **mBlock programming**  

Me PIR Motion Sensor supports the mBlock programming environment and its instructions are introduced as
follows:

<img src="images/me-pir-motion-sensor_微信截图_20160129150358.png" alt="微信截图_20160129150358" width="807" style="padding:5px 5px 12px 0px;">

This is an example on how to use mBlock control the Me PIR Motion Sensor. If the detected result from the module is nobody is moving, the panda will say “Nobody is playing with me!” Otherwise, the panda will say “Somebody is playing with me!” 

The running result is as follows:

<img src="images/me-pir-motion-sensor_微信截图_20160129150427.png" alt="微信截图_20160129150427" width="799" style="padding:5px 5px 12px 0px;">

### Principle analysis

Me PIR Motion Sensor adopts a specific PIR chip BISS0001 which is a sensor with the ability to detect the infrared radiation emitted from human or animal body and output electrical signal. It is a kind of digital-analog hybrid application-specific integrated circuit (ASIC)
comprised of operational amplifier, voltage comparator, state
controller, delay timer and block timer. When the radiation of human body is focused on the detector of pyroelectric infrared sensor by the Fresnel lens, the sensor in the circuit will output the voltage signal to identify the movement of human nearby. Its structure diagram is as
follows:

<img src="images/me-pir-motion-sensor_微信截图_20160129150512.png" alt="微信截图_20160129150512" width="751" style="padding:5px 5px 12px 0px;">

This module has a potentiometer which can be used to adjust the detection range. The level of Mode pin can be controlled to select the operating mode. When it is in high level, the module can be triggered repeatedly for real-time induction. When it is in low level, the module is in non-repeatable trigger mode. If somebody is moving in the induction range, the module is triggered and maintained for a period
of time. In this period, the state will not be interfered whether there is somebody moving in the deduction range.

### Schematic

<img src="images/me-pir-motion-sensor_PIR-sensor-1.png" alt="PIR sensor" width="1542" style="padding:5px 5px 12px 0px;">
