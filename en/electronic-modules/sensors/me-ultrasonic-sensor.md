# Me Ultrasonic Sensor


![](images/me-ultrasonic-sensor_Me-Ultrasonic-Sensor.jpg)

<img src="images/me-ultrasonic-sensor_微信截图_20160129143938.png" alt="微信截图_20160129143938" width="246" style="padding:5px 5px 12px 0px;">

### Overview

Me Ultrasonic Sensor is an electronic module to measuring distance in the range of 3cm to 400cm. It can be used for the projects to help the handcart avoid obstacles or other relevant distance-measuring and obstacle avoidance projects. Its yellow ID means that it has a single-digital port and needs to be connected to the port with yellow ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Measurement angle: 30 degree  
- Measurement range: 3-400cm (with error less than 1cm)  
- Ultrasonic frequency: 42 KHz  
- Control mode: Single-digital port control  
- Module size: 56 x 36 x 31 mm (L x W x H)

### Functional characteristics

- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- White area of module is the reference area to contact metal beams  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type of port to support most development boards including Arduino series

### Pin definition

The port of Me Ultrasonic Sensor has three pins, and their functions are as follows:

<img src="images/me-ultrasonic-sensor_微信截图_20160129144121.png" alt="微信截图_20160129144121" width="786" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25** 

Since the port of Me Ultrasonic Sensor has yellow ID, you need to connect to the port with yellow ID on Makeblock Orion when using RJ25 port. Taking Makeblock Orion as example, you can connect to ports No. 3, 4, 5, 6, 7, and 8 as follows:

<img src="images/me-ultrasonic-sensor_微信截图_20160129144158.png" alt="微信截图_20160129144158" width="376" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its SIG pin should be connected to digital port as follows:

<img src="images/me-ultrasonic-sensor_微信截图_20160129144228.png" alt="微信截图_20160129144228" width="387" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the library
``Makeblock-Library-master`` should be invoked to control the Me Ultrasonic Sensor. This is a routine to instruct the module for distance measurement through Arduino
programming.

<img src="images/me-ultrasonic-sensor_微信截图_20160129144319.png" alt="微信截图_20160129144319" width="511" style="padding:5px 5px 12px 0px;">

<img src="images/me-ultrasonic-sensor_微信截图_20160129144346.png" alt="微信截图_20160129144346" width="799" style="padding:5px 5px 12px 0px;">

The code segment serves to read the measured distance (cm) and output the result to Arduino IDE serial monitor in the cycle of 100ms. Upload the code to the Makeblock Orion, click the Arduino serial monitor, you will see the running result as
follows:

<img src="images/me-ultrasonic-sensor_微信截图_20160129144417.png" alt="微信截图_20160129144417" width="471" style="padding:5px 5px 12px 0px;">

● **mBlock programming**

Me Ultrasonic Sensor supports the mBlock programming environment and its instructions are introduced as
follows:

<img src="images/me-ultrasonic-sensor_微信截图_20160129144500.png" alt="微信截图_20160129144500" width="790" style="padding:5px 5px 12px 0px;">

This example will show you how to use mBlock to control the Me
Ultrasonic Sensor.

This program can make the panda speak out the distance detected by Me Ultrasonic Sensor. Its running result is
as follows:

<img src="images/me-ultrasonic-sensor_微信截图_20160129144530.png" alt="微信截图_20160129144530" width="787" style="padding:5px 5px 12px 0px;">

### Principle analysis

The Me Ultrasonic Sensor comes with two “eyes”, one of which (marked with T) is an ultrasonic transmitter, the other of which (marked with R) is an ultrasonic receiver. The ultrasonic transmitter launches ultrasonic wave in a direction and start time-keeping at the launch. The ultrasonic wave spreads in the air, returns back immediately
when meeting obstacles in the way, and stops time-keeping immediately when the ultrasonic receiver receives the reflected wave. The transmission speed of sonic wave in the air is 340 m/s. Use the time to recorded by the timer to calculate the distance s from the launching point to the obstacle, that is, s=340×t/2.

### Schematic

<img src="images/me-ultrasonic-sensor_Me_Ultrasonic_V3.0.png" alt="Me_Ultrasonic_V3.0" width="1512" style="padding:5px 5px 12px 0px;">
