# Me Temperature Sensor-Waterproof(DS18B20)

<img src="images/temperature-sensor-waterproofds18b20_微信截图_20160129140754.png" alt="微信截图_20160129140754" width="412" style="padding:5px 5px 12px 0px;">

### Overview

The Me Temperature Sensor module is a metal tube thermometer with DS18B20 detector. Featuring strong anti-interference ability and high accuracy, it is covered with waterproof rubber hose outside and able to measure temperature in the range -55℃\~+125℃. It can be connected to the RJ25 Adapter module which is then connected to Makeblock Orion for temperature measurement.

### Technical specifications

- Operating voltage: 5V DC  
- Sensor type: DS18B20  
- Temperature range: -55℃\~125℃  
- Control mode: Single bus port  
- Dimension: 500 x 6 x 6mm

### Functional characteristics

- 9\~12 bits A/D conversion accuracy  
- High precision: ±0.5℃ (-10℃\~+85℃)  
- Probe diameter 6 mm, length 50 mm approximately, total length 1 m
(including line)  
- Short time delay in temperature conversion, maximum 750ms  
- Support multi-point networking  
- Perfect waterproof property  
- Support mBlock GUI programming, and applicable to users of all ages

### Pin definition

The port of Me Temperature Sensor has three pins, and their functions are as follows:

<img src="images/temperature-sensor-waterproofds18b20_微信截图_20160129141047.png" alt="微信截图_20160129141047" width="784" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**  

If you want to connect to a thermometer through the RJ25 port, simply connect it to the RJ25 Adapter module, and then connect the module to Makeblock Orion as follows:

<img src="images/temperature-sensor-waterproofds18b20_微信截图_20160129141125.png" alt="微信截图_20160129141125" width="387" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire** 

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its SIG pin should be connected to digital port as follows:

<img src="images/temperature-sensor-waterproofds18b20_微信截图_20160129141205.png" alt="微信截图_20160129141205" width="391" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me Temperature Sensor-Waterproof.  

This program serves to read current temperature through Arduino programming.

<img src="images/temperature-sensor-waterproofds18b20_微信截图_20160129141241.png" alt="微信截图_20160129141241" width="625" style="padding:5px 5px 12px 0px;">

The code segment serves to read the readings of Me Temperature Sensor and output the result to the serial monitor in Arduino IDE with the cycle of 1s.  

Upload the code segment to Makeblock Orion, click the Arduino serial monitor and you will see the running result as follows:

<img src="images/temperature-sensor-waterproofds18b20_微信截图_20160129141323.png" alt="微信截图_20160129141323" width="470" style="padding:5px 5px 12px 0px;">

● **mBlock programming** 

The Me Temperature Sensor-Waterproof module supports the mBlock programming environment and its instructions are introduced as follows:

<img src="images/temperature-sensor-waterproofds18b20_微信截图_20160129141356.png" alt="微信截图_20160129141356" width="787" style="padding:5px 5px 12px 0px;">

This is an example on how to use mBlock to control the Me Temperature Sensor module. It will make the panda speaking out the value of temperature captured by the Me Temperature Sensor, and the running result is as follows:

<img src="images/temperature-sensor-waterproofds18b20_微信截图_20160129141428.png" alt="微信截图_20160129141428" width="788" style="padding:5px 5px 12px 0px;">

### Principle analysis

With the main component DS18B20, the Me Temperature Sensor module features miniaturization, low power consumption, high performance, strong anti-interference ability, and matching with microprocessor easily. Adopting unique single bus port mode, DS18B20 is in connection with a microprocessor by only a bus to implement two-way communication between the microprocessor and DS18B20. The module supports the function
of multi-point networking, so several DS18B20 can be connected in parallel on the only three-line to implement multi-point temperature measurement, and the measurement results are transmitted in 9\~12 bits serial digital mode. When communicated with a single chip computer, you can set the resolution by configuring the register. The temperature sensor in DS18B20 implements measurement of temperature
and provides results in 16-bit binary, in  which S is the sign
bit.

<img src="images/temperature-sensor-waterproofds18b20_微信截图_20160129141520.png" alt="微信截图_20160129141520" width="610" style="padding:5px 5px 12px 0px;"> 
 
For example:    
The digital output of +125℃ is 07D0H (For a positive temperature, directly convert the hexadecimal number into a decimal number). The digital output of -55℃ is FC90H (For a negative temperature, invert the hexadecimal number and increase it by 1, and then convert into a decimal number).

<img src="images/temperature-sensor-waterproofds18b20_微信截图_20160129141525.png" alt="微信截图_20160129141525" width="304" style="padding:5px 5px 12px 0px;">
