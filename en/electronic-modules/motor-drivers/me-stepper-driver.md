# Me Stepper Motor Driver


![](images/me-stepper-driver_Me-Stepper-Motor-Driver.jpg)

<img src="images/me-stepper-driver_微信截图_20160203152127.png" alt="微信截图_20160203152127" width="347" style="padding:5px 5px 12px 0px;">

### Overview

The Me Stepper Motor Driver module is designed to precisely drive the bipolar stepper motor. When pulse signals are input into the stepper motor, it rotates step by step. For each pulse signal, it rotates a certain angle. This module features adjustable drive current and microstepping hardware adjustment. It can be used in 3D printing, numerical control, Makeblock music robot, precise motion control, etc. Its red ID means that it should be connected to the port with red ID on Makeblock Orion by RJ25 wire.

### Technical specifications

- Drive voltage: 6V-12V DC  
- Maximum current: 1.35A  
- Dimension: 51 x 24 x 18mm (L x W x H)

### Functional characteristics

- Compatible with 4-wire bipolar stepper motor  
- Simply two I/O ports to control stepping and direction  
- Provide an adjustable potentiometer to adjust current and change the torque of stepper motor  
- Provide onboard DIP switch to support the full, half, 1/4, 1/8, 1/16 stepping mode  
- Provide grounding short-circuit protection and loading short-circuit protection  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- White area of module is the reference area to contact metal beams  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type port to support most development boards including Arduino series

### Pin definition

The port of Me Stepper Motor Driver has seven pins, and their functions are as follows:

<img src="images/me-stepper-driver_微信截图_20160203152355.png" alt="微信截图_20160203152355" width="720" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**  

Since the port of Me Stepper Motor Driver has red ID and the module is motor drive, you need to connect the port with red ID on Makeblock Orion when using RJ25 port.  

Taking Makeblock Orion as example, you can connect it to ports No. 1 and 2 as follows:

<img src="images/me-stepper-driver_微信截图_20160203152448.png" alt="微信截图_20160203152448" width="388" style="padding:5px 5px 12px 0px;">

> Figure 1 Connecting Me Stepper Motor Driver to Makeblock Orion

> Note: if the drive board works for a long time, the chip will emit heat. Please note this point, and add a heat sink over the board on demand to help the chip for heat dissipation.

● **Connecting with Dupont wire**  
When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its pin EN should be connected to low level, RST and SLP should be connected to high level, STP and DIR should be connected to digital ports (also can only be connected with STP and DIR pins) as follows:

<img src="images/me-stepper-driver_微信截图_20160203152526.png" alt="微信截图_20160203152526" width="389" style="padding:5px 5px 12px 0px;">

> Figure 1 Connecting Me Stepper Motor Driver to Arduino UNO

> Note: when Dupont wire is used, pin header should be welded on the module.

### Guide to programming

● **Arduino programming** 

If you use Arduino to write a program, the
library `Makeblock-Library-master` should be invoked to control the Me Stepper Motor Driver. This program serves to make the motor rotating on demand through Arduino programming. (Input an integer between 0 to 4 on the serial port control tab and send it out).

<img src="images/me-stepper-driver_微信截图_20160203152612.png" alt="微信截图_20160203152612" width="487" style="padding:5px 5px 12px 0px;">

<img src="images/me-stepper-driver_微信截图_20160203152751.png" alt="微信截图_20160203152751" width="583" style="padding:5px 5px 12px 0px;">

● **mBlock programming**  

Me Stepper Motor Driver supports the mBlock programming environment and its instructions are introduced as follows:

<img src="images/me-stepper-driver_微信截图_20160203152822.png" alt="微信截图_20160203152822" width="581" style="padding:5px 5px 12px 0px;">

This is an example on how to use mBlock to control the Me Stepper Motor Driver module. mBlock can make the stepper motor rotating from lower speed to higher speed again and
again.

<img src="images/me-stepper-driver_微信截图_20160203152902.png" alt="微信截图_20160203152902" width="791" style="padding:5px 5px 12px 0px;">

### Principle analysis

Stepping motor is a kind of electromagnetic device to transform pulse signal into corresponding angular displacement or linear displacement. It is a special kind of motor. Rotation of general motor is continuous, but stepper motor has two basic states – positioning and operating. When pulse signals are input to the stepper motor, it rotates step by step. For each pulse signal, it rotates a certain angle.  

The main component of Me Stepper Motor Driver module is A4988 micro-step driver which is designed to operate the bipolar stepper motor in such stepping modes as Full, Half, 1/4, 1/8, and 1/16 step. When it is used, simply control the STEP and DIR. For example, when in the Full stepping mode, the motor should rotate a circle by 200 steps (i.e. 1.8° per step). If higher accuracy is required, we can choose other mode. For
example, we choose the 1/4 stepping mode (that is, 0.45° per step), and then the motor should rotate by 800 micro steps to complete a round. 

The stepping mode table of this module:

<img src="images/me-stepper-driver_微信截图_20160203153000.png" alt="微信截图_20160203153000" width="635" style="padding:5px 5px 12px 0px;">

The module has a potentiometer which can be used to adjust the torque of motor, However,over-torquing may burn the chip due to heat dissipation,so it’s not recommended adjusting the torque to too large.

### Schematic

<img src="images/me-stepper-driver_Me-Stepper-Module.png" alt="Me Stepper Module" width="1220" style="padding:5px 5px 12px 0px;">
