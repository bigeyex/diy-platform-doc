<img src="project_images/mbot-li-polymer-battery_mbot锂电池-1.png" alt="mBot Li-polymer battery" width="1080" />

<img src="project_images/mbot-li-polymer-battery_mbot锂电池包装-1.png" width="596" />

### Overview:

mBot Li-polymer battery(4.2V,1800mAh)is suitable for the mBot line-up of
Makeblock.It takes only 1 hour to fully charge the battery and the
battery can last up to 10 hours in the offline mode.

<img src="project_images/mbot-li-polymer-battery_lADPDgQ9qTlHGdnNA-jNA-g_1000_1000-1.jpg" width="504" />

<img src="project_images/mbot-li-polymer-battery_图片3-1.png" width="268" /> 
 <img src="project_images/mbot-li-polymer-battery_图片2-1.png" width="193" />

### Specifications:

-   Protective cases(prevent overcharge/overdischarge/short
    circuit/overcurrent）
-   Type: Li-polymer battery/Chargeable battery
-   Charging method:Connect to mCore via USB
-   Capacity:1800mAh
-   Nominal voltage: 3.7V
-   Charge cutoff voltage:4.2V
-   Discharge cutoff voltage:2.75V
-   Charging current:≤1.0C
-   Discharge current:≤1.0C
-   Weight：33g
-   Dimension：10\*35\*50mm

### Features:

-   Multiple protections ensure safer and more efficient experiences;
-   High performance and low-energy.

[← Airblock App Guide](https://makeblock.com/project/airblock-app-2)
[Smart Servo MS-12A →](https://makeblock.com/project/smart-servo-ms-12a)

<img src="project_images/mbot-li-polymer-battery_mbot锂电池包装-1.png" width="596" />

### Overview:

mBot Li-polymer battery(4.2V,1800mAh)is suitable for the mBot line-up of
Makeblock.It takes only 1 hour to fully charge the battery and the
battery can last up to 10 hours in the offline mode.

<img src="project_images/mbot-li-polymer-battery_lADPDgQ9qTlHGdnNA-jNA-g_1000_1000-1.jpg" width="504" />

<img src="project_images/mbot-li-polymer-battery_图片3-1.png" width="268" /> 
 <img src="project_images/mbot-li-polymer-battery_图片2-1.png" width="193" />

### Specifications:

-   Protective cases(prevent overcharge/overdischarge/short
    circuit/overcurrent）
-   Type: Li-polymer battery/Chargeable battery
-   Charging method:Connect to mCore via USB
-   Capacity:1800mAh
-   Nominal voltage: 3.7V
-   Charge cutoff voltage:4.2V
-   Discharge cutoff voltage:2.75V
-   Charging current:≤1.0C
-   Discharge current:≤1.0C
-   Weight：33g
-   Dimension：10\*35\*50mm

### Features:

-   Multiple protections ensure safer and more efficient experiences;
-   High performance and low-energy.
