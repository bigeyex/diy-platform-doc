# Me Shutter


![](images/me-shutter_Me-Shutter.jpg)

<img src="images/me-shutter_微信截图_20160128164616.png" alt="微信截图_20160128164616" width="256" style="padding:5px 5px 12px 0px;">

### Overview

Me Shutter is a special module designed to implement auto-photographing for digital SLR camera. Users can use it to take high speed photos, or take time-elapse videotape and photo through controlling time exposure. Its blue ID means that it has a double-digital port and needs to be connected to the port with blue ID on Makeblock Orion, and specific
cable should be used to connect the camera.

### Technical specifications

- Operating voltage: 5V  
- Control mode: Double-digital port control  
- Module size: 52 x 24 x 18 mm (L x W x H)

### Functional characteristics

- Control the shutter and focusing of camera
- Applicable to different types of camera
- Short time delay, perfect performance, and strong ability of anti-interference
- White area of module is the reference area to contact metal beams
- Support mBlock GUI programming, and applicable to users of all ages
- Adopt RJ25 port for easy connection
- Provide pin-type of port to support most development boards including Arduino series.

### Pin definition

The port of Me Shutter has four pins, and their functions are as follows:

<img src="images/me-shutter_微信截图_20160128165107.png" alt="微信截图_20160128165107" width="720" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25** 

Since the port of Me Shutter V1.0 has blue ID, you need to connect the port with blue ID on Makeblock Orion when using RJ25 port. 

Taking Makeblock Orion as example, you can connect to ports No. 3, 4, 5, and 6 as follows:

<img src="images/me-shutter_微信截图_20160128165218.png" alt="微信截图_20160128165218" width="425" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire** 

When the Dupont wire is used to connect the module to the Arduino UNO, its AOP pin should be connected to analog pin as follows: 
 
<img src="images/me-shutter_微信截图_20160128165333.png" alt="微信截图_20160128165333" width="396" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me Shutter.

This is a routine to instruct the camera for auto-photographing when receiving signal through Arduino programming.

<img src="images/me-shutter_微信截图_20160128165412.png" alt="微信截图_20160128165412" width="463" style="padding:5px 5px 12px 0px;">

### Schematic

<img src="images/me-shutter_Shutter.png" alt="Shutter" width="1259" style="padding:5px 5px 12px 0px;">
