# Me UNO Shield

![](images/me-uno-shield_Me-UNO-Shield.jpg)

<img src="images/me-uno-shield_微信截图_20160128161320.png" alt="微信截图_20160128161320" width="342" style="padding:5px 5px 12px 0px;">

### Description

This module is a shield of Arduino UNO. It can turn Arduino pins into Makeblock RJ25 ports. You can connect Arduino UNO with Makeblock electronic modules. This shield provides a stable power, so it can drive many servos and motors.

### Features

- 2.54mm pin holes for connecting with Dupont wire
- Easy wiring with 6P6C RJ25 interface
- Arduino library for easy programming

### Specification

- Rated Voltage: 6V – 12V
- Rated Current: 1.5A

### Schematic

<img src="images/me-uno-shield_shield-uno.png" alt="shield uno" width="1533" style="padding:5px 5px 12px 0px;">
