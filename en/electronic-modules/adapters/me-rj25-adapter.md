# Me RJ25 Adapter

<img src="images/me-rj25-adapter_微信截图_20160128162440.png" alt="微信截图_20160128162440" style="padding:5px 5px 12px 0px;">

### Overview

The Me RJ25 Adapter module converts the standard RJ25 port into six pins (VCC, GND, S1, S2, SDA, and SCL) so that they can be easily drawn out from MakeBlock port in compatible with electronic modules from other manufacturers, such as temperature sensor and servo module. This module should be connected to the ports with yellow, blue, or black ID on
Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Maximum current: 3A  
- Space between pins: 2.54 mm  
- Module size: 51 x 24 x 18 mm (L x W x H)

### Functional characteristics

- Red LED is power indicator  
- Provide I²C port and two digital/analog ports  
- Can connect to electronic modules of other manufacturers  
- White area of module is the reference area to contact metal beams
- Two 3-pin anti-reverse base  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection

### Pin definition

The port of RJ25 Adapter module has 6 pins, and their functions are as follows:

<img src="images/me-rj25-adapter_微信截图_20160128162813.png" alt="微信截图_20160128162813" width="636" style="padding:5px 5px 12px 0px;">

### Wiring mode

<img src="images/me-rj25-adapter_微信截图_20160128163016.png" alt="微信截图_20160128163016" width="469" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming** 

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me RJ25 Adapter.

This program serves to read the state of Limit Switch and output results to the serial monitor in Arduino IDE through Arduino programming.

<img src="images/me-rj25-adapter_微信截图_20160128163321.png" alt="微信截图_20160128163321" width="568" style="padding:5px 5px 12px 0px;">

<img src="images/me-rj25-adapter_微信截图_20160128163428.png" alt="微信截图_20160128163428" width="821" style="padding:5px 5px 12px 0px;">

The function of the code segment is: to read the state of Limit Switch and output results to the serial monitor. Upload the code segment to Makeblock Orion, click the serial monitor and you will see the running result as follows:

<img src="images/me-rj25-adapter_微信截图_20160128163703.png" alt="微信截图_20160128163703" width="495" style="padding:5px 5px 12px 0px;">

● **mBlock programming**  

Me RJ25 Adapter supports the mBlock programming environment and its instructions are introduced as follows:

<img src="images/me-rj25-adapter_微信截图_20160128163747.png" alt="微信截图_20160128163747" width="799" style="padding:5px 5px 12px 0px;">

### Principle analysis

The Me RJ25 Adapter module lead out 6 IO ports of Makeblock Orion, they are SDA, SCL, GND, VCC, S1, and S2 respectively. S1 and S2 can be used as input/output of digital/analog signal; SDA is I²C data bus, SCL is I²C clock bus, and they can be connected to sensors which support I²C
bus. For example, the Me Temperature Sensor-Waterproof module supporting I²C protocol can be connected to the I²C bus in series to construct a network for temperature measurement.

### Schematic

<img src="images/me-rj25-adapter_s2-560x288.png" alt="s2" width="1032" style="padding:5px 5px 12px 0px;">
