# Me Shield for Raspberry Pi


![](images/me-shield-for-raspberry-pi_Me-Shield-for-Raspberry-Pi.jpg)

<img src="images/me-shield-for-raspberry-pi_微信截图_20160128161844.png" alt="微信截图_20160128161844" width="276" style="padding:5px 5px 12px 0px;">

### Description

Me shield for Raspberry Pi is attached on the Raspberry Pi to give Me Baseboard or Makeblock Orion access with hardware serial communication. 

<img src="images/me-shield-for-raspberry-pi_微信截图_20160128162026.png" alt="微信截图_20160128162026" width="738" style="padding:5px 5px 12px 0px;">

### Features

- No soldering needed and ready to use
- Support Python programming
- Compatible with Model A,B, B+
- With a RJ25 connector for Me Baseboard or Makeblock Orion
- Come with header, without occupying GPIO pins

### Specification

- Dimension: 45 x 35 x 27 mm (Length x Width x Height)

### User Guide

#### Getting started


<img src="images/me-shield-for-raspberry-pi_s4r_wiring.jpg" alt="s4r_wiring" width="990" style="padding:5px 5px 12px 0px;">

#### Suggested Reading

[What is Raspberry Pi?](http://www.raspberrypi.org/help/what-is-a-raspberry-pi/)

[How to install operating system images on Raspberry Pi?](http://www.raspberrypi.org/documentation/installation/installing-images/README.md)

**step 1 Wiring**

Make sure you have installed operating system images on Raspberry PI and connect your Raspberry Pi to the internet via Ethernet cable. Slide the Me Shield for Raspberry Pi on your Raspberry Pi, and connect the shield to Me Baseboard Port 4(Leonardo) or Makeblock Orion Port 5(Uno). Next, power on the Me Baseboard or Makeblock Orion with 12V power supply (the shield already supplies 5V voltage to raspberry, so it is not necessary to power raspberry separately).

<img src="images/me-shield-for-raspberry-pi_s4r_wiring2.jpg" alt="s4r_wiring2" width="1000" style="padding:5px 5px 12px 0px;">

**Step 2 Login Raspberry Pi**

If you are under windows, then you need to download a free SSH software (Windows) [here](https://it.wm.edu/software/public/ssh/sshsecureshellclient-3.2.9.exe).

Then click button “Quick Connect” to log in. Type the IP, user name, password(user name: pi, password: raspberry).

<img src="images/me-shield-for-raspberry-pi_quickconnect.png" alt="quickconnect" width="595" style="padding:5px 5px 12px 0px;">

<img src="images/me-shield-for-raspberry-pi_login.png" alt="login" width="398" style="padding:5px 5px 12px 0px;">

<img src="images/me-shield-for-raspberry-pi_SSHlogin.png" alt="SSHlogin" width="605" style="padding:5px 5px 12px 0px;">

If you are on Mac or linux, then open a terminal with samba service installed on raspberry pi. Type the following command in the terminal window.

`ssh pi@<your-rpi-ip>`

More useful document <http://www.raspberrypi.org/documentation/remote-access/ssh/unix.md>.

**Step 3 Disable the occupied hardware serial ttyAMA0.**

Make sure that pyserial is installed on your rip, if not, type the following command and wait until the process done.

`sudo apt-get install python-serial`

On your Raspberry Pi, disable the login prompt.

<img src="images/me-shield-for-raspberry-pi_serial.jpg" alt="serial" width="495" style="padding:5px 5px 12px 0px;">

**Step 4 Install Python library for Makeblock**

Type the following command to install the library.

`sudo pip install megapi`

The initial code for Python:

```py
from megapi import *
bot = Megapi()
bot.start() # if using usb cable, need to call bot.start('/dev/ttyACM0')

python your code
```

**Step 5 Upload an Arduino firmware to Me Baseboard or Me Orion**

Download the Arduino library for Makeblock: 
<https://github.com/Makeblock-official/Makeblock-Libraries/archive/master.zip>

Copy the makeblock folder to your Arduino default library. Your Arduino library folder should now look like this:

- (On Windows):
    *\[x:\\Users\\XXX\\Documents\]\\Arduino\\libraries\\makeblock\\src*
- (On Mac OSX):
    *\[\\Users\\XXX\\Documents\]\\Arduino\\libraries\\makeblock\\src*

Open Arduino IDE, choose the firmware from *File&gt;Examples* according to your board type (Orion in this case).

<img src="images/me-shield-for-raspberry-pi_firmware.png" alt="firmware" width="842" style="padding:5px 5px 12px 0px;">

 
**Step 6 Connect a motor to port M1 on Me Orion**.

Make sure that board is powered by external power.

<img src="images/me-shield-for-raspberry-pi_OrionMotor-266x300.png" alt="Orion&amp;Motor" width="450" style="padding:5px 5px 12px 0px;">

**Step 7 Run the demo script on raspberry Pi**

Go to the directory where the demo script located and run the `motorRun.py`

```
cd PythonForMegaPi

cd examples

python motorRun.py
```

Everything should be fine here, have fun! 

### Python API

- Start
    - **MegaPi**()
    - **start**()
- GPIO
    - **digitalWrite**( pin, level )
    - **pwmWrite**( pin, pwm )
    - **digitalRead**( pin, **def** onResult )
    - **analogRead**( pin, **def** onResult )
- Motion
    - DC Motor
        - **motorRun**( port, speed )
        - **motorMove**( leftspeed, rightspeed )
    - Servo Motor
        - **servoRun**( port, slot, angle )
    - Encoder Motor
        - **encoderMotorRun**( port, speed )
        - **encoderMotorMove**( port, speed, distance, **def**
            onFinish )
        - **encoderMotorMoveTo**( port, speed, position, **def**
            onFinish )
    - Stepper Motor
        - **stepperMotorSetting**( port, microsteps, acceleration )
        - **stepperMotorRun**( port, speed )
        - **stepperMotorMove**( port, speed, distance, **def**
            onFinish )
        - **stepperMotorMoveTo**( port, speed, position, **def**
            onFinish )
- Sensors
    - Ultrasonic Sensor
        - **ultrasonicSensorRead** ( port, **def** onResult )
    - LineFollow Sensor
        - **lineFollowerRead** ( port, **def** onResult )
    - Light Sensor
        - **lightSensorRead** ( port, **def** onResult )
    - Sound Sensor
        - **soundSensorRead** ( port, **def** onResult )
    - Temperature Sensor
        - **temperatureRead** ( port, **def** onResult )
    - PIR Motion Sensor
        - **pirMotionSensorRead** ( port, **def** onResult )
    - Touch Sensor
        - **touchSensorRead** ( port, **def** onResult )
    - LimitSwitch
        - **limitSwitchRead** ( port, slot, **def** onResult )
    - Humiture Sensor
        - **humitureSensorRead** ( port, type, **def** onResult )
    - Gas Sensor
        - **gasSensorRead** ( port, **def** onResult )
    - Flame Sensor
        - **flameSensorRead** ( port, **def** onResult )
    - Button
        - **buttonRead** ( port, **def** onResult )
    - Potentiometer
        - **potentiometerRead** ( port, **def** onResult )
    - Joystick
        - **joystickRead** ( port, axis, **def** onResult )
    - 3-Axis Accelerometer and Gyro Sensor
        - **gyroRead** ( axis, **def** onResult )
    - Compass
        - **compassRead** ( **def** onResult )
- Display
    - RGB Led
        - **rgbLedSetColor** ( port, slot, index, r, g, b )
        - **rgbLedShow** ( port, slot )
        - **rgbLedDisplay** ( port, slot, index, r, g, b )
    - 7-segment Display
        - **sevenSegmentDisplay** ( port, value )
    - Led Matrix Display
        - **ledMatrixDisplayMessage** ( port, x, y, msg )
        - **ledMatrixDisplayRaw** ( port, buffer )
    - Serial LCD Display
        - **lcdDisplay** ( string )
- Others
    - DSLR Shutter
        - **shutterOn** ( port )
        - **shutterOff** ( port )
        - **focusOn** ( port )
        - **focusOff** ( port )

### Schematic

<img src="images/me-shield-for-raspberry-pi_shield-pi.png" alt="shield pi" width="993" style="padding:5px 5px 12px 0px;">
