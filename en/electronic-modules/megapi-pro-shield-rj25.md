<img src="project_images/megapi-pro-shield-rj25_Megapi-Pro-RJ25转接板.jpg" alt="MegaPi Pro Shield for RJ25" width="1080" />

### Overview:

This module is suitable for converting the pins
of MegaPi Pro into RJ25 interfaces of Makeblock, and can be used to
connect the electronic modules of MakeBlock.

###  

### Technical Specifications:

-   Operating voltage: 5 V
-   Module size: 66 mm\*42 mm (L×W)

Note that
Interface 6/11/12 is incompatible with Me Infrared Receiver
Decode.

### Features：

-   Eight RJ25 interfaces are drawn
    out
-   With a power supply, just as a
    lamp
-   With Arduino library for easy
    programming

### Connection Mode:

<img src="project_images/megapi-pro-shield-rj25_连接方式.jpg" width="585" />

### Relevant module information

MegaPi
Pro:<https://makeblock.com/project/megapi-pro>

[← Bluetooth
Controller](https://makeblock.com/project/bluetooth-controller) [MegaPi
Pro 4DC Motor Driver
→](https://makeblock.com/project/megapi-pro-4dc-motor-driver)

### Overview:

This module is suitable for converting the pins
of MegaPi Pro into RJ25 interfaces of Makeblock, and can be used to
connect the electronic modules of MakeBlock.

###  

### Technical Specifications:

-   Operating voltage: 5 V
-   Module size: 66 mm\*42 mm (L×W)

Note that
Interface 6/11/12 is incompatible with Me Infrared Receiver
Decode.

### Features：

-   Eight RJ25 interfaces are drawn
    out
-   With a power supply, just as a
    lamp
-   With Arduino library for easy
    programming

### Connection Mode:

<img src="project_images/megapi-pro-shield-rj25_连接方式.jpg" width="585" />

### Relevant module information

MegaPi
Pro:<https://makeblock.com/project/megapi-pro>
