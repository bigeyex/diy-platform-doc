# Shutter Cable

![](images/shutter-cable-c1-c3-n1-n3-for-canon_Shutter-Cable-C1-for-Canon.jpg)

<img src="images/shutter-cable-c1-c3-n1-n3-for-canon_微信截图_20160126151-300x275.png" alt="微信截图_20160126151" width="331" style="padding:5px 5px 12px 0px;">

### Overview

The Shutter Cable is designed to connect the Me Shutter module and digital camera in the Arduino project. It can be controlled through the Makeblock Orion program to implement real time and repeated photo-taking.

### Technical specifications

- Length of cable: 26 cm, full expanded – 60 cm approximately
-  Shutter Cable N1 for Nikon: F6. F5. F90. F90X. F100. D3. D1/ D1H/ D1X. D2/ D2H/ D2X.D3/ D3X/ D100/ D200/D300/D700/D300S/D800
- Shutter Cable N3 for Nikon:D90/D5000/D5100/D3100/D7000/D600/D3200
- Shutter Cable C1 for Canon: 650D/600D/1100D/60D/550D/500D/1000D/450D/400D/350D/300D
- Shutter Cable C3 for Canon:EOS 5DII /5D /7D / 6D / 5D3 / 70D / 60D / 50D / 40D / 30D / 20D / 10D / EOS1V / EOS3 / EOS1D / 1DS / 1DSMKⅡ / 1DSMKⅢ / EOSD30

###  Functional characteristics

- Easy to plug
- Portable
