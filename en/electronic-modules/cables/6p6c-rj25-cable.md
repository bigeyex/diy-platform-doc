# 6P6C RJ25 cable


![](images/6p6c-rj25-cable_6P6C-RJ25-cable-35cm.jpg)

<img src="images/6p6c-rj25-cable_微信截图_20160126174309-300x276.png" alt="微信截图_20160126174309" width="300" style="padding:5px 5px 12px 0px;">

### Overview

RJ25 Cable can be used to connect mCore, Makeblock Orion and most Makeblock electronic modules that support RJ25 port. Compared to traditional DuPont line, it has advantages of fast connecting, less  error-prone, and more beautiful. An RJ25 cable integrates six wires.

### Technical specifications

- Three lengths of cable: 20 cm, 35 cm, 50 cm
- RJ25 cable: 6P6C

### Functional characteristics

- Quick plug
- Portable
- Unified standard
