# USB 2.0 A-Male to Micro B-Male Cable

![](images/usb-2-0-a-male-to-micro-b-male-cable_USB-2.0-A-Male-to-Micro-B-Male-Cable.jpg)


<img src="images/usb-2-0-a-male-to-micro-b-male-cable_微信截图_20160126173525.png" alt="微信截图_20160126173525" width="230" style="padding:5px 5px 12px 0px;">

### Overview

As a portable version of USB 2.0 standard, Micro USB is smaller and space saving, has longer plug life and higher plug strength, in comparison with standard USB and Mini-USB connectors. 

Micro-USB is compatible with USB1.1 (low speed: 1.5Mb/s, full speed: 12Mb/s) and USB 2.0 (high speed: 480Mb/s), meanwhile provides data transmission and charging function, so it is the best choice to connect to small devices (such as mobile phone, PDA, digital camera, digital video, portable digital player, etc.). 

The Micro USB port can be connected to Makeblock Orion development board for downloading programs and debugging.

### Technical specifications

-   Cable length: 1 m
-   Plug: Micro USB standard port
-   Function: data transmission and charging

### Functional characteristics

-   Hot pluggable
-   Portable and convenient
-   Unified standard
