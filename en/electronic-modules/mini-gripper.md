<img src="project_images/mini-gripper_mini-gripper.jpg" alt="Mini Gripper" width="1080" />

### Overview

Makeblock Mini Gripper can hook up with mBot, mBot Ranger and other
robots. With the claw, you can add more abilities to robots, like
grabbing items and picking up balls. The robot gripper uses a 9g micro
servo as the motor driver and is ready to use when it’s in your hand.
All you need to do is connecting the gripper to your robot to make it
work.

### Specifications

-   Material: Acrylic
-   Weight: 71g (servo included)
-   Max width: 55mm (maximum stretching width)
-   Max length: 86.3mm (maximum length of a closed claw)

### Connection

<img src="project_images/mini-gripper_2.png" width="1268" />

### Dimension(mm)

<img src="project_images/mini-gripper_86508-Size-Chart.png" width="900" />

### Assembling

<img src="project_images/mini-gripper_迷你机械手.png" width="1299" />

### Installation

<img src="project_images/mini-gripper_QQ截图20190213153311.jpg" width="1491" />

### Related electronic parts

9g micro servo:
<https://makeblock.com/project/9g-micro-servo>

RJ25
adapter:<https://makeblock.com/project/me-rj25-adapter>

[← MegaPi Pro Encoder/DC Motor
Driver](https://makeblock.com/project/megapi-pro-encoder-dc-motor-driver)

### Overview

Makeblock Mini Gripper can hook up with mBot, mBot Ranger and other
robots. With the claw, you can add more abilities to robots, like
grabbing items and picking up balls. The robot gripper uses a 9g micro
servo as the motor driver and is ready to use when it’s in your hand.
All you need to do is connecting the gripper to your robot to make it
work.

### Specifications

-   Material: Acrylic
-   Weight: 71g (servo included)
-   Max width: 55mm (maximum stretching width)
-   Max length: 86.3mm (maximum length of a closed claw)

### Connection

<img src="project_images/mini-gripper_2.png" width="1268" />

### Dimension(mm)

<img src="project_images/mini-gripper_86508-Size-Chart.png" width="900" />

### Assembling

<img src="project_images/mini-gripper_迷你机械手.png" width="1299" />

### Installation

<img src="project_images/mini-gripper_QQ截图20190213153311.jpg" width="1491" />

### Related electronic parts

9g micro servo:
<https://makeblock.com/project/9g-micro-servo>

RJ25
adapter:<https://makeblock.com/project/me-rj25-adapter>
