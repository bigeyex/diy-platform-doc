<img src="project_images/megapi-pro_Megapi-Pro.jpg" alt="MegaPi Pro" width="1080" />

### Overview:

MegaPi Pro is an ATmega2560-based micro control
board, and is fully compatible with Arduino programming. MegaPi Pro has
powerful programming capabilities, and its output power is up to 120 W.
MegaPi Pro has four port jacks, one 4-way DC motor expansion interface,
one RJ25 expansion board interface, and one smart servo interface. Its
strong expansion ability enables it to meet the requirements of
education, competition, entertainment, etc. MegaPi Pro can be easily
installed on the Raspberry Pi, and connected through serial ports. The
motor can be quickly controlled with the Raspberry Pi together with
corresponding program.

### Technical Specifications:

-   Main control chip:
    ATMEGA2560-16AU
-   Input voltage: DC 6 V-12 V
-   Operating voltage: DC 5 V
-   Serial ports: 3
-   I2C interface: 1
-   SPI interface: 1
-   Analog input ports: 16
-   DC Current per I/O Pin: 20 mA
-   Flash: 256 KB
-   SRAM: 8 KB
-   EEPROM: 4KB
-   Clock frequency: 16 MHz

 

### Features:

-   Four motor driver interfaces can easily
    insert and take the encoder motor driver and stepper motor driver
    modules to drive DC motors, encoder motors, and stepper
    motors.
-   One wireless communication module interface
    allows you to add a Bluetooth module or 2.4G module.
-   One smart servo interface can drive up to six
    intelligent servos in serial simultaneously.
-   One 4-way DC motor interface can drive four
    DC motors.
-   One RJ25 expansion board interface can
    connect eight RJ25 interfaces.
-   Three M4 mounting holes are consistent with
    those of Raspberry Pi.
-   Over-current protection.
-   Fully compatible with Arduino.
-   Use RJ25 interface to wire.
-   Supports Arduino programming and is equipped
    with dedicated Makeblock library functions to simplify
    programming.
-   Supports mblock (upgrade version of Scrartch)
    for all age groups of users.

### <img src="project_images/megapi-pro_clip_image002.jpg" width="472" />Interface Description:

-   Red pin—firmware burning port
-   Red socket—power output/motor
    output
-   Yellow pin, black pin, black socket—I/O
    port
-   White pin—smart management system
    interface
-   Green connector—motor interface
-   Yellow connector—4-way motor driving power
    port 
-   White connector—smart servo
    interface

### <img src="project_images/megapi-pro_MegaPi-Pro-V1.0-1.png" width="3220" />

### Relevant module information

MegaPi Pro Shield for
RJ25：<https://makeblock.com/project/megapi-pro-shield-rj25>

MegaPi Pro 4DC Motor
Driver：<https://makeblock.com/project/megapi-pro-4dc-motor-driver>

MegaPi Pro Encoder/DC Motor
Driver：<https://makeblock.com/project/megapi-pro-encoder-dc-motor-driver>

MegaPi Pro ESC
Driver：<https://makeblock.com/project/megapi-pro-esc-driver>

[← MegaPi Pro 4DC Motor
Driver](https://makeblock.com/project/megapi-pro-4dc-motor-driver)
[MegaPi Pro ESC Driver
→](https://makeblock.com/project/megapi-pro-esc-driver)

### Overview:

MegaPi Pro is an ATmega2560-based micro control
board, and is fully compatible with Arduino programming. MegaPi Pro has
powerful programming capabilities, and its output power is up to 120 W.
MegaPi Pro has four port jacks, one 4-way DC motor expansion interface,
one RJ25 expansion board interface, and one smart servo interface. Its
strong expansion ability enables it to meet the requirements of
education, competition, entertainment, etc. MegaPi Pro can be easily
installed on the Raspberry Pi, and connected through serial ports. The
motor can be quickly controlled with the Raspberry Pi together with
corresponding program.

### Technical Specifications:

-   Main control chip:
    ATMEGA2560-16AU
-   Input voltage: DC 6 V-12 V
-   Operating voltage: DC 5 V
-   Serial ports: 3
-   I2C interface: 1
-   SPI interface: 1
-   Analog input ports: 16
-   DC Current per I/O Pin: 20 mA
-   Flash: 256 KB
-   SRAM: 8 KB
-   EEPROM: 4KB
-   Clock frequency: 16 MHz

 

### Features:

-   Four motor driver interfaces can easily
    insert and take the encoder motor driver and stepper motor driver
    modules to drive DC motors, encoder motors, and stepper
    motors.
-   One wireless communication module interface
    allows you to add a Bluetooth module or 2.4G module.
-   One smart servo interface can drive up to six
    intelligent servos in serial simultaneously.
-   One 4-way DC motor interface can drive four
    DC motors.
-   One RJ25 expansion board interface can
    connect eight RJ25 interfaces.
-   Three M4 mounting holes are consistent with
    those of Raspberry Pi.
-   Over-current protection.
-   Fully compatible with Arduino.
-   Use RJ25 interface to wire.
-   Supports Arduino programming and is equipped
    with dedicated Makeblock library functions to simplify
    programming.
-   Supports mblock (upgrade version of Scrartch)
    for all age groups of users.

### <img src="project_images/megapi-pro_clip_image002.jpg" width="472" />Interface Description:

-   Red pin—firmware burning port
-   Red socket—power output/motor
    output
-   Yellow pin, black pin, black socket—I/O
    port
-   White pin—smart management system
    interface
-   Green connector—motor interface
-   Yellow connector—4-way motor driving power
    port 
-   White connector—smart servo
    interface

### <img src="project_images/megapi-pro_MegaPi-Pro-V1.0-1.png" width="3220" />

### Relevant module information

MegaPi Pro Shield for
RJ25：<https://makeblock.com/project/megapi-pro-shield-rj25>

MegaPi Pro 4DC Motor
Driver：<https://makeblock.com/project/megapi-pro-4dc-motor-driver>

MegaPi Pro Encoder/DC Motor
Driver：<https://makeblock.com/project/megapi-pro-encoder-dc-motor-driver>

MegaPi Pro ESC
Driver：<https://makeblock.com/project/megapi-pro-esc-driver>
