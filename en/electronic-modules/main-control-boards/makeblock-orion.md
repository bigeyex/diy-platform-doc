# Makeblock Orion

![](images/makeblock-orion_makeblock_orion.jpg)

<img src="images/makeblock-orion_微信截图_20160129152731.png" alt="微信截图_20160129152731" width="209" style="padding:5px 5px 12px 0px;">

### Overview

Makeblock Orion is a main control board upgraded and improved for teaching and entertainment on the basis of Arduino Uno. With powerful driving ability and maximum output power of 36W (3A), it can drive four DC motors simultaneously. Well-designed color system is used with sensor modules perfectly, and eight user-friendly independent RJ25 ports implements circuit connection easily. In addition, it supports most Arduino programming tools (Arduino/ArduBlock), and provides the GUI programming tool (mBlock) upgraded from Scratch and mobile APP to meet
the needs of various users.

### Technical specifications

- Output voltage: 5V DC  
- Input voltage: 6V-12V DC  
- Maximum input current: 3A  
- Communication mode: UART, I²C,digital I/O, analog input  
- Control chip: Atmega 328P  
- Dimension: 80 x 60 x 18 mm (L x W x H)

### Functional characteristics

- Easy to connect with a variety of sensors, electronic modules, and drive modules  
- Support the DC motor, stepper motor, servo driver, and encoder motor driver  
- Drive two motors directly  
- Supply 5V voltage and 2A current  
- Onboard buzzer  
- Over-current and over-voltage protection  
- Fully compatible with Arduino  
- Easy to use RJ25 cable  
- Provide specific Arduino library functions in Makeblock to simplify programming  
- Support mBlock (upgrade of Scratch 2.0) and applicable to users of all ages

### Introduction to ports

Makeblock Orion provides eight RJ25 ports identified by labels of six different colors. Their colors and functions are as follows:

<img src="images/makeblock-orion_微信截图_20160129152940.png" alt="微信截图_20160129152940" width="595" style="padding:5px 5px 12px 0px;"> 

<img src="images/makeblock-orion_微信截图_20160129153022.png" alt="微信截图_20160129153022" width="593" style="padding:5px 5px 12px 0px;">

The output voltages of yellow, blue, gray, black, and white ports are constant 5V DC current. These ports are usually connected to modules of 5V supply voltage.

<img src="images/makeblock-orion_微信截图_20160129153132.png" alt="微信截图_20160129153132" width="581" style="padding:5px 5px 12px 0px;">

<img src="images/makeblock-orion_start_0s.jpg" alt="start_0s" width="704" style="padding:5px 5px 12px 0px;">

### Guide to programming

1. This is an example that show you how to use a program in Arduino IDE to control the Me Sound Sensor. 

A) Connect the Makeblock Orion to the computer with Micro-USB cable, and connect the Me Sound Sensor to the port No.6.

<img src="images/makeblock-orion_微信截图_20160129153247.png" alt="微信截图_20160129153247" width="304" style="padding:5px 5px 12px 0px;">   

B) Create a new Arduino IDE file and input the following codes into IDE. 

C) Upload the program to the Makeblock Orion (Don’t connect the port No.5 to the electronic module during upload).

<img src="images/makeblock-orion_微信截图_20160129153303.png" alt="微信截图_20160129153303" width="380" style="padding:5px 5px 12px 0px;">

D) Open the serial monitor, and you can see that the output values increase with the increase of sound.

<img src="images/makeblock-orion_微信截图_20160129153313.png" alt="微信截图_20160129153313" width="436" style="padding:5px 5px 12px 0px;">

2. This is an example to show you how to write a program to drive the DC motor in Arduino IDE.

A) Connect the Makeblock Orion to your computer with Micro-USB cable.  
B) Connect the Me 130 DC Motor to the Makeblock Orion as
follows.

<img src="images/makeblock-orion_微信截图_20160129153447.png" alt="微信截图_20160129153447" width="286" style="padding:5px 5px 12px 0px;">

C) Create a new Arduino IDE file and input the following codes into
IDE. 

D) Upload the program to the Makeblock Orion (Don’t connect the port No.5 to the electronic module during upload).  

E) When it is uploaded successfully, connect with external power supply(6-12V), turn on the switch, and you can see the motor rotating clockwise for 2 seconds, stopping for 2 seconds, rotating counterclockwise for 2 seconds, stopping for 2 seconds, and the operation goes repeatedly.

<img src="images/makeblock-orion_微信截图_20160129153454.png" alt="微信截图_20160129153454" width="260" style="padding:5px 5px 12px 0px;">

3. The example uses an mBlock program to control the RGB LED, set it as red, and let it flash for a second and extinguish for a second
alternately.  

A) Connect the Makeblock Orion to your computer with Micro-USB cable, and connect the Me RGB LED module to the port No. 3. 

B) Turn on the mBlock, and create a program for the module as follows.

C) Select a corresponding port in the (“Connect”to“Serial port”) option and make a connection.  

D) After they are connected, click the green flag in mBlock. The Makeblock Orion will execute the program, and the red LED will flash. 

<img src="images/makeblock-orion_微信截图_20160129153501.png" alt="微信截图_20160129153501" width="746" style="padding:5px 5px 12px 0px;">

### Schematics

<img src="images/makeblock-orion_Orion-Sch-1.png" alt="Orion Sch" width="1350" style="padding:5px 5px 12px 0px;">

### Resource

Getting Driver and Tool
Link: [https://www.dropbox.com/s/y8o6u64awxoc10m/Orion%20firmware.rar?dl=0](//www.dropbox.com/s/y8o6u64awxoc10m/Orion%20firmware.rar?dl=0)
