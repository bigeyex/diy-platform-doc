# mCore – Main Control Board for mBot


![](images/mcore_mCore.jpg)

<img src="images/mcore_微信截图_20160203154445.png" alt="微信截图_20160203154445" width="345" style="padding:5px 5px 12px 0px;">

### Overview

mCore is an easy-to-use main control board specially designed for
mBot. Based on Arduino Uno, mCore integrates various onboard sensors,
such as buzzer, light sensor, RGB LED, etc., which provides you an
easier way to start learning electronics.

### Features

- Comes with Arduino and Makeblock Library for easy programming.
- Include various featured sensors, such as buzzer, light sensor, RGB LED, etc.
- On-board USB type B connector design for long-time use.
- Resettable fuse prevents the board from getting burn.
- Four color-labelled RJ25 connectors for easy wiring and expansion with more Arduino sensors

### Specifications

- Operating Voltage:  3.7-6V DC power                             
- Microcontroller:    ATmega328                                   
- Product Dimension:  90 x 79 x 18mm (3.54 x 3.11 x 0.71inch)     
- Package Dimension:  110 x 150 x 10 mm (4.33 x 5.91 x 0.39 inch) 
- Net Weight:         43g (1.52oz)                                
- Gross Weight:       47g (1.66oz)                                
- Package Content:    1 x mCore                                   

### Schematic

<img src="images/mcore_Mcore.png" alt="Mcore" width="1329" style="padding:5px 5px 12px 0px;">
