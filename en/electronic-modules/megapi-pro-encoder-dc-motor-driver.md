<img src="project_images/megapi-pro-encoder-dc-motor-driver_电机驱动.jpg" alt="MegaPi Pro Encoder/DC Motor Driver" width="1080" />

### Overview

This module can drive two DC motors or one
encoder motor. It can be easily installed on MegaPi Pro and MegaPi by
2×8Pin insertion.

### Technical Specifications

-   Motor driver: MP8049S
-   Motor channel: 2
-   Minimum operating voltage: 6 V
-   Maximum operating voltage: 12 V
-   Logic voltage: 5 V
-   Rated operating current of each channel: 3
    A
-   Peak operating current: 5.5 A
-   Module size: 30 mm×15 mm (L×W)

### Features

-     
    Supports motors
    with operating voltages of 6\~12 V.
-    
    Provides
    operating current up to 3 A (peak current up to 5.5 A) for 12 V
    power supply.
-   The module has
    over-voltage protection, over-current protection and
    over-temperature protection to comprehensively ensure use
    safety.
-   Allows you to
    drive one encoder motor (white interface) or two DC motors (green
    interface).
-   Provides
    colorful male and female sockets to prevent
    mis-insertion.
-   The encoder
    motor driving terminal has reverse electromotive force protection
    (TVS protection).
-   The module is
    small in size and easy to be replaced.

### Wiring mode

<img src="project_images/megapi-pro-encoder-dc-motor-driver_电机驱动插法.jpg" width="824" />

 

### Programming Guidance

MegaPi Pro DC motor programming:

This program serves to make the DC motor rotate
counterclockwise for 2s, stop for 1s, and then rotate clockwise for 2s,
stop for 1s, and repeat the cycle.

**mBlock programming:**

<img src="project_images/megapi-pro-encoder-dc-motor-driver_电机驱动程序.jpg" width="1433" />

**Arduino programming:**

<img src="project_images/megapi-pro-encoder-dc-motor-driver_电机驱动程序1-1.jpg" width="815" />

### Relevant module information

MegaPi Pro:<https://makeblock.com/project/megapi-pro>

[← MegaPi Pro ESC
Driver](https://makeblock.com/project/megapi-pro-esc-driver) [Mini
Gripper →](https://makeblock.com/project/mini-gripper)

### Overview

This module can drive two DC motors or one
encoder motor. It can be easily installed on MegaPi Pro and MegaPi by
2×8Pin insertion.

### Technical Specifications

-   Motor driver: MP8049S
-   Motor channel: 2
-   Minimum operating voltage: 6 V
-   Maximum operating voltage: 12 V
-   Logic voltage: 5 V
-   Rated operating current of each channel: 3
    A
-   Peak operating current: 5.5 A
-   Module size: 30 mm×15 mm (L×W)

### Features

-     
    Supports motors
    with operating voltages of 6\~12 V.
-    
    Provides
    operating current up to 3 A (peak current up to 5.5 A) for 12 V
    power supply.
-   The module has
    over-voltage protection, over-current protection and
    over-temperature protection to comprehensively ensure use
    safety.
-   Allows you to
    drive one encoder motor (white interface) or two DC motors (green
    interface).
-   Provides
    colorful male and female sockets to prevent
    mis-insertion.
-   The encoder
    motor driving terminal has reverse electromotive force protection
    (TVS protection).
-   The module is
    small in size and easy to be replaced.

### Wiring mode

<img src="project_images/megapi-pro-encoder-dc-motor-driver_电机驱动插法.jpg" width="824" />

 

### Programming Guidance

MegaPi Pro DC motor programming:

This program serves to make the DC motor rotate
counterclockwise for 2s, stop for 1s, and then rotate clockwise for 2s,
stop for 1s, and repeat the cycle.

**mBlock programming:**

<img src="project_images/megapi-pro-encoder-dc-motor-driver_电机驱动程序.jpg" width="1433" />

**Arduino programming:**

<img src="project_images/megapi-pro-encoder-dc-motor-driver_电机驱动程序1-1.jpg" width="815" />

### Relevant module information

MegaPi Pro:<https://makeblock.com/project/megapi-pro>
