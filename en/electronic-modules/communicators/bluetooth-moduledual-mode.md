# Bluetooth Module(Dual Mode)

![](images/bluetooth-moduledual-mode_Bluetooth-Module-for-mBot.jpg)

<img src="images/bluetooth-moduledual-mode_微信截图_20160203161005.png" alt="微信截图_20160203161005" width="301" style="padding:5px 5px 12px 0px;">

### Description

This Bluetooth module is designed specially for individual users or family to play fun. You can use your smart phones or computers (Bluetooth enabled) to control the mBots wirelessly with this module.

### Features

- Android&IOS App provided for users to play in different scenes
- Easy to work with mBlock software for graphical programming

### Specifications

- Operating Voltage: 5V DC power;
- Version: Bluetooth 2.0 and 4.0 compatible;
- Level Output Voltage: 5V/high, 0V/low;
- Dimension: 30mm\*20mm\*14mm (Length x Width x Height)
