# Me Bluetooth Module (Dual Mode)


<img src="images/me-bluetooth-moduledual-mode_微信截图_20160129111600.png" alt="微信截图_20160129111600" style="padding:5px 5px 12px 0px;">

### Overview

Mainly used in the field of wireless transmission in short distance, the Me Bluetooth Module (Dual Mode) can be conveniently connected with the Bluetooth device in the wireless terminals such as PC and smart phones, and avoid complicated cable connection and space restriction to replace the USB data line directly. As a dual mode Bluetooth module, it also supports the Bluetooth 2.1, 3.0 and 4.0 protocols, most Android devices and all the Apple devices, and serial output data as well. Its blue/gray
ID means that it has a double-digital signal and hardware serial port, and needs to be connected to the port No. 5 on Makeblock Orion.

### Technical specifications

- Operating voltage: 3.3V  
- Input voltage: 5V DC  
- Operating frequency: 2.4GHz  
- Baud rate: 115200  
- Sending/receiving distance: 10\~15 m (open field)  
- Signal mode: serial signal  
- Module size: 51 x 24 x 18 mm (L x W x H)

### Functional characteristics

- White area of module is the reference area to contact metal beams  
- Support BT2.1+EDR/3.0/BT4.0 (BLE) dual mode, and two modes can work simultaneously  
- LED indicates the state of Bluetooth: If the blue LED is flashing, it means no Bluetooth connection; if it lights on, it means the Bluetooth is connected and the port is turned on  
- Outside reset input, valid for low level, provide internal pull-up resistor  
- Support mobile devices (Android/IOS) to control electronic module  
- Password may be required for pairing during the connection (the password is 0000 or 1234)  
- Support mBlock GUI programming control through Bluetooth  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- Adopt RJ25 port for easy connection  
- Provide RX, TX, VCC, GND pins to support most Arduino Baseboards  
- Disconnect the module to Port 5 when writing (burning) program into Arduino through USB, only one-to-one pairing communication is supported

### Pin definition

The port of Me Bluetooth Module (Dual Mode) has four pins, and their functions are as follows:

<img src="images/me-bluetooth-moduledual-mode_微信截图_20160129112133.png" alt="微信截图_20160129112133" width="903" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**

Since the port of Me Bluetooth Module (Dual Mode) has blue/gray ID, you need to connect the port with blue or gray ID on Makeblock Orion when using RJ25 port. 

Taking Makeblock Orion as example, you can connect to Port 5 as follows:

<img src="images/me-bluetooth-moduledual-mode_微信截图_20160129112231.png" alt="微信截图_20160129112231" width="377" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire** 

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its RX and TX pins should be connected to digital port 0 and 1 respectively as follows:

<img src="images/me-bluetooth-moduledual-mode_微信截图_20160129112311.png" alt="微信截图_20160129112311" width="379" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**

If you use Arduino to write a program, the
library `Makeblock-Library-master` should be invoked to control the Me Bluetooth Module (Dual Mode). 

This program serves to make the mobile communicating with Arduino IDE through Arduino programming. Whenever the data are received from the mobile, they will be displayed on the serial port. 

Before the communication, make sure that your device is connected to Me Bluetooth Module (Dual Mode), and send letters to it to see the running result.

<img src="images/me-bluetooth-moduledual-mode_微信截图_20160129112410.png" alt="微信截图_20160129112410" width="441" style="padding:5px 5px 12px 0px;">

<img src="images/me-bluetooth-moduledual-mode_微信截图_20160129112437.png" alt="微信截图_20160129112437" width="731" style="padding:5px 5px 12px 0px;">

### Principle analysis

Bluetooth is a kind of radio technology supporting communication of short distance. Bluetooth technology can be used to effectively simplify the communication between mobile terminals, and make data transmission faster and more efficient so as to widen the way of mobile communication. Adopting the distributed network structure, Fast Frequency Hopping (SFH), and short packet technology, Bluetooth
works in the global ISM (i.e. industrial, scientific, and medical) frequency band of 2.4 GHz with data rate of 1 Mbps.

### Schematic

<img src="images/me-bluetooth-moduledual-mode_Me-Bluetoothdual-mode.png" alt="Me Bluetooth(dual mode)" width="1498" style="padding:5px 5px 12px 0px;">
