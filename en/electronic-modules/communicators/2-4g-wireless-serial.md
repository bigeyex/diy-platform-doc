# 2.4G Wireless Serial for mBot


![](images/2-4g-wireless-serial_2.4G-Wireless-Serial-for-mBot.jpg)

<img src="images/2-4g-wireless-serial_微信截图_20160129112845.png" alt="微信截图_20160129112845" width="347" style="padding:5px 5px 12px 0px;">

### Description

This 2.4G Wireless Serial is designed for mBots to be used in classroom or workshop when many people use wireless communication simultaneously. It uses the same the technology as wireless mouse. With the feature of no pairing needed and no drivers needed, there is no signal interference
when many modules work simultaneously. With two parts included: a dongle to plug on your computer; a module to plug on the mCore, you can easily establish a wireless connection between the software mBlock and mBot.

### Features

- Allow auto pairing, the same technology as wireless mouse,
- USB Dongle support 32&64 bit Windows&Mac OS. Driver installed automatically
- No signal interference when many mBots work simultaneously

### Specifications

- Default Baud rate:115200
- Communication distance: about 10 meters
- Power supply: 5V DC
- Dimension: 30mm\*20mm\*14mm

### Part list

- 2.4G Wireless Serial Module × 1
- 2.4G Wireless Serial USB × 1

**User Guide:** <https://www.youtube.com/watch?v=kiO8VYpIGsw>
