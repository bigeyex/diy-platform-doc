# Battery Holder for (6)AA

<img src="images/battery-holder-for-6-aa_Battery-Holder-for-(6)AA.jpg">

<br>

<img src="images/battery-holder-for-6-aa_微信截图_20160126174443.png" alt="微信截图_20160126174443" width="220" style="padding:5px 5px 12px 0px;">

### Overview

The 6AA Battery Holder can accommodate 6 AA batteries which are cascaded to provide 9V DC voltage. Through standard DC port it can be connected to the Makeblock Orion development board for power supply to the development board and motor. The two round holes in the Battery Holder are designed to fix itself onto the bracket and Makeblock mechanical parts.

### Technical specifications

- Cable length: 20 cm
- Size of DC plug: 5.5 x 2.1 x 10 mm (OD x ID x L)

### Functional characteristics

- Portable and convenient
- Easy to assemble
