<img src="project_images/megapi-pro-4dc-motor-driver_Megapi-Pro-4路直流电机驱动.jpg" alt="MegaPi Pro 4DC Motor Driver" width="1080" />

### Overview:

This module is used in MegaPi Pro, and can expand 4 DC motor interfaces.
Its unique and convenient plug-in structure provides more creation space
for customers.

### Technical Specifications:

Motor driver: MP80495

Motor channel: 4

Minimum operating voltage: 6 V

Maximum operating voltage: 12 V

Logic voltage: 5 V

Rated operating current of each channel: 3 A

Peak operating current: 5.5 A

Module size: 39 mm×39 mm (L×W)

### Features:

Supports the operating voltage of 6\~12 V

Provides operating current up to 3 A (peak current up to 5.5 A) for 12 V
power supply

The module has over-voltage protection, over-current protection, and
over-temperature protection to comprehensively ensure use safety.

Enables to drive four DC motors

Motor driving terminal has reverse electromotive force protection (TVS
protection).

The motor steering of this module is opposite to that of the encoder/DC
motor driving module, making it easy for users to use in a variety of
ways.

### Connection Mode:

### <img src="project_images/megapi-pro-4dc-motor-driver_Megapi-Pro-四路直流电机转接板.jpg.png" width="816" /><img src="project_images/megapi-pro-4dc-motor-driver_QQ截图20181223181110.png" width="782" />

### Programming Example:

MegaPi Pro 4-way DC motor driver programming:

mBlock programming:

This program serves to make the DC motor rotate clockwise for 2s, stop
for 1s, and then rotate counterclockwise for 2s, stop for 1s, and repeat
the cycle.

<img src="project_images/megapi-pro-4dc-motor-driver_MegaPi-Pro四路电机驱动-1.jpg" width="1916" />

Arduino programming:

<img src="project_images/megapi-pro-4dc-motor-driver_MegaPi-Pro四路电机驱动-arduino编程.jpg" width="840" />

### Relevant module information

MegaPi Pro：<https://makeblock.com/project/megapi-pro>

[← MegaPi Pro Shield for
RJ25](https://makeblock.com/project/megapi-pro-shield-rj25) [MegaPi Pro
→](https://makeblock.com/project/megapi-pro)

### Overview:

This module is used in MegaPi Pro, and can expand 4 DC motor interfaces.
Its unique and convenient plug-in structure provides more creation space
for customers.

### Technical Specifications:

Motor driver: MP80495

Motor channel: 4

Minimum operating voltage: 6 V

Maximum operating voltage: 12 V

Logic voltage: 5 V

Rated operating current of each channel: 3 A

Peak operating current: 5.5 A

Module size: 39 mm×39 mm (L×W)

### Features:

Supports the operating voltage of 6\~12 V

Provides operating current up to 3 A (peak current up to 5.5 A) for 12 V
power supply

The module has over-voltage protection, over-current protection, and
over-temperature protection to comprehensively ensure use safety.

Enables to drive four DC motors

Motor driving terminal has reverse electromotive force protection (TVS
protection).

The motor steering of this module is opposite to that of the encoder/DC
motor driving module, making it easy for users to use in a variety of
ways.

### Connection Mode:

### <img src="project_images/megapi-pro-4dc-motor-driver_Megapi-Pro-四路直流电机转接板.jpg.png" width="816" /><img src="project_images/megapi-pro-4dc-motor-driver_QQ截图20181223181110.png" width="782" />

### Programming Example:

MegaPi Pro 4-way DC motor driver programming:

mBlock programming:

This program serves to make the DC motor rotate clockwise for 2s, stop
for 1s, and then rotate counterclockwise for 2s, stop for 1s, and repeat
the cycle.

<img src="project_images/megapi-pro-4dc-motor-driver_MegaPi-Pro四路电机驱动-1.jpg" width="1916" />

Arduino programming:

<img src="project_images/megapi-pro-4dc-motor-driver_MegaPi-Pro四路电机驱动-arduino编程.jpg" width="840" />

### Relevant module information

MegaPi Pro：<https://makeblock.com/project/megapi-pro>
