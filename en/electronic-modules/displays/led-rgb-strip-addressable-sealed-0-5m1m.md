# LED RGB Strip-Addressable, Sealed


![](images/led-rgb-strip-addressable-sealed-0-5m1m_LED-RGB-Strip-Addressable,-Sealed.jpg)

<img src="images/led-rgb-strip-addressable-sealed-0-5m1m_微信截图_20160129103438.png" alt="微信截图_20160129103438" width="308" style="padding:5px 5px 12px 0px;">

### Overview

The LED RGB Strip (Addressable, Sealed) comprises a number of
addressable LED RGB lamps. The LED RGB strip with the length of 1m includes 30 RGB LEDs totally. In addition to the characteristics of high brightness and full color, you can also control the color and brightness of each RGB individually by programming. The LED RGB Strip (Addressable, Sealed) can be connected to Makeblock Orion through the RJ25 Adapter module.

### Technical specifications

- Length of cable: 1 m, 0.5 m  
- Operating voltage: 5V DC  
- Operating current: 1.8A/m (Maximum current)  
- Operating temperature: -40\~+60℃  
- Visible angle: &gt;140°  
- Service life: &gt; 50,000 hours  
- Dimension: 1000 x 13 x 3 mm (L x W x H) excluding cable

### Functional characteristics

- Display in 256 grades of brightness, 16,777,216 kinds of pure color
display  
- Fully match with Arduino library to enable users programming easily  
- Each LED can be controlled individually  
- Change the shape of LED RGB strip to implement different luminous pattern

### Connection diagram

<img src="images/led-rgb-strip-addressable-sealed-0-5m1m_微信截图_20160129103957.png" alt="微信截图_20160129103957" width="435" style="padding:5px 5px 12px 0px;">
