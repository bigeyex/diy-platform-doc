# Me LED Matrix 8×16

![](images/me-led-matrix-8x16_Me-LED-Matrix-8×16.jpg)

<img src="images/me-led-matrix-8x16_LED_Matrix3.png" alt="LED_Matrix3" width="230" style="padding:5px 5px 12px 0px;">

### Overview

The Me LED Matrix(8 x 16) contains total 128 pieces of aligned LED. The color is blue. By receiving data from main board, it can be controlled to show the number, letter or symbol. There is a blue ID on the connector. It means that this module can be connected to the port with blue ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Signal mode: I²C communication  
- Module size: 73 x 31 x 15 mm (L x W x
H)

### Functional characteristics

- Display number, string or symbol programmed by main board  
- White area of module is the reference area to contact metal beams  
- Anti-reverse protection – connecting the power supply inversely will
not damage IC  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type port to support most development boards including Arduino series

### Pin definition

The port of Me LED Matrix(8 x 16) has three pins, and their functions are as follows:

<img src="images/me-led-matrix-8x16_LED2.png" alt="LED2" width="502" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25** 

Since the port of LED Matrix(8 x 16) has Blue ID, you need to connect the port with blue ID on Makeblock Orion when using RJ25 port.

Taking Makeblock Orion as example, you can connect it to ports No. 3, 4, 5 and 6 as follows:

<img src="images/me-led-matrix-8x16_board.png" alt="board" width="448" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming** 

If you use Arduino to write a program, the library `Makeblock-Library-master` should be invoked to control the Me LED Matrix. The LED Matrix will display the time, string or number.

<img src="images/me-led-matrix-8x16_code.png" alt="code" width="259" style="padding:5px 5px 12px 0px;"> 
 
<img src="images/me-led-matrix-8x16_function.png" alt="function" width="576" style="padding:5px 5px 12px 0px;">

### Principle analysis

The Me LED Matrix contain 128 pieces of matrix LED.There are 8 piece per line and 16 per row. The main board sends the data to module through I2C bus, and LED control specific chip in module can handle the data and interpret it. And output relative signal to power on or off the LED.
Make them display the content.

### Schematic

<img src="images/me-led-matrix-8x16_LED-Matrix.png" alt="LED Matrix" width="1293" style="padding:5px 5px 12px 0px;">
