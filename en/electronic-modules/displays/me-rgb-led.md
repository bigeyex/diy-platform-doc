# Me RGB LED


![](images/me-rgb-led_Me-RGB-LED.jpg)

<img src="images/me-rgb-led_微信截图_20160129105729.png" alt="微信截图_20160129105729" width="213" style="padding:5px 5px 12px 0px;">

### Overview

The Me RGB LED module comprises four adjustable and panchromatic RGB LEDs. The color of each LED can be decided by the values of red (R), green (G), and blue (B). With built-in control chip in each RGB LED, you need only a single signal line to implement independent full-color function. It features adjustable brightness, so the effects of running water, twinkling, and rainbow light can be achieved. Its yellow ID means that it has a single-digital port and needs to be connected to the port with yellow ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Quantity of LED: 4 x RGB LED  
- Maximum current: 60mA each, 240mA in total  
- LED type: WS2812-4  
- Luminance range: 0\~255  
- Control mode: Single-digital port control  
- Angle of visibility: &gt;140°  
- Module size: 51 x 24 x 18 mm (L x W x H)

### Functional characteristics

- With 256 levels of brightness for the RGB color of each pixel, it features true color display of 16,777,216 colors and scanning frequency no less than 400Hz  
- With concatenated serial ports, you can complete receiving and decoding of data with a single line  
- White area of module is the reference area to contact metal beams  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type port to support most development boards including Arduino series

<img src="images/me-rgb-led_微信截图_20160129105959.png" alt="微信截图_20160129105959" width="580" style="padding:5px 5px 12px 0px;">

### Pin definition

The port of Me Ultrasonic Sensor has three pins, and their functions are as follows:

<img src="images/me-rgb-led_微信截图_20160129110034.png" alt="微信截图_20160129110034" width="576" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25** 

Since the port of Me Ultrasonic Sensor has yellow ID, you need to connect the port with yellow ID on Makeblock Orion when using RJ25 port.

Taking Makeblock Orion as example, you can connect to ports No. 3, 4, 5, 6, 7, and 8 as follows:

<img src="images/me-rgb-led_微信截图_20160129110115.png" alt="微信截图_20160129110115" width="259" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its SIG pin should be connected to digital port as follows:

<img src="images/me-rgb-led_微信截图_20160129110158.png" alt="微信截图_20160129110158" width="300" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the `Makeblock-Library-master` should be invoked to control the Me RGB LED. This is a routine to change the color of four LEDs regularly to implement beautiful color cycling through Arduino programming.

<img src="images/me-rgb-led_微信截图_20160129110241.png" alt="微信截图_20160129110241" width="523" style="padding:5px 5px 12px 0px;">

<img src="images/me-rgb-led_微信截图_20160129110307.png" alt="微信截图_20160129110307" width="738" style="padding:5px 5px 12px 0px;">

● **mBlock programming**  

Me RGB LED supports the mBlock programming environment and its instructions are introduced as follows:

<img src="images/me-rgb-led_微信截图_20160129110342.png" alt="微信截图_20160129110342" width="718" style="padding:5px 5px 12px 0px;">

### Principle analysis

Me RGB LED adopts the mode of single-line-return-to-zero code as its communication protocol. When the pixel is in power and reset, the DIN end receives the data sent from the controller. The first 24 bits of the data are retrieved by the first pixel and sent to the data latch for the pixel; the rest of the data is reshaped and amplified by internal
reshaping circuit, and then forwarded to the next cascaded pixel through the DO port. The signal is reduced by 24 bits transmitted through a pixel. The automatic reshaping and forwarding technology is used by the pixels, so that the number of cascaded pixels is not limited by the
transmission of signal, but limited by the speed of signal transmission.

### Schematic

<img src="images/me-rgb-led_LED-RGB.png" alt="LED RGB" width="1269" style="padding:5px 5px 12px 0px;">

### Related links

RGB color table: http://tool.oschina.net/commons?type=3
