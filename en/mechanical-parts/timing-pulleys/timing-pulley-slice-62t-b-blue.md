<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-62t-b.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Timing Pulley Slice 62T B

Makeblock
Timing Pulley Slice 62T-B is the new version of Timing Pulley Slice
62T-Blue. As the "dam-board" of Timing Pulley 62T, Timing Pulley Slice
62T-B can prevent the slipping of the timing belt when working
.

### Features

-   Made
    from 6061 aluminum , 0.8mm thick, anodized surface.
-   Diameters:
    25mm (inside), 42mm (outside).
-   8
    Plastic Rivets included.
-   Sold
    in four.

 

### Size Chart(mm)

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-62t-b-1.png" style="width:800;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-62t-b-2.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-62t-b-3.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-62t-b-4.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-62t-b-5.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-62t-b-6.jpg" style="width:400;padding:5px 5px 15px 0px;">

 

 
