<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-belt.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Timing Belt

### Features

-   Timing Belt Standard: MXL
-   Pitch: 2.032mm 
-   Width: 6.6 mm
-   Material: Neoprene
-   Tooth Number: 123, 140, 160, 216, 378

### Demo

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-belt-1.png" style="width:800;padding:5px 5px 15px 0px;">
