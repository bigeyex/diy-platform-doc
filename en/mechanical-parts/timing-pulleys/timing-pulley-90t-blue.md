<img src="../../../zh/mechanical-parts/timing-pulleys/images/plastic-timing-pulley-90t.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Timing Pulley 90T

### Features

-   Made
    from 6061 aluminum extrusion , 8mm thick, anodized surface.

-   Compatible
    with MXL Timing Belt.

-   Can
    also be used as a car wheel. 

-   Sold
    in Packs of 4.

 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/timing-pulleys/images/plastic-timing-pulley-90t-1.png" style="width:600;padding:5px 5px 15px 0px;">

 
