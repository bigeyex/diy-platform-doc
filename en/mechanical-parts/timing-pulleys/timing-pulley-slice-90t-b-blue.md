<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-90t-b.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Timing Pulley Slice 90T B

### Description

Timing
Pulley Slice 90T-B is the new version of Timing Pulley Slice
90T-Blue. As the "dam-board" of Timing Pulley 90T, Timing Pulley Slice
90T-B can prevent the slipping of the Timing Belt when working .

 

### Features

-   Made
    from 6061 aluminum , 0.8mm thick, anodized surface.
-   8
    Plastic Rivets included.
-   Sold
    in pack of 4.

 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-90t-b-0.jpg" style="width:800;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-90t-b-1.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-90t-b-2.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-90t-b-3.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-90t-b-4.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-slice-90t-b-5.jpg" style="width:400;padding:5px 5px 15px 0px;">


