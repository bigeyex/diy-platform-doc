<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-32t.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Timing Pulley 32T

### Features

- Made from 6061 aluminum, anodized surface.

- 32 tooth timing pulley with a center diameter of 4mm.

- Compatible with MXL Timing Belt.

- 2 Headless Screw M3x5 included.

- Sold in Pair.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-32t-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-pulley-32t-2.jpg" style="width:600;padding:5px 5px 15px 0px;">

 
