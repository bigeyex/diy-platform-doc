<img src="../../../zh/mechanical-parts/timing-pulleys/images/timing-belt-5m-open-end.jpg" style="width:400;padding:5px 5px 15px 0px;">


# Timing Belt Open-end

### Description

This neoprene
timing belt is perfect for hobby 3D printers, camera sliders, or other
linear motion systems. It is open-ended so that each end can be attached
to the printer head, camera slider, etc.  

The Makeblock
Timing Pulley are additionally recommended.   

### Features

-   Timing
    Belt Standard: MXL
-   Pitch:
    2.032mm 
-   Width: 6.6 mm
-   Material: Neoprene
-   Length: 1m, 2m, 3m, 5m
