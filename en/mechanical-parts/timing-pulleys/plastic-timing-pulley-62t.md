<img src="../../../zh/mechanical-parts/timing-pulleys/images/plastic-timing-pulley-62t.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Plastic Timing Pulley 62T

### Description

Makeblock plastic timing pulley 62T is
made of plastic, which has a lighter weight. It is used as a car's wheel in DIY designs. Moreover, 12 M4 holes in it also allow to be used as the plate in Makeblock platform.

### Features

- Made from plastic.
- 62 tooth timing pulley with a step diameter of 10\*7.5mm.
- Compatible with MXL Timing Belt.
- Can be used with TT Motor.
- Can also be used as a car wheel. 
- With 12 M4 holes 
- Sold in Packs of 4.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/timing-pulleys/images/plastic-timing-pulley-62t-1.png" style="width:600;padding:5px 5px 15px 0px;">

