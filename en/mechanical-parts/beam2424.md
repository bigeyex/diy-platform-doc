<img src="../../zh/mechanical-parts/beam2424/images/beam2424.jpg" style="width:600;padding:5px 5px 15px 0px;">

# Beam 2424

### Description

Makeblock Beam2424 is one of the most frequently used part in Makeblock platform. It is compatible with most Makeblock motion and structure
components.

### Specifications

- Length: 74 - 504mm
- Size (mm): 072, 088, 104, 120, 136, 152, 168, 184, 248, 312, 504
- Cross-section area: 24 x 24mm
- Material: 6061 aluminum extrusion

### Features

- Excellent strength
- Twist resistance
- Easy and flexible connection

### Instructions

### Size Chart

<table style="text-align:center;cellpadding:12px;cellspacing:10px;">

<tr>
<td><img src="../../zh/mechanical-parts/beam2424/images/beam2424-072.jpg" width="170px;"><br><p>Beam2424-072</p></td>
<td><img src="../../zh/mechanical-parts/beam2424/images/beam2424-088.jpg" width="170px;"><br><p>Beam2424-088</p></td>
<td><img src="../../zh/mechanical-parts/beam2424/images/beam2424-104.jpg" width="170px;"><br><p>Beam2424-104</p></td>
<td><img src="../../zh/mechanical-parts/beam2424/images/beam2424-120.jpg" width="170px;"><br><p>Beam2424-120</p></td>
</tr>

<tr>
<td><img src="../../zh/mechanical-parts/beam2424/images/beam2424-136.jpg" width="170px;"><br><p>Beam2424-136</p></td>
<td><img src="../../zh/mechanical-parts/beam2424/images/beam2424-152.jpg" width="170px;"><br><p>Beam2424-152</p></td>
<td><img src="../../zh/mechanical-parts/beam2424/images/beam2424-168.jpg" width="170px;"><br><p>Beam2424-168</p></td>
<td><img src="../../zh/mechanical-parts/beam2424/images/beam2424-184.jpg" width="170px;"><br><p>Beam2424-184</p></td>
</tr>

<tr>
<td><img src="../../zh/mechanical-parts/beam2424/images/beam2424-248.jpg" width="170px;"><br><p>Beam2424-248</p></td>
<td><img src="../../zh/mechanical-parts/beam2424/images/beam2424-312.jpg" width="170px;"><br><p>Beam2424-312</p></td>
<td><img src="../../zh/mechanical-parts/beam2424/images/beam2424-504.jpg" width="170px;"><br><p>Beam2424-504</p></td>
</tr>

</table>