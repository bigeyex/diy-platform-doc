# Linear Motion Shaft D8x496mm


![](images/linear-motion-shaft-d8x496mm_Linear-Motion-Shaft-D8x496mm.jpg)

**Description:**

This linear
motion shaft of 8 mm in diameter and 496 mm in length is chrome plated
and case hardened, and is suitable for use with slide units in linear
motion applications. The high-carbon steel shaft is chrome plated for
corrosion resistance, case hardened for wear resistance, and precision
ground for consistent ball bushing radial clearance.

** Features:**

-   Round
    steel shaft for use with slide units in linear motion
    applications.
-   Chrome
    plated for corrosion resistance
-   Case
    hardened for wear resistance
-   Precision
    ground for consistent ball bushing radial clearance

**Size
Charts(mm):**

 <img src="images/linear-motion-shaft-d8x496mm_85421-s1.jpg" alt="85421-s1.jpg" width="760" />

 

**Demo：**

****<img src="images/linear-motion-shaft-d8x496mm_00211.png" alt="00211.png" width="722" />****

 
