# Beam0808-072


![](images/beam0808-072-blue-4-pack_Beam0808-072.jpg)

**Description:**

Makeblock Beam0808-072 is one of the
most frequently used in Makeblock platform, it is compatible with most
Makeblock motions and structure components. 

** **

**Features:**

-   Made from 6061 aluminum extrusion,
    anodized surface. Excellent strength and twist resistance.
-   With holes on 16mm increments, can
    be drilled for 4mm hardware.
-   Threaded slot enables easy and
    flexible connection.
-   Cross-sectional area 8x8mm, length
    72mm.
-   Sold in Packs of 4.

 

**Size Charts(mm):**

**<img src="images/beam0808-072-blue-4-pack_60516.png" alt="60516.png" width="812" />**

**Demo (Beam 0808 Series):**
