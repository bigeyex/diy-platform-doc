# Beam0412-044


![](images/beam0412-044-blue-4-pack_Beam0412-044.jpg)


### Building Examples:

<img src="images/beam0412-044-blue-4-pack_beam0412-044.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-044-blue-4-pack_2015-12-24-15-40-26.jpg" width="720" />

### Specifications

- SKU: 60703
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 44mm
- Package content: 4 x Beam 0412-044
- Dimension: 344 x 12 x 4mm (1.73 x 0.47 x 0.16'')
- Net Weight: 14.4g (0.51oz)
