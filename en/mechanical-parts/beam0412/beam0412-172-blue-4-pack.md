# Beam0412-172

![](images/beam0412-172-blue-4-pack_Beam0412-172.jpg)

 

### Building Examples:

<img src="images/beam0412-172-blue-4-pack_beam0412-172.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-172-blue-4-pack_2015-12-29-12-03-10.jpg" width="720" />

### Specifications

- SKU: 60719
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 172mm
- Package content: 4 x Beam 0412-172
- Dimension: 172 x 12 x 4mm (6.77 x 0.47 x 0.16'')
- Net Weight: 62.4g (2.2oz)
