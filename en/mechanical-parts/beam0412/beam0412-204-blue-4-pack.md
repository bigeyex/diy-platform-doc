# Beam0412-204

![](images/beam0412-204-blue-4-pack_Beam0412-204.jpg)

 

### Building Examples:

<img src="images/beam0412-204-blue-4-pack_beam0412-204.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-204-blue-4-pack_2015-12-29-17-33-19.jpg" width="720" />

### Specifications

- SKU: 60723
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 204mm
- Package content: 4 x Beam 0412-204
- Dimension: 204 x 12 x 4mm (8.03 x 0.47 x 0.16'')
- Net Weight: 76g (2.68oz)
