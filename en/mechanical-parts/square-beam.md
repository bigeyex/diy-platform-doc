<img src="../../zh/mechanical-parts/square-beam/square-beam.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# Square Beam

### Description

Makeblock square beam is one of the most frequently used part in Makeblock platform. It is compatible with most Makeblock motion and structure
components.

### Specifications

- Length: 16 - 184mm
- Size (mm): 016, 032, 056, 072, 088, 104, 120, 136, 152, 168, 184
- Cross-section area: 8 x 8mm
- Material: 6061 aluminum extrusion

### Features

- Excellent strength
- Twist resistance
- Easy and flexible connection

### Demo

<img src="../../zh/mechanical-parts/square-beam/square-beam-5.jpg" style="width:300px;padding:5px 5px 15px 0px;">
<img src="../../zh/mechanical-parts/square-beam/square-beam-6.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="../../zh/mechanical-parts/square-beam/square-beam-7.jpg" style="width:300px;padding:5px 5px 15px 0px;">
<img src="../../zh/mechanical-parts/square-beam/square-beam-8.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="../../zh/mechanical-parts/square-beam/square-beam-9.jpg" style="width:300px;padding:5px 5px 15px 0px;">
<img src="../../zh/mechanical-parts/square-beam/square-beam-10.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="../../zh/mechanical-parts/square-beam/square-beam-11.jpg" style="width:300px;padding:5px 5px 15px 0px;">
<img src="../../zh/mechanical-parts/square-beam/square-beam-12.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="../../zh/mechanical-parts/square-beam/square-beam-13.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="../../zh/mechanical-parts/square-beam/square-beam-1.png" style="width:300px;padding:5px 5px 15px 0px;">
<img src="../../zh/mechanical-parts/square-beam/square-beam-2.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="../../zh/mechanical-parts/square-beam/square-beam-3.jpg" style="width:300px;padding:5px 5px 15px 0px;">
<img src="../../zh/mechanical-parts/square-beam/square-beam-4.jpg" style="width:300px;padding:5px 5px 15px 0px;">