<img src="../../../zh/mechanical-parts/slider-v-slot/slider496.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Slider496

### Features

- Made from 6061 aluminum extrusion, anodized surface. Excellent strength and twist resistance.
- Threaded slot enables easy and flexible connection.
- Compatible to V-slot Bearing

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/slider-v-slot/slider496-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/slider-v-slot/slider496-2.jpg" style="width:600;padding:5px 5px 15px 0px;">