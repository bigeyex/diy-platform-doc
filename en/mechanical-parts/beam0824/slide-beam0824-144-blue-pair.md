# Beam0824-144

![](images/slide-beam0824-144-blue-pair_Beam0824-144.jpg)

 <img src="images/slide-beam0824-144-blue-pair_3L4A0284.jpg" width="500" />

### What Is Slide Beam0824-144?

Slide Beam0824-144 is a frequently-used mechanical part of Makeblock
platform，and also compatible with most Makeblock mechanical
components. There are various mounting holes and a threaded slot on the
plane of this beam which allow you to assemble on other structures
easily.

### Features

- Made from heavy-duty 6061 aluminum extrusion
- With holes on 16mm increments，and M4 threaded holes on both ends
- Cross-section area 8x24mm, length 144mm
- High wear resistance and high intensity
- Strong rigidity

<img src="images/slide-beam0824-144-blue-pair_3L4A0287.jpg" width="500" />

### DEMO

<img src="images/slide-beam0824-144-blue-pair_demo60034.jpg" width="800" />

### Size Charts

<img src="images/slide-beam0824-144-blue-pair_Size_Chart.png" width="800" />

### Specifications

- SKU: 60034
- Product Name: Slide Beam0824-144-Blue (Pair)
- Length: 144mm
- Cross-section Area: 8 x 24mm
- Gross Weight: 64g (2.26oz)
- Package Content (Quantity x Part Name): 2x Slide Beam0824-144

 
