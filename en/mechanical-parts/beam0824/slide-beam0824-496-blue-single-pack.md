# Beam0824-496

![](images/slide-beam0824-496-blue-single-pack_Beam0824-496.jpg)

<img src="images/slide-beam0824-496-blue-single-pack_3L4A0227.jpg">

### Features

<img src="images/slide-beam0824-496-blue-single-pack_3L4A0218.jpg" width="500" />

### DEMO

<img src="images/slide-beam0824-496-blue-single-pack_demo.jpg" 
width="500" />

### Size Charts

<img src="images/slide-beam0824-496-blue-single-pack_Size_Chart.png" width="800" />

