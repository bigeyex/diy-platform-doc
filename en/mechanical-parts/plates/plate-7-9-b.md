<img src="../../../zh/mechanical-parts/plates/images/plate-7-9-b.jpg" style="width:400;padding:5px 5px 15px 0px;">


# Plate 7\*9-B

### Description

Plate
7×9 B-Blue is the same size as Plate 7×9, but it comes with some 8mm
through holes.

 

### SizeChart(mm)

<img src="../../../zh/mechanical-parts/plates/images/plate-7-9-b-1.jpg" style="width:700;padding:5px 5px 15px 0px;">