<img src="../../../zh/mechanical-parts/plates/images/plate-0324-184.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Plate 0324-184

### Features

- Material: 6061 aluminum

### Size Chart

<img src="../../../zh/mechanical-parts/plates/images/plate-0324-184-1.png" style="width:700;padding:5px 5px 15px 0px;">