<img src="../../../zh/mechanical-parts/plates/images/plate-i1.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Plate I1


### Description

Plate
I1 is usually used to build line motion mechanism,rocker mechanisms
because it is specially designed with a long slot .

 

### Features

-   A
    long slot ( width:4mm, length:60mm )
-   18
    M4 mounting holes compatible with most Makeblock componets.
-   Aluminum
    extrusion (high strength),8msick, anodized surface(long time to
    shelve with barely rusting)

 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/plates/images/plate-i1-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/plates/images/plate-i1-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-i1-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-i1-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-i1-5.gif" style="width:300;padding:5px 5px 15px 0px;">
