<img src="../../../zh/mechanical-parts/plates/images/plate-o1.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Plate O1


### Description

Plate O1 is designed as attachment points to connect
mechanical parts at 45°,90° or 135° angles.
Using it in your mechanical structures, you will have more angle
choices.

 

### Features

-   Can
    be used to build 45°，90°,135°
    angles
-   Three
    8mm diameter center holes compatible with 8mm shafts and
    bearings
-   Four
    M4 threaded holes and other M4 mounting holes compatible with most
    Makeblock components.
-   Made
    from aluminum extrusion (high strength), 3mm thick, anodized
    surface(long time to shelve without barely rusting)

 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/plates/images/plate-o1-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/plates/images/plate-o1-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-o1-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-o1-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-o1-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-o1-6.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-o1-7.jpg" style="width:300;padding:5px 5px 15px 0px;">