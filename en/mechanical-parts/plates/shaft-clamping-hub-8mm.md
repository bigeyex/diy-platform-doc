<img src="../../../zh/mechanical-parts/plates/images/shaft-clamping-8mm.jpg" style="padding:5px 5px 15px 0px;">

# Shaft Clamping Hub 8mm

### Description

This Shaft Clamping Hub 8mm is suitable for 8mm shafts, with two specially designed 8mm-diameter holes. You can use it as an 8mm shaft bracket, or a chassis for a robotic car.

### Specifications

- Material: Aluminum alloy
- Thickness: 3mm
- Length: 90mm
- Width: 12mm



 

 

 
