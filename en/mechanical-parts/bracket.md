# Brackets

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="brackets/8mm-bearing-bracket-a.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/8mm-bearing-bracket-a.jpg" width="150px;"></a><br>
<p>8mm Bearing Bracket A</p></td>

<td width="25%;"><a href="brackets/36mm-motor-bracket.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/36mm-motor-bracket.jpg" width="150px;"></a><br>
<p>36mm Motor Bracket</p></td>

<td width="25%;"><a href="brackets/42byg-stepper-motor-bracket-b-blue.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/42byg-stepper-motor-bracket-b.jpg" width="150px;"></a><br>
<p>42BYG Stepper Motor Bracket B-Blue</p></td>

<td width="25%;"><a href="brackets/57byg-stepper-motor-bracket-black.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/57byg-stepper-motor-bracket.jpg" width="150px;"></a><br>
<p>57BYG Stepper Motor Bracket Pack-Black</p></td>
</tr>

<tr>
<td><a href="brackets/base-bracket.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/base-bracket.jpg" width="150px;"></a><br>
<p>Base Bracket B</p></td>
<td><a href="brackets/bracket-3x3.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/bracket-3x3.jpg" width="150px;"></a><br>
<p>Bracket 3*3</p></td>
<td><a href="brackets/bracket-l1.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/bracket-l1.jpg" width="150px;"></a><br>
<p>Bracket L1</p></td>
<td><a href="brackets/bracket-p1.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/bracket-p1.jpg" width="150px;"></a><br>
<p>Bracket P1</p></td>
</tr>

<tr>
<td><a href="brackets/bracket-p3.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/bracket-p3.jpg" width="150px;"></a><br>
<p>Bracket P3</p></td>
<td><a href="brackets/bracket-u1.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/bracket-u1.jpg" width="150px;"></a><br>
<p>Bracket U1</p></td>
<td><a href="brackets/dc-motor-25-bracket-b.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/dc-motor-25-bracket-b.jpg" width="150px;"></a><br>
<p>DC Motor-25 Bracket</p></td>
<td><a href="brackets/dc-motor-37-bracket.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/dc-motor-37-bracket-b-gold_DC-Motor-37-Bracket-B.jpg" width="150px;"></a><br>
<p>DC Motor-37 Bracket</p></td>
</tr>

<tr>
<td><a href="brackets/meds15-servo-motor-bracket.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/mecds15-servo-motor-bracket.jpg" width="150px;"></a><br>
<p>MECDS-150 Servo Bracket</p></td>
<td><a href="brackets/u-bracket-b.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/u-bracket-b.jpg" width="150px;"></a><br>
<p>U Bracket B</p></td>
<td><a href="brackets/u-bracket-c.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/u-bracket-c.jpg" width="150px;"></a><br>
<p>U Bracket C</p></td>
<td><a href="brackets/versatile-motor-bracket.html" target="_blank"><img src="../../zh/mechanical-parts/brackets/images/versatile-motor-bracket.jpg" width="150px;"></a><br>
<p>Versatile Motor Bracket</p></td>
</tr>

</table>