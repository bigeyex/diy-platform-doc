# Sprockets

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="sprockets/04c-10t-sprocket.html" target="_blank"><img src="../../zh/mechanical-parts/sprockets/images/04c-10t-sprocket.jpg" width="150px;"></a><br>
<p>04C 10T Sprocket</p></td>

<td width="25%;"><a href="sprockets/04c-20t-sprocket.html" target="_blank"><img src="../../zh/mechanical-parts/sprockets/images/04c-20t-sprocket.jpg" width="150px;"></a><br>
<p>04C 20T Sprocket</p></td>

<td width="25%;"><a href="sprockets/04c-30t-sprocket.html" target="_blank"><img src="../../zh/mechanical-parts/sprockets/images/04c-30t-sprocket.jpg" width="150px;"></a><br>
<p>04C 30T Sprocket</p></td>

<td width="25%;"><a href="sprockets/04c-roller-chain-1-5m.html" target="_blank"><img src="../../zh/mechanical-parts/sprockets/images/04c-roller-chain-1-5m.jpg" width="150px;"></a><br>
<p>04C Roller Chain-1.5m</p></td>
</tr>
</table>