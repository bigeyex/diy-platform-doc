<img src="../../../zh/mechanical-parts/wheels/images/caster-wheel.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Caster Wheel

### Features

- Made of steel and plastic, excellent strength.
- Be able to rolled in all directions.
- Compatible with Bracket P3.
- Sold in Pair.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/wheels/images/caster-wheel-1.png" style="width:600;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/wheels/images/caster-wheel-2.png" style="width:600;padding:5px 5px 15px 0px;">
