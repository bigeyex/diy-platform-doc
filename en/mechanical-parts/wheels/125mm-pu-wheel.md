<img src="../../../zh/mechanical-parts/wheels/images/125mm-pu-wheel.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 125x24mm Pu Wheel

### Introduction

125mm PU wheel can be used as a driving wheel, a driven wheel or a friction wheel. As a driving or driven wheel, it can bear heavy weight. 

### Features

- Can bear heavy weight.

### Specifications

- Material: polyurethane
- Diameter: 125mm
- Shaft diameter: 8mm

### Package contents

- 1 x 125mm PU wheel
- 1 x Wheel connector
- 1 x M8\*96mm D-shaft
- 1 x 8x22x2 Plain Washer
- 1 x M8 nut
- 2 x M5x5 jbck screw

 
### Examples

<img src="../../../zh/mechanical-parts/wheels/images/125mm-pu-wheel-1.jpg" style="width:400;padding:5px 5px 15px 0px;">
