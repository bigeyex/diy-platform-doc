<img src="../../../zh/mechanical-parts/wheels/images/injection-omnidirectional.png" style="width:400;padding:5px 5px 15px 0px;">

# Injection Omnidirectional Wheel

### Features

- Length: 24mm
- Width: 16mm
- Height: 16mm

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/wheels/images/injection-omnidirectional-2.png" style="width:400;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/wheels/images/injection-omnidirectional-1.jpg" style="width:400;padding:5px 5px 15px 0px;">