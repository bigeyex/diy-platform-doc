# Tyre 68.5\*22mm


![](images/tire-68-5x22mm-4-pack_Tire-68.5x22mm.jpg)

  

**Features:**

-   Made
    of silicon
-   Compatible
    to Timing Pulley 90T
-   Sold
    in pack of 4

**Demo:**

**<img src="images/tire-68-5x22mm-4-pack_87030-demo.jpg" alt="tire.png" width="697" />**

 
