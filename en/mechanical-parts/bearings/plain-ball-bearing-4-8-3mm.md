<img src="../../../zh/mechanical-parts/bearings/images/plain-ball-bearing-4-8-3mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Plain Ball Bearing 4\*8\*3mm

### Description

Makeblock plain ball bearing 4\*8\*3mm
can be used with Makeblock 4mm bearing bracket
A to support heavy mechanisms.