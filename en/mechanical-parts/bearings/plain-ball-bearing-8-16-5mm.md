<img src="../../../zh/mechanical-parts/bearings/images/plain-ball-bearing-8-16-5mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Plain Ball Bearing 8\*16\*5mm

### Description

Makeblock plain ball bearing 8\*16\*5mm
can be used with Makeblock 8mm bearing bracket
A to support heavy mechanisms.

### Features

- Perfect for adding to 8mm shaft and 8mm
bearing bracket A.
- Makes the rotating part virtually
friction-free.
- Diameters: 8mm (inside), 16mm
(outside).

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/bearings/images/plain-ball-bearing-8-16-5mm-1.png" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/bearings/images/plain-ball-bearing-8-16-5mm-2.png" style="width:700;padding:5px 5px 15px 0px;">

 
