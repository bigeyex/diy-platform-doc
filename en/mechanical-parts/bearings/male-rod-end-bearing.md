<img src="../../../zh/mechanical-parts/bearings/images/male-rod-end-bearing.jpg" style="width:400;padding:5px 5px 15px 0px;">


# Male Rod End Bearing


### Description

 

A
rod end bearing, also known as a heim joint (N. America) or rose joint
(U.K. and elsewhere), is a mechanical articulating joint. Such joints
are used on the ends of control rods, steering links, tie rods, or
anywhere a precision articulating joint is required. A ball swivel with
an opening through which a bolt or other attaching hardware may pass is
pressed into a circular casing with a threaded shaft attached.

 

### Features

 

-   With
    a M4 screw hole, compatible with most Makeblock components
-   Designed
    to support high loads, reversing loads, and shock loads. Widely used
    in engraving machines, automation equipments, CNC equipment.
-   Rolling
    bearing seat material : cast iron
-   Rod
    end joint bearing ball head diameter is 4 mm
-   Sold
    in pack of three  
      
    

### Size Charts(mm)

 

<img src="../../../zh/mechanical-parts/bearings/images/male-rod-end-bearing-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

 

### Demo

<img src="../../../zh/mechanical-parts/bearings/images/male-rod-end-bearing-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/bearings/images/male-rod-end-bearing-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/bearings/images/male-rod-end-bearing-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/bearings/images/male-rod-end-bearing-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/bearings/images/male-rod-end-bearing-6.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/bearings/images/male-rod-end-bearing-7.jpg" style="width:300;padding:5px 5px 15px 0px;">

