# Plastic Spacer

### Features

- Material: nylon

## Spacer 4x7x1

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/spacer-4x7x1.jpg" style="width:170px;padding:5px 5px 15px 0px;">

## Spacer 4x7x2

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/spacer-4x7x2.jpg" style="width:170px;padding:5px 5px 15px 0px;">

## Spacer 4x7x10

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/spacer-4x7x10.jpg" style="width:170px;padding:5px 5px 15px 0px;">