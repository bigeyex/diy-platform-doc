# Linear Motion Shaft D4x288mm


![](images/linear-motion-shaft-4x288mm-4-pack_Linear-Motion-Shaft-4x288mm.jpg)

**Features:**

-   Made of stainless Steel
-   Can drill into 4mm holes.
    Compatible with Thread Drive
    Beam
-   Length: 288mm
-   Sold in 4-Pack.

Size
Charts(mm):

 <img src="images/linear-motion-shaft-4x288mm-4-pack_qq-20130925143050.jpg" alt="qq-20130925143050.jpg" width="558" />

 
