<img src="../../../zh/mechanical-parts/shafts/images/threaded-shaft-4x39mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Threaded Shaft 4\*39mm

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/shafts/images/threaded-shaft-4x39mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/shafts/images/threaded-shaft-4x39mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
