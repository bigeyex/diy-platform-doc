# Linear Motion Shaft D4x240mm


![](images/linear-motion-shaft-d4-240mm_Linear-Motion-Shaft-D4x240mm.jpg)

**Features:**

-   Made
    of stainless steel.
-   Can drill into
    4mm holes.
-   Compatible
    with thread drive beam.
-   Length:
    240mm
-   

**Size
Charts(mm):**

 <img src="images/linear-motion-shaft-d4-240mm_85407-size.jpg" alt="85407-size.jpg" width="606" />
