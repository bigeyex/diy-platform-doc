<img src="../../../zh/mechanical-parts/shafts/images/flexible-coupling-4x4mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Flexible coupling 4\*4mm

### Description

 
This is the single spiral groove of flexible coupling made by aluminum.
This design utilizes a single piece of material and becomes flexible by
removal of material along a spiral path resulting in a curved flexible
beam of helical shape.Changes to the lead of the helical beam provide
changes to misalignment capabilities as well as other performance
characteristics such as torque capacity and torsional stiffness. It is
even possible to have multiple starts within the same
helix.                                                

 

### Features

-   Made
    from aluminum.
-   Compatible
    with most 4mm shaft especially D shaft.
-   Size: D9\*Φ4\*Φ4\*M3\*L22mm.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/shafts/images/flexible-coupling-4x4mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">
 

### Demo

<img src="../../../zh/mechanical-parts/shafts/images/flexible-coupling-4x4mm-2.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/shafts/images/flexible-coupling-4x4mm-3.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/shafts/images/flexible-coupling-4x4mm-4.jpg" style="width:400;padding:5px 5px 15px 0px;">
