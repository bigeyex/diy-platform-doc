<img src="../../../zh/mechanical-parts/shafts/images/shaft-collar-4mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Shaft Collar 4mm 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/shafts/images/shaft-collar-4mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/shafts/images/shaft-collar-4mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
