<img src="../../../zh/mechanical-parts/shafts/images/shaft-collar-8mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Shaft Collar 8mm 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/shafts/images/shaft-collar-8mm-0.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/shafts/images/shaft-collar-8mm-1.png" style="width:700;padding:5px 5px 15px 0px;">


<img src="../../../zh/mechanical-parts/shafts/images/shaft-collar-8mm-2.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/shafts/images/shaft-collar-8mm-3.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/shafts/images/shaft-collar-8mm-4.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/shafts/images/shaft-collar-8mm-5.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/shafts/images/shaft-collar-8mm-6.jpg" style="width:400;padding:5px 5px 15px 0px;">

 

 
