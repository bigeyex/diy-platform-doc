<img src="../../../zh/mechanical-parts/brackets/images/dc-motor-37-bracket.jpg" style="width:400;padding:5px 5px 15px 0px;">

# DC Motor-37 Bracket

### Features

-   Made
    from 6061 aluminum, 3mm thick, anodized surface.
-   Compatible
    to 37mm DC Motor.
-   Sold
    in Pair.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/brackets/images/dc-motor-37-bracket-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/brackets/images/dc-motor-37-bracket-2.jpg" style="width:700;padding:5px 5px 15px 0px;">

