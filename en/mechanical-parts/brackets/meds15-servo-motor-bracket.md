<img src="../../../zh/mechanical-parts/brackets/images/mecds15-servo-motor-bracket.jpg" style="width:400;padding:5px 5px 15px 0px;">

# MECDS-150 Servo Bracket

### Features

-   Made
    from 6061 aluminum extrusion , 2mm thick, anodized surface.

-   With
    holes on 8mm increments,  can be drilled for 4mm hardware.

-   Compatible
    to MEDS150 Servo. 4 M4x8 Screw included.

-   Sold
    in Pair.


### Size Charts(mm)

<img src="../../../zh/mechanical-parts/brackets/images/mecds15-servo-motor-bracket-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/brackets/images/mecds15-servo-motor-bracket-2.jpg" style="width:700;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/mecds15-servo-motor-bracket-3.jpg" style="width:700;padding:5px 5px 15px 0px;">