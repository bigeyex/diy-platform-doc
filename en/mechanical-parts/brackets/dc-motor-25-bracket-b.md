<img src="../../../zh/mechanical-parts/brackets/images/dc-motor-25-bracket-b.jpg" style="width:400;padding:5px 5px 15px 0px;">

# DC Motor-25 Bracket

### Description

DC
Motor-25 Bracket B-Blue(Pair) is the update version of DC Motor-25
Bracket -Blue(Pair). 

 

### Features

-   Made
    from 6061 aluminum, 3mm thick, anodized surface.

-   Compatible
    to 37mm DC Motor.

-   Sold
    in Pair.


### Size chart(mm)

<img src="../../../zh/mechanical-parts/brackets/images/dc-motor-25-bracket-b-1.png" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/brackets/images/dc-motor-25-bracket-b-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/dc-motor-25-bracket-b-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/dc-motor-25-bracket-b-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/dc-motor-25-bracket-b-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

 
