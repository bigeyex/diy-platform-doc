<img src="../../../zh/mechanical-parts/brackets/images/bracket-3x3.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Bracket 3\*3

### Features

-   Made
    from 6061 aluminum, 2mm thick, anodized surface.
-   With
    holes on 8mm increments, can be drilled for 4mm hardware.
-   Can
    be used to build rectangular structure. 
-   Sold
    in pack of 4.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/brackets/images/bracket-3x3-1.png" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/brackets/images/bracket-3x3-2.png" style="width:600;padding:5px 5px 15px 0px;">

 
