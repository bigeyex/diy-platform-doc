<img src="../../../zh/mechanical-parts/brackets/images/57byg-stepper-motor-bracket.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 57BYG Stepper Motor Bracket Pack-Black

### Description

Makeblock
57BYG Stepper Motor Braket Pack-Black is usually used as the structural
support or connection point for 57BYG Stepper
Motor. 

### Features

-  High strength and high temperature resistance  

-  Easy-to-use 57BYG Stepper Motor 

-  Senior black coating surface for a long time service  


-  Compatible with Makeblock components

### Size charts(mm)

<img src="../../../zh/mechanical-parts/brackets/images/57byg-stepper-motor-bracket-1.png" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/brackets/images/57byg-stepper-motor-bracket-2.jpg" style="width:300;padding:5px 5px 15px 0px;">


<img src="../../../zh/mechanical-parts/brackets/images/57byg-stepper-motor-bracket-3.png" style="width:300;padding:5px 5px 15px 0px;">


<img src="../../../zh/mechanical-parts/brackets/images/57byg-stepper-motor-bracket-4.png" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/57byg-stepper-motor-bracket-5.png" style="width:300;padding:5px 5px 15px 0px;">


 
