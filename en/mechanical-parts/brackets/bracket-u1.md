<img src="../../../zh/mechanical-parts/brackets/images/bracket-u1.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Bracket U1

### Description

Bracket
U1 is usually used as structural support or connection points
for servos,  motors, and shafts .

 

### Features

-   With
    three
    8mm center holes that can be attached with 8mm shaft or
    bearings
-   24
     4mm
    holes which is compatible with most Makeblock components
-   Aluminum
    extrusion(high strength) , 3mm thick, anodized surface(long time to
    shelve with barely rusting)

 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/brackets/images/bracket-u1-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/brackets/images/bracket-u1-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/bracket-u1-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/bracket-u1-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/bracket-u1-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/bracket-u1-6.jpg" style="width:300;padding:5px 5px 15px 0px;">
