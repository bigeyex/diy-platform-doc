<img src="../../../zh/mechanical-parts/brackets/images/42byg-stepper-motor-bracket-b.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 42BYG Stepper Motor Bracket B-Blue

### Description

42BYG
Stepper Motor Bracket B has the same function as 42BYG Stepper Motor
Bracket. It comes with a width of 40mm to adapt to some special
scenarios. This 42BYG
Stepper Motor Bracket B is Blue.
 
### Features

-   Made
    from 6061 aluminum extrusion , 3mm thick, anodized surface.
-   With
    four taped holes for mounting your stepper motor and M4 through
    holes for attachment
-   Compatible
    to Step Motor. 4 Screw M3x8 included.

### SizeChart(mm)
 
<img src="../../../zh/mechanical-parts/brackets/images/42byg-stepper-motor-bracket-b-1.png" style="width:700;padding:5px 5px 15px 0px;">
