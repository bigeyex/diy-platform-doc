<img src="../../../zh/mechanical-parts/brackets/images/36mm-motor-bracket.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 36mm Motor Bracket

36mm
Motor Bracket is compatible with 36mm DC Motor. The U-Shape give it
higher strength than the Versatile Motor
Bracket.

### Features

-   Compatible
    with 36mm Motor.
-   With
    aluminum alloy material, it can vertically bear high
    pressure.

 

### Specifications

-   Material:
    Aluminum alloy 
-   Thickness:
    3 mm 
-   Length:
    30 mm 
-   Width:
    24 mm 
-   Height:
    60 mm

### Size Chart

<img src="../../../zh/mechanical-parts/brackets/images/36mm-motor-bracket-1.jpg" style="width:800;padding:5px 5px 15px 0px;">

### Examples

<img src="../../../zh/mechanical-parts/brackets/images/36mm-motor-bracket-2.jpg" style="width:600;padding:5px 5px 15px 0px;">
