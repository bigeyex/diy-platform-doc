<img src="../../../zh/mechanical-parts/brackets/images/base-bracket.jpg" style="padding:5px 5px 15px 0px;">

# Base Bracket B

### Description  

Makeblock
Base Bracket is a kind of multi-purpose bracket which can use for
Arduino, Meduino, and even Raspberry Pi.

Base
Bracket B is bigger than Base Bracket A and has more holes to connect
with electronic boards and mechanical parts. Additionally, the two
biggest holes in the bracket will allow the RJ25 cables to go
through.

Plastic
Rivet 3075, Plastic Rivet 4060 and Plastic Rivet 4100 are
included.

Users
can choose Plastic Rivet3075  to connect Base Bracket with Arduino or
Meduino, choose Plastic Rivet 4060 to connect Base Bracket with Battery
Holder, and choose Plastic Rivet 4100 to connect Base Bracket with Base
Board. 

 

### Features

- Made of transparent Acrylic, light and nice looking
- Compatible with Meduino, Me BaseBoard, Battery Holder, and Raspberry Pi.
- 3 kinds of Plastic Rivet are included.

 

### Size Chart

<img src="../../../zh/mechanical-parts/brackets/images/base-bracket-1.jpg" style="width:800;padding:5px 5px 15px 0px;"> 

### Demo

<img src="../../../zh/mechanical-parts/brackets/images/base-bracket-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

 <img src="../../../zh/mechanical-parts/brackets/images/base-bracket-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

 <img src="../../../zh/mechanical-parts/brackets/images/base-bracket-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

 <img src="../../../zh/mechanical-parts/brackets/images/base-bracket-5.jpg" style="width:300;padding:5px 5px 15px 0px;">
 

 

 
