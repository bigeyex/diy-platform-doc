<img src="../../../zh/mechanical-parts/brackets/images/bracket-p3.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Bracket P3

### Features

-   Made
    from 6061 aluminum extrusion ,  anodized surface.
-   With
    holes on 8mm increments,  can be drilled for 4mm hardware.
-   Compatible with makeblock caster
    wheel.
-   Sold
    in Pair.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/brackets/images/bracket-p3-1.png" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/brackets/images/bracket-p3-2.png" style="width:700;padding:5px 5px 15px 0px;">
