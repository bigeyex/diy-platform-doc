<img src="../../../zh/mechanical-parts/brackets/images/8mm-bearing-bracket-a.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 8mm Bearing Bracket A

### Description

8mm
bearing bracket A has a ball bearing for 8mm shafts, it is usually used
as the bracket of 8mm shafts to support heavy mechanisms. With some M4
tapped holes and though holes, you can easily attach it to Makeblock
mechanical parts. Bearing 8\*16\*5 is included.


### Size Charts(mm)

<img src="../../../zh/mechanical-parts/brackets/images/8mm-bearing-bracket-a-1.jpg" style="width:800;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/brackets/images/8mm-bearing-bracket-a-2.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/8mm-bearing-bracket-a-3.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/8mm-bearing-bracket-a-4.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/8mm-bearing-bracket-a-5.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/8mm-bearing-bracket-a-6.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/8mm-bearing-bracket-a-7.jpg" style="width:400;padding:5px 5px 15px 0px;">
 

 

 

 
