# Fasteners and Gaskets

### [Brass Stud](fasteners-gaskets/brass-stud.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="fasteners-gaskets/brass-stud-m4x8-6.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/brass-stud-m4×8-6.jpg" width="150px;"></a><br>
<p>Brass Stud M4x8+6</p></td>

<td width="25%;"><a href="fasteners-gaskets/brass-stud-m4x12-6.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/brass-stud-m4×12-6.jpg" width="150px;"></a><br>
<p>Brass Stud M4x12+6</p></td>

<td width="25%;"><a href="fasteners-gaskets/brass-stud-m4x16.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/brass-stud-m4×16.jpg" width="150px;"></a><br>
<p>Brass Stud M4x16</p></td>

<td width="25%;"><a href="fasteners-gaskets/brass-stud-m4x20.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/brass-stud-m4×20.jpg" width="150px;"></a><br>
<p>Brass Stud M4x20</p></td>
</tr>

<tr>
<td><a href="fasteners-gaskets/brass-stud-m4x32.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/brass-stud-m4×32.jpg" width="150px;"></a><br>
<p>Brass Stud M4x32</p></td>
</tr>
</table>

### [Nut](fasteners-gaskets/nut.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="50%;"><a href="fasteners-gaskets/nut-4mm.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/nut-4mm.jpg" width="150px;"></a><br>
<p>Nut 4mm</p></td>

<td width="50%;"><a href="fasteners-gaskets/nylon-lock-nut-4mm.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/nylon-lock-nut-4mm.jpg" width="150px;"></a><br>
<p>Nylon Lock Nut 4mm</p></td>
</tr>
</table>

### [Rivet](fasteners-gaskets/rivet.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="fasteners-gaskets/rivet-r4060.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/rivet-r4060.jpg" width="150px;"></a><br>
<p>Rivet R4060</p></td>

<td width="25%;"><a href="fasteners-gaskets/rivet-r4100.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/rivet-r4100.jpg" width="150px;"></a><br>
<p>Rivet R4100</p></td>

<td width="25%;"><a href="fasteners-gaskets/rivet-r4120.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/rivet-r4120.jpg" width="150px;"></a><br>
<p>Rivet R4120</p></td>

</tr>
</table>

### Socket Cap Screw

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td><a href="fasteners-gaskets/socket-cap-screw.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/socket-cap-screw-m4x8.jpg" width="150px;"></a></td>
</tr>
</table>

### Plastic Spacer

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td><a href="fasteners-gaskets/plastic-spacer.html" target="_blank"><img src="../../zh/mechanical-parts/fasteners-gaskets/images/spacer-4x7x1.jpg" width="150px;"></a></td>
</tr>
</table>
