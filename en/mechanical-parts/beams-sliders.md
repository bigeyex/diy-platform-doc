# Beams & Sliders

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="beam0824.html" target="_blank"><img src="../../zh/mechanical-parts/beam0824/images/beam0824.jpg" width="150px;"></a><br>
<p>Beam 0824</p></td>

<td width="25%;"><a href="beam0808.html" target="_blank"><img src="../../zh/mechanical-parts/beam0808/images/beam0808-072.jpg" width="150px;"></a><br>
<p>Beam 0808</p></td>

<td width="25%;"><a href="beam2424.html" target="_blank"><img src="../../zh/mechanical-parts/beam2424/images/beam2424.jpg" width="150px;"></a><br>
<p>Beam 2424</p></td>

<td width="25%;"><a href="beam0412.html" target="_blank"><img src="../../zh/mechanical-parts/beam0412/images/beam0412.jpg" width="150px;"></a><br>
<p>Beam 0412</p></td>
</tr>

<tr>
<td><a href="square-beam.html" target="_blank"><img src="../../zh/mechanical-parts/square-beam/square-beam.jpg" width="150px;"></a><br>
<p>Square Beam</p></td>
<td><a href="slider-v-slot/slider256.html" target="_blank"><img src="../../zh/mechanical-parts/slider-v-slot/slider256.jpg" width="150px;"></a><br>
<p>Slider256</p></td>
<td><a href="slider-v-slot/slider496.html" target="_blank"><img src="../../zh/mechanical-parts/slider-v-slot/slider496.jpg" width="150px;"></a><br>
<p>Slider496</p></td>
</tr>

</table>