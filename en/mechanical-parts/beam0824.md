<img src="../../zh/mechanical-parts/beam0824/images/beam0824-0.jpg" style="width:600;padding:5px 5px 15px 0px;">

# Beam 0824

### Description

Makeblock Beam0824 is one of the most frequently used part in Makeblock platform. It is compatible with most Makeblock motion and structure
components.

### Specifications

- Length: 16 - 504mm
- Size (mm): 016, 032, 048, 064, 080, 096, 112, 128, 144, 160, 176, 192, 224, 256, 320, 336, 400, 496, 504
- Cross-section area: 8 x 24mm
- Material: 6061 aluminum extrusion

### Features

- Excellent strength
- Twist resistance
- Easy and flexible connection

### Instructions

### Demo

<img src="../../zh/mechanical-parts/beam0824/images/beam0824-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

### Size Chart

<table style="text-align:center;cellpadding:12px;cellspacing:10px;">

<tr>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-016-1.png" width="170px;"><br><p>Beam0824-016-Blue</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-032.jpg" width="170px;"><br><p>Beam0824-032</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-048.jpg" width="170px;"><br><p>Beam0824-048</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-064.jpg" width="170px;"><br><p>Beam0824-064</p></td>
</tr>

<tr>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-080.jpg" width="170px;"><br><p>Beam0824-080</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-096.jpg" width="170px;"><br><p>Beam0824-096</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-112.jpg" width="170px;"><br><p>Beam0824-112</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-128.jpg" width="170px;"><br><p>Beam0824-128</p></td>
</tr>

<tr>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-144.jpg" width="170px;"><br><p>Beam0824-144</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-160.jpg" width="170px;"><br><p>Beam0824-160</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-176.jpg" width="170px;"><br><p>Beam0824-176</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-192.jpg" width="170px;"><br><p>Beam0824-192</p></td>
</tr>

<tr>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-224.png" width="170px;"><br><p>Beam0824-224</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-256.jpg" width="170px;"><br><p>Beam0824-256</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-320.jpg" width="170px;"><br><p>Beam0824-320</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-336.jpg" width="170px;"><br><p>Beam0824-336</p></td>
</tr>

<tr>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-400.jpg" width="170px;"><br><p>Beam0824-400</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-496.png" width="170px;"><br><p>Beam0824-496</p></td>
<td><img src="../../zh/mechanical-parts/beam0824/images/beam0824-504.png" width="170px;"><br><p>Beam0824-504</p></td>
</tr>

</table>