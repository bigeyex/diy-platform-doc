<img src="../../../zh/mechanical-parts/sprockets/images/04c-30t-sprocket.jpg" style="padding:5px 5px 15px 0px;">

# 04C 30T Sprocket

The
chain can be used together with the sprocket for transmission of motion
and power. Compared with synchronous belt, the chain can transmit more
force and drive large load.  

 

### Features

-   It
    is made of integrated steel, capable of bearing large
    strength.
-   Through
    M5x5 of jackbolt, it can firmly combine with 8mm of D-shaped
    shaft. 
-   By
    surface blackening, it can prevent rusting and increasing
    rigidity.

 

### Specifications

-   Type:
    Shaft
-   Material:
    Steel 
-   Inner
    diameter: 8 mm 