# Wheels

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td><a href="wheels/mecanum-wheel.html" target="_blank"><img src="../../zh/mechanical-parts/wheels/images/mecanum-2.png" width="150px;"></a><br>
<p>Mecanum Wheel</p></td>

<td><a href="wheels/125mm-pu-wheel.html" target="_blank"><img src="../../zh/mechanical-parts/wheels/images/125mm-pu-wheel.jpg" width="150px;"></a><br>
<p>125*24mm Pu Wheel</p></td>

<td><a href="wheels/caster-wheel-pair.html" target="_blank"><img src="../../zh/mechanical-parts/wheels/images/caster-wheel.jpg" width="150px;"></a><br>
<p>Caster Wheel</p></td>

<td><a href="wheels/injection-omnidirectional-wheel.html" target="_blank"><img src="../../zh/mechanical-parts/wheels/images/injection-omnidirectional.png" width="150px;"></a><br>
<p>Injection<br>Omni-directional<br>Wheel</p></td>
</tr>

<tr>
<td><a href="wheels/nylon-pulley-with-bearing.html" target="_blank"><img src="../../zh/mechanical-parts/wheels/images/nylon-pulley.jpg" width="150px;"></a><br>
<p>Nylon Pulley</p></td>
<td><a href="wheels/slick-tyre-64-16mm.html" target="_blank"><img src="../../zh/mechanical-parts/wheels/images/slick-tyre.jpg" width="150px;"></a><br>
<p>Tire</p></td>
<td><a href="wheels/tire-68-5x22mm.html" target="_blank"><img src="../../zh/mechanical-parts/wheels/images/tyre-68-5.jpg" width="150px;"></a><br>
<p>Tyre 68.5*22mm</p></td>
<td><a href="wheels/track-with-track-axle.html" target="_blank"><img src="../../zh/mechanical-parts/wheels/images/track-with-track-axle.jpg" width="150px;"></a><br>
<p>Track with Track Axle</p></td>
</tr>

<tr>
<td><a href="wheels/track.html" target="_blank"><img src="../../zh/mechanical-parts/wheels/images/track.png" width="150px;"></a><br>
<p>Track</p></td>
</tr>

</table>