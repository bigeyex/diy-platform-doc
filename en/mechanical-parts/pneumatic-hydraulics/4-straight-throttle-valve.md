# φ4 Straight Throttle Valve

![](../../../zh/mechanical-parts/pneumatic-hydraulics/images/φ4-Straight-Throttle-Valve.jpg)

### Description

Makeblock φ4 straight throttle valve is
a flow control valve of straight-through type which is used for reducing and controlling the flow rate, regulated by hand and can be locked and closed completely.

 

### Features

- Straight throttling.
- Controlled by manual knob.
- Air pipe is connected with a quick plug connector.

### Dimension

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/4-straight-throttle-valve-2-pack_4-dimension.png" alt="4-dimension.png" width="800" />

