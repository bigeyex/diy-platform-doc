# DC Frame Type Solenoid HCNE1-0530

![](images/dc-frame-type-solenoid-hcne1-0530_DC-Frame-Type-Solenoid-HCNE1-0530.jpg)

Open
Frame Solenoids are a basic, opened-coil type of linear motion
solenoids. Due to their simple design structure, they are less costly,
and are offered in a wide variety of sizes and types to best meet your
application requirements.

**Technical data:**

Voltage:12VDC

Resistance:6.6 ohms

Stroke:6mm

Force:50g

ED:25%

 

**Application range:**  
1.Game  
Tiger eating angles Coin chooser  
2.Salar machine  
Saler machine coil changer ticket saler  
3.Office equipment  
listing machine computer fax duty printer  
Photo printer type writer caser drawing machine drinking  
4.Transport devices  
auto door lock safe belt lock car electromagnetic valve  
5.Domestic appliance  
tadioelectronic organ auto weave machine choir  
6.Other  
package machine mechtval hand farming device pressure equipment fir
prevention,burglary alarm save water valve

 

**Characteristics:**

Easy
fix and continuous loading  
Stable temperature rising can length usage lift and sure good
character  
E-ring and rubber pad make valve no loice  
No rub length usage lift  
Easy and stable construre  
Normal type

  
\***Operation condition**  
Operation temperature-5 to 40,the valve will not solidfication  
Operation humiditynot solidify within the realtive humidity of 45% to
85%  
Store temperaturenot solidify at the temperature of -40 to 75  
Storage humiditynot solidify during the relative humidity of 0% to
95%

  
**\*Character note**  
Temperature rising under 65(class a) lift: more than 1 million under
loading

  
**\*Test
temperature**  
Environment
temperature23±2  
Realative
humidity50±10%  
Air
pressure1013MPa  
Insulation
resistanceneed 100MΩ insulating resistance when 500VDC insulator
testing  
Insulation
strengththe strength should be 600VAC/1 min

 

**Size
Charts(mm):**

 

**<img src="images/dc-frame-type-solenoid-hcne1-0530_519543897-5401.jpg" alt="519543897-5401.jpg" width="673" />**

 

 
