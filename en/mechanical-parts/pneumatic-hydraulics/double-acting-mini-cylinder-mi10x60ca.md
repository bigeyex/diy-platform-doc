# Double Acting Mini Cylinder Pack MI10X60CA

![](../../../zh/mechanical-parts/pneumatic-hydraulics/images/makeblock-double-acting-mini-cylinder-pack-mi10x60ca_Makeblock-Double-Acting-Mini-Cylinder-Pack-MI10X60CA.jpg)

### Description

It is a mini cylinder of double acting type with a bore size of 10mm and stroke of 60mm. The installation methods are panel installation,double bracket installation, tail stock
installation, etc. and the mounting nut is M12X1.25. It is used with
M5-φ4 quick plug connector and its theoretical thrust is 39.3N and the
tension 33N under the pressure of 0.5MPa.

 

**Features:**

- Mini & small and easy to install.
- Double acting type.
- The theoretical thrust is 39.3N and the tension 33N under the pressure of 0.5MPa.
- Used with M5-φ4 quick plug connector.

 
**Size Charts(mm):**

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/makeblock-double-acting-mini-cylinder-pack-mi10x60ca_mi10x60casdb-0-1013.png" alt="mi10x60casdb-0-1013.png" width="800" />
