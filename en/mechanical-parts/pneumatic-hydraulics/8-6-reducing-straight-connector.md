# φ8 – φ6 Reducing Straight Connector

![](../../../zh/mechanical-parts/pneumatic-hydraulics/images/φ8---φ6-Reducing-Straight-Connector.jpg)

### Description

Makeblock φ8 - φ6 reducing straight
connector's one end is used with φ8 air pipe and the other end with φ6 air pipe. The channel is of straight-through type and reduces the φ8 air
pipe to φ6 air pipe.

### Features

- Reduce the φ8 air pipe to φ6 air pipe.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/8-6-reducing-straight-connector-4-pack_59011.png" alt="59011.png" width="675" />

### Connection Illustration

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/8-6-reducing-straight-connector-4-pack_1329.jpg" alt="1329.jpg" width="700" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/8-6-reducing-straight-connector-4-pack_59011-connector.jpg" alt="59011-connector.jpg" width="700" />

 
