# φ4 Elbow Connector

![](../../../zh/mechanical-parts/pneumatic-hydraulics/images/φ4-Elbow-Connector.jpg)

### Description

It is used with φ4 air pipe and M5 threaded elbow connector and can rotate 360° and make the installation and arrangement of air pipes simple and pleasing.

 
### Features

- The air circuit turns 90°.
- The air pipe can rotate 360° freely.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/4-elbow-connector-4-pack_59010.png" alt="59010.png" width="587" />

### Connection Illustration

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/4-elbow-connector-4-pack_59050-connector.jpg" alt="59050-connector.jpg" width="700" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/4-elbow-connector-4-pack_59010connector2.jpg" alt="59010connector2.jpg" width="700" />

 
