# Single Acting Mini Cylinder Pack MSI8X20UFA

![](../../../zh/mechanical-parts/pneumatic-hydraulics/images/single-acting-mini-cylinder-pack-msi8x20ufa_Single-Acting-Mini-Cylinder-Pack-MSI8X20UFA.jpg)

### Description

It is a mini cylinder of single acting
extrusion type with a bore size of 8mm and stroke of 20mm. The
installation method is panel installation and the mounting nut is
M12X1.25. It is equipped with a exhaust silencer, used with M5-φ4 quick
plug connector and its theoretical thrust is 18.6N under the pressure of
0.5MPa.


### Features

- Mini & small and easy to install.
- Single acting extrusion type and reset by a reset spring.
- The theoretical thrust is 18.6N under the pressure of 0.5MPa.
- Used with M5-φ4 quick plug connector.

 
### Size Charts(mm)

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/single-acting-mini-cylinder-pack-msi8x20ufa_msi8x20ufa-1013.png" alt="msi8x20ufa-1013.png" width="800" />

### Connection Illustration

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/single-acting-mini-cylinder-pack-msi8x20ufa_1128.jpg" alt="1128.jpg" width="700" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/single-acting-mini-cylinder-pack-msi8x20ufa_1163.jpg" alt="1163.jpg" width="700" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/single-acting-mini-cylinder-pack-msi8x20ufa_single.jpg" alt="single.jpg" width="700" /><img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/single-acting-mini-cylinder-pack-msi8x20ufa_single-acting-cylinder.jpg" alt="single-acting-cylinder.jpg" width="700" />
