# Vacuum Suction Cup – SP-30

![](../../../zh/mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-sp-30_Vacuum-Suction-Cup---SP-30.jpg)

**Description:**

Makeblock vacuum suction cup - SP-30 is made from silicon, it is great for attaching things to a windshield, mirror, window, or any other clean glass or non-painted metal surface. 

Vacuum suction cup - SP-30 can be assemble with Makeblock vacuum suction cup connector holder to better
use.

### Features

- Material: silicon
- Max Diameter: 30mm
- Height: 17mm
- Flexible to connect Makeblock vacuum suction cup connector holder
- Vacuum for high adsorption

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-sp-30_59000-size-1.png" alt="59000-size-1.png" width="599" />

### Demo

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-sp-30_59000-demo.png" alt="59000-demo.png" width="660" />  


**<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-sp-30_50000-50001-59000-59001-59002-59003-59004-off.jpg" width="868" />**

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-sp-30_50000-50001-59000-59001-59002-59003-59004-on.jpg" width="868" />

 
