# Solenoid Valve DC 12V/0520E

![](../../../zh/mechanical-parts/pneumatic-hydraulics/images/solenoid-valve-dc-12v-0520e_Solenoid-Valve-DC-12V-0520E.jpg)

### Description

Makeblock solenoid valve DC 12V/0520E has a mini body, and is widely used in industry device and DIY
projects.

 
### Specification

- Rated Voltage: DC 12V
- Load: Air
- Current (With load): Less than 240mA
- Pattern: Two position, three -way 
- Total Size : 34 x 21mm
- Maximum pressure: More than 300mmHg
- Insulation class: A

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/solenoid-valve-dc-12v-0520e_59001-size.png" alt="59001-size.png" width="729" />
