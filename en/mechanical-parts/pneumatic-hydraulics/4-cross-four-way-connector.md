# φ4 Cross Four-Way Connector

![](../../../zh/mechanical-parts/pneumatic-hydraulics/images/φ4-Cross-Four-Way-Connector.jpg)

### Description

Makeblock φ4 Cross Four-Way Connector is a cross-shaped four-way connector with a diversion
function, cooperated with control elements in pipeline system and used with φ4 air pipe.

### Features

- Used with φ4 air pipe.
- Cross-shaped four-way and even division.

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/5639d68d35809.png" alt="5639d68d35809.png" width="700" />

### Connection Illustration

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/1317.jpg" alt="1317.jpg" width="700" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/4-cross-four-way-connector-4-pack_59013-connector.jpg" alt="59013-connector.jpg" width="700" />
