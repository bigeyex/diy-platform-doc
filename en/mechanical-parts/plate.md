# Plates

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="plates/disc-d72.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/disc-d72.jpg" width="150px;"></a><br>
<p>Disc D72</p></td>

<td width="25%;"><a href="plates/linear-motion-block-bracket.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/linear-motion-block-bracket-a.jpg" width="150px;"></a><br>
<p>Linear Motion Block Bracket A</p></td>

<td width="25%;"><a href="plates/plate-3x6.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/plate-3x6.jpg" width="150px;"></a><br>
<p>Plate 3*6</p></td>

<td width="25%;"><a href="plates/plate-7-9-b.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/plate-7-9-b.jpg" width="150px;"></a><br>
<p>Plate 7*9-B</p></td>
</tr>

<tr>
<td><a href="plates/plate-45.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/plate-45.jpg" width="150px;"></a><br>
<p>Plate 45°</p></td>
<td><a href="plates/plate-135.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/plate-135.jpg" width="150px;"></a><br>
<p>Plate 135°</p></td>
<td><a href="plates/plate-i1.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/plate-i1.jpg" width="150px;"></a><br>
<p>Plate I1</p></td>
<td><a href="plates/plate-o1.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/plate-o1.jpg" width="150px;"></a><br>
<p>Plate O1</p></td>
</tr>

<tr>
<td><a href="plates/shaft-clamping-hub-8mm.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/shaft-clamping-8mm.jpg" width="150px;"></a><br>
<p>Shaft Clamping Hub 8mm</p></td>
<td><a href="plates/triangle-plate-6x8.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/triangle-plate-6x8.jpg" width="150px;"></a><br>
<p>Triangle Plate 6*8</p></td>
<td><a href="plates/cross-plate.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/cross-plate.jpg" width="150px;"></a><br>
<p>Cross Plate</p></td>
<td><a href="plates/t-plate.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/t-plate.jpg" width="150px;"></a><br>
<p>T Plate</p></td>
</tr>

<tr>
<td><a href="plates/plate-0324-184.html" target="_blank"><img src="../../zh/mechanical-parts/plates/images/plate-0324-184.jpg" width="150px;"></a><br>
<p>Plate 0324-184</p></td>
</tr>

</table>