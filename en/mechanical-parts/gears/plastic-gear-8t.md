<img src="../../../zh/mechanical-parts/gears/images/plastic-gear-8t.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# Plastic Gear 8T

### Description

Gear 8T is designed for transmitting motion from a motor to a wheel or shaft.

### Features

- Teeth: 8
- Pressure angle: α=20°
- Material: nylon