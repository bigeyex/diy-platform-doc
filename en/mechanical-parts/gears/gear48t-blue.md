<img src="../../../zh/mechanical-parts/gears/images/gear-48t.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# Gear 48T

### Description

Gear 48T is designed for transmitting motion from a motor to a wheel or shaft. Usually it is used with shaft connector or flange bearing 4\*8\*3.
  
### Features

- 48 teeth
- With 8mm diameter hub compatible with shaft connector or bearings

### Size chart

<img src="../../../zh/mechanical-parts/gears/images/gear48t-blue_83420-sizechart.jpg" alt="83420-sizechart.jpg" width="760" />

### Note

This product contains functional sharp points on components, please be careful to use it.
