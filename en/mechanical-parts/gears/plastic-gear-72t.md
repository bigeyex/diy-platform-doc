<img src="../../../zh/mechanical-parts/gears/images/plastic-gear-72t.png" style="width:400px;padding:5px 5px 15px 0px;">

# Plastic Gear 72T

### Description

Gear 72T is designed for transmitting motion from a motor to a wheel or shaft.

### Features

- Teeth: 72
- Pressure angle: α=20°
- Material: nylon