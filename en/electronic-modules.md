# Electronic Modules

### [Main Control Boards](electronic-modules/main-control-board.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%"><a href="electronic-modules/main-control-boards/makeblock-orion.html" target="_blank"><img src="electronic-modules/main-control-boards/images/makeblock-orion_makeblock_orion.jpg" width="150px;"></a><br>
<p>Makeblock Orion</p></td>

<td width="33%"><a href="electronic-modules/main-control-boards/mcore.html" target="_blank"><img src="electronic-modules/main-control-boards/images/mcore_mCore.jpg" width="150px;"></a><br>
<p>mCore – Main Control Board for mBot</p></td>

<td width="33%"><a href="electronic-modules/main-control-boards/megapi.html" target="_blank"><img src="electronic-modules/main-control-boards/images/megapi_MegaPi.jpg" width="150px;"></a><br>
<p>MegaPi</p></td>
</tr>
</table>

### [Motor Drivers](electronic-modules/motor-driver.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="electronic-modules/motor-drivers/2h-microstep-driver.html" target="_blank"><img src="electronic-modules/motor-drivers/images/2h-microstep-driver_Me-2H-Microstep-Driver.jpg" width="150px;"></a><br>
<p>Me 2H Microstep Driver</p></td>

<td width="25%;"><a href="electronic-modules/motor-drivers/me-130-dc-motor.html" target="_blank"><img src="electronic-modules/motor-drivers/images/me-130-dc-motor_Me-130-DC-Motor.jpg" width="150px;"></a><br>
<p>Me 130 DC Motor</p></td>

<td width="25%;"><a href="electronic-modules/motor-drivers/me-dual-motor-driver.html" target="_blank"><img src="electronic-modules/motor-drivers/images/me-dual-motor-driver_Me-Dual-DC-Motor-Driver.jpg" width="150px;"></a><br>
<p>Me Dual DC Motor Driver</p></td>

<td width="25%;"><a href="electronic-modules/motor-drivers/me-encoder-motor-driver.html" target="_blank"><img src="electronic-modules/motor-drivers/images/me-encoder-motor-driver_encoderimage3.png" width="150px;"></a><br>
<p>Me Encoder Motor Driver</p></td>
</tr>

<tr>
<td><a href="electronic-modules/motor-drivers/me-stepper-driver.html" target="_blank"><img src="electronic-modules/motor-drivers/images/me-stepper-driver_Me-Stepper-Motor-Driver.jpg" width="150px;"></a><br>
<p>Me Stepper Motor Driver</p></td>
</tr>
</table>


### [Sensors](electronic-modules/sensor.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size:12px;text-align:center;">
<tr>
<td width="25%;"><a href="electronic-modules/sensors/me-3-axis-accelerometer-and-gyro-sensor.html" target="_blank"><img src="electronic-modules/sensors/images/me-3-axis-accelerometer-and-gyro-sensor_Me-3-Axis-Accelerometer-and-Gyro-Sensor.jpg" width="150px;"></a><br>
<p>Me 3-Axis Accelerometer and Gyro Sensor</p></td>

<td width="25%;"><a href="electronic-modules/sensors/me-flame-sensor.html" target="_blank"><img src="electronic-modules/sensors/images/me-flame-sensor_Me-Flame-Sensor.jpg" width="150px;"></a><br>
<p>Me Flame Sensor</p></td>

<td width="25%;"><a href="electronic-modules/sensors/me-gas-sensormq2.html" target="_blank"><img src="electronic-modules/sensors/images/me-gas-sensormq2_Me-Gas-Sensor.jpg" width="150px;"></a><br>
<p>Me Gas Sensor</p></td>

<td width="25%;"><a href="electronic-modules/sensors/me-light-sensor.html" target="_blank"><img src="electronic-modules/sensors/images/me-light-sensor_Me-Light-Sensor.jpg" width="150px;"></a><br>
<p>Me Light Sensor</p></td>
</tr>

<tr>
<td><a href="electronic-modules/sensors/me-line-follower.html" target="_blank"><img src="electronic-modules/sensors/images/me-line-follower_Me-Line-Follower.jpg" width="150px;"></a><br>
<p>Me Line Follower</p></td>
<td><a href="electronic-modules/sensors/me-micro-switch-ab.html" target="_blank"><img src="electronic-modules/sensors/images/me-micro-switch-ab_Me-Micro-Switch-A.jpg" width="150px;"></a><br>
<p>Me Micro Switch A</p></td>
<td><a href="electronic-modules/sensors/me-pir-motion-sensor.html" target="_blank"><img src="electronic-modules/sensors/images/me-pir-motion-sensor_Me-PIR-Motion-Sensor.jpg" width="150px;"></a><br>
<p>Me PIR Motion Sensor</p></td>
<td><a href="electronic-modules/sensors/me-sound-sensor.html" target="_blank"><img src="electronic-modules/sensors/images/me-sound-sensor_Me-Sound-Sensor.jpg" width="150px;"></a><br>
<p>Me Sound Sensor</p></td>
</tr>

<tr>
<td><a href="electronic-modules/sensors/me-temperature-and-humidity-sensor.html" target="_blank"><img src="electronic-modules/sensors/images/me-temperature-and-humidity-sensor_Me-Temperature-and-Humidity-Sensor.jpg" width="150px;"></a><br>
<p>Me Temperature and Humidity Sensor</p></td>
<td><a href="electronic-modules/sensors/temperature-sensor-waterproofds18b20.html" target="_blank"><img src="electronic-modules/sensors/images/temperature-sensor-waterproofds18b20_Me-Temperature-Sensor-Waterproof(DS18B20).jpg" width="150px;"></a><br>
<p>Me Temperature Sensor-Waterproof (DS18B20)</p></td>
<td><a href="electronic-modules/sensors/me-touch-sensor.html" target="_blank"><img src="electronic-modules/sensors/images/me-touch-sensor_Me-Touch-Sensor.jpg" width="150px;"></a><br>
<p>Me Touch Sensor</p></td>
<td><a href="electronic-modules/sensors/me-ultrasonic-sensor.html" target="_blank"><img src="electronic-modules/sensors/images/me-ultrasonic-sensor_Me-Ultrasonic-Sensor.jpg" width="150px;"></a><br>
<p>Me Ultrasonic Sensor</p></td>
</tr>

<tr>
<td><a href="electronic-modules/sensors/me-compass.html" target="_blank"><img src="electronic-modules/sensors/images/me-compass_Me-Compass.jpg" width="150px;"></a><br>
<p>Me Compass</p></td>
<td><a href="electronic-modules/sensors/me-color-sensor-v1.html" target="_blank"><img src="electronic-modules/sensors/images/color-sensor-2.jpg" width="150px;"></a><br>
<p>Me Color Sensor</p></td>
<td><a href="electronic-modules/sensors/rgb-line-follower.html" target="_blank"><img src="electronic-modules/sensors/images/rgb-line-follower_20183271632.png" width="150px;"></a><br>
<p>Me RGB Line Follower</p></td>
<td><a href="electronic-modules/sensors/me-audio-player.html" target="_blank"><img src="electronic-modules/sensors/images/me-audio-player-1.jpg" width="150px;"></a><br>
<p>Me audio player</p></td>
</tr>

</table>

### [Communicators](electronic-modules/communicator.md)


<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="electronic-modules/communicators/2-4g-wireless-serial.html" target="_blank"><img src="electronic-modules/communicators/images/2-4g-wireless-serial_2.4G-Wireless-Serial-for-mBot.jpg" width="150px;"></a><br>
<p>2.4G Wireless Serial for mBot</p></td>

<td width="25%;"><a href="electronic-modules/communicators/bluetooth-moduledual-mode.html" target="_blank"><img src="electronic-modules/communicators/images/bluetooth-moduledual-mode_Bluetooth-Module-for-mBot.jpg" width="150px;"></a><br>
<p>Bluetooth Module(Dual Mode)</p></td>

<td width="25%;"><a href="electronic-modules/communicators/me-bluetooth-moduledual-mode.html" target="_blank"><img src="electronic-modules/communicators/images/me-bluetooth-moduledual-mode_Me-Bluetooth-Module-(Dual-Mode).jpg" width="150px;"></a><br>
<p>Me Bluetooth Module (Dual Mode)</p></td>

<td width="25%;"><a href="electronic-modules/communicators/me-infrared-reciver-decode.html" target="_blank"><img src="electronic-modules/communicators/images/me-infrared-reciver-decode_Me-Infrared-Receiver-Decode.jpg" width="150px;"></a><br>
<p>Me Infrared Receiver Decode</p></td>
</tr>

<tr>
<td><a href="electronic-modules/communicators/me-usb-host.html" target="_blank"><img src="electronic-modules/communicators/images/me-usb-host_Me-USB-Host.jpg" width="150px;"></a><br>
<p>Me USB Host</p></td>

<td><a href="electronic-modules/communicators/me-wifi.html" target="_blank"><img src="electronic-modules/communicators/images/me-wifi_Me-WiFi-Module.jpg" width="150px;"></a><br>
<p>Me WiFi Module</p></td>

<td><a href="electronic-modules/communicators/bluetooth-modulesingle-mode.html" target="_blank"><img src="electronic-modules/communicators/images/bluetooth-modulesingle-mode_S5.png" width="150px;"></a><br>
<p>Bluetooth Module(Single Mode)</p></td>
</tr>

</table>

### [Displays](electronic-modules/display.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="electronic-modules/displays/me-7-segment-display.html" target="_blank"><img src="electronic-modules/displays/images/me-7-segment-display_Me-7-Segment-Serial-Display---Red.jpg" width="150px;"></a><br>
<p>Me 7-Segment Serial Display – Red</p></td>

<td width="25%;"><a href="electronic-modules/displays/led-rgb-strip-addressable-sealed-0-5m1m.html" target="_blank"><img src="electronic-modules/displays/images/led-rgb-strip-addressable-sealed-0-5m1m_LED-RGB-Strip-Addressable,-Sealed.jpg" width="150px;"></a><br>
<p>LED RGB Strip-Addressable, Sealed</p></td>

<td width="25%;"><a href="electronic-modules/displays/me-led-matrix-8x16.html" target="_blank"><img src="electronic-modules/displays/images/me-led-matrix-8x16_Me-LED-Matrix-8×16.jpg" width="150px;"></a><br>
<p>Me LED Matrix 8×16</p></td>

<td width="25%;"><a href="electronic-modules/displays/me-rgb-led.html" target="_blank"><img src="electronic-modules/displays/images/me-rgb-led_Me-RGB-LED.jpg" width="150px;"></a><br>
<p>Me RGB LED</p></td>
</tr>

<tr>
<td><a href="electronic-modules/displays/me-tft-lcd-screen-2-2-inch.html" target="_blank"><img src="electronic-modules/displays/images/me-tft-lcd-screen-2-2-inch_Me-TFT-LCD-Screen---2.2-Inch.jpg" width="150px;"></a><br>
<p>Me TFT LCD Screen – 2.2 Inch</p></td>
</tr>
</table>


### [Control](electronic-modules/controls.md)


<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="electronic-modules/control/me-4-button.html" target="_blank"><img src="electronic-modules/control/images/me-4-button_Me-4-Button.jpg" width="150px;"></a><br>
<p>Me 4 Button</p></td>

<td width="33%;"><a href="electronic-modules/control/me-joystick.html" target="_blank"><img src="electronic-modules/control/images/me-joystick_Me-Joystick.jpg" width="150px;"></a><br>
<p>Me Joystick</p></td>

<td width="33%;"><a href="electronic-modules/control/me-potentiometer.html" target="_blank"><img src="electronic-modules/control/images/me-potentiometer_potentionmeter.jpg" width="150px;"> </a><br>
<p>Me Potentiometer</p></td>
</tr>
</table>

### [Execution](electronic-modules/executions.md)


<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="50%;"><a href="electronic-modules/execution/dc-frame-type-solenoid-hcne1-0530.html" target="_blank"><img src="electronic-modules/execution/images/dc-frame-type-solenoid-hcne1-0530_DC-Frame-Type-Solenoid-HCNE1-0530.jpg" width="150px;"></a><br>
<p>DC Frame Type Solenoid HCNE1-0530</p></td>

<td width="50%;"><a href="electronic-modules/electronic-modules/execution/me-shutter.html" target="_blank"><img src="electronic-modules/execution/images/me-shutter_Me-Shutter.jpg" width="150px;"></a><br>
<p>Me Shutter</p></td>
</tr>
</table>


### [Adapters](electronic-modules/adapter.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="electronic-modules/adapters/me-rj25-adapter.html" target="_blank"><img src="electronic-modules/adapters/images/megapi-pro-shield-rj25_Megapi-Pro-RJ25转接板.jpg" width="150px;"></a><br>
<p>Me RJ25 Adapter</p></td>

<td width="33%;"><a href="electronic-modules/adapters/me-shield-for-raspberry-pi.html" target="_blank"><img src="electronic-modules/adapters/images/me-shield-for-raspberry-pi_Me-Shield-for-Raspberry-Pi.jpg" width="150px;"></a><br>
<p>Me Shield for Raspberry Pi</p></td>

<td width="33%;"><a href="electronic-modules/adapters/me-uno-shield.html" target="_blank"><img src="electronic-modules/adapters/images/me-uno-shield_Me-UNO-Shield.jpg" width="150px;"> </a><br>
<p>Me UNO Shield</p></td>
</tr>
</table>


### [Motors](electronic-modules/motor.md)


<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="electronic-modules/motors/9g-micro-servo.html" target="_blank"><img src="electronic-modules/motors/images/9g-micro-servo_9g-Micro-Servo-Pack.jpg" width="150px;"></a><br>
<p>9g Micro Servo Pack</p></td>

<td width="25%;"><a href="electronic-modules/motors/36-dc-geared-motor-12v240rpm.html" target="_blank"><img src="electronic-modules/motors/images/36-dc-geared-motor-12v240rpm_36-DC-Geared-Motor-12V240RPM.jpg" width="150px;"></a><br>
<p>36 DC Geared Motor 12V240RPM</p></td>

<td width="25%;"><a href="electronic-modules/motors/42byg-geared-stepper-motor.html" target="_blank"><img src="electronic-modules/motors/images/42byg-geared-stepper-motor_42BYG-Geared-Stepper-Motor.jpg" width="150px;"></a><br>
<p>42BYG Geared Stepper Motor</p></td>

<td width="25%;"><a href="electronic-modules/motors/42byg-stepper-motor.html" target="_blank"><img src="electronic-modules/motors/images/42byg-stepper-motor_42BYG-Stepper-Motor.jpg" width="150px;"></a><br>
<p>42BYG Stepper Motor</p></td>
</tr>

<tr>
<td><a href="electronic-modules/motors/57byg-stepper-motor.html" target="_blank"><img src="electronic-modules/motors/images/57byg-stepper-motor_57BYG-Stepper-Motor.jpg" width="150px;"></a><br>
<p>57BYG Stepper Motor</p></td>
<td><a href="electronic-modules/motors/555-high-speed-cnc-motor-24v-10000rpm.html" target="_blank"><img src="electronic-modules/motors/images/555-high-speed-cnc-motor-24v-10000rpm_555-High-speed-CNC-Motor-24V-10000RPM.jpg" width="150px;"></a><br>
<p>555 High-speed CNC Motor 24V-10000RPM</p></td>
<td><a href="electronic-modules/motors/820-coreless-motor.html" target="_blank"><img src="electronic-modules/motors/images/820-coreless-motor_820-Coreless-Motor.jpg" width="150px;"></a><br>
<p>820 Coreless Motor</p></td>
<td><a href="electronic-modules/motors/air-pump-motor-dc-12v-370-02pm.html" target="_blank"><img src="electronic-modules/motors/images/air-pump-motor-dc-12v-370-02pm_Air-Pump-Motor---DC-12V-370-02PM.jpg" width="150px;"></a><br>
<p>Air Pump Motor – DC 12V-370-02PM</p></td>
</tr>

<tr>
<td><a href="electronic-modules/motors/air-pump-motor-dc-12v-3202pm.html" target="_blank"><img src="electronic-modules/motors/images/air-pump-motor-dc-12v-3202pm_Air-Pump-Motor-DC-12V-3202PM.jpg" width="150px;"></a><br>
<p>Air Pump Motor DC 12V-3202PM</p></td>
<td><a href="electronic-modules/motors/dc-encoder-motor-25-6v-185rpm.html" target="_blank"><img src="electronic-modules/motors/images/dc-encoder-motor-25-6v-185rpm_DC-Encoder-Motor---25-6V-185RPM.jpg" width="150px;"></a><br>
<p>DC Encoder Motor – 25 6V-185RPM</p></td>
<td><a href="electronic-modules/motors/dc-motor-25-6v.html" target="_blank"><img src="electronic-modules/motors/images/dc-motor-25-6v_DC-Motor-25-6V.jpg" width="150px;"></a><br>
<p>DC Motor-25 6V</p></td>
<td><a href="electronic-modules/motors/dc-motor-37-12v.html" target="_blank"><img src="electronic-modules/motors/images/dc-motor-37-12v_DC-Motor-37-12V.jpg" width="150px;"></a><br>
<p>DC Motor-37 12V</p></td>
</tr>

<tr>
<td><a href="electronic-modules/motors/meds15-servo-motor.html" target="_blank"><img src="electronic-modules/motors/images/meds15-servo-motor_MEDS15-Servo-Motor.jpg" width="150px;"></a><br>
<p>MEDS15 Servo Motor</p></td>
<td><a href="electronic-modules/motors/mg995-standard-servo.html" target="_blank"><img src="electronic-modules/motors/images/mg995-standard-servo_MG995-Standard-Servo.jpg" width="150px;"></a><br>
<p>MG995 Standard Servo</p></td>
<td><a href="electronic-modules/motors/micro-peristaltic-pump-dc12-0v.html" target="_blank"><img src="electronic-modules/motors/images/micro-peristaltic-pump-dc12-0v_Micro-Peristaltic-Pump-DC12.0V.jpg" width="150px;"></a><br>
<p>Micro Peristaltic Pump DC12.0V</p></td>
<td><a href="electronic-modules/motors/mini-metal-gear-motor-n20-dc-12v.html" target="_blank"><img src="electronic-modules/motors/images/mini-metal-gear-motor-n20-dc-12v_Mini-Metal-Gear-Motor---N20-DC-12V.jpg" width="150px;"></a><br>
<p>Mini Metal Gear Motor – N20 DC 12V</p></td>
</tr>

<tr>
<td><a href="electronic-modules/motors/solenoid-valve-dc-12v-0520e.html" target="_blank"><img src="electronic-modules/motors/images/solenoid-valve-dc-12v-0520e_Solenoid-Valve-DC-12V-0520E.jpg" width="150px;"></a><br>
<p>Solenoid Valve DC 12V-0520E</p></td>
<td><a href="electronic-modules/motors/tt-geared-motor-dc-6v-200rpm.html" target="_blank"><img src="electronic-modules/motors/images/tt-geared-motor-dc-6v-200rpm_TT-Geared-Motor-DC-6V-200RPM.jpg" width="150px;"></a><br>
<p>TT Geared Motor DC 6V-200RPM</p></td>
<td><a href="electronic-modules/motors/water-pump-motor-dc-12v-370-04pm.html" target="_blank"><img src="electronic-modules/motors/images/water-pump-motor-dc-12v-370-04pm_Water-Pump-Motor---DC-12V-370-04PM.jpg" width="150px;"></a><br>
<p>Water Pump Motor – DC 12V-370-04PM</p></td>
</tr>

</table>

### [Power](electronic-modules/powers.md)


<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="50%;"><a href="electronic-modules/power/ac-to-dc-12v-3a-wall-adapter-power-supply-for-arduino.html" target="_blank"><img src="electronic-modules/power/images/ac-to-dc-12v-3a-wall-adapter-power-supply-for-arduino_AC-to-DC-12V-3A-Wall-Adapter-Power-Supply-For-Arduino-Meduino.jpg" width="150px;"></a><br>
<p>AC to DC 12V 3A Wall Adapter Power Supply For Arduino-Meduino</p></td>

<td width="50%;"><a href="electronic-modules/power/battery-holder-for-6-aa.html" target="_blank"><img src="electronic-modules/power/images/battery-holder-for-6-aa_Battery-Holder-for-(6)AA.jpg" width="150px;"></a><br>
<p>Battery Holder for (6)AA</p></td>
</tr>
</table>

### [Cables](electronic-modules/cable.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="electronic-modules/cables/6p6c-rj25-cable.html" target="_blank"><img src="electronic-modules/cables/images/6p6c-rj25-cable_6P6C-RJ25-cable-35cm.jpg" width="150px;"></a><br>
<p>6P6C RJ25 cable</p></td>

<td width="25%;"><a href="electronic-modules/cables/rj25-to-dupont-wire.html" target="_blank"><img src="electronic-modules/cables/images/rj25-to-dupont-wire_RJ25-to-Dupont-Wire.jpg" width="150px;"></a><br>
<p>RJ25 to Dupont Wire</p></td>

<td width="25%;"><a href="electronic-modules/cables/shutter-cable-c1-c3-n1-n3-for-canon.html" target="_blank"><img src="electronic-modules/cables/images/shutter-cable-c1-c3-n1-n3-for-canon_Shutter-Cable-C1-for-Canon.jpg" width="150px;"></a><br>
<p>Shutter Cable</p></td>

<td width="25%;"><a href="electronic-modules/cables/usb-2-0-a-male-to-micro-b-male-cable.html" target="_blank"><img src="electronic-modules/cables/images/usb-2-0-a-male-to-micro-b-male-cable_USB-2.0-A-Male-to-Micro-B-Male-Cable.jpg" width="150px;"></a><br>
<p>USB 2.0 A-Male to Micro B-Male Cable</p></td>
</tr>
</table>