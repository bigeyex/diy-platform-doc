# 舵机驱动

舵机驱动能够驱动各类180°舵机转动到指定的角度，其引脚从左到右依次为数字输出、VCC、GND。

<img src="images/servo-driver.png" style="padding:3px 0px 12px 3px;width:200px;">

### 参数

- 尺寸：24×24mm
- 工作电流：小于1A