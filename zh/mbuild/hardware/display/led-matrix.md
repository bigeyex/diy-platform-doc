# 8×16 LED 点阵

LED点阵可以单独设置点阵上每一个点的亮灭，从而实现对动画、表情、文字、数字的表现。

<img src="images/led-matrix.png" style="padding:3px 0px 12px 3px;width:300px;">

### 生活实例

- 机场的航班班次屏幕就是一个LED点阵屏<br>
<img src="images/led-matrix-1.jpg" style="padding:8px 0px 12px 3px;width:300px;">

### 参数

- 尺寸：64×44mm
- 工作电流：150mA

