# 红外收发

红外收发模块上集成了红外发射与红外接受功能，能够发射和接受940nm波段的红外信号。

<img src="images/ir.png" style="padding:3px 0px 12px 3px;width:200px;">

### 生活实例

- 空调遥控器与空调<br>
<img src="images/ir-1.jpg" style="padding:8px 0px 12px 3px;width:300px;">
- 电视遥控器与电视<br>
<img src="images/ir-2.jpg" style="padding:8px 0px 12px 3px;width:300px;">
- 红外手柄与遥控车

### 参数

- 尺寸：24×24mm
- 频段：940nm
- 支持协议：NEC
- 发射范围：6m
- 接受范围：6m
- 工作电流：40mA