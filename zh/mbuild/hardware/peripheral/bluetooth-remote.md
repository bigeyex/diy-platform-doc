# 蓝牙遥控器

蓝牙遥控器可以和 Makeblock 各类机器人，主控板，蓝牙模组，蓝牙适配器进行配对，从而实现对机器人、舞台、主控板、项目作品的远程遥控。

<img src="images/bluetooth-remote.png" style="padding:6px 0px 12px 3px;width:300px;">

该遥控器主要被用于 MakeX 赛事中手动环节的机器人控制上。

### 参数

- 材质：ABS
- 蓝牙版本：4.0
- 传输距离：20m
- 抗干扰能力：支持80个同时工作
- 供电方式：2节 5号电池