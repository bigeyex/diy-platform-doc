# 电源模块

打开电源模块上的开关，电源模块就能够为其他 mbuild 模块供电，使其正常工作。

<img src="images/power.png" style="padding:3px 0px 12px 3px;width:300px;">

### 电源的灯效和含义

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;">
<tr>
<th style="border: 1px solid black;width:33%;"></th>
<th style="border: 1px solid black;width:33%;">亮</th>
<th style="border: 1px solid black;width:33%;">灭</th>
</tr>

</td>
<td style="border: 1px solid black;">绿灯</td>
<td style="border: 1px solid black;">电源开启</td>
<td style="border: 1px solid black;">电源关闭</td>
</tr>

<tr>
<td style="border: 1px solid black;">红灯</td>
<td style="border: 1px solid black;">低电量或正在充电</td>
<td style="border: 1px solid black;">高电量或充电完成</td>
</tr>
</table>

<br>

### 参数

- 尺寸：48×48mm
- 电池容量：950mAh
- 电池电压：3.7V
- 放电倍率：3CC
- 续航时间：3小时（不同模块组合下存在较大波动）
- 充电时长：1.25小时
- 输出电压：DC 5V
- 放电电流：5V 1.5A，瞬间最大值 5V 2A
- 保护电流：5V 3A（保护后需要连接充电器重启模块）
- 输入电压：DC 5V
- 输入电流：＜2A
- 工作温度：0℃~45℃
- 使用寿命：循环次数≥300

### 注意事项

- 使用电池前，请仔细阅读使用说明；
- 使用过程中，应远离热源，高压场所，并勿摔打，撞击电池；
- 废弃电池模块请妥善安全处理，切勿投入火中或水中；
- 在使用或储存期间，如发现电池有出现高温发热，漏液，散发异味，变形，及其他异常现象时，请立即停止使用；