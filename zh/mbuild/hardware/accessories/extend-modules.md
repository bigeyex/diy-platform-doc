# 延长模块

延长模块可以用于连接两条 5V通用连接线，延长其使用长度。

<img src="images/extend.png" style="padding:6px 0px 12px 3px;width:200px;">

### 连接示意

<img src="images/extend-1.png" style="padding:6px 0px 12px 3px;width:300px;">