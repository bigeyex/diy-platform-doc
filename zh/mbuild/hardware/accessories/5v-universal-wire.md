# 5V通用连接线

Makeblock 提供10cm、20cm、60cm 三种规格的5V通用连接线，该连接线可以用于 mbuild 与 mbuild 模块，mbuild 与光环板之间的连接。

<img src="images/wire.png" style="padding:3px 0px 12px 3px;width:300px;">

如需要其他长度的连接线，你可以将现有连接线通过与延长模块组合。