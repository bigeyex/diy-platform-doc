# 按钮

按钮模块可以用做事件触发，状态切换或是计数。

<img src="images/button.png" style="padding:3px 0px 12px 3px;width:300px;">

### 生活实例

- 电脑开机键可以用来触发电脑开机<br>
<img src="images/button-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">
- 手机锁屏键可以切换屏幕点亮和熄灭状态<br>
<img src="images/button-2.jpg" style="padding:10px 0px 12px 3px;width:300px;">
- 鼠标按键可以记录按键按下的次数<br>
<img src="images/button-3.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### 参数

- 尺寸：24×20mm
- 使用寿命：10万次
- 工作电流：15mA