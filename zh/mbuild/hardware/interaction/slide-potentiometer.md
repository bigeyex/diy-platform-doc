# 滑动电位器

滑动电位器可以用于调节输入的大小。

<img src="images/slide-potentiometer.png" style="padding:3px 0px 12px 3px;width:200px;">

### 原理介绍

滑动电位器可以通过滑块的位置改变其电阻，电阻的变化会导致对应电路的电压也发生变化。透过电压与电阻的对应关系，我们就能推测电阻的大小，从而判断滑块所在的位置了。

### 生活实例

- 手机的音量靠滑块来调节<br>
<img src="images/slide-3.png" style="padding:8px 0px 12px 3px;width:300px;">
- 视频的进度条也是一个滑块<br>
<img src="images/slide-2.jpg" style="padding:8px 0px 12px 3px;width:500px;">

### 参数

- 尺寸：24×72mm
- 使用寿命：15000次
- 读值范围：0~100
- 读值精度：±2%
- 工作电流：15mA