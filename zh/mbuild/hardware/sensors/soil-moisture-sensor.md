# 土壤湿度传感器

土壤湿度传感器能够检测土壤中的水分湿度。

<img src="images/soil-moisture.png" style="padding:3px 0px 12px 3px;width:200px;">

### 原理介绍

常见的土壤湿度传感器分为电容式和电阻式两种。电容式通过测量土壤湿度引起的电容的变化推测土壤的湿度，电阻式则是根据测量两个触点间的电阻大小来推测土壤湿度的大小。土壤湿度的严格定义上是土壤中水分所占质量的百分比，因此无论是电阻式或是电容式都无法准确地预估土壤中的含水量，而只能尽可能地做到读值与土壤中的含水量正相关。

mBuild 的土壤湿度传感器是电阻式的，当土壤湿度提高时，水分能够溶解部分土壤中包含的离子，从而使得土壤的电阻变小，也因此土壤湿度传感器的读值将变大。而当土壤中的含水量进一步提高时（极限的情况下，想象往水里扔一些泥土），土壤中的离子浓度反而会被过量的水稀释，导致导电能力下降，电阻值变高。因此你会发现，该土壤湿度传感器在纯水中的读值将小于在湿润土壤中的读值（尽管前者的含水量明显更高），这是因为湿润土壤中的离子浓度更高，电阻更小。

备注：也许你已经注意到了，由于是土壤湿度传感器使用了电阻式的方案，某种程度上它还能够被用来不十分精确地测量电阻的大小，我们会考虑在之后开发相关接口。

### 参数

- 尺寸：24×72mm
- 读值范围：0~100
- 一致性误差：±5%
- 工作电流：14mA
