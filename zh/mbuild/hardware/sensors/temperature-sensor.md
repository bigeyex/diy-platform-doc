# 温度传感器

温度传感器能够直接和被检测物体接触，检测其温度。

<img src="images/temperature-sensor.png" style="padding:3px 0px 12px 3px;width:450px;">

使用前，请先将温度传感器探头与模块如上图组装。

### 生活实例

- 电子温度计上使用了温度传感器<br>
<img src="images/temperature-sensor-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### 参数

- 尺寸：24×20mm
- 读值范围：-55~125℃
- 读值误差：±0.5℃
- 工作电流：14mA