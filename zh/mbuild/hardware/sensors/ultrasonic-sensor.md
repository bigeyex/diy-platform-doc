# 超声波传感器

超声波传感器能够检测它与被测物体之间的距离。超声波模块的左边探头负责发射超声波，而右边探头则负责接收超声波。

<img src="images/ultrasonic-1.png" style="padding:3px 0px 12px 3px;width:250px;">

<img src="images/ultrasonic.png" style="padding:3px 0px 12px 3px;width:250px;">

### 原理介绍

人耳朵能听到的声波频率为20～20000Hz，高于20000Hz的声波被称为超声波，声波在遇到障碍物时会被反弹并被超声波传感器的探头接收到，利用发送时间到接收时间的时间差，我们得以计算超声波探头到障碍物之间的距离。

### 生活实例

- 蝙蝠利用超声波定位物体<br>
<img src="images/ultrasonic-2.png" style="padding:15px 0px 12px 3px;width:300px;">

### 参数

- 读值范围：5~300cm（高于或低于读值范围时，读值保留在300）
- 读值误差：±5%
- 工作电流：26mA