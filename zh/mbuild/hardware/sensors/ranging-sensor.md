# 测距传感器

测距传感器能够通过红外波检测与障碍物之间的距离。

<img src="images/ranging-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### 生活实例

- iPhone使用的 Face ID 使用红外光检测面部结构作为生物特征数据<br>
<img src="images/ranging-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### 参数

- 尺寸：24×20mm
- 读值范围：2~200cm
- 读值精度：±5%
- 工作电流：33mA