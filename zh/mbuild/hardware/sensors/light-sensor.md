# 光线传感器

光线传感器能够检测环境光线的强弱。

<img src="images/light-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### 原理介绍

mBuild 的光线传感器使用了光敏电阻作为核心元器件，这是一种能够随光线强度改变电阻值大小的元器件，电阻值的改变能够被MCU所检测，MCU因此能够判断环境中的光线强弱。

### 生活实例

- 光线传感器能够帮助手机智能调节屏幕亮度<br>
<img src="images/light-2.jpg" style="padding:8px 0px 12px 3px;width:400px;">
- 一些智能灯泡可以根据环境光线调整亮度<br>
<img src="images/light-1.jpg" style="padding:8px 0px 12px 3px;width:400px;">

### 参数

- 尺寸：24×20mm
- 读值范围：0~100%
- 工作电流：15mA