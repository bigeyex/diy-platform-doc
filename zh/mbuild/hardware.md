# 硬件说明

* [能源类](hardware/power.md)
* [通信类](hardware/communication.md)
* [交互类](hardware/interaction.md)
* [传感器类](hardware/sensors.md)
* [显示类](hardware/display.md)
* [灯效类](hardware/light.md)
* [播放类](hardware/sound.md)
* [运动类](hardware/motion.md)
* [外设类](hardware/peripheral.md)
* [配件类](hardware/accessories.md)