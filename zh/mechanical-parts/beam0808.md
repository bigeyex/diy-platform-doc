<img src="beam0808/images/beam0808.jpg" style="width:600;padding:5px 5px 15px 0px;">

# 单孔梁

### 概述

单孔梁是makeblock平台常用的机械零件，与大多数makeblock平台的机械零件兼容，单孔梁的表面上有各种安装孔和螺纹槽，可以方便安装在其他结构上。

### 参数

- 长度：24-504mm
- 尺寸规格：024、040-A、040-B、056、072、088、104、120、136、152、168、184、312、504
- 横截面：8x8mm
- 材质：6061挤压铝合金

### 功能特性

- 高耐磨
- 高强度
- 高刚度

### 使用说明

通过梁上的通孔、中间的螺纹槽，梁与梁之间可以相互连接，也可以与支架、滑轨、轴、电机等各类零件连接。

### 搭建案例

<img src="beam0808/images/beam0808-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

<img src="beam0808/images/beam0808-2.jpg" style="width:600;padding:5px 5px 15px 0px;">

### 规格表

<table style="text-align:center;cellpadding:12px;cellspacing:10px;">

<tr>
<td><img src="beam0808/images/beam0808-024.jpg" width="170px;"><br><p>单孔梁-024</p></td>
<td><img src="beam0808/images/beam0808-40-A.jpg" width="170px;"><br><p>单孔梁-040-A</p></td>
<td><img src="beam0808/images/beam0808-40-B.jpg" width="170px;"><br><p>单孔梁-040-B</p></td>
<td><img src="beam0808/images/beam0808-056.jpg" width="170px;"><br><p>单孔梁-056</p></td>
</tr>

<tr>
<td><img src="beam0808/images/beam0808-072.jpg" width="170px;"><br><p>单孔梁-072</p></td>
<td><img src="beam0808/images/beam0808-088.jpg" width="170px;"><br><p>单孔梁-088</p></td>
<td><img src="beam0808/images/beam0808-104.jpg" width="170px;"><br><p>单孔梁-104</p></td>
<td><img src="beam0808/images/beam0808-120.jpg" width="170px;"><br><p>单孔梁-120</p></td>
</tr>

<tr>
<td><img src="beam0808/images/beam0808-136.jpg" width="170px;"><br><p>单孔梁-136</p></td>
<td><img src="beam0808/images/beam0808-152.jpg" width="170px;"><br><p>单孔梁-152</p></td>
<td><img src="beam0808/images/beam0808-168.jpg" width="170px;"><br><p>单孔梁-168</p></td>
<td><img src="beam0808/images/beam0808-184.jpg" width="170px;"><br><p>单孔梁-184</p></td>
</tr>

<tr>
<td><img src="beam0808/images/beam0808-312.jpg" width="170px;"><br><p>单孔梁-312</p></td>
<td><img src="beam0808/images/beam0808-504.jpg" width="170px;"><br><p>单孔梁-504</p></td>
</tr>


</table>