<img src="images/injection-omnidirectional.png" style="width:400;padding:5px 5px 15px 0px;">

# 注塑全向轮
 
### 概述

Makeblock注塑全向轮由钢球和塑料支架制成，车轮自由而灵活地转动。它可以与Makeblock mBot或其他三轮机器人车兼容。

### 参数

- 长度：24mm
- 宽度：16mm
- 高度：16mm

### 尺寸

<img src="images/injection-omnidirectional-2.png" style="width:400;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/injection-omnidirectional-1.jpg" style="width:400;padding:5px 5px 15px 0px;">
