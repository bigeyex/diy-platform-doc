<img src="images/tyre-68-5.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 花纹轮胎
 
### 概述

花纹轮胎通常与同步带轮90T一起使用。	

### 参数

- 材质：硅胶
- 直径：68.5mm
- 宽度：22mm

### 搭建案例

<img src="images/tyre-68-5-1.jpg" style="width:600;padding:5px 5px 15px 0px;">