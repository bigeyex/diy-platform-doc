<img src="images/slick-tyre.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 光面轮胎
 
### 概述

Makeblock 光面轮胎64*16mm由硅制成，表面光滑，可以降低阻力，使您的遥控车跑得更快。它也兼容于mBot中的Makeblock同步带轮90T。

### 参数

- 材质：硅胶
- 直径：64mm
- 宽度：16mm

### 尺寸图纸

<img src="images/slick-tyre-1.png" style="width:600;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/slick-tyre-2.jpg" style="width:400;padding:5px 5px 15px 0px;">