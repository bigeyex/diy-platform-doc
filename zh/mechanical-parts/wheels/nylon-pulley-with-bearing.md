<img src="images/nylon-pulley.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 尼龙滑轮
 
### 概述
该尼龙滑轮可用于8mm宽零件上的平滑线性运动。它具有8mm宽的槽，可以精确地配合8mm宽度的梁或杆。每个滑轮需要2个轴承。

### 参数

- 内径：14.5mm
- 长度：16mm
- 配合轴承：滚珠轴承4x8x3mm
- 材质：尼龙

### 尺寸

<img src="images/nylon-pulley-1.jpg" style="width:600;padding:5px 5px 15px 0px;">



