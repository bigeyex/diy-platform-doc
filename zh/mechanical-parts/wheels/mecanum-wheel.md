<img src="images/mecanum-1.jpg" style="width:150;padding:5px 5px 15px 0px;">

<img src="images/mecanum-2.png" style="width:400;padding:5px 5px 15px 0px;">

# 麦克纳姆轮

### 概述

100 mm铝制麦克纳姆轮组（2 x左，2 x右）包括2个右车轮和2个左车轮。每个车轮由9个滚轮组成，可以独立驱动。麦克纳姆轮让你的机器人不仅可以前后移动，而且可以左右平移。有了4个麦克纳姆轮，你就可以造出一辆可以向各个方向移动的汽车/机器人。该组件的每个轮子都配有一个兼容安装轮毂4mm轴连接器，因此它很容易与我们的编码器电机或步进电机配合。

### 产品特性

- 麦克纳姆轮允许机器人实现全方位运动，同时支持较大载重
- 使用编码电机或步进电机
- 兼容4mm和8mm电机轴
- 冷轧SPCC钢制成，刚性好，抗冲击性强
- 易组装

### 尺寸图纸

<img src="images/mecanum-5.png" style="width:400;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/mecanum-4.jpg" style="width:400;padding:5px 5px 15px 0px;">
