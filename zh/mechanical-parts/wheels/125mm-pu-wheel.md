<img src="images/125mm-pu-wheel.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 125x24mm Pu轮
 
### 概述

125mm Pu轮可用作驱动轮、驱动轮或摩擦轮。作为驱动轮或从动轮，它能承受很重的重量。

### 参数

- 轴径：8mm
- 直径：125mm
- 材质：聚氨酯

### 搭建案例

<img src="images/125mm-pu-wheel-1.jpg" style="width:400;padding:5px 5px 15px 0px;">
