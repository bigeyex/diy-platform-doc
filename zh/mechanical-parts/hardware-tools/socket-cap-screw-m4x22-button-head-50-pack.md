# Socket Cap Screw M4x22-Button Head

![](images/socket-cap-screw-m4x22-button-head-50-pack_Socket-Cap-Screw-M4x22-Button-Head.jpg)

**Description：
**

Button
Head Socket Cap Screw is a new kind of Socket Cap Screw, it has smaller
head that can solve most problem of interfere with other components.
 The tool of this Button Head Socket Cap Screw  is [HEX Screwdriver
2.5mm](http://www.makeblock.cc/cross-2-5mm-hex-screwdriver/).

** **

**Features:**

-   Made
    of stainless steel.
-   Compatible
    with structures which have 4mm holes.
-   Smaller
    head that can solve most problem of interfere with other
    components.
-   Sold
    in packs of 50.

 

**Size
charts(mm):**

 

**Demo:**

<img src="images/34.jpg" alt="34.jpg" width="760" /> 
