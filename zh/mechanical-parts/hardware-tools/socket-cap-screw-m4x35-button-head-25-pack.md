# Socket Cap Screw M4x35-Button Head


![](images/socket-cap-screw-m4x35-button-head-25-pack_Socket-Cap-Screw-M4x35-Button-Head.jpg)

**Description：**

[Button
Head Socket Cap
Screw](http://www.makeblock.cc/botton-head-socket-cap-screw/) is a new
kind of Socket Cap Screw, it has smaller head that can solve most
problem of interfere with other components. The tool of this Button Head
Socket Cap Screw  is HEX Screwdriver 2.5mm. 

**Features:**

-   Made
    of stainless steel.
-   Compatible
    with structures which have 4mm holes.
-   Smaller
    head that can solve most problem of interfere with other
    components.
-   Sold
    in packs of 25.

 

**Size
charts(mm):**

**<img src="images/socket-cap-screw-m4x35-button-head-25-pack_70567-s1.jpg" alt="70567-s1.jpg" width="760" />**

**![](http://)**

 

**Demo:**

<img src="images/34.jpg" alt="34.jpg" width="760" /> 
