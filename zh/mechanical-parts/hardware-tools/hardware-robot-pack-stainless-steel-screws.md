# Hardware Robot Pack

![](images/hardware-robot-pack-stainless-steel-screws_Hardware-Robot-Pack-(Stainless-Steel-Screws).jpg)

**Description:**

The
Makeblock Hardware Robot Pack includes frequently used hardware in an
easily accessible, two-sided component box.

The
screws in this pack are stainless steel screws that can prevent
Oxidation , they  have no lubricating on the surface so they look
beautiful and clean. 

 

**Parts List:**

|                                                                                                                 |                                                                                                              |
|-----------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| 1 × Wrench 7mm&5mm            | 20 × Nylon lock Nut 4mm    |
| 1 × Plastic  Box              | 4 × Brass Stud M4\*8+6     |
| 40 × Socket Cap Screw M4x8    | 4 × Brass Stud M4\*16      |
| 40 × Socket Cap Screw M4x14   | 4 × Brass Stud M4\*32      |
| 30 × Socket Cap Screw M4x22   | 20 × Plastic Rivet R4060   |
| 16 × Socket Cap Screw M4x35   | 20 × Plastic Rivet R4120   |
| 20 × Headless Set Screw M3x5  | 100 × Plastic Ring 4x7x2mm |
| 16 × Screw M3x8               |  80 × Nut M4               |

 
