<img src="images/flexible-coupling-4x4mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 联轴器4x4mm
 
### 概述

这是由铝制成的柔性联轴器的单螺旋槽。这种设计利用了一种材料，通过沿着螺旋路径将材料移除，从而形成了一种弯曲的弯曲的螺旋形状，从而变得更加灵活. 对螺旋梁的改变提供了对不匹配能力的改变，以及其他性能特征，如转矩能力和扭转刚度。甚至有可能在同一个螺旋中有多个开始。

### 参数

内径：4mm
材质：铝
外径：16mm

### 尺寸图纸

<img src="images/flexible-coupling-4x4mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例
 
<img src="images/flexible-coupling-4x4mm-2.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/flexible-coupling-4x4mm-3.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/flexible-coupling-4x4mm-4.jpg" style="width:400;padding:5px 5px 15px 0px;">
