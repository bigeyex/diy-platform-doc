<img src="images/universal-joint-4x4mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 万向节4x4mm
 
### 概述

这种交叉耦合的通用接头是由两种类型的连接器和一个十字轴组成的。这种结构很简单，但功率传输很大。偏转角的最大角度是45度。

### 参数

- 内径：4mm
- 材质：不锈钢
- 最大偏转角：45°
- 外径：9mm

### 尺寸图纸

<img src="images/universal-joint-4x4mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/universal-joint-4x4mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
 
<img src="images/universal-joint-4x4mm-3.jpg" style="width:700;padding:5px 5px 15px 0px;">

 

 
