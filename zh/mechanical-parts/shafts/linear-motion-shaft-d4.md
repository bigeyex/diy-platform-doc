<img src="images/linear-motion-shaft-d4-80mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 光轴D4
 
### 概述

直径4mm的光轴适用于直线运动中的轴，支撑部件。 它由高碳钢镀铬制成，具有耐腐蚀性，表面硬化具有耐磨性和高精度。

### 参数

- 直径：4mm
- 长度：80mm、128mm、240mm、288mm
- 材质：镀铬高碳钢

## 光轴D4x80mm

<img src="images/linear-motion-shaft-d4-80mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/linear-motion-shaft-d4-80mm-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

## 光轴D4x128mm

<img src="images/linear-motion-shaft-d4-128mm.jpg" style="padding:5px 5px 15px 0px;">

<img src="images/linear-motion-shaft-d4-128mm-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

## 光轴D4x240mm

<img src="images/linear-motion-shaft-d4-240mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/linear-motion-shaft-d4-248mm-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

## 光轴D4x288mm

<img src="images/linear-motion-shaft-d4-288mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

