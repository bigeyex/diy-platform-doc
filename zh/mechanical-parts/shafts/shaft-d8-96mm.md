<img src="images/d-shaft-8x96mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# D型轴8x96mm
 
### 概述

D轴8x96毫米通常作为轴、齿轮,皮带轮、联轴器或轴承配合使用。	
### 参数

- 直径：8mm
- 长度：96mm
- 材质：镀铬不锈钢

### 尺寸图纸
 
<img src="images/d-shaft-8x96mm-1.png" style="width:700;padding:5px 5px 15px 0px;">

 

 
