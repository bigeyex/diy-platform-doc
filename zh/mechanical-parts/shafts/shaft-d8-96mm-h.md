# D Shaft 8x96mm H

![](images/D-Shaft-D8x96mm.jpg)

**Description:**

This
linear motion shaft of 8 mm in diameter and 96mm in length is suitable
for using as shaft or supporting part. It is made from high-carbon steel
and chrome plated for corrosion resistance, case hardened for wear
resistance.

  
**Features:**

-   With
    M4 thread holes on both ends
-   Round
    steel shaft for using as shaft, supporting part, or using with slide
    units in linear motion applications
-   Chrome
    plated for corrosion resistance
-   Case
    hardened for wear resistance

 

**SizeCharts:**

<img src="images/shaft-d8-96mm-h_shaft-d8-96mm-h-sj1.jpg" alt="shaft-d8-96mm-h-sj1.jpg" width="760" />

 

**Demo:**

<img src="images/shaft-d8-96mm-h_shaft-d8-128mm-h-demo01.jpg" alt="shaft-d8-128mm-h-demo01.jpg" width="380" /><img src="images/shaft-d8-96mm-h_shaft-d8-96mm-h-demo-a03.jpg" alt="shaft-d8-96mm-h-demo-a03.jpg" width="380" />

<img src="images/shaft-d8-96mm-h_shaft-d8-128mm-h-demo-a02.jpg" alt="shaft-d8-128mm-h-demo-a02.jpg" width="380" /><img src="images/shaft-d8-96mm-h_shaft-d8-128mm-h-demo-a04.jpg" alt="shaft-d8-128mm-h-demo-a04.jpg" width="380" />

 

 

 
