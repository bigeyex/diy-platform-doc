<img src="images/threaded-shaft-4x39mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 螺纹轴4x39mm
 
### 概述

螺纹轴 4*39mm通常作为轴配合齿轮、同步带轮、联轴器或一些轴承使用。它由不锈钢材料制成, 一端螺纹轴, 一端光轴。		

### 参数

- 直径：4mm
- 长度：39mm
- 材质：镀铬高碳钢

### 尺寸图纸

<img src="images/threaded-shaft-4x39mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/threaded-shaft-4x39mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">


 


