<img src="images/t6-l256mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# T6-L256丝杠传动装置
 
### 概述

Makeblock T6 L256mm 丝杆传动装置可以自锁，它适用于传动的高扭矩要求，如3D打印机的轴。丝杆也能承受比螺纹轴更大的负荷。	
### 参数

- 法兰铜螺母直径：8mm
- 法兰螺母长度：24mm
- 丝杠长度：256mm
- 丝杠直径：6mm
- 丝杠螺距：2mm
- 丝杠材质：不锈钢
- 法兰螺母材质：黄铜

### 丝杠尺寸图纸

<img src="images/t6-l256mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 铜螺母尺寸图纸

<img src="images/t6-l256mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
 
### 搭建案例

<img src="images/t6-l256mm-3.jpg" style="width:700;padding:5px 5px 15px 0px;">

<img src="images/t6-l256mm-4.jpg" style="width:700;padding:5px 5px 15px 0px;">

 

 
