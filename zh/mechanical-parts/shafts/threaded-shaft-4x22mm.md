<img src="images/threaded-shaft-4x22mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 螺纹轴4x22mm
 
### 概述

螺纹轴 4*22mm通常作为轴配合齿轮、同步带轮、联轴器或一些轴承使用。它由不锈钢材料制成, 一端螺纹轴, 一端光轴。