<img src="images/d-shaft-4x56mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# D型轴4x56mm
 
### 概述

D轴4x56毫米通常与作为轴、齿轮,皮带轮、联轴器或轴承配合使用。

### 参数

- 直径：4mm
- 长度：56mm
- 材质：镀铬不锈钢

### 尺寸图纸

<img src="images/d-shaft-4x56mm-1.png" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/d-shaft-4x56mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
 
