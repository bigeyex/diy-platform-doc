<img src="images/shaft-collar-8mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 轴套8mm
 
### 概述

利用8mm轴套与无头螺丝的完美配合能起到很好的固定作用。与 Makeblock 大多数M8零件兼容。	

### 参数

- 内径：8mm
- 材质：6061铝

### 尺寸图纸

<img src="images/shaft-collar-8mm-0.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/shaft-collar-8mm-1.png" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/shaft-collar-8mm-2.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/shaft-collar-8mm-3.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/shaft-collar-8mm-4.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/shaft-collar-8mm-5.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/shaft-collar-8mm-6.jpg" style="width:400;padding:5px 5px 15px 0px;">


 

 

 
