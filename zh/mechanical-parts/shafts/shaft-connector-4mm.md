<img src="images/shaft-connector-4mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 传动固定盘
 
### 概述

传动类通用型零件，可用于电机或4mm轴与同步带轮、齿轮及连接件之间的传动连接。

### 参数

- 内径：4mm
- 材质：6061铝

### 尺寸图纸
 
<img src="images/shaft-connector-4mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/shaft-connector-4mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">