<img src="images/shaft-collar-4mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 轴套4mm
 
### 概述

利用4mm轴套与无头螺丝的完美配合能起到很好的固定作用。与 Makeblock 大多数M4零件兼容。	

### 参数

- 外径：10mm
- 内径：4mm
- 宽度：5mm

### 尺寸图纸

<img src="images/shaft-collar-4mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/shaft-collar-4mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">

