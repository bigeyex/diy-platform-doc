<img src="images/plate-o1.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 连接片O1
 
### 概述

连接片O1设计为连接45°、90°或135°的机械零件。在机械结构中使用它，你将有更多的角度选择。

### 参数

- 材质：60601铝
- 厚度：3mm


### 尺寸图纸

<img src="images/plate-o1-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例
 
<img src="images/plate-o1-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-o1-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-o1-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-o1-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-o1-6.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-o1-7.jpg" style="width:300;padding:5px 5px 15px 0px;">


