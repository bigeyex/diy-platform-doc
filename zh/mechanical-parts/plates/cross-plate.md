<img src="images/cross-plate.jpg" style="width:400;padding:5px 5px 15px 0px;">


# 十字连接片
 
### 概述

十字连接片是DIY的Makeblock零件之一。 它采用6061铝合金铝挤压技术制成。 与所有Makeblock零件兼容，可用于构建各种结构。 而且还有助于在mBot Ranger中安装和修复Auriga的情况。

### 参数

- 材质：6061铝
- 厚度：3mm
- 长度：104mm
- 宽度：66mm

