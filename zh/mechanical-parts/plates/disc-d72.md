<img src="images/disc-d72.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 圆盘D72
 
### 概述

D72圆盘是用来搭建120°中心对称结构的理想选择，比如三轮汽车等，另外，D72圆盘也可以作为辅助件，配合各种梁来搭建系统框架。

### 参数
- 厚度 3mm
- 外径 72mm
- 材质 6061挤压铝合金

### 功能特性
- 由铝挤压（高强度）制成，表面阳极氧化
- 高强度
- 耐磨
- 高刚度

### 使用说明
中心为8毫米的通孔，与8毫米轴或轴承兼容，由于安装孔为M3，因此可与直流电机-25和步进电机连接。带有M4安装孔和与makeblock组件兼容的4毫米安装槽。

### 尺寸图纸

<img src="images/disc-d72-1.png" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例
 
<img src="images/disc-d72-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/disc-d72-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/disc-d72-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/disc-d72-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/disc-d72-6.jpg" style="width:300;padding:5px 5px 15px 0px;">