<img src="images/plate-7-9-b.jpg" style="width:400;padding:5px 5px 15px 0px;">


# 连接片7x9
 

### 概述

连接片是最常用的机械部件，可以与各种梁连接以构建机器人结构。 板上的各种孔径也让您与轴承和电机轻松连接。	

### 参数

- 材质：6061铝
- 厚度：2mm
- 长度：72mm
- 宽度：56mm

### 尺寸图纸

<img src="images/plate-7-9-b-1.jpg" style="width:700;padding:5px 5px 15px 0px;">
