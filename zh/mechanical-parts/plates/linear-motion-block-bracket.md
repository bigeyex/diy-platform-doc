<img src="images/linear-motion-block-bracket-a.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 直线导轨滑块连接器A
 

### 概述

Makeblock直线导轨滑块连接器A作为直线运动引导机器人的必要部分。 它具有平滑的连接性和灵活性，适用于多个直线运动结构。	

### 参数

- 材质：6061铝
- 长度：40mm
- 宽度：40mm
- 高度：13mm

