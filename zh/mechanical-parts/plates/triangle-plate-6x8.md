<img src="images/triangle-plate-6x8.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 三角连接片6x8
 
### 概述


连接片是最常用的机械部件，可以与各种梁连接以构建机器人结构。 板上的各种孔径也让您轻松连接轴承和电机。


### 参数

- 材质：60601铝
- 厚度：3mm


### 尺寸图纸

<img src="images/triangle-plate-6x8-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例
 
<img src="images/triangle-plate-6x8-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/triangle-plate-6x8-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/triangle-plate-6x8-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/triangle-plate-6x8-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

 

 
