<img src="square-beam/square-beam.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# 单孔方梁

### 概述

单孔方梁是 Makeblock 平台常用的机械零件，与大多数 Makeblock 平台的机械零件兼容，单孔方梁的表面上有各种安装孔，两端有螺纹孔，可以方便安装在其他结构上。

### 参数
- 长度：16-184mm
- 尺寸规格（mm）：016、032、056、072、088、104、120、136、152、168、184
- 横截面：8x8mm
- 材质：6061挤压铝合金

### 功能特性
- 高耐磨
- 高强度
- 高刚度

### 使用说明

通过梁上的通孔、中间的螺纹槽以及端面上的螺纹孔，梁与梁之间可以相互连接，也可以与支架、滑轨、轴、电机等各类零件连接。

### 搭建案例

<img src="square-beam/square-beam-5.jpg" style="width:300px;padding:5px 5px 15px 0px;">
<img src="square-beam/square-beam-6.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="square-beam/square-beam-7.jpg" style="width:300px;padding:5px 5px 15px 0px;">
<img src="square-beam/square-beam-8.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="square-beam/square-beam-9.jpg" style="width:300px;padding:5px 5px 15px 0px;">
<img src="square-beam/square-beam-10.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="square-beam/square-beam-11.jpg" style="width:300px;padding:5px 5px 15px 0px;">
<img src="square-beam/square-beam-12.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="square-beam/square-beam-13.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="square-beam/square-beam-1.png" style="width:300px;padding:5px 5px 15px 0px;">
<img src="square-beam/square-beam-2.jpg" style="width:300px;padding:5px 5px 15px 0px;">

<img src="square-beam/square-beam-3.jpg" style="width:300px;padding:5px 5px 15px 0px;">
<img src="square-beam/square-beam-4.jpg" style="width:300px;padding:5px 5px 15px 0px;">




