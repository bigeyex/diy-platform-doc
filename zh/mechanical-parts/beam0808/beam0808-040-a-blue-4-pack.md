# Beam0808-040-A


![](images/beam0808-040-a-blue-4-pack_Beam0808-040-A.jpg)

**Description:**

Makeblock
Beam0808 is one of the most frequently used part in Makeblock platform,
it compatible with most Makeblock motion and structure
components. 

** **

**Features:**

-   Made
    from 6061 aluminum extrusion, anodized surface. Excellent strength
    and twist resistance.
-   With
    holes on 16mm increments, can be drilled for 4mm hardware.
-   Threaded
    slot enables easy and flexible connection.
-   Cross-sectional
    area 8x8mm, length 40mm.
-   Sold
    in Packs of 4.

 

**Size
Charts(mm):**

 ![00808-.jpg](images/00808-.jpg)

 

**Demo:**
