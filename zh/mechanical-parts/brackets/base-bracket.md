<img src="images/base-bracket.jpg" style="padding:5px 5px 15px 0px;">

# 主控板支架B
 
### 概述

主控板支架B是一种多用途支架，可用于Arduino、Meduino甚至树莓派。

主控板支架B比主控板支架A大，有更多的孔与电子板和机械部件连接。此外，支架上的两个最大孔将允许RJ25电缆穿过。

包括塑料铆钉3075、塑料铆钉4060和塑料铆钉4100。

用户可选择塑料铆钉3075与Arduino或Meduino连接底座支架，选择塑料铆钉4060与电池座连接底座支架，选择塑料铆钉4100与底板连接底座支架。

### 参数

- 材质：透明亚克力
- 厚度：3mm

### 尺寸图纸

<img src="images/base-bracket-1.jpg" style="width:800;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/base-bracket-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

 <img src="images/base-bracket-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

 <img src="images/base-bracket-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

 <img src="images/base-bracket-5.jpg" style="width:300;padding:5px 5px 15px 0px;">


 

 
