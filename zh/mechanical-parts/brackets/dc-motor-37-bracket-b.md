# DC Motor-37 Bracket B

![](images/dc-motor-37-bracket-b-gold_DC-Motor-37-Bracket-B.jpg)

**Description:**

Makeblock
Motor Bracket-37 B is another bracket for DC Motor-37. With more
mounting holes and slots, you can easier attach Motor-37 to most
Makeblock components.

 

**Features:**

-   Compatible
    with 37mm DC motors
-   Made
    from aluminum extrusion(high strength)
-   Anodized
    surface
-   Sold
    in single pack, 6 Screw M3\*8 included.

 

**Size
Charts(mm):**

**<img src="images/dc-motor-37-bracket-b-gold_b-8-91-11q-w3hb-mlb-kmw.jpg" alt="b-8-91-11q-w3hb-mlb-kmw.jpg" width="760" />**

 

**Demo:**

<img src="images/dc-motor-37-bracket-b-gold_61806-d3.jpg" alt="61806-d3.jpg" width="380" /><img src="images/dc-motor-37-bracket-b-gold_61806-d11.jpg" alt="61806-d11.jpg" width="380" />

<img src="images/dc-motor-37-bracket-b-gold_61806-d9.jpg" alt="61806-d9.jpg" width="380" /><img src="images/dc-motor-37-bracket-b-gold_61806-d5.jpg" alt="61806-d5.jpg" width="380" />

<img src="images/dc-motor-37-bracket-b-gold_61806-d14.jpg" alt="61806-d14.jpg" width="380" /><img src="images/dc-motor-37-bracket-b-gold_61806-d4.jpg" alt="61806-d4.jpg" width="380" />

 
