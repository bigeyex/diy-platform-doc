<img src="images/8mm-bearing-bracket-a.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 8mm轴承支架A
 
### 概述

8mm轴承支架A有一个用于8毫米轴的滚珠轴承，通常用作8毫米轴的支架来支撑重型机构。它带有M4螺纹孔，你可以很容易地将其连接到makeblock机械零件上。

### 参数

配套使用轴承：8x16x5mm
材质：6061铝

### 尺寸图纸

<img src="images/8mm-bearing-bracket-a-1.jpg" style="width:800;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/8mm-bearing-bracket-a-2.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/8mm-bearing-bracket-a-3.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/8mm-bearing-bracket-a-4.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/8mm-bearing-bracket-a-5.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/8mm-bearing-bracket-a-6.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/8mm-bearing-bracket-a-7.jpg" style="width:400;padding:5px 5px 15px 0px;">
 

 

 
