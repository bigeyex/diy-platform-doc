<img src="images/57byg-stepper-motor-bracket.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 57步进电机支架
 
### 概述

57步进电机支架是用来安装固定57步进电机的，它与makeblock平台零件相兼容。

### 参数

- 长度：68mm
- 宽度：65mm
- 高度：68mm

### 尺寸图纸

<img src="images/57byg-stepper-motor-bracket-1.png" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/57byg-stepper-motor-bracket-2.jpg" style="width:300;padding:5px 5px 15px 0px;">


<img src="images/57byg-stepper-motor-bracket-3.png" style="width:300;padding:5px 5px 15px 0px;">


<img src="images/57byg-stepper-motor-bracket-4.png" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/57byg-stepper-motor-bracket-5.png" style="width:300;padding:5px 5px 15px 0px;">
 

