<img src="images/bracket-u1.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 支架U1
 
### 概述

支架U1通常用作伺服系统、电机和轴的结构支撑或连接点。它带有3个8mm中心孔可连接8mm轴，24个4mm孔可与makeblock平台零件兼容。

### 参数

- 材质：6061铝
- 厚度：3mm

### 尺寸图纸

<img src="images/bracket-u1-1.jpg" style="width:700;padding:5px 5px 15px 0px;">
 
### 搭建案例

<img src="images/bracket-u1-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/bracket-u1-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/bracket-u1-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/bracket-u1-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/bracket-u1-6.jpg" style="width:300;padding:5px 5px 15px 0px;">