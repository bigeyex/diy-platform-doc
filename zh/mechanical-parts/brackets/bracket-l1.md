<img src="images/bracket-l1.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 支架L1
 
### 概述

支架L1常用来加固直角结构，有两个直径为8mm的孔和16个M4安装孔。也可作为辅助件配合单孔梁、双孔梁、方梁来搭建系统框架。

### 参数

- 材质：6061铝
- 厚度：2mm

### 尺寸图纸

<img src="images/bracket-l1-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/bracket-l1-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/bracket-l1-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/bracket-l1-4.jpg" style="width:300;padding:5px 5px 15px 0px;">
 
<img src="images/bracket-l1-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/bracket-l1-6.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/bracket-l1-7.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/bracket-l1-8.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/bracket-l1-9.jpg" style="width:300;padding:5px 5px 15px 0px;">