<img src="images/u-bracket-b.jpg" style="width:400;padding:5px 5px 15px 0px;">

# U型支架B
 
### 概述

U形支架B通常用作轴、同步带轮18t、同步带轮62t、同步带轮90t等的结构支撑或连接点。

### 参数

- 厚度：3mm
- 材质：6061铝

### 尺寸图纸

<img src="images/u-bracket-b-1.jpg" style="width:700;padding:5px 5px 15px 0px;">
 
### 搭建案例

<img src="images/u-bracket-b-2.jpg" style="width:400;padding:5px 5px 15px 0px;">
 
<img src="images/u-bracket-b-3.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/u-bracket-b-4.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/u-bracket-b-5.png" style="width:400;padding:5px 5px 15px 0px;">