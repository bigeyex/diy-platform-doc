<img src="images/bracket-3x3.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 支架3x3
 
### 概述

支架可作为辅助件，配合单孔梁、双孔梁、方梁来搭建系统框架。

### 参数

- 厚度：2mm
- 材质：6061铝

### 尺寸图纸

<img src="images/bracket-3x3-1.png" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/bracket-3x3-2.png" style="width:600;padding:5px 5px 15px 0px;">
 


 
