<img src="images/versatile-motor-bracket.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 通用电机支架
 
### 概述

Makeblock通用电机支架比42BYG步进电机支架B功能更大，兼容Makeblock 42BYG步进电机和Makeblock 36直流齿轮电机。

### 参数

材质：6061铝
厚度：3mm

### 尺寸图纸

<img src="images/versatile-motor-bracket-1.png" style="width:700;padding:5px 5px 15px 0px;">


### 搭建案例
 
<img src="images/versatile-motor-bracket-2.png" style="width:700;padding:5px 5px 15px 0px;">

<img src="images/versatile-motor-bracket-3.png" style="width:700;padding:5px 5px 15px 0px;">