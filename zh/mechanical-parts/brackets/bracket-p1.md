<img src="images/bracket-p1.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 支架P1
 
### 概述

平面上4mm安装孔和两端的螺纹槽, 可以轻松地组装在其他结构上, 兼容大多数Makeblock机械部件。	

### 参数

- 材质：6061铝
- 长度：33mm
- 宽度：8mm
- 高度：10mm



 
