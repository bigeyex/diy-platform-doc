# Beam0824-192

![](images/slider-beam0824-192-blue-pair_Beam0824-192.jpg)

 <img src="images/slider-beam0824-192-blue-pair_60046-1.jpg" width="500" />

### What Is Slider Beam0824-192-Blue?

Slider Beam0824-192-Blue is a series of structural parts which can drive
other parts with tackles. On the surface there are assemble holes with
16mm distance between each other, so it can be used with other parts as
a whole structure.

### Features of the Slider Beam0824-176-Blue

- Made from 6061 aluminum extrusion, anodized surface;
- Threaded slot enables easy and flexible connection;
- With holes on 16mm increments;
- Sold in Packs of 2;

  

<img src="images/slider-beam0824-192-blue-pair_60046-2.jpg" width="500" />

### Building Examples:

<img src="images/slider-beam0824-192-blue-pair_60046-demo.jpg" width="1000" />

### Size Chart:

![](images/slider-beam0824-192-blue-pair_60046-size-chart.jpg)

### Specifications

- SKU: 60046                
- Product Name: Slide Beam0824-192-Blue (Pair)
- Total Weight: 90 g
- Package Content (Quantity x Part Name): 2 x Slide Beam0824-192-Blue
