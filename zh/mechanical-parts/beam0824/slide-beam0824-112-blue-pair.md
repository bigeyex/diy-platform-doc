# Beam0824-112

![](images/slide-beam0824-112-blue-pair_Beam0824-112.jpg)

 <img src="images/slide-beam0824-112-blue-pair_3L4A0303.jpg" width="500" />

### What Is Slide Beam0824-112?

Slide Beam0824-112 is a frequently-used mechanical part of Makeblock
platform，and also compatible with most Makeblock mechanical
components. There are various mounting holes and a threaded slot on the
plane of this beam which allow you to assemble on other structures
easily.

### Features

- Made from heavy-duty 6061 aluminum extrusion
- With holes on 16mm increments，and M4 threaded holes on both ends
- Cross-section area 8x24mm, length 112mm
- High wear resistance and high intensity
- Strong rigidity

<img src="images/slide-beam0824-112-blue-pair_3L4A0305.jpg" width="500" />

### DEMO

<img src="images/slide-beam0824-112-blue-pair_60026.jpg" width="800" />

### Size Charts

<img src="images/slide-beam0824-112-blue-pair_Size_Chart.png" width="800" />

### Specifications

- SKU: 60026
- Product Name: Slide Beam0824-112-Blue (Pair)
- Length: 112mm
- Cross-section Area: 8 x 24mm
- Gross Weight: 50g (1.76oz)
- Package Content (Quantity x Part Name): 2x Slide Beam0824-112

 
