# Beam0824-400

![](images/beam0824-400-blue-pair_Beam0824-400.jpg)

**Description:**

Makeblock Beam0824 is one of the most
frequently used part in Makeblock platform, it is compatible with most
Makeblock motion and structure components.

 

**Features:**

-   One of the most frequently used
    parts in Makeblock platform, it is compatible with all Makeblock
    motion and structure components.
-   Made from 6061 aluminum extrusion,
    anodized surface, Excellent strength and twist resistance.
-   Threaded slot enables easy and
    flexible connection.
-   With 16 holes and M4 thread holes
    on both ends.
-   Cross-sectional area 8x24mm, length
    400mm. 
-   Sold in Pair.

**Demo:**

<img src="images/beam0824-400-blue-pair_qq-20150328115729.png" alt="qq-20150328115729.png" width="761" />
