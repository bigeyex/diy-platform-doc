<img src="images/flange-bearing-4x8x3mm.jpg" style="width:400;padding:5px 5px 15px 0px;">


# 法兰轴承4x8x3
 
### 概述

适合用于同步带轮66T，同步带轮90T和P3支架

### 参数

- 内径：4mm
- 外径：8mm
- 法兰外径：9.2mm
- 材质：钢

### 尺寸图纸

<img src="images/flange-bearing-4x8x3mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/flange-bearing-4x8x3mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
 
