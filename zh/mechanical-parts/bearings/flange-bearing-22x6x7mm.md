# Flange Bearing 22x6x7mm


![](images/flange-bearing-22x6x7mm-pair_Flange-Bearing-22x6x7mm.jpg)

Product Description
-----------------------------------------------------------

Makeblock flange bearings are perfect
for adding to 42BYG Stepper Motor Bracket.

 

**Features:**  
 ?  Makes the rotating part virtually
friction-free.  
 ?  Diameters: 6mm (inside), 22mm
(outside)  
 ?  Sold in pack of 2.

 

************Size
Charts(mm):************

 <img src="images/flange-bearing-22x6x7mm-pair_size-chats.jpg" alt="size-chats.jpg" width="666" />

 

********Demo:********

 <img src="images/flange-bearing-22x6x7mm-pair_a.jpg" alt="a.jpg" width="699" />

<img src="images/flange-bearing-22x6x7mm-pair_b.jpg" alt="b.jpg" width="715" />

 
