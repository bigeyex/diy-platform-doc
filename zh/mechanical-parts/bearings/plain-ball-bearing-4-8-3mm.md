<img src="images/plain-ball-bearing-4-8-3mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 滚珠轴承4x8x3
 

### 概述
Makeblock普通滚珠轴承 4x8x3mm 可与 Makeblock 8mm轴承支架A一起使用，支持重型机构。

### 参数

- 内径：4mm
- 外径：8mm
- 宽度：3mm
- 材质：钢