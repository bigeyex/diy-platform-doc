<img src="images/plain-ball-bearing-8-16-5mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 滚珠轴承8x16x5
 

### 概述
Makeblock普通滚珠轴承 8x16x5mm 可与 Makeblock 8mm轴承支架A一起使用，支持重型机构。

### 参数

- 内径：8mm
- 外径：16mm
- 宽度：5mm
- 材质：钢

### 尺寸图纸
 
<img src="images/plain-ball-bearing-8-16-5mm-1.png" style="width:700;padding:5px 5px 15px 0px;">


### 搭建案例

<img src="images/plain-ball-bearing-8-16-5mm-2.png" style="width:700;padding:5px 5px 15px 0px;">