<img src="images/plane-bearing-turntable-d34x24mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 平面轴承转盘D34x24mm
 

### 概述

Makeblock平面轴承转盘可用作高负载和高精度设计中的运动接头或可旋转转盘.
它具有运动稳定性和平滑度的平面轴承。 它也可以在Makeblock mDrawbot中使用。

### 参数

- 直径：34mm
- 长度：24mm
- 材质：钢

### 尺寸图纸

<img src="images/plane-bearing-turntable-d34x24mm-1.png" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例
 
<img src="images/plane-bearing-turntable-d34x24mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
 

 
