<img src="beam2424/images/beam2424.jpg" style="width:600;padding:5px 5px 15px 0px;">

# 方形梁

### 概述

方形梁是 Makeblock 平台常用的机械零件，与大多数 Makeblock 平台的机械零件兼容，方形梁的表面上有各种安装孔和侧面安装槽，可以方便安装在其他结构上。

### 参数
- 长度：74 - 504mm
- 尺寸规格（mm）：072、088、104、120、136、152、168、184、248、312、504
- 横截面：24 x 24mm
- 材质：6061挤压铝合金

### 功能特性
- 高耐磨
- 高强度
- 高刚度

### 使用说明

通过梁上的通孔和侧面的安装槽、梁与梁之间可以相互连接，也可以与支架、滑轨、轴、电机等各类零件连接。

### 规格表

<table style="text-align:center;cellpadding:12px;cellspacing:10px;">

<tr>
<td><img src="beam2424/images/beam2424-072.jpg" width="170px;"><br><p>方形梁-072</p></td>
<td><img src="beam2424/images/beam2424-088.jpg" width="170px;"><br><p>方形梁-088</p></td>
<td><img src="beam2424/images/beam2424-104.jpg" width="170px;"><br><p>方形梁-104</p></td>
<td><img src="beam2424/images/beam2424-120.jpg" width="170px;"><br><p>方形梁-120</p></td>
</tr>

<tr>
<td><img src="beam2424/images/beam2424-136.jpg" width="170px;"><br><p>方形梁-136</p></td>
<td><img src="beam2424/images/beam2424-152.jpg" width="170px;"><br><p>方形梁-152</p></td>
<td><img src="beam2424/images/beam2424-168.jpg" width="170px;"><br><p>方形梁-168</p></td>
<td><img src="beam2424/images/beam2424-184.jpg" width="170px;"><br><p>方形梁-184</p></td>
</tr>

<tr>
<td><img src="beam2424/images/beam2424-248.jpg" width="170px;"><br><p>方形梁-248</p></td>
<td><img src="beam2424/images/beam2424-312.jpg" width="170px;"><br><p>方形梁-312</p></td>
<td><img src="beam2424/images/beam2424-504.jpg" width="170px;"><br><p>方形梁-504</p></td>
</tr>

</table>
