<img src="images/timing-belt-5m-open-end.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 开口同步带
 
### 概述

开口同步带是根据热塑特性由开口带加工而成的同步带，可以接驳成任何长度的同步带。

### 参数

- 同步带标准：MXL
- 长度规格：1m、2m、3m、5m
- 材质：氯丁橡胶
