<img src="images/belt-connector.jpg" style="width:300;padding:5px 5px 15px 0px;">

# 同步带固定片
 
### 概述

在很多情况下，市场上的固定尺寸的同步带不能满足普通的设计需求，而使用开放式同步带时固定开放式同步带时一个大问题，而Makeblock 同步带固定片可以解决这个问题，它通常用于将开放式同步带固定在一起。 

### 参数

- 尺寸：24x24mm
- 厚度：3mm
- 材质：6061铝

 
