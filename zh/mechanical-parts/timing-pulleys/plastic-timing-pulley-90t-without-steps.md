<img src="images/plastic-timing-pulley-90t-without-steps-new.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 注塑带轮90T无台阶
 
### 概述

同步带传动通过传动带内表面上等距分布的横向齿和带轮上的相应齿槽的啮合来传递运动。具有转动比准确以及结构紧凑的优点。

### 参数

- 齿数：90
- 厚度：8mm
- 材质：塑料

### 功能特性

- 与MXL同步带兼容。
- 可用作小车车轮。
- 带12个M4孔

### 使用说明

不带台阶的 Makeblock 塑料同步带轮90T由塑料制成，重量较轻。它被用作小车的轮子。此外，其中12个M4孔也可以用作 Makeblock 平台的底板。

