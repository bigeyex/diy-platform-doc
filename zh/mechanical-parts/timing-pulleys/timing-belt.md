<img src="images/timing-belt.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 同步带
 
### 概述

Makeblock同步带可用于连接同步带轮驱动运动。	

### 参数

- 同步带标准：MXL
- 规格：123T、140T、216T、160T、378T
- 宽度：6.6mm
- 材质：氯丁橡胶

### 搭建案例

<img src="images/timing-belt-1.png" style="width:800;padding:5px 5px 15px 0px;">



