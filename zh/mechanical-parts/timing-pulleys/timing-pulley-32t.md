<img src="images/timing-pulley-32t.jpg" style="width:400;padding:5px 5px 15px 0px;">

### 同步带轮32T
 
### 概述

同步带传动通过传动带内表面上等距分布的横向齿和带轮上的相应齿槽的啮合来传递运动。具有转动比准确以及结构紧凑的优点。

### 参数

- 齿数：32
- 中心孔径：4mm
- 材质：6061铝合金

### 功能特性

- 与MXL同步带兼容。

### 使用说明

一般用作同步带传动的主动轮，使用M3顶丝与4mm电机轴固定。

### 尺寸图纸

<img src="images/timing-pulley-32t-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/timing-pulley-32t-2.jpg" style="width:600;padding:5px 5px 15px 0px;">
 
