<img src="images/timing-pulley-slice-90t-b.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 同步带轮90T挡片B
 
### 概述

MakeBlock同步带轮片90T-B是新版本的同步带轮片90T。同步带轮片90T-B作为同步带轮90T的“挡板”，工作时可防止同步带打滑。

### 参数

- 内径：42mm
- 外径：62mm
- 材质：6061铝

### 尺寸图纸

<img src="images/timing-pulley-slice-90t-b-0.jpg" style="width:800;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/timing-pulley-slice-90t-b-1.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/timing-pulley-slice-90t-b-2.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/timing-pulley-slice-90t-b-3.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/timing-pulley-slice-90t-b-4.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="images/timing-pulley-slice-90t-b-5.jpg" style="width:400;padding:5px 5px 15px 0px;">



