# Linear Motion Guide Rail

![](images/linear-motion-guide-rail-single-pack_Linear-Motion-Guide-Rail.jpg)

 <img src="images/linear-motion-guide-rail-single-pack_IMG_4065__92256-800x800.jpg" width="500" />

  

### What Is Linear Motion Guide Rail?

Linear motion guides permit directional movement of a carriage along
their length. It will be used in connect with Linear Motion Block.

Makeblock linear motion guide can be use in high-precision linear motion
applications, such as 3D Printer,computer numerical control (CNC)
equipment or other linear motion systems.

### Features

- Steel material and stiffness;

- With holes on 48mm increments that can be installed with M4 screw;

- High accuracy and ability to work at high speed for long periods with
total reliability.

  

<img src="images/linear-motion-guide-rail-single-pack_sell01__05249-800x800.jpg" width="500" />

  

### Demo:

![](images/linear-motion-guide-rail-single-pack_86060-demo.jpg)

### Size Chart (mm):

![](images/linear-motion-guide-rail-single-pack_86020-size-chart.jpg)

None
