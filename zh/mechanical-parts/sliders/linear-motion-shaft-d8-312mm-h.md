# Linear Motion Shaft D8x312mm


![](images/Linear-Motion-Shaft-D8×312mm.jpg)

**Description:**  
This
linear motion shaft of 8 mm in diameter and 312 mm in length is chrome
plated and case hardened, and is suitable for use with slide units in
linear motion applications. The high-carbon steel shaft is chrome plated
for corrosion resistance, case hardened for wear resistance, and
precision ground for consistent ball bushing radial clearance.  
**Features:**

-   With
    thread holes on two ends
-   Round
    steel shaft for use with slide units in linear motion
    applications.
-   Chrome
    plated for corrosion resistance
-   Case
    hardened for wear resistance
-   Precision
    ground for consistent ball bushing radial clearance

 

**SizeChart(mm):**

<img src="images/linear-motion-shaft-d8-312mm-h_312mm-sizechart.png" alt="312mm-sizechart.png" width="760" />

 

 
