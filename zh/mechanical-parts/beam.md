

<table style="text-align:center;cellpadding:12px;cellspacing:30px;">
<tr>
<td><img src="Beam/images/beam-1.jpg" width="300px;"></td>
<td><img src="Beam/images/beam-3.jpg" width="300px;"></td>
</tr></table>

# 精益梁

### 概述

精益梁是 Makeblock 平台新研发的梁类零件，通过截面改型设计，使得同一根梁具备多种接驳方式：
1）四个快接平面能够与多种接头实现快速拆装；
2）四个带螺纹槽的平面能够与现有平台体系零件相互补充，丰富了与平台零件的连接方式。

结实的 6061 铝挤压制造工艺与精湛的表面阳极氧化处理，赋予了精益梁亮丽的金属质感。
同时两端配有 M4 螺纹孔，便于固定在任意位置。


截面尺寸为 24*24mm，长度从 72mm 到 504mm 不等。
<img src="Beam/images/beam-2.jpg" style="width:600;padding:5px 5px 15px 0px;">


### 参数
- 长度：72-504mm
- 尺寸规格（mm）：072、136、248、312、424、504
- 横截面：24*24mm
- 材质：6061挤压铝合金


### 功能特性

- 高耐磨
- 高强度
- 高刚度

### 使用说明

通过梁上的螺纹槽、端面的螺纹孔、精益梁连接件以及转接件，梁与梁之间可以相互连接，也可以与  Makeblock 平台的支架、滑轨、电机等各类零件连接。

### 实际搭建案例

<img src="Beam/images/beam-4.jpg" style="width:600;padding:5px 5px 15px 0px;">
<img src="Beam/images/beam-5.jpg" style="width:600;padding:5px 5px 15px 0px;">

### 精益梁连接件及连接方式介绍
<p>（1）精益梁平行接头连接：使用精益梁平行接头可以将两根精益梁平行连接起来。</p>
<table style="text-align:center;cellpadding:12px;cellspacing:30px;">
<tr>
<td><img src="Beam/images/beam-6.jpg" width="200px;"></td>
<td><img src="Beam/images/beam-7.jpg" width="200px;"></td>
</tr>
<tr><td><small>精益梁平行接头</td>
<td><small>精益梁平行接头连接示例</td></tr>
</table>

<p>（2）精益梁十字交叉接头连接：使用精益梁十字交叉接头可以将精益梁十字交叉连接。</p>
<table style="text-align:center;cellpadding:12px;cellspacing:30px;">
<tr>
<td><img src="Beam/images/beam-8.jpg" width="200px;"></td>
<td><img src="Beam/images/beam-9.jpg" width="200px;"></td>
</tr>
</tr>
<tr><td><small>精益梁十字交叉接头</td>
<td><small>精益梁十字交叉接头连接示例</td></tr></table>

<p>（3）精益梁外接接头：使用精益梁外接接头可以将两根精益梁进行T型连接。</p>
<table style="text-align:center;cellpadding:12px;cellspacing:30px;">
<tr>
<td><img src="Beam/images/beam-10.jpg" width="200px;"></td>
<td><img src="Beam/images/beam-11.jpg" width="200px;"></td>
</tr>
<tr><td><small>精益梁外接接头</td>
<td><small>精益梁外接接头连接示例</td></tr></table>

<p>（4）精益梁直角加强接头：使用精益梁直角加强接头可以加固已经连接好的直角结构。</p>
<table style="text-align:center;cellpadding:12px;cellspacing:30px;">
<tr>
<td><img src="Beam/images/beam-12.jpg" width="200px;"></td>
<td><img src="Beam/images/beam-13.jpg" width="200px;"></td>
</tr>
<tr><td><small>精益梁直角加强接头</td>
<td><small>精益梁直角加强接头连接示例</td></tr></table>

<p>（5）精益梁延长接头：精益梁延长接头可以将两根精益梁连接起来以达到增加长度的作用。</p>
<table style="text-align:center;cellpadding:12px;cellspacing:30px;">
<tr>
<td><img src="Beam/images/beam-14.jpg" width="200px;"></td>
<td><img src="Beam/images/beam-15.jpg" width="200px;"></td>
</tr>
<tr><td><small>精益梁延长接头</td>
<td><small>精益梁延长接头连接示例</td></tr></table>

<p>（6）精益梁45度接头：使用精益梁45度接头可以搭建出45度的结构。</p>
<table style="text-align:center;cellpadding:12px;cellspacing:30px;">
<tr>
<td><img src="Beam/images/beam-16.jpg" width="200px;"></td>
<td><img src="Beam/images/beam-17.jpg" width="200px;"></td>
</tr>
<tr><td><small>精益梁45度接头</td>
<td><small>精益梁45度接头连接示例</td></tr></table>

<p>（7）精益梁转接件A：使用精益梁转接件A可以方便地将 Makeblock 平台的其他金属零件和精益梁连接起来，便于搭建出更多形态。</p>
<table style="text-align:center;cellpadding:12px;cellspacing:30px;">
<tr>
<td><img src="Beam/images/beam-18.jpg" width="200px;"></td>
<td><img src="Beam/images/beam-19.jpg" width="200px;"></td>
</tr>
<tr><td><small>精益梁转接件A</td>
<td><small>精益梁转接件A连接 Makeblock 平台金属件示例</td></tr></table>

<p>（8）精益梁转接件B：使用精益梁转接件B可以方便地将 Makeblock 平台的其他金属零件和精益梁连接起来，便于搭建出更多形态。</p>
<table style="text-align:center;cellpadding:12px;cellspacing:30px;">
<tr>
<td><img src="Beam/images/beam-20.jpg" width="200px;"></td>
<td><img src="Beam/images/beam-21.jpg" width="200px;"></td>
</tr>
<tr><td><small>精益梁转接件B</td>
<td><small>精益梁转接件B连接 Makeblock 平台金属件示例</td></tr></table>

<p>（9）精益梁端面螺纹孔连接：精益梁端面的四个螺纹孔和 Makeblock 平台的蓝色金属件孔位兼容，因此可以用螺丝将 Makeblock 平台的金属零件连接到精益梁端面。</p>
<table style="text-align:center;cellpadding:12px;cellspacing:30px;">
<tr>
<td><img src="Beam/images/beam-22.jpg" width="300px;"></td>
</tr>
<tr><td><small>精益梁端面螺纹孔连接示例</td>
</tr></table>

<p>（10）精益梁管身螺纹槽连接：精益梁管身的螺纹槽和 Makeblock 平台的蓝色金属件兼容，因此可以用螺丝将 Makeblock 平台的金属零件连接到精益梁管身的螺纹槽。</p>
<table style="text-align:center;cellpadding:12px;cellspacing:30px;">
<tr>
<td><img src="Beam/images/beam-23.jpg" width="300px;"></td>
</tr>
<tr><td><small>精益梁管身螺纹槽连接示例</td></tr></table>