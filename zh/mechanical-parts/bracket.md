# 支架

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="brackets/8mm-bearing-bracket-a.html" target="_blank"><img src="brackets/images/8mm-bearing-bracket-a.jpg" width="150px;"></a><br>
<p>8mm轴承支架 A</p></td>

<td width="25%;"><a href="brackets/36mm-motor-bracket.html" target="_blank"><img src="brackets/images/36mm-motor-bracket.jpg" width="150px;"></a><br>
<p>36mm电机支架</p></td>

<td width="25%;"><a href="brackets/42byg-stepper-motor-bracket-b-blue.html" target="_blank"><img src="brackets/images/42byg-stepper-motor-bracket-b.jpg" width="150px;"></a><br>
<p>42步进电机支架</p></td>

<td width="25%;"><a href="brackets/57byg-stepper-motor-bracket-black.html" target="_blank"><img src="brackets/images/57byg-stepper-motor-bracket.jpg" width="150px;"></a><br>
<p>57步进电机支架（黑）</p></td>
</tr>

<tr>
<td><a href="brackets/base-bracket.html" target="_blank"><img src="brackets/images/base-bracket.jpg" width="150px;"></a><br>
<p>主控板支架 B</p></td>
<td><a href="brackets/bracket-3x3.html" target="_blank"><img src="brackets/images/bracket-3x3.jpg" width="150px;"></a><br>
<p>支架 3*3</p></td>
<td><a href="brackets/bracket-l1.html" target="_blank"><img src="brackets/images/bracket-l1.jpg" width="150px;"></a><br>
<p>支架 L1</p></td>
<td><a href="brackets/bracket-p1.html" target="_blank"><img src="brackets/images/bracket-p1.jpg" width="150px;"></a><br>
<p>支架 P1</p></td>
</tr>

<tr>
<td><a href="brackets/bracket-p3.html" target="_blank"><img src="brackets/images/bracket-p3.jpg" width="150px;"></a><br>
<p>支架 P3</p></td>
<td><a href="brackets/bracket-u1.html" target="_blank"><img src="brackets/images/bracket-u1.jpg" width="150px;"></a><br>
<p>支架 U1</p></td>
<td><a href="brackets/dc-motor-25-bracket-b.html" target="_blank"><img src="brackets/images/dc-motor-25-bracket-b.jpg" width="150px;"></a><br>
<p>25电机支架</p></td>
<td><a href="brackets/dc-motor-37-bracket.html" target="_blank"><img src="brackets/images/dc-motor-37-bracket-b-gold_DC-Motor-37-Bracket-B.jpg" width="150px;"></a><br>
<p>37电机支架</p></td>
</tr>

<tr>
<td><a href="brackets/meds15-servo-motor-bracket.html" target="_blank"><img src="brackets/images/mecds15-servo-motor-bracket.jpg" width="150px;"></a><br>
<p>MECDS-150 舵机支架</p></td>
<td><a href="brackets/u-bracket-b.html" target="_blank"><img src="brackets/images/u-bracket-b.jpg" width="150px;"></a><br>
<p>U形支架 B</p></td>
<td><a href="brackets/u-bracket-c.html" target="_blank"><img src="brackets/images/u-bracket-c.jpg" width="150px;"></a><br>
<p>U形支架 C</p></td>
<td><a href="brackets/versatile-motor-bracket.html" target="_blank"><img src="brackets/images/versatile-motor-bracket.jpg" width="150px;"></a><br>
<p>通用电机支架</p></td>
</tr>

</table>