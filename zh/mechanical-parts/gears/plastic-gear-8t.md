<img src="images/plastic-gear-8t.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# 塑料齿轮8T

### 概述

齿轮机构是现代机械中应用最广泛的传动机构之一，它可以用来传递空间任意两轴之间的运动和动力，具有传动功率范围大、效率高、传动比准确、使用寿命长、工作安全可靠等特点。

### 参数

- 齿数：   Z : 8
- 模数：   m=1
- 压力角： α=20°
- 材质： 尼龙

### 功能特性

- 传动比稳定。传动比稳定是对传动性能的基本要求，齿轮传动因此特点而广泛应用
安装简单无需螺丝固定
- 摩擦阻力小传动效率高，噪音小

### 使用说明

8T属于齿轮传动系列零件，一般与电机轴配合，作为传动零件使用。与电机轴是过盈配合，装配时用力把齿轮压进直径4mm电机轴里，拆卸时请用螺母扳手按搭建图的方法拆卸。如果齿轮承受的力很大，且要求电机轴与齿轮不松动，则需要涂抹点胶水，如502胶水。
