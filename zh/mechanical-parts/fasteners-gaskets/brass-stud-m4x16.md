<img src="images/brass-stud-m4×16.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# M4x16 铜螺柱

### 概述

铜螺柱 M4x16 带有螺纹槽，可与M4梁，轴或者螺丝兼容，连接Makeblock各种零件。	

### 参数

- 材质：黄铜

### 尺寸图纸

<img src="images/brass-stud-m4×16-1.png" style="width:800px;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/brass-stud-m4×16-2.png" style="width:800px;padding:5px 5px 15px 0px;">