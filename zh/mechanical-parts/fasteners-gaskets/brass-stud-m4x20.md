<img src="images/brass-stud-m4×16.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# M4x20 铜螺柱

### 概述

铜螺柱 M4x20 带有螺纹槽，可与M4梁，轴或者螺丝兼容，连接Makeblock各种零件。	

### 参数

- 材质：黄铜

### 尺寸图纸

<img src="images/brass-stud-m4×20.png" style="width:800px;padding:5px 5px 15px 0px;">