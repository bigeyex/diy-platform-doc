<img src="images/nylon-lock-nut-4mm.jpg" style="padding:5px 5px 15px 0px;">

# 防松螺母M4
 
### 概述

防松螺母比普通螺母更加能紧固M4型号螺丝，并防止松落。

### 参数

- 材质：不锈钢
