<img src="images/rivet-r4060.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# R4060 铆钉

### 概述

铆钉用于各种零件之间的加固连接。

### 参数

- 材质：塑料

### 尺寸图纸

<img src="images/rivet-r4060-1.png" style="width:500px;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/rivet-r4060-2.png" style="width:500px;padding:5px 5px 15px 0px;">
