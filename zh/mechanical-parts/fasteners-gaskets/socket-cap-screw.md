# 半圆头螺丝

### 概述

半圆头螺丝是一种新型的螺丝，它具有较小的头部，可以解决大多数螺丝头干扰其他部件的问题。 该螺丝需要使用HEX螺丝刀2.5mm进行紧固。

### 参数

- 材质：不锈钢

## 半圆头螺丝M4x8

<img src="images/socket-cap-screw-m4x8.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## 半圆头螺丝M4x14

<img src="images/socket-cap-screw-m4x14.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## 半圆头螺丝M4x16

<img src="images/socket-cap-screw-m4x16.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## 半圆头螺丝M4x22

<img src="images/socket-cap-screw-m4x22.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## 半圆头螺丝M4x30

<img src="images/socket-cap-screw-m4x30.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## 半圆头螺丝M4x35

<img src="images/socket-cap-screw-m4x35.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## 半圆头螺丝M4x40

<img src="images/socket-cap-screw-m4x40.jpg" style="width:360px;padding:5px 5px 15px 0px;">

