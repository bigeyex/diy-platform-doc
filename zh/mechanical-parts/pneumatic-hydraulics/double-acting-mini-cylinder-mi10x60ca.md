# 10x60mm 双作用气缸

![](images/single-acting-mini-cylinder-pack-msi8x20ufa_Single-Acting-Mini-Cylinder-Pack-MSI8X20UFA.jpg)

### 概述

气缸是气压传动中将压缩气体的压力能转换为机械能的气动执行元件。10x60mm气缸是一个双作用式的微型气缸，安装方式为面板安装、双支架安装、尾架安装等，与M5-φ4快插接头配套使用。

### 参数

- 缸径：10mm
- 行程：60mm
- 安装螺母：M12X1.25
- 推力：39.3N
- 拉力：33N
- 气压：0.5Mpa

### 尺寸图纸

<img src="images/single-acting-mini-cylinder-pack-msi8x20ufa_msi8x20ufa-1013.png" alt="msi8x20ufa-1013.png" width="800" />

### 搭建案例

<img src="images/single-acting-mini-cylinder-pack-msi8x20ufa_1128.jpg" alt="1128.jpg" width="700" />

<img src="images/single-acting-mini-cylinder-pack-msi8x20ufa_1163.jpg" alt="1163.jpg" width="700" />

<img src="images/single-acting-mini-cylinder-pack-msi8x20ufa_single.jpg" alt="single.jpg" width="700" /><img src="images/single-acting-mini-cylinder-pack-msi8x20ufa_single-acting-cylinder.jpg" alt="single-acting-cylinder.jpg" width="700" />
