# 两通迷你电磁阀

![](images/solenoid-valve-dc-12v-0520e_Solenoid-Valve-DC-12V-0520E.jpg)

### 概述
MAKEBLOCK电磁阀DC12V/0520E是一种微型阀体，广泛应用于工业装置和DIY工程中。

### 参数

- 电压：DC12V
- 模式：两位三通
- 绝缘等级：A

### 尺寸图纸

<img src="images/solenoid-valve-dc-12v-0520e_59001-size.png" alt="59001-size.png" width="729" />
