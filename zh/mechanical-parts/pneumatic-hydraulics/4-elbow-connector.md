# φ4 L形接头

![](images/φ4-Elbow-Connector.jpg)

### 概述

与φ4气管、M5螺纹弯管接头配套使用，可360°旋转，使气管安装布置简单、美观。

### 尺寸图纸

<img src="images/4-elbow-connector-4-pack_59010.png" alt="59010.png" width="587" />

### 搭建案例

<img src="images/4-elbow-connector-4-pack_59050-connector.jpg" alt="59050-connector.jpg" width="700" />

<img src="images/4-elbow-connector-4-pack_59010connector2.jpg" alt="59010connector2.jpg" width="700" />

 
