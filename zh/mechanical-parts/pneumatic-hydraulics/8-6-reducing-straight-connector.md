# φ8 – φ6 直通接头

![](images/φ8---φ6-Reducing-Straight-Connector.jpg)

### 概述

一端用于φ8气管，另一端用φ6气管。 通道为直通式，并将φ8气管转换到φ6气管。

### 尺寸图纸

<img src="images/8-6-reducing-straight-connector-4-pack_59011.png" alt="59011.png" width="675" />
 

### 搭建案例

<img src="images/8-6-reducing-straight-connector-4-pack_1329.jpg" alt="1329.jpg" width="700" />

<img src="images/8-6-reducing-straight-connector-4-pack_59011-connector.jpg" alt="59011-connector.jpg" width="700" />

 
