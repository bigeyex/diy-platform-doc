# 两位三通电磁阀

![](images/solenoid-valve-3-2way_Solenoid-Valve-3-2way.jpg)

### 概述

Makeblock两位三通电磁阀是一个单作用气动执行器。二位三通电磁阀为双线圈控制，一个线圈瞬间通电后关闭电源、阀打开，另一个线圈瞬间通电后关闭电源、阀关闭。

### 参数

- 电压：DC12V

### 尺寸图纸
 
<img src="images/solenoid-valve-3-2way_95061-1.png" alt="95061-1.png" width="785" />

<img src="images/solenoid-valve-3-2way_95061-2.png" alt="95061-2.png" width="783" />

<img src="images/solenoid-valve-3-2way_95061-3.png" alt="95061-3.png" width="607" />

<img src="images/solenoid-valve-3-2way_4.png" alt="4.png" width="580" />
