# φ6 – φ4 直通接头

![](images/φ6---φ4-Reducing-Straight-Connector.jpg)

### 概述

一端用于φ6气管，另一端用φ4气管。 通道为直通式，并将φ6气管转换到φ4气管。

### 尺寸图纸

<img src="images/6-4-reducing-straight-connector-4-pack_59012.png" alt="59012.png" width="600" />

### 搭建案例

<img src="images/6-4-reducing-straight-connector-4-pack_1322.jpg" alt="1322.jpg" width="700" />

<img src="images/6-4-reducing-straight-connector-4-pack_59012-connector.jpg" alt="59012-connector.jpg" width="700" />
