# 真空吸盘接头


![](images/vacuum-suction-cup-connector-holder_Vacuum-Suction-Cup-Connector-Holder.jpg)

### 概述
Makeblock真空吸盘接头用于连接气管和真空吸盘，带有M4螺纹接头可以和makeblock平台零件兼容。

### 参数

- 材质：钢
- 吸盘连接直径：8.2mm
- 气嘴外径：5.2mm

### 尺寸图纸

<img src="images/vacuum-suction-cup-connector-holder_59004-size.png" alt="59004-size.png" width="565" />

<img src="images/vacuum-suction-cup-connector-holder_59004-size-2.png" alt="59004-size-2.png" width="332" />

### 搭建案例

<img src="images/vacuum-suction-cup-connector-holder_59000-demo.png" alt="59000-demo.png" width="686" />

 

<img src="images/vacuum-suction-cup-connector-holder_50000-50001-59000-59001-59002-59003-59004-off.jpg" width="868" />

<img src="images/vacuum-suction-cup-connector-holder_50000-50001-59000-59001-59002-59003-59004-on.jpg" width="868" />
