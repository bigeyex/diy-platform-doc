<img src="beam0824/images/beam0824-0.jpg" style="width:600;padding:5px 5px 15px 0px;">

# 双孔梁

### 概述

双孔梁是makeblock平台常用的机械零件，与大多数makeblock平台的机械零件兼容，双孔梁的表面上有各种安装孔和螺纹槽，两端截面有螺纹孔，可以方便安装在其他结构上。

### 参数
- 长度：16 - 504mm
- 尺寸规格（mm）：016、032、048、064、080、096、112、128、144、160、176、192、224、256、320、336、400、496、504
- 横截面：8 x 24mm
- 材质：6061挤压铝合金

### 功能特性
- 高耐磨
- 高强度
- 高刚度

### 使用说明

通过梁上的通孔、中间的螺纹槽以及端面上的螺纹孔，梁与梁之间可以相互连接，也可以与支架、滑轨、轴、电机等各类零件连接。

### 搭建案例

<img src="beam0824/images/beam0824-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

### 规格表

<table style="text-align:center;cellpadding:12px;cellspacing:10px;">

<tr>
<td><img src="beam0824/images/beam0824-016-1.png" width="170px;"><br><p>双孔梁-016-Blue</p></td>
<td><img src="beam0824/images/beam0824-032.jpg" width="170px;"><br><p>双孔梁-032</p></td>
<td><img src="beam0824/images/beam0824-048.jpg" width="170px;"><br><p>双孔梁-048</p></td>
<td><img src="beam0824/images/beam0824-064.jpg" width="170px;"><br><p>双孔梁-064</p></td>
</tr>

<tr>
<td><img src="beam0824/images/beam0824-080.jpg" width="170px;"><br><p>双孔梁-080</p></td>
<td><img src="beam0824/images/beam0824-096.jpg" width="170px;"><br><p>双孔梁-096</p></td>
<td><img src="beam0824/images/beam0824-112.jpg" width="170px;"><br><p>双孔梁-112</p></td>
<td><img src="beam0824/images/beam0824-128.jpg" width="170px;"><br><p>双孔梁-128</p></td>
</tr>

<tr>
<td><img src="beam0824/images/beam0824-144.jpg" width="170px;"><br><p>双孔梁-144</p></td>
<td><img src="beam0824/images/beam0824-160.jpg" width="170px;"><br><p>双孔梁-160</p></td>
<td><img src="beam0824/images/beam0824-176.jpg" width="170px;"><br><p>双孔梁-176</p></td>
<td><img src="beam0824/images/beam0824-192.jpg" width="170px;"><br><p>双孔梁-192</p></td>
</tr>

<tr>
<td><img src="beam0824/images/beam0824-224.png" width="170px;"><br><p>双孔梁-224</p></td>
<td><img src="beam0824/images/beam0824-256.jpg" width="170px;"><br><p>双孔梁-256</p></td>
<td><img src="beam0824/images/beam0824-320.jpg" width="170px;"><br><p>双孔梁-320</p></td>
<td><img src="beam0824/images/beam0824-336.jpg" width="170px;"><br><p>双孔梁-336</p></td>
</tr>

<tr>
<td><img src="beam0824/images/beam0824-400.jpg" width="170px;"><br><p>双孔梁-400</p></td>
<td><img src="beam0824/images/beam0824-496.png" width="170px;"><br><p>双孔梁-496</p></td>
<td><img src="beam0824/images/beam0824-504.png" width="170px;"><br><p>双孔梁-504</p></td>
</tr>

</table>