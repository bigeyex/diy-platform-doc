# Beam0412-236

![](images/beam0412-236-blue-4-pack_Beam0412-236.jpg)


### Building Examples:

<img src="images/beam0412-236-blue-4-pack_beam0412-236.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-236-blue-4-pack_2015-12-29-16-41-19.jpg" width="720" />

### Specifications

- SKU: 60727
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 236mm
- Package content: 4 x Beam 0412-236
- Dimension: 236 x 12 x 4mm (9.29 x 0.47 x 0.16'')
- Net Weight: 88g (3.1oz)
