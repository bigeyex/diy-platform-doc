# Beam0412-060

![](images/beam0412-060-blue-4-pack_Beam0412-060.jpg)


### Building Examples:

<img src="images/beam0412-060-blue-4-pack_beam0412-060.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-060-blue-4-pack_2015-12-24-17-09-35.jpg" width="720" />

### Specifications

- SKU: 60705
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 60mm
- Package content: 4 x Beam 0412-060
- Dimension: 60 x 12 x 4mm (2.36 x 0.47 x 0.16'')
- Net Weight: 20g (0.71oz)
