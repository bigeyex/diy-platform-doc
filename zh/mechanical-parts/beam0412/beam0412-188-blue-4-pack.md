# Beam0412-188

![](images/beam0412-188-blue-4-pack_Beam0412-188.jpg)


### Building Examples:

<img src="images/beam0412-188-blue-4-pack_beam0412-188.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-188-blue-4-pack_2015-12-29-12-15-01.jpg" width="720" />

### Specifications

- SKU: 60721
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 188mm
- Package content: 4 x Beam 0412-188
- Dimension: 188 x 12 x 4mm (7.40 x 0.47 x 0.16'')
- Net Weight: 70.4g (2.48oz)
