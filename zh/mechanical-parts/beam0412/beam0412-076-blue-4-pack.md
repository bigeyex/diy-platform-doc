# Beam0412-076

![](images/beam0412-076-blue-4-pack_Beam0412-076.jpg)

 
### Building Examples:

<img src="images/beam0412-076-blue-4-pack_beam0412-076.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-076-blue-4-pack_2015-12-24-17-24-23.jpg" width="720" />

### Specifications

- SKU: 60707
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 76mm
- Package content: 4 x Beam 0412-076
- Dimension: 76 x 12 x 4mm (2.99 x 0.47 x 0.16'')
- Net Weight: 25.6g (0.9oz)
