# Beam0412-140

![](images/beam0412-140-blue-4-pack_Beam0412-140.jpg)


### Building Examples:

<img src="images/beam0412-140-blue-4-pack_beam0412-140.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-140-blue-4-pack_2015-12-25-16-58-52.jpg" width="720" />

### Specifications

- SKU: 60715
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 140mm
- Package content: 4 x Beam 0412-140
- Dimension: 140 x 12 x 4mm (5.51 x 0.47 x 0.16'')
- Net Weight: 50.4g (1.78oz)
