# 25 直流减速编码电机 6V/185RPM


![](../../../en/electronic-modules/motors/images/dc-encoder-motor-25-6v-185rpm_DC-Encoder-Motor---25-6V-185RPM.jpg)

<img src="../../../en/electronic-modules/motors/images/dc-encoder-motor-25-6v-185rpm_微信截图_20160128115903-300x243.png" alt="微信截图_20160128115903" width="300" style="padding:5px 5px 12px 0px;">

### 概述

编码电机驱动可以控制多达2个直流编码器电机。2台电机的精确速度和位置可以设置和分开。电机驱动器IC是TB6612，与双直流电机驱动相同。我们提供了一个用于调整驱动器的P-I-D参数的软件，以便在不同的条件下可以正常工作。电机的减速比约为46.您也可以在软件中编辑减速比。

> 注意：它需要一些PID调整经验，以及一个 USB 转 UART 模块。

### 特点

- 精确的速度和位置控制。
- 实时速度和位置反馈。
- TB6612PNG电机驱动器IC具有高效率的基于 MOSFET 的H桥。
- 使用Arduino库轻松编程。
- 6V至12V电机电源范围。

### 尺寸图纸 (mm) 

<img src="../../../en/electronic-modules/motors/images/dc-encoder-motor-25-6v-185rpm_微信截图_20160128160612.png" alt="微信截图_20160128160612" width="732" style="padding:5px 5px 12px 0px;">

### 搭建案例

<img src="../../../en/electronic-modules/motors/images/dc-encoder-motor-25-6v-185rpm_微信截图_20160128160724.png" alt="微信截图_20160128160724" width="703" style="padding:5px 5px 12px 0px;">  

<img src="../../../en/electronic-modules/motors/images/dc-encoder-motor-25-6v-185rpm_微信截图_20160128120227-300x199.png" alt="微信截图_20160128120227" width="455" style="padding:5px 5px 12px 0px;">