# 25直流电机12V


![](../../../en/electronic-modules/motors/images/dc-motor-25-6v_DC-Motor-25-6V.jpg)

<img src="../../../en/electronic-modules/motors/images/dc-motor-25-6v_微信截图_20160128160459.png" alt="微信截图_20160128160459" width="353" style="padding:5px 5px 12px 0px;">

### 概述

直流电机是Makeblock平台中最常用的电机。使用Makeblock直流电机-25支架，他们可能很容易连接到Makeblock结构组件。

### 技术规格

<img src="../../../en/electronic-modules/motors/images/dc-motor-25-6v_微信截图_20160128160649.png" alt="微信截图_20160128160649" width="625" style="padding:5px 5px 12px 0px;">

### 尺寸图(mm) 

<img src="../../../en/electronic-modules/motors/images/dc-motor-25-6v_微信截图_20160128160612.png" alt="微信截图_20160128160612" width="732" style="padding:5px 5px 12px 0px;">

### Demo

<img src="../../../en/electronic-modules/motors/images/dc-motor-25-6v_微信截图_20160128160724.png" alt="微信截图_20160128160724" width="703" style="padding:5px 5px 12px 0px;">

可以通过在剥线端添加插头3.96-2P来连接 Makeblock Orion 或 Makeblock
双电机驱动器V1，以控制电机。

<img src="../../../en/electronic-modules/motors/images/dc-motor-25-6v_微信截图_20160128160812.png" alt="微信截图_20160128160812" width="442" style="padding:5px 5px 12px 0px;">

 
