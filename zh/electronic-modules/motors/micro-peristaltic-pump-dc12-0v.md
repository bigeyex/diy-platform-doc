# 蠕动马达 12V


![](../../../en/electronic-modules/motors/images/micro-peristaltic-pump-dc12-0v_Micro-Peristaltic-Pump-DC12.0V.jpg)

<img src="../../../en/electronic-modules/motors/images/micro-peristaltic-pump-dc12-0v_微信截图_20160128111459-300x288.png" alt="微信截图_20160128111459" width="300" style="padding:5px 5px 12px 0px;">

### 描述

蠕动马达（直流12.0V）是一种新型微型液化泵，体积小，功能强大。具有易于使用的功能，可用于DIY项目或工业项目。泵体配有三辊和环保硅胶软管。没有阀门和密封件，硅胶软管可以很容易地更换。液体不会直接与电机接触，因此更环保，无污染。微型蠕动电机也可用于泵送敏感液体和有机溶剂。

### 内部结构

<img src="../../../en/electronic-modules/motors/images/micro-peristaltic-pump-dc12-0v_微信截图_20160128112231-300x152.png" alt="微信截图_20160128112231" width="421" style="padding:5px 5px 12px 0px;">

### 特点

- 为正和负转印支持。
- 在流量控制高准确性。
- d（1.5-4）毫米\*0.5米硅胶管包括用于使用。
- 用于食品和医疗行业环保硅胶管。
- 简单结构和低的维护。

### 规格

- 电压：DC12.0V
- 流速：（6〜24）毫升/分钟
- 工作温度：0〜40℃
- 相对湿度＆LT;80％

### 尺寸图(mm)

<img src="../../../en/electronic-modules/motors/images/micro-peristaltic-pump-dc12-0v_微信截图_20160128113231-300x127.png" alt="微信截图_20160128113231" width="619" style="padding:5px 5px 12px 0px;">

### 演示 

<img src="../../../en/electronic-modules/motors/images/micro-peristaltic-pump-dc12-0v_微信截图_20160128113246-300x293.png" alt="微信截图_20160128113246" width="547" style="padding:5px 5px 12px 0px;">

 
