# MG995 标准舵机


![](../../../en/electronic-modules/motors/images/mg995-standard-servo_MG995-Standard-Servo.jpg)

<img src="../../../en/electronic-modules/motors/images/mg995-standard-servo_微信截图_20160128132425-300x238.png" alt="微信截图_20160128132425" width="300" style="padding:5px 5px 12px 0px;">

### 描述

MG995伺服是一种简单的，常用的标准伺服器，可满足您的机械需求，例如机器人头部，机器人臂部。
它带有一个标准3针电源和控制电缆，便于使用，金属齿轮用于高扭矩。RJ25适配器还可帮助您轻松地将伺服器与Me底板或Makeblock
Orion连接起来。

### 尺寸图

<img src="../../../en/electronic-modules/motors/images/mg995-standard-servo_微信截图_20160128132755-297x300.png" alt="微信截图_20160128132755" width="391" style="padding:5px 5px 12px 0px;">

### 机械参数

- 尺寸：40.4\*19.9\*37.5mm
- 重量：58g
- 齿轮类型：5金属齿轮 
- 边界角度：180°±5° 
- 轴承：DUAL BB 
- 喇叭齿轮条
- 喇叭类型：Metal
- 外壳：塑料(尼龙)
- 连接线：FP：240mm±5mm；JR：300mm±5mm 
- 电机：直流电机
- 水溅阻力：No 

### 电子规格

- 工作电压：4.8V 
- 闲置电流：5mA
- 空载速度：0.17sec/60° 
- 运行电流：350mA
- 峰值扭矩：9.0kg.cm
- 堵转电流：1500mA 

### 控制规格

- 命令信号：脉冲宽度调节 
- 放大器类型：数字控制器
- 脉冲宽度范围：500-2500usec 
- 中性位置：1500usec
- 运行角度：180°±3°(500～2500usec) 
- 限制带宽：4 usec 
- 旋转方向：逆时针 (500～2500usec) 