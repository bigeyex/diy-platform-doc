# 气泵马达 12V/3202PM


![](../../../en/electronic-modules/motors/images/air-pump-motor-dc-12v-3202pm_Air-Pump-Motor-DC-12V-3202PM.jpg)

<img src="../../../en/electronic-modules/motors/images/air-pump-motor-dc-12v-3202pm_微信截图_20160128110215-279x300.png" alt="微信截图_20160128110215" width="279" style="padding:5px 5px 12px 0px;">

### 概述

Makeblock气泵电机 - 直流 12V / 3202PM广泛用于水族箱氧气循环，DIY项目。

### 技术规格

- 额定电压：DC 12V
- 负载：空气
- 电流（负载）：大于400mA以下
- 流速：1.8LPM
- 尺寸：D27 X52毫米
- 最大压力：大于600mmHg更多
- 噪声：&lt;&nbsp;60分贝

### 尺寸图(mm)

<img src="../../../en/electronic-modules/motors/images/air-pump-motor-dc-12v-3202pm_微信截图_20160128110614-300x113.png" alt="微信截图_20160128110614" width="478" style="padding:5px 5px 12px 0px;">

### 演示

<img src="../../../en/electronic-modules/motors/images/air-pump-motor-dc-12v-3202pm_微信截图_20160128110711-300x140.png" alt="微信截图_20160128110711" width="474" style="padding:5px 5px 12px 0px;">
