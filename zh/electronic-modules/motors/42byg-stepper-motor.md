# 42步进电机

![](../../../en/electronic-modules/motors/images/42byg-stepper-motor_42BYG-Stepper-Motor.jpg)

<img src="../../../en/electronic-modules/motors/images/42byg-stepper-motor_微信截图_20160128152924.png" alt="微信截图_20160128152924" width="334" style="padding:5px 5px 12px 0px;">

### 规格

- 相位 : 双相
- 步进角度 : 1.8+-5%°/步
- 额定电压 : 12V
- 电流 : 1.7A/PHASE
- 电阻 : 1.5+-10%/PHASE
- 感应系数 : 2.8+-20%mH/PHASE
- 静力矩 : {% raw %}40N.cm Min{% endraw %}
- 保持扭矩 : {% raw %}2.2N.cm Max{% endraw %}
- 绝缘等级 : B
- 线材规格 : AWG26 UL1007
- 转矩 : 54G.cm2

### 尺寸图

<img src="../../../en/electronic-modules/motors/images/42byg-stepper-motor_微信截图_20160128153028.png" alt="微信截图_20160128153028" width="587" style="padding:5px 5px 12px 0px;">

### 演示

<img src="../../../en/electronic-modules/motors/images/42byg-stepper-motor_微信截图_20160128153108.png" alt="微信截图_20160128153108" width="298" style="padding:5px 5px 12px 0px;">

步进电机支架不包括在内。

<img src="../../../en/electronic-modules/motors/images/42byg-stepper-motor_微信截图_20160128153143.png" alt="微信截图_20160128153143" width="510" style="padding:5px 5px 12px 0px;">

### 连接类型

也许你对步进电机上的哪个颜色电线应该连接到驱动器上的（A +，A-，B
+，B-）哪个端口感到困惑，下面相关的指导：

- 我们的步进电机有两相。所以，即使改变了阶段A和阶段B之间的连接顺序。我的意思是黑色和绿色可以连接到A或B，红色和蓝色可以连接到B或A.是的，它会改变电机方向。
