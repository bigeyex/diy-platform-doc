# 9g 小舵机


![](../../../en/electronic-modules/motors/images/9g-micro-servo_9g-Micro-Servo-Pack.jpg)

<img src="../../../en/electronic-modules/motors/images/9g-micro-servo_微信截图_20160128144154-300x243.png" alt="微信截图_20160128144154" width="300" style="padding:5px 5px 12px 0px;">

### 描述

9克小舵机是一种位置(角度)伺服的驱动器，适用于那些需要角度不断变化并可以保持的控制系统。常见于航模，飞机模型，遥控机器人及机械部件当中。在使用中，舵机的配件通常包含一个能把舵机固定到基座上的支架以及可以套在驱动轴上的舵盘，通过舵盘上的孔可以连接其它物体构成传动模型。小舵机自带的3线接口可以通过RJ25适配器与主板相连。

### 技术规格

- 工作电压：4.8V到6V DC  
- 工作电流：80到100mA  
- 待机电流：5mA  
- 极限角度： 210°±5%  
- 扭力： 1.3到1.7kg/cm  
- 工作温度：-10℃到60℃  
- 湿度范围：60%±10%  
- 转速： 0.09到0.10 sec/60°(4.8V)  
- 信号周期：20 ms  
- 信号高电平时间范围：1000到2000 us/周期  
- 尺寸： 32.3 x 12.3 x 30.6 mm (长x宽x高)

### 功能特性

- 体积小，重量轻；  
- 采用防反插接口；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持mBlock图形化编程，适合全年龄用户。

### 引脚定义

9克小舵机模块有三个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

</td>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;"> GND</td>
<td style="border: 1px solid black;">地线(黑色)</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线（红色）</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">SIG</td>
<td style="border: 1px solid black;">控制信号（白色）</td>
</tr>
</table>

<br>

### 接线方式

● **RJ25连接** 

9克小舵机可以通过RJ25适配器与主板相连。

以 Makeblock Orion 为例，可以连接到3，4，5，6，7,8号接口，当接到7，8号接口时，舵机只能在RJ25适配器的SLOT2端口上，如图  

<img src="images/9g-micro-servo_2.png" alt="微信截图_20160129151012" width="421" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**  

当使用杜邦线连接到 Arduino Uno 主板时，舵机SIG引脚需要连接到
DIGITAL（数字）口，如下图所示： 

<img src="images/9g-micro-servo_34.png" alt="微信截图_20160129151012" width="421" style="padding:5px 5px 12px 0px;">

### 编程指南

● **Arduino编程**

如果使用Arduino编程，需要调用库`Makeblock-Library-master` 来控制
9克小舵机模块。

本程序通过Arduino编程使舵机每两秒从0°转到180°。

<img src="images/9g-micro-servo_微信截图_20160129151012.png" alt="微信截图_20160129151012" width="421" style="padding:5px 5px 12px 0px;">

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MePort(uint8_t port)</td>
<td style="border: 1px solid black;">定义连接端口</td>
</tr>

<tr>
<td style="border: 1px solid black;">void attach()</td>
<td style="border: 1px solid black;"> 选定引脚</td>
</tr>

<tr>
<td style="border: 1px solid black;">void write(int angle)</td>
<td style="border: 1px solid black;">控制舵机旋转到指定角 </td>
</tr>
</table>

<br>

● **mBlock编程** 

9克小舵机支持mBlock编程环境，如下是该模块指令简介： 

<img src="images/9g-micro-servo_QQ截图20180205160024.jpg" alt="微信截图_20160129151012" width="521" style="padding:5px 5px 12px 0px;"> 

下面是实现控制小舵机每秒旋转一次效果：  

<img src="images/9g-micro-servo_35.jpg" alt="微信截图_20160129151012" width="551" style="padding:5px 5px 12px 0px;">

### 原理解析

9克小舵机由直流电机、减速齿轮组、传感器和控制电路组成。其外部红黑白线分别代表电源正极、地线与控制信号线。Arduino开发环境下的微控制器所产生的控制信号是一种脉宽调制（PWM）信号，9克小舵机接收一束每20ms触发1-2ms的脉冲，例如：在1ms脉冲下舵机在0度位置；在1.5ms脉冲下，舵机会保持在90度位置；在2ms脉冲下，舵机在180度位置；通过调整脉宽可以实现舵机在不同范围内的运动。

### 演示

<img src="../../../en/electronic-modules/motors/images/9g-micro-servo_微信截图_20160128144637.png" alt="微信截图_20160128144637" width="411" style="padding:5px 5px 12px 0px;">
