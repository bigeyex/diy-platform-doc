# 36 直流编码减速电机 12V


![](../../../en/electronic-modules/motors/images/36-dc-geared-motor-12v240rpm_36-DC-Geared-Motor-12V240RPM.jpg)

<img src="../../../en/electronic-modules/motors/images/36-dc-geared-motor-12v240rpm_微信截图_20160128122512-300x281.png" alt="微信截图_20160128122512" width="300" style="padding:5px 5px 12px 0px;">

### 描述

36直流减速电机性能更好，扭矩更高。
它可以作为车底盘的动力电机或一些高性能的小型车。

### 技术规格

- 电压：12V
- 无负载RPM：240rpm
- 额定转速：182rpm
- 额定扭矩：4kg.cm
- 目前：1.2A

### 尺寸规格

<img src="../../../en/electronic-modules/motors/images/36-dc-geared-motor-12v240rpm_微信截图_20160128122745-300x121.png" alt="微信截图_20160128122745" width="540" style="padding:5px 5px 12px 0px;">

### 演示

<img src="../../../en/electronic-modules/motors/images/36-dc-geared-motor-12v240rpm_微信截图_20160128122822-300x133.png" alt="微信截图_20160128122822" width="528" style="padding:5px 5px 12px 0px;">
