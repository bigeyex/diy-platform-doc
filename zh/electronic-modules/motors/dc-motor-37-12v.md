# 37直流电机12V


![](../../../en/electronic-modules/motors/images/dc-motor-37-12v_DC-Motor-37-12V.jpg)

<img src="../../../en/electronic-modules/motors/images/dc-motor-37-12v_微信截图_20160128155423.png" alt="微信截图_20160128155423" width="288" style="padding:5px 5px 12px 0px;">

### 概述

直流电机是Makeblock平台中最常用的电机。使用Makeblock直流电机-37支架，他们可能很容易连接到Makeblock结构组件。

### 技术规格

37直流电机的型号如下：
  
<img src="../../../en/electronic-modules/motors/images/dc-motor-37-12v_微信截图_20160128160057.png" alt="微信截图_20160128160057" width="794" style="padding:5px 5px 12px 0px;">

### 尺寸图 (mm)

<img src="../../../en/electronic-modules/motors/images/dc-motor-37-12v_微信截图_20160128155640.png" alt="微信截图_20160128155640" width="748" style="padding:5px 5px 12px 0px;">

### 演示

<img src="../../../en/electronic-modules/motors/images/dc-motor-37-12v_微信截图_20160128155833.png" alt="微信截图_20160128155833" width="729" style="padding:5px 5px 12px 0px;">

<img src="../../../en/electronic-modules/motors/images/dc-motor-37-12v_微信截图_20160128155948.png" alt="微信截图_20160128155948" width="430" style="padding:5px 5px 12px 0px;">

 
