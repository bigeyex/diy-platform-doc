# 57 步进电机


![](../../../en/electronic-modules/motors/images/57byg-stepper-motor_57BYG-Stepper-Motor.jpg)

<img src="../../../en/electronic-modules/motors/images/57byg-stepper-motor_微信截图_20160128152218.png" alt="微信截图_20160128152218" width="370" style="padding:5px 5px 12px 0px;">

### 描述

Makeblock 57步进电机比42步进电机具有更高的扭矩和更快的响应速度。
使用 Makeblock 57步进电机支架，他们可能很容易连接到 Makeblock 结构组件。

### 特点

- 高输出扭矩
- 低噪音
- 低功耗

### 技术规格

- 重量：700克
- 电机长度：56mm
- 输出轴长度：8mm D轴
- 引脚数：4
- 步角（度）：1.8°
- 保持转矩：1.2N.m
- 相电流：2.8A

### 尺寸图(mm)

<img src="../../../en/electronic-modules/motors/images/57byg-stepper-motor_微信截图_20160128152447.png" alt="微信截图_20160128152447" width="575" style="padding:5px 5px 12px 0px;">

### 演示

<img src="../../../en/electronic-modules/motors/images/57byg-stepper-motor_微信截图_20160128152516.png" alt="微信截图_20160128152516" width="580" style="padding:5px 5px 12px 0px;">

<img src="../../../en/electronic-modules/motors/images/57byg-stepper-motor_微信截图_20160128152547.png" alt="微信截图_20160128152547" width="559" style="padding:5px 5px 12px 0px;">
