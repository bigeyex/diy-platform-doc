# 42 减速步进电机


![](../../../en/electronic-modules/motors/images/42byg-geared-stepper-motor_42BYG-Geared-Stepper-Motor.jpg)

<img src="../../../en/electronic-modules/motors/images/42byg-geared-stepper-motor_微信截图_20160128150831.png" alt="微信截图_20160128150831" width="332" style="padding:5px 5px 12px 0px;">

### 描述

Makeblock 42步进电机是一种简单但功能强大的步进电机，具有高输出扭矩和响应速度，但噪音低，能耗低。

它具有比42步进电机更高的扭矩。 它可以用作一些高性能机器的动力电机。

### 技术规格

-   步伐角度（度）：1.8
-   相电流：2.8A
-   电线数量：4
-   电机长度：56mm
-   比例： 5.18：1 
-   输出轴：D轴8mm

### 尺寸图(mm)

<img src="../../../en/electronic-modules/motors/images/42byg-geared-stepper-motor_微信截图_20160128151208.png" alt="微信截图_20160128151208" width="567" style="padding:5px 5px 12px 0px;">

### 演示

<img src="../../../en/electronic-modules/motors/images/42byg-geared-stepper-motor_微信截图_20160128151237.png" alt="微信截图_20160128151237" width="464" style="padding:5px 5px 12px 0px;">

不包括Makeblock多功能电机支架。

<img src="../../../en/electronic-modules/motors/images/42byg-geared-stepper-motor_微信截图_20160128151348.png" alt="微信截图_20160128151348" width="626" style="padding:5px 5px 12px 0px;">

### 连接类型

您可能会对步进电机上的哪一根颜色电缆应该连接到驱动器上的哪个端口（A
+，A-，B +，B-）感到困惑，下面是它的指导：

1. 我们的步进电机有两个阶段。所以，你改变阶段A和阶段B之间的连接顺序并不重要。意思是黑色和绿色可以连接到A或B，红色和蓝色可以连接到B或A.是的，它会改变电机方向。
2. 你需要遵循的规则只有一条，不能把它们混在一起。

- A+:红  
- A-:黑  
- B+:蓝  
- B-:绿
