# MEDS15 舵机


![](../../../en/electronic-modules/motors/images/meds15-servo-motor_MEDS15-Servo-Motor.jpg)

<img src="../../../en/electronic-modules/motors/images/meds15-servo-motor_微信截图_20160128145039.png" alt="微信截图_20160128145039" width="306" style="padding:5px 5px 12px 0px;">

### 机械规格

- 尺寸：40×20×41mm 
- 重量：75g
- 齿轮类型：5 金属齿轮 
- 限制角：210°±5°
- 轴承：2\*轴承
- 齿轮：Φ8mm35T
- 齿轮类型：塑料, 聚甲醛 
- 外壳：铝合金加塑料 
- 连接电线：230mm±5mm
- 电机：空心杯电机 
- 水溅阻力：No 

### 电子规格

- 工作电压：6.0V
- 闲置电流：8mA 
- 空载速度：0.19sec/60° 
- 运转电流：450mA 
- 峰值扭矩：16.5kg.cm 
- 堵转电流：2500mA

### 控制规格

- 命令信号：脉宽调节 
- 放大器类型：数字控制器 
- 脉宽范围：500-2500usec 
- 中性位置：1500usec 
- 运转角度：180°±3°(1000～2000usec) 
- 死区宽度：5 usec 
- 旋转方向：顺时钟 (1000～1500usec)

### 尺寸图(mm)

<img src="../../../en/electronic-modules/motors/images/meds15-servo-motor_微信截图_20160128145325.png" alt="微信截图_20160128145325" width="620" style="padding:5px 5px 12px 0px;">

### Demo

<img src="../../../en/electronic-modules/motors/images/meds15-servo-motor_微信截图_20160128145357.png" alt="微信截图_20160128145357" width="615" style="padding:5px 5px 12px 0px;">
