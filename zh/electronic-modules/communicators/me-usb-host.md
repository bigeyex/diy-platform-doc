# USB-Host模块

![](../../../en/electronic-modules/communicators/images/me-usb-host_Me-USB-Host.jpg)

<img src="../../../en/electronic-modules/communicators/images/me-usb-host_微信截图_20160129113429.png" alt="微信截图_20160129113429" width="329" style="padding:5px 5px 12px 0px;">

### 描述

USB Host 模块是 USB 设备的适配器。您可以使用它将 Makeblock 主板连接到USB设备，例如USB操纵杆，鼠标或拇指驱动器。所以你可以用自己的操纵杆或其他东西来控制你的机器人。

现在，我们可以使用无线手柄为 mBot 添加新的控制方法：mBot 使用 USB
Host 模块控制无线手柄。

### 特点

- 主芯片: CH375B
- 预留了高级DIY所用的焊点
- 不支持热插拔
- 16mm间隔M4安装孔，与Makeblock横梁兼容
- 用于连接杜邦线的2.54毫米针孔
- 使用6P6C RJ25接口轻松连接
- 支持Arduino库，方便编程

### 规格

- 支持的设备：描述符小于64字节的HID设备
- 额定电压：5V
- 尺寸：24 x 48 x 16毫米（长x宽x高）

### 原理图

<img src="../../../en/electronic-modules/communicators/images/me-usb-host_Me-USB-Host.png" alt="Me USB Host" width="1284" style="padding:5px 5px 12px 0px;">
