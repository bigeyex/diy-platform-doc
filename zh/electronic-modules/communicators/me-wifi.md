# Wi-Fi模块


![](../../../en/electronic-modules/communicators/images/me-wifi_Me-WiFi-Module.jpg)

<img src="../../../en/electronic-modules/communicators/images/me-wifi_微信截图_20160129114325.png" alt="微信截图_20160129114325" width="223" style="padding:5px 5px 12px 0px;">

### 概述

Wi-Fi 模块主要部件为 ESP8266 模块，ESP8266 是一款超低功耗的 UART-WiFi 透传模块，支持无线802.11 b/g/n 标准，工作电压3.3V。Wi-Fi 模块内置电平转换，将5V转为3.3V，可以通过设置连接 Wi-Fi，来制作 Wi-Fi 遥控小车，遥控台灯等。本模块接口是蓝灰色标，需要通过RJ25连接主板上的标有蓝色或者灰色的接口。

### 技术规格

- 工作电压： 5V DC  
- 支持无线：802.11 b/g/n 标准  
- 频率范围：2.412GHz\~2.484GHz，  
- 工作电流：50mA；  
- 峰值电流：200mA；  
- 工作温度：-25℃\~80℃  
- 芯片型号：ESP8266

### 功能特性

- 工作模式：STA（工作站模式）+AP（热点模式）  
- 内置 TCP/IP 协议栈  
- 支持 WPA WPA2/WPA2–PSK加密  
- 模块的白色区域是与金属梁接触的参考区域  
- 具有反接保护，电源反接不会损坏IC。  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有接头支持绝大多数 Arduino 系列主控板。

### 引脚定义

Wi-Fi模块有四个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能 </th>
</tr>

<tr>
<td style="border: 1px solid black;"> 1</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">TX</td>
<td style="border: 1px solid black;">串口信号发送</td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">RX</td>
<td style="border: 1px solid black;">串口信号接收</td>
</tr>
</table>

<br>

### 连线模式

● **RJ25连接** 

由于Wi-Fi模块接口是蓝或灰色色标，当使用RJ25接口时，需要连接到主控板上带有蓝或灰色色标的接口。

以 Makeblock Orion 为例，可以连接到3，4，5，6号接口，如图：  

<img src="../../../en/electronic-modules/communicators/images/me-wifi_微信截图_20160129114619.png" alt="微信截图_20160129114619" width="270" style="padding:5px 5px 12px 0px;">

● **杜邦线连接** 

当使用杜邦线连接到 Arduino Uno 主板时，模块TX、RX引脚需要分别连接到 Uno 板上的RX、TX引脚，如下图所示：

<img src="../../../en/electronic-modules/communicators/images/me-wifi_微信截图_20160129114705.png" alt="微信截图_20160129114705" width="299" style="padding:5px 5px 12px 0px;">

### 编程指南

● **Arduino 编程** 

如果使用 Arduino 编程，需要调用库`Makeblock-Library-master`来控制 Wi-Fi模块。

本程序通过 Arduino 编程通过Wi-Fi模块接收数据。 

<img src="../../../en/electronic-modules/communicators/images/me-wifi_微信截图_20160129114840.png" alt="微信截图_20160129114840" width="425" style="padding:5px 5px 12px 0px;">

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数 </th>
<th style="border: 1px solid black;">功能 </th>
</tr>

<tr>
<td style="border: 1px solid black;">MeWifi (uint8_tport)</td>
<td style="border: 1px solid black;">选定接口 </td>
</tr>

<tr>
<td style="border: 1px solid black;">void.begin(9600)</td>
<td style="border: 1px solid black;">设定带宽并启动</td>
</tr>

<tr>
<td style="border: 1px solid black;">int available()</td>
<td style="border: 1px solid black;">判断是否接收到数据</td>
</tr>

<tr>
<td style="border: 1px solid black;">char read()  </td>
<td style="border: 1px solid black;">读取接受的数据</td>
</tr>

<tr>
<td style="border: 1px solid black;">char write(outDat)</td>
<td style="border: 1px solid black;">输出数据 </td>
</tr>
</table>

<br>

### 原理分析

本模块支持STA/AP/STA+AP 三种工作模式。  
- STA 模式：模块通过路由器连接互联网，手机或电脑通过互联网实现对设备的远程控制。  
- AP 模式：模块作为热点，实现手机或电脑直接与模块通信，实现局域网无线控制。  
- STA+AP 模式：两种模式的共存模式，即可以通过互联网控制可实现无缝切换，方便操作。

连接好模块后，红色电源灯亮，约1秒后，蓝色Link指示灯闪烁（闪烁代表正常启动，但未连接）。当连接设备成功，并进行一次数据发送后，指示灯常亮，模块接收数据时蓝色接收指示灯闪烁。拨动开关用于选择模式，Work和PROG工作模式，Work是正常工作状态（平时应在这个状态），PROG是编程模式，切换模式时需要重启。  
模块上电时，在配置模式下模块WiFi信号为：“ESP（+芯片ID号）”，无密码。在浏览器输入WiFi扩展板地址：192.168.4.1打开配置页面即可进行配置。

### 原理图

<img src="../../../en/electronic-modules/communicators/images/me-wifi_Me-wifi.png" alt="Me wifi" width="1082" style="padding:5px 5px 12px 0px;">
