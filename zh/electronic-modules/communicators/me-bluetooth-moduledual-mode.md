# 蓝牙模块

<img src="../../../en/electronic-modules/communicators/images/me-bluetooth-moduledual-mode_微信截图_20160129111600.png" alt="微信截图_20160129111600" width="103" style="padding:5px 5px 12px 0px;">

<br>

<img src="../../../en/electronic-modules/communicators/images/me-bluetooth-moduledual-mode_微信截图_20160129111600.png" alt="微信截图_20160129111600" width="203" style="padding:5px 5px 12px 0px;">

### 概 述

蓝牙双模模块主要应用于短距离的数据无线传输领域，可以方便地和PC机、智能手机等无线终端上的蓝牙设备相连，避免繁琐的线缆连接和空间限制，可以直接替代USB数据线。本模块接口是蓝灰色标，说明是双数字接口与硬件串口，需要连接到Orion主板上5号接口（也可以连接到其它蓝色端口上，不过蓝色端口只能使用波特率为9600的串口通信）。

### 技术规格

- 工作电压：5V DC  
- 工作频率：2.4GHz  
- 波特率：115200  
- 收发距离：10到15米（空旷地带）  
- 工作温度：-25到70 ℃  
- 信号模式：双数字信号  
- 模块尺寸：51 x 24 x 18 mm (长x宽x高)

### 功能特性

- 模块的白色区域是与金属梁接触的参考区域；  
- 支持 BT2.1+EDR 和 BT4.0(BLE) 双模式，两种模式可同时工作；  
- 在通过 USB 向 Arduino 烧写程序的时候，必须先断开模块与5号端口的连接；
- 连接的过程中需要密码进行配对，可以尝试0000或1234；  
- LED指示蓝牙状态，蓝灯闪烁代表没有蓝牙连接，常亮表示蓝牙已连接并打开了端口；  
- 外部复位输入，低有效，带内部上拉；  
- 支持移动设备（Android/IOS）控制电子模块；  
- 支持通过蓝牙进行mBlock图形化编程控制模块；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有RX、TX、VCC、GND接头支持绝大多数 Arduino 系列主控板。

### 引脚定义

蓝牙双模模块有四个针脚的接头,每个针脚的功能如下表:

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">RX</td>
<td style="border: 1px solid black;">串口信号接收</td>
</tr>

<tr>
<td style="border: 1px solid black;">4 </td>
<td style="border: 1px solid black;">TX</td>
<td style="border: 1px solid black;">串口信号发送</td>
</tr>
</table>

<br>

### 接线方式

● **RJ25连接**

由于蓝牙双模模块接口是蓝灰色标，当使用RJ25接口时，需要连接到主控板上带有蓝灰色标的接口。以 Makeblock Orion 为例，可以连接到5号接口，如图:  

<img src="../../../en/electronic-modules/communicators/images/me-bluetooth-moduledual-mode_微信截图_20160129112231.png" alt="微信截图_20160129112231" width="377" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**

当使用杜邦线连接到 Arduino Uno 主板时，模块RX与TX引脚需要连接到
DIGITAL（数字）口0、1，如下图所示：

<img src="../../../en/electronic-modules/communicators/images/me-bluetooth-moduledual-mode_微信截图_20160129112311.png" alt="微信截图_20160129112311" width="379" style="padding:5px 5px 12px 0px;">

### 编程指南

● **Arduino 编程**  

如果使用Arduino编程，需要调用库 `Makeblock-Library-master` 来控制蓝牙双模模块。

本程序通过 Arduino 编程使手机与 Arduino IDE之间进行通信。每当收到来自手机的数据，都会显示在串口上。

在通讯开始前，请确保您的设备已经链接到蓝牙双模模块，发送字母到蓝牙模块查看运行结果。 

<img src="../../../en/electronic-modules/communicators/images/me-bluetooth-moduledual-mode_微信截图_20160129112410.png" alt="微信截图_20160129112410" width="441" style="padding:5px 5px 12px 0px;">

**蓝牙双模函数功能列表**

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeBluetooth(uint8_t port)</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">void begin(long baudrate)</td>
<td style="border: 1px solid black;">设定波特率并启动</td>
</tr>

<tr>
<td style="border: 1px solid black;">byte available()</td>
<td style="border: 1px solid black;">获取接收缓冲区中未读的数据字节数</td>
</tr>

<tr>
<td style="border: 1px solid black;">int read()</td>
<td style="border: 1px solid black;">从模块中读取1字节数据</td>
</tr>

<tr>
<td style="border: 1px solid black;">size_t write(byte value)</td>
<td style="border: 1px solid black;">写入一字节数据到模块并发送</td>
</tr>
</table>

<br>

### 原理解析

蓝牙是一种支持设备短距离通信的无线电技术。利用蓝牙技术，能够有效地简化移动通信终端设备之间的通信，从而数据传输变得更加迅速高效，为无线通信拓宽道路。

蓝牙采用分散式网络结构以及快跳频和短包技术，支持点对点及点对多点通信，工作在全球通用的2.4GHz ISM（即工业、科学、医学）频段。其数据速率为1Mbps。采用时分双工传输方案实现全双工传输。

蓝牙网络（通常称为微网）使用主从模型来控制设备发送数据的时间与地址。主动提出通信要求的设备是主设备，被动进行通信的设备为从设备。1台主设备最多可同时与7台从设备进行通信，并可以和多达256个从设备保持同步但不通信。主从设备通过微网可以互相收发数据，1台从设备与另1台从设备通信的唯一途径是通过主设备转发，从设备之间无法互相收发数据。

### 原理图

<img src="../../../en/electronic-modules/communicators/images/me-bluetooth-moduledual-mode_Me-Bluetoothdual-mode.png" alt="Me Bluetooth(dual mode)" width="1498" style="padding:5px 5px 12px 0px;">
