# mCore 主控板

![](images/mcore_mCore.jpg)

<img src="images/mcore_微信截图_20160203154445.png" alt="微信截图_20160203154445" width="345" style="padding:5px 5px 12px 0px;">

### 概述

mCore 是一款专为 mBot 设计的易于使用的主控板.mCore基于 Arduino
Uno，集成了各种板载传感器，如蜂鸣器，光线传感器，RGB
LED 等，为您提供更为简单的学习电子模块的方法。

### 特点

-  支持 Arduino 与 Makeblock 库，方便编程
-  包括各种特色传感器，如蜂鸣器，光传感器，RGB LED等
-  板载USB B型连接器设计，可长时间使用
-  自恢复保险丝可防止电路板烧毁
-  四个颜色标签的RJ25连接器，可通过更多的 Arduino 传感器进行连接和扩展

### 规格


- 工作电压： 3.7-6V 直流电源                             
- 微控制器： ATmega238                                   
- 产品尺寸： 90 x 79 x 18mm (3.54 x 3.11 x 0.71inch)     
- 包装尺寸： 110 x 150 x 10 mm (4.33 x 5.91 x 0.39 inch) 
- 净重： 43g (1.52oz)                                
- 毛重： 47g (1.66oz)                                
- 包装内容： 1 x mCore                                   

### 原理图

<img src="images/mcore_Mcore.png" alt="Mcore" width="1329" style="padding:5px 5px 12px 0px;">
