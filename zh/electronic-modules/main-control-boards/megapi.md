# MegaPi 主控板


![](images/megapi_MegaPi.jpg)

<img src="images/megapi_4SEWX90X_EMLLLJ2XEY.png" alt="4sewx90x_emlllj2xey" width="800" style="padding:5px 5px 12px 0px;">

### 简介

MegaPi 是一款基于 ATmega2560 芯片的主控板。通过对驱动接口的良好封装，它可以快速简单地驱动编码电机、直流电机、步进电机。强大的驱动能力表现在可同时驱动 4 个编码电机（或 4 个步进电机或 8 个直流电机）和 10 个舵机，可外接各种传感器。其拥有最大 DC12V，10A 的电流输出能力。MegaPi 同时支持 Arduino IDE
和图形化编程。更棒的是，它还可以与树莓派完美结合，树莓派用户可使用
Python 就能实现对各种电机及电子模块的控制。强大的运动控制能力和拓展性使得
MegaPi 可以适用于3D 打印机，CNC 和机器人等各种应用场景。

### 特点

-   4个电机驱动接口，可以方便插取编码电机驱动模块、步进电机驱动等模块，从而实现直流电机、编码电机、步进电机的驱动。
-   1个无线通讯模块接口，可以方便插取蓝牙模块、2.4G模块等。
-   10个舵机接口，可以同时驱动10个舵机。
-   2个大功率MOS管驱动端口，可以控制最大电流10A的设备。可以输出最大DC5V
    3A的工作电流。
-   1个树莓派转接端口（需要手动焊接），实现5V转3.3V串口通讯。
-   USB-B型接口，用于下载程序和通讯。并采用CH340GUSB转串口芯片，能轻松稳定的实现电脑和MegaPi的通讯。
-   支持 Arduino IDE 编程，配有完整的库文件，支持 mBlock 图形化编程软件
-   兼容 Raspberry Pi，实现 Raspberry Pi 和 MegaPi 的相互控制

<img src="images/megapi_10050_09.jpg" alt="10050_09" width="600" style="padding:5px 5px 12px 0px;">


### 技术规格

- 主控芯片：ATMEGA2560-16AU  
- 输入电压/电流:DC 6-12V 10A  
- 工作电压:DC 5V  
- 通用I/O管脚:43  
- 串口:3  
- I2C口:1  
- SPI口:1  
- 模拟输入口:15  
- 每个I/O 管脚额定直流电流:20mA  
- 闪存:256KB  
- 静态随机存取存储器:8KB  
- 电可擦可编程只读存储器:4KB  
- 主频:16MHz  
- 产品尺寸:86mm×53mm

### 端口及接插模块介绍

<img src="images/megapi_XNVYBVAP4NDIJ4RF.png" alt="xnvybvap4ndij4rf" width="901" style="padding:5px 5px 12px 0px;">

MegaPi具有较丰富的色彩，每种颜色代表不同的作用和功能：  
- 红色插针——电源或者电机输出  
- 黄色插针——I/O口  
- 蓝色插针——无线通讯接口  
- 黑色插针——电源地  
- 绿色接头——电机或功率输出

### 接口介绍

MegaPi提供4个RJ25接口，共有5个不同的标签颜色定义不同的功能。其颜色和功能意义对照如下：

<img src="images/megapi_B4UON2DCFO82UWQLVO.png" alt="b4uon2dcfo82uwqlvo" width="1259" style="padding:5px 5px 12px 0px;">

### 编程指南

1\. [图形化编程-mBlock](http://learn.makeblock.com/getting-started-programming-with-mblock/)

2\. [Arduino 编程](http://learn.makeblock.com/ultimate2-arduino-programming/)

Arduino 是一个以易用的硬件和软件为基础的开源电子平台，适合想要做交互式项目的用户。它的开发环境有利于人们撰写和上传代码至控制板。您可以通过使用Arduino的编程语言（C\\C++）对 Arduino 的硬件进行编程。

MegaPi 与 Arduino Mega 2560 兼容，因此您可以使用 Arduino IDE 编写程序。
如果使用 Makeblock 的电子模块，我们建议您安装 Makeblock 程序库。

链接：[Makeblock程序库](https://github.com/Makeblock-official/Makeblock-Libraries)

3\. [Python 编程](http://learn.makeblock.com/python-for-megapi/)

4\. [Node JS编程](https://github.com/Makeblock-official/NodeForMegaPi)

<img src="images/megapi_M7DAN2@S@IYH8ALKR5OG5.png" alt="m7dan2siyh8alkr5og5" width="700" style="padding:5px 5px 12px 0px;">

### 电路原理图

<img src="images/megapi_MegaPiSchematic1.jpg" alt="megapischematic1" width="820" style="padding:5px 5px 12px 0px;">

[点击下载](http://download.makeblock.com/MegaPi/Schematic/MegaPiSchematic1.pdf)

<img src="images/megapi_MegaPiSchematic-2.jpg" alt="megapischematic-2" width="820" style="padding:5px 5px 12px 0px;">

[点击下载](http://download.makeblock.com/MegaPi/Schematic/MegaPiSchematic%202.pdf)

### 常见问题

1.驱动自动安装失败，如何解决？

答：您可以手动安装驱动，链接如下：

Windows：[Download](https://raw.githubusercontent.com/Makeblock-official/Makeblock-USB-Driver/master/Makeblock_Driver_Installer.zip)

Mac OSX：[Me Baseboard](https://raw.githubusercontent.com/Makeblock-official/Makeblock-USB-Driver/master/Arduino_Driver_for_MacOSX.zip) / [Makeblock Orion](https://raw.githubusercontent.com/Makeblock-official/Makeblock-USB-Driver/master/Makeblock_Orion_Driver_for_MacOSX.zip) / [Arduino Uno](https://raw.githubusercontent.com/Makeblock-official/Makeblock-USB-Driver/master/Arduino_Driver_for_MacOSX.zip)

2.为什么 MegaPi 的Port1—Port4接口是反向的？

答：因为 MegaPi 的 Port1—Port4 采用了一排公座、一排母座的形式，并用红色和黄色的颜色标识帮助用户更好地加与区分，有效防止插反。红色端为大电流输入/输出端，黄色端为I/O管脚控制端，一旦接反很有可能会损坏主控芯片或驱动模块，所以防反接处理还是很有必要的。

3.MegaPi的大电流驱动接口（MOS驱动接口）可以输出多大的驱动电流？

答：2个一共可以最大输出DC12V10A的电流。

4.步进电机驱动时为什么会发烫？

答：考虑到步进电机的使用场景，我们在出厂时已经把模块的驱动电流调到较大的位置，所以会有发热的现象。使用时请务必保证散热片和驱动模块接触良好，不然芯片很容易因为过热而烧毁。

5.步进电机上面的黑色可调电位器是干什么的？

答：这个电位器是调整步进电机驱动电流的，默认中间位置，可以调大也可以调小，调大时芯片发热也会增大，注意散热，要通风条件好或者换成更大的散热片。

6.MegaPi驱动多个电机时，主控重启的原因是什么？如何解决这个问题？

答：当电机启动和快速正反转的时候，电机的耗电量是很大的，如果几个电机同时工作在这种状态时就会因为电源瞬时放电能力不足而使电压过低，导致主控芯片重启。这时需要放电能力更大的电源，或者程序上做调整避免出现电机频繁正反转。

7.MegaPi跟树莓派连接时，如何焊接2\*10的排针？

答：MegaPi出厂时是默认不焊接与树莓派相连插座的，所以需要用户自己焊接，焊接引脚时要避免短路。

8.MegaPi跟树莓派连接时，驱动大功率器件时，树莓派重启，如何解决？

答：当通过MegaPi给树莓派供电时，如果操作电机，会因电机运动而造成电压不稳，有可能会是树莓派重启。这时可以再单独给树莓派供电。

9.MegaPi出现质量问题，如何联系官方售后？

答：请联系微信公众号：makeblock2013，
详细描述您的困惑，我们定将努力让您满意。

10.MegaPi有哪些相关配件可用？

答：编码电机驱动、步进电机驱动、蓝牙、2.4G、直流电机、编码电机、步进电机、Shield、RJ25接口的传感器、舵机、结构件等。

### 注意事项

- 应用MegaPi电压 DC6-12V
- 谨防短路
- 防止水、酸/碱度液体或固体杂物等等的污染
- 远离儿童和宠物
- 请勿随意丢弃

