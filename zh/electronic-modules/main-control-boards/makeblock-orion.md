# Orion 主控板


![](images/makeblock-orion_makeblock_orion.jpg)

<img src="images/makeblock-orion_微信截图_20160129152731.png" alt="微信截图_20160129152731" width="209" style="padding:5px 5px 12px 0px;">

### 概 述

Makeblock Orion 是一个基于 Arduino Uno 针对教学用途，升级改进的主控板。它拥有强大的驱动能力，输出功率可达 18W，可以驱动4个直流电机。精心设计的色标体系，与传感器模块完美匹配，8个独立的 RJ25 接口，轻松实现电路连接，非常方便用户使用。另外，它不仅支持绝大多数 Arduino 编程工具（Arduino /Scratch /
adublock），而且我们提供了两种 Scratch 升级版的图形编程工具（mBlock/
MakeblockHD）。

### 技术规格

- 输出电压: 5V DC  
- 工作电压: 6V-12V DC  
- 最大输入电流: 3A  
- 通讯模式: UART口, I²C，数字输入/输出, 模拟输入  
- 主控芯片: Atmega 328P  
- 尺寸: 80 x 60 x 18 mm (长 x 宽 x 高)

### 功能特性

- 完全兼容 Arduino；  
- 配备专用 Makeblock Arduino 库函数，简化编程难度；  
- 支持 mBlock （Scratch2.0升级版）适合全年龄用户；  
- 使用 RJ25 接口连线十分容易；  
- 模块化安装，兼容乐高系列；  
- 集成双路电机驱动。

### 接口介绍

主控板一共有8个RJ25接口， 接口上有六种不同颜色标签。

下图表是相对应的颜色与功能：

<img src="images/makeblock-orion_QQ截图20171213163332.jpg" alt="微信截图_20160129152940" style="padding:5px 5px 12px 0px;"> 

黄色，蓝色，灰色，黑色，紫色和白色的输出电压均为恒定的 5V 直流电。通常来说这些接口会连接到供电电压为 5v 的模块。

<img src="images/makeblock-orion_Makeblock电子模块手册——Makeblock-Orion-Powered-by-Disc_副本.png" alt="微信截图_20160129153132" style="padding:5px 5px 12px 0px;">

<img src="images/makeblock-orion_start_0s.jpg" alt="start_0s" width="704" style="padding:5px 5px 12px 0px;">

### 编程指导

1. 以下例子将向您展示如何使用 Arduino IDE 来控制声音传感器：

A)首先将主控板与电脑通过Micro-USB线连接，并将声音传感器连接到6号接口。

<img src="images/makeblock-orion_微信截图_20160129153247.png" alt="微信截图_20160129153247" width="304" style="padding:5px 5px 12px 0px;"> 

B) 创建新的Arduino IDE文档 ，并将如下代码拷贝到IDE。

C) 将程序上传到主控板。(上传过程5号接口请勿接东西)。

<img src="images/makeblock-orion_微信截图_20160129153303.png" alt="微信截图_20160129153303" width="380" style="padding:5px 5px 12px 0px;"> 

D)完成以上步骤后打开串口监视器，便可以观察到输出的数值随声音增大而增加。 

<img src="images/makeblock-orion_微信截图_20160129153313.png" alt="微信截图_20160129153313" width="436" style="padding:5px 5px 12px 0px;">

2. 以下实例将向展示如何在Arduino IDE环境下驱动直流电机。  

A) 首先将主控板与电脑通过Micro-USB线连接；

B) 然后将 Me 130 DCMotor 与主控板连接，如图所示:  

<img src="images/makeblock-orion_微信截图_20160129153447.png" alt="微信截图_20160129153447" width="286" style="padding:5px 5px 12px 0px;"> 

C) 创建新的Arduino IDE文档 并将如下代码拷贝到IDE；

D) 将程序上传到主控板；

E) 当程序成功上传，将看到马达顺时针转动2秒，停止2秒，逆时针转动2秒，停止2秒，循环往复

<img src="images/makeblock-orion_微信截图_20160129153454.png" alt="微信截图_20160129153454" width="260" style="padding:5px 5px 12px 0px;">

3.以下使用 mBlock 编程来控制 RGB 彩灯，设置灯为红色并让其进行亮一秒灭一秒的循环闪烁。

A) 首先将主控板与电脑通过Micro-USB线连接并将RGB模块连接到3号口；

B) 打开mBlock，创建如图所示的模块程序‘

C) 在“连接”选项中选择对应端口并连接；

D) 连接成功后，点击 mBlock 绿色旗子，主控板便会执行程序，红色灯闪烁

<img src="images/makeblock-orion_orion程序.png" alt="微信截图_20160129153501" width="446" style="padding:5px 5px 12px 0px;">


### 原理图

<img src="images/makeblock-orion_Orion-Sch-1.png" alt="Orion Sch" width="1350" style="padding:5px 5px 12px 0px;">

### 资源

驱动及工具链接: [https: //www.dropbox.com/s/y8o6u64awxoc10m/Orion%20firmware.rar?dl=0](//www.dropbox.com/s/y8o6u64awxoc10m/Orion%20firmware.rar?dl=0)
