# 传感器类

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="sensors/me-3-axis-accelerometer-and-gyro-sensor.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-3-axis-accelerometer-and-gyro-sensor_Me-3-Axis-Accelerometer-and-Gyro-Sensor.jpg" width="150px;"></a><br>
<p>陀螺仪</p></td>

<td width="25%;"><a href="sensors/me-flame-sensor.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-flame-sensor_Me-Flame-Sensor.jpg" width="150px;"></a><br>
<p>火焰传感器</p></td>

<td width="25%;"><a href="sensors/me-gas-sensormq2.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-gas-sensormq2_Me-Gas-Sensor.jpg" width="150px;"></a><br>
<p>气体传感器</p></td>

<td width="25%;"><a href="sensors/me-light-sensor.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-light-sensor_Me-Light-Sensor.jpg" width="150px;"></a><br>
<p>光线传感器</p></td>
</tr>

<tr>
<td><a href="sensors/me-line-follower.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-line-follower_Me-Line-Follower.jpg" width="150px;"></a><br>
<p>巡线传感器</p></td>
<td><a href="sensors/me-micro-switch-ab.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-micro-switch-ab_Me-Micro-Switch-A.jpg" width="150px;"></a><br>
<p>限位开关</p></td>
<td><a href="sensors/me-pir-motion-sensor.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-pir-motion-sensor_Me-PIR-Motion-Sensor.jpg" width="150px;"></a><br>
<p>人体红外传感器</p></td>
<td><a href="sensors/me-sound-sensor.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-sound-sensor_Me-Sound-Sensor.jpg" width="150px;"></a><br>
<p>声音传感器</p></td>
</tr>

<tr>
<td><a href="sensors/me-temperature-and-humidity-sensor.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-temperature-and-humidity-sensor_Me-Temperature-and-Humidity-Sensor.jpg" width="150px;"></a><br>
<p>温湿度传感器</p></td>
<td><a href="sensors/temperature-sensor-waterproofds18b20.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/temperature-sensor-waterproofds18b20_Me-Temperature-Sensor-Waterproof(DS18B20).jpg" width="150px;"></a><br>
<p>温度传感器</p></td>
<td><a href="sensors/me-touch-sensor.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-touch-sensor_Me-Touch-Sensor.jpg" width="150px;"></a><br>
<p>触摸传感器</p></td>
<td><a href="sensors/me-ultrasonic-sensor.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-ultrasonic-sensor_Me-Ultrasonic-Sensor.jpg" width="150px;"></a><br>
<p>超声波模块</p></td>
</tr>

<tr>
<td><a href="sensors/me-compass.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-compass_Me-Compass.jpg" width="150px;"></a><br>
<p>指南针模块</p></td>
<td><a href="sensors/me-color-sensor-v1.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/color-sensor-2.jpg" width="150px;"></a><br>
<p>颜色传感器</p></td>
<td><a href="sensors/rgb-line-follower.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/rgb-line-follower_20183271632.png" width="150px;"></a><br>
<p>RGB巡线模块</p></td>
<td><a href="sensors/me-audio-player.html" target="_blank"><img src="../../en/electronic-modules/sensors/images/me-audio-player-1.jpg" width="150px;"></a><br>
<p>音频播放模块</p></td>
</tr>

</table>
