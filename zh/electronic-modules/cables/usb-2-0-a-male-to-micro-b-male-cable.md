# USB 转 Micro线


![](../../../en/electronic-modules/cables/images/usb-2-0-a-male-to-micro-b-male-cable_USB-2.0-A-Male-to-Micro-B-Male-Cable.jpg)

 
<img src="../../../en/electronic-modules/cables/images/usb-2-0-a-male-to-micro-b-male-cable_微信截图_20160126173525.png" alt="微信截图_20160126173525" width="230" style="padding:5px 5px 12px 0px;">

### 描述

作为USB 2.0标准的便携版本，Micro USB更小，节省空间，与标准 USB 和迷你 USB 接口相比，它的插塞寿命更长，插塞强度更高。

Micro USB兼容 USB1.1（低速:1.5Mb/s，全速:12Mb/s）和USB2.0（高速:480Mb/s），同时提供数据传输和充电功能，是连接小设备（如手机、PDA、数码相机、数字视频、便携式数字播放器等）的最佳选择。

微型USB接口可以连接到 Makeblock Orion 开发板，用于下载程序和调试。

### 技术规格

-   线长: 1 m
-   接口: Micro USB 标准端口
-   功能: 数据传输与充电

### 功能特性

-   热插拔
-   便携与方便
-   标准
