# 快门线

![](../../../en/electronic-modules/cables/images/shutter-cable-c1-c3-n1-n3-for-canon_Shutter-Cable-C1-for-Canon.jpg)

<img src="../../../en/electronic-modules/cables/images/shutter-cable-c1-c3-n1-n3-for-canon_微信截图_20160126151-300x275.png" alt="微信截图_20160126151" width="331" style="padding:5px 5px 12px 0px;">

### 描述

快门线设计用于连接Arduino项目中的Me快门模块和数码相机。
它可以通过Makeblock Orion程序进行控制，以实现实时和重复拍照。

### 技术规格

-   线长: 26 cm, 伸展 – 60 cm
-   N1适配尼康: F6. F5. F90. F90X. F100. D3. D1/ D1H/ D1X. D2/ D2H/ D2X.D3/ D3X/ D100/ D200/D300/D700/D300S/D800
-   快门线N3 适配Nikon:D90/D5000/D5100/D3100/D7000/D600/D3200
-   快门线C1 适配Canon: 650D/600D/1100D/60D/550D/500D/1000D/450D/
    400D/350D/300D
-   快门线C3 适配Canon:EOS 5DII /5D /7D / 6D /5D3/70D/60D
    50D /40D/ 30D/20D /10D /EOS1V /EOS3/ EOS1D/1DS/1DSMKⅡ/1DSMKⅢ /EOSD30

### 功能特性

-   容易插拔
-   易携带的
