# 35cm RJ25 线


![](../../../en/electronic-modules/cables/images/6p6c-rj25-cable_6P6C-RJ25-cable-35cm.jpg)

<img src="../../../en/electronic-modules/cables/images/6p6c-rj25-cable_微信截图_20160126174309-300x276.png" alt="微信截图_20160126174309" width="300" style="padding:5px 5px 12px 0px;">

### 描述

RJ25线可用于连接mCore，Makeblock Orion 和大多数支持RJ25端口的 Makeblock 电子模块。相对于传统的杜邦线，它具有快速连接，减少出错，而且更加美观的优势。RJ25线集成了六根电线。

### 技术规格

- 三种长度: 20 cm, 35 cm, 50 cm
- RJ25线: 6P6C

### 功能特性

- 快速插头
- 便携式
- 统一标准
