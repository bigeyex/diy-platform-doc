# 显示类

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="displays/me-7-segment-display.html" target="_blank"><img src="../../en/electronic-modules/displays/images/me-7-segment-display_Me-7-Segment-Serial-Display---Red.jpg" width="150px;"></a><br>
<p>数码管模块</p></td>

<td width="25%;"><a href="displays/led-rgb-strip-addressable-sealed-0-5m1m.html" target="_blank"><img src="../../en/electronic-modules/displays/images/led-rgb-strip-addressable-sealed-0-5m1m_LED-RGB-Strip-Addressable,-Sealed.jpg" width="150px;"></a><br>
<p>灯条</p></td>

<td width="25%;"><a href="displays/me-led-matrix-8x16.html" target="_blank"><img src="../../en/electronic-modules/displays/images/me-led-matrix-8x16_Me-LED-Matrix-8×16.jpg" width="150px;"></a><br>
<p>表情面板模块</p></td>

<td width="25%;"><a href="displays/me-rgb-led.html" target="_blank"><img src="../../en/electronic-modules/displays/images/me-rgb-led_Me-RGB-LED.jpg" width="150px;"></a><br>
<p>RGB灯模块</p></td>
</tr>

<tr>
<td><a href="displays/me-tft-lcd-screen-2-2-inch.html" target="_blank"><img src="../../en/electronic-modules/displays/images/me-tft-lcd-screen-2-2-inch_Me-TFT-LCD-Screen---2.2-Inch.jpg" width="150px;"></a><br>
<p>2.2吋TFT显示屏模块</p></td>
</tr>
</table>