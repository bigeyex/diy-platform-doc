# 2.2吋TFT显示屏模块


![](../../../en/electronic-modules/displays/images/me-tft-lcd-screen-2-2-inch_Me-TFT-LCD-Screen---2.2-Inch.jpg)

<img src="../../../en/electronic-modules/displays/images/me-tft-lcd-screen-2-2-inch_微信截图_20160129104542.png" alt="微信截图_20160129104542" width="213" style="padding:5px 5px 12px 0px;">

### 概述

TFT彩屏模块主要部件为LCD显示屏，此液晶屏为串口液晶屏，能通过串口和主控板通信，从而显示大小不同，颜色不同的字体和图形。本模块接口是蓝/灰色色标，说明是双数字接口，需要连接到主板上蓝色或灰色标识的接口。

### 技术规格

- 工作电压： 5V DC  
- 工作温度: -10\~70℃  
- 屏幕型号：Usart GPU22A  
- 屏幕尺寸: 2.2″  
- 屏幕分辨率: 240×320  
- 控制方式：串口通信

### 功能特性

- Flash存储容量：2M；  
- 只占2个IO口（串口脚）；  
- 支持自动抽取式24点阵 32点阵 48点阵 64点阵汉字；  
- 支持点、线、圆、方框、填充方框等绘图；  
- 支持JPG格式真彩图形的显示；  
- 模块的白色区域是与金属梁接触的参考区域；  
- 支持 Arduino IDE 编程；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有接头支持绝大多数 Arduino 系列主控板。

### 引脚定义

TFT彩屏模块有四个针脚的接头,每个针脚的功能如下表:

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;"> 1 </td>
<td style="border: 1px solid black;">TX</td>
<td style="border: 1px solid black;">串口数据发送口</td>
</tr>

<tr>
<td style="border: 1px solid black;"> 2 </td>
<td style="border: 1px solid black;">RX</td>
<td style="border: 1px solid black;">串口数据接收口</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">接电源 </td>
</tr>

<tr>
<td style="border: 1px solid black;"> 4</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">接地</td>
</tr>
</table>

<br>

### 接线方式

● **RJ25连接**

由于TFT彩屏模块接口是蓝/灰色色标，当使用RJ25接口时，需要连接到主控板上带有蓝色或灰色色标的接口。以 Makeblock Orion 为例，可以连接到5号接口，如图：  

<img src="../../../en/electronic-modules/displays/images/me-tft-lcd-screen-2-2-inch_微信截图_20160129104918.png" alt="微信截图_20160129104918" width="366" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**  

当使用杜邦线连接到 Arduino Uno 主板时，模块TX、RX引脚需要分别连接到RX、TX口上，如下图所示： 

<img src="../../../en/electronic-modules/displays/images/me-tft-lcd-screen-2-2-inch_微信截图_20160129104954.png" alt="微信截图_20160129104954" width="399" style="padding:5px 5px 12px 0px;">

### 编程指南

● **Arduino编程**  

如果使用 Arduino 编程，需要调用库`Makeblock-Library-master`来控制TFT彩屏。 

本程序通过 Arduino 编程显示不同的图案及文字。

<img src="../../../en/electronic-modules/displays/images/me-tft-lcd-screen-2-2-inch_微信截图_20160129105214.png" alt="微信截图_20160129105214" width="480" style="padding:5px 5px 12px 0px;">

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">说明</th>
</tr>
<tr>
<td style="border: 1px solid black;">CLS(c)</td>
<td style="border: 1px solid black;">用c颜色清屏</td>
</tr>
<tr>
<td style="border: 1px solid black;">SBC(c)</td>
<td style="border: 1px solid black;">设置背景色C，显示汉字等时无点阵时填的颜色</td>
</tr>
<tr>
<td style="border: 1px solid black;"> PS(x,y,c)</td>
<td style="border: 1px solid black;">在(x,y)的地方画一个颜色c的点    </td>
</tr>
<tr>
<td style="border: 1px solid black;">PL(x1,y1,x2,y2,c);/td&gt; </td>
<td style="border: 1px solid black;"></td>
</tr>
</table>

<br>