# 灯条

![](../../../en/electronic-modules/displays/images/led-rgb-strip-addressable-sealed-0-5m1m_LED-RGB-Strip-Addressable,-Sealed.jpg)

<img src="../../../en/electronic-modules/displays/images/led-rgb-strip-addressable-sealed-0-5m1m_微信截图_20160129103438.png" alt="微信截图_20160129103438" width="308" style="padding:5px 5px 12px 0px;">

### 概述

LED RGB灯带（可寻址，密封）包含许多可寻址的LED RGB灯。 长度为1米的LED
RGB灯带共包含30个RGB LED。

除了高亮度和全彩色的特点之外，您还可以通过编程来单独控制每个RGB的颜色和亮度。LED RGB灯带可以通过RJ25适配器模块连接到Makeblock Orion。

### 技术规格

- 长度: 1 m, 0.5 m  
- 工作电压: 5V DC  
- 工作电流: 1.8A/m (Maximum current)  
- 工作温度: -40\~+60℃  
- 可视角度: 140°  
- 使用寿命: 50,000 hours  
- 尺寸: 1000 x 13 x 3 mm (L x W x H) excluding cable

### 功能特性

- 256级亮度显示，16777216种真彩显示  
- 与Arduino库完全匹配，方便用户编程  
- 每个LED都可以单独控制  
- 改变灯带的形状来实现不同的发光模式

### 电路原理图

<img src="../../../en/electronic-modules/displays/images/led-rgb-strip-addressable-sealed-0-5m1m_微信截图_20160129103957.png" alt="微信截图_20160129103957" width="435" style="padding:5px 5px 12px 0px;">
