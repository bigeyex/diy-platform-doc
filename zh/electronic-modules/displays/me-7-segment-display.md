# 数码管模块

![](../../../en/electronic-modules/displays/images/me-7-segment-display_Me-7-Segment-Serial-Display---Red.jpg)

<img src="../../../en/electronic-modules/displays/images/me-7-segment-display_微信截图_20160129110823.png" alt="微信截图_20160129110823" width="181" style="padding:5px 5px 12px 0px;">

### 概述

数码管模块采用四位共阳极数码管，用于显示数字和少数特殊字符。可以在机器人项目中使用该模块，用于显示速度、时间、分数、温度、距离等传感器的值。同时，Makeblock 提供易于编程的 Arduino 库，使用户能够方便地控制数码管。本模块接口是蓝色色标，说明是双数字口控制，需要连接到主板上带有蓝色标识接口。

### 技术规格

- 工作电压：5V DC  
- 数字位数：4  
- 工作温度：-40到85℃  
- 控制方式：双数字控制  
- 模块尺寸：51 x 24 x 23.4 mm (长x宽x高)

### 功能特性

- 模块的白色区域是与金属梁接触的参考区域；  
- 4位红色LED，每位有一个小数点；  
- 亮度可调节，使得用户即使在白天也能看清显示内容；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持 mBlock 图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有CLK、DIO、VCC、GND接头支持绝大多数Arduino系列主控板。

### 引脚定义

数码管模块有四个针脚的接头,每个针脚的功能如下表:

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚 </th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;"> DIO </td>
<td style="border: 1px solid black;">数据线</td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">CLK </td>
<td style="border: 1px solid black;">时钟线</td>
</tr>
</table>

<br>

### 接线方式

● **RJ25连接**  

由于数码管模块接口是蓝色色标，当使用RJ25接口时，需要连接到主控板上带有蓝色色标的接口。以 Makeblock Orion 为例，可以连接到3，4，5，6号接口，如图：  

<img src="../../../en/electronic-modules/displays/images/me-7-segment-display_微信截图_20160129111212.png" alt="微信截图_20160129111212" width="391" style="padding:5px 5px 12px 0px;">

● **杜邦线连接** 

当使用杜邦线连接到 Arduino Uno 主板时，模块 DIO 与 CLK 引脚需要连接到
DIGITAL（数字）口，如下图所示：

<img src="../../../en/electronic-modules/displays/images/me-7-segment-display_微信截图_20160129111242.png" alt="微信截图_20160129111242" width="365" style="padding:5px 5px 12px 0px;">

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">接口函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">Me7SegmentDisplay(uint8_t port)</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>
<tr>
<td style="border: 1px solid black;">void init()</td>
<td style="border: 1px solid black;">初始化模块，清空显示器</td>
</tr>
<tr>
<td style="border: 1px solid black;">void set(uint8_t brightness, uint8_t SetData, uint8_t SetAddr)</td>
<td style="border: 1px solid black;">调整亮度设定数据到指定地址</td>
</tr>
<tr>
<td style="border: 1px solid black;">void display(float value)<br>
void display(int8_t value)<br>
void display(uint8_t BitAddr,int8_t DispData)</td>
<td style="border: 1px solid black;">显示数字</td>
</tr>
</table>

<br>

### 编程指南

● **Arduino 编程**  

如果使用 Arduino 编程，需要调用库`Makeblock-Library-master` 来控制数码管模块。

本程序通过 Arduino 编程可以使数码管显示15位数字（1,2,3,4,5,6,7,8,9,A,b,C,d,E,F）从右向左移动。 

<img src="../../../en/electronic-modules/displays/images/me-7-segment-display_微信截图_20160129111343.png" alt="微信截图_20160129111343" width="548" style="padding:5px 5px 12px 0px;">

### 原理图

<img src="../../../en/electronic-modules/displays/images/me-7-segment-display_Me-7-segment.png" alt="Me 7-segment" width="1489" style="padding:5px 5px 12px 0px;">
