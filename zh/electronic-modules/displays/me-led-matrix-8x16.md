# 表情面板模块

![](../../../en/electronic-modules/displays/images/me-led-matrix-8x16_Me-LED-Matrix-8×16.jpg)

<img src="../../../en/electronic-modules/displays/images/me-led-matrix-8x16_LED_Matrix3.png" alt="LED_Matrix3" width="230" style="padding:5px 5px 12px 0px;">

### 概述

表情面板共包含128颗对齐的LED。 颜色是蓝色的。通过接收来自主板的数据，可以控制显示数字，字母或符号。连接器上有一个蓝色的ID。 这意味着这个模块可以连接到 Makeblock Orion 上有蓝色标识的端口。

### 技术规格

- 工作电压: 5V DC  
- 通信模式: I²C  
- 模块尺寸e: 73 x 31 x 15 mm (L x W x H)

### 功能特性

- 显示主板编程而来的数字，字符串或符号；  
- 模块的白色区域是与金属梁接触的参考区域；  
- 防反接保护，反接电源不会损坏IC；  
- 支持 mBlock GUI 编程，适用于各年龄段用户；  
- 采用RJ25接口，连接方便；  
- 提供接入 Arduino 系列开发板的引脚；

### 引脚定义

表情面板模块有三个引脚, 他们的功能如下:

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">DIN</td>
<td style="border: 1px solid black;">数据</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">SCK</td>
<td style="border: 1px solid black;">时钟</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">5V </td>
<td style="border: 1px solid black;">电源供应</td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;"> 接地</td>
</tr>
</table>

<br>

### 连线模式

● **与RJ25连接**

由于表情面板模块的端口具有蓝色标识，因此在使用RJ25端口时，需要使用 Makeblock Orion 上的蓝色端口连接端口。 

以Makeblock Orion为例，您可以将它连接到端口号3,4,5和6，如下所示：

<img src="../../../en/electronic-modules/displays/images/me-led-matrix-8x16_board.png" alt="board" width="448" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino 编程**  

如果使用Arduino编写程序，则应调用库`Makeblock-Library-master`来控制表情面板。

从而显示时间，字符串或数字。

<img src="../../../en/electronic-modules/displays/images/me-led-matrix-8x16_code.png" alt="code" width="259" style="padding:5px 5px 12px 0px;">

**表情面板函数功能表**

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeLEDMatrix ledMx(PORT_4)</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">ledMx.showClock(12.03.1)</td>
<td style="border: 1px solid black;">显示时间</td>
</tr>

<tr>
<td style="border: 1px solid black;">ledMx.drawStr(0.8,s)</td>
<td style="border: 1px solid black;">显示字符串</td>
</tr>

<tr>
<td style="border: 1px solid black;">ledMx.showNum()</td>
<td style="border: 1px solid black;">显示数字</td>
</tr>
</table>

<br>

### 原理分析

表情面板包含128颗矩阵LED，每行8个，每行16个。主板通过I2C总线将数据发送给模块，模块中的LED控制专用芯片可以处理数据并解释。并输出相关信号打开或关闭LED的电源。 让他们显示内容。

### 原理图

<img src="../../../en/electronic-modules/displays/images/me-led-matrix-8x16_LED-Matrix.png" alt="LED Matrix" width="1293" style="padding:5px 5px 12px 0px;">
