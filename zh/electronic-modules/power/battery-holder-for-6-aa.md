# 6节AA电池盒


<img src="../../../en/electronic-modules/power/images/battery-holder-for-6-aa_Battery-Holder-for-(6)AA.jpg">

<br>

<img src="../../../en/electronic-modules/power/images/battery-holder-for-6-aa_微信截图_20160126174443.png" alt="微信截图_20160126174443" width="220" style="padding:5px 5px 12px 0px;">

### 概述

6AA电池座可容纳6节AA级电池，级联后可提供9V直流电压。通过标准的DC端口，它可以连接到 Makeblock Orion 开发板，为开发板和电机提供电源。电池座上的两个圆孔设计用于固定在支架和 Makeblock 机械部件上。

### 技术规格

- 电缆长度20cm
- DC插头的大小：5.5×2.1×10毫米（外径×内径×L）

### 功能特性

- 便携式和方便
- 易于组装
