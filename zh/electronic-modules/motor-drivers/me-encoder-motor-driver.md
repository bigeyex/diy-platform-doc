# 编码电机驱动模块


![](../../../en/electronic-modules/motor-drivers/images/me-encoder-motor-driver_encoderimage3.png)

<img src="../../../en/electronic-modules/motor-drivers/images/me-encoder-motor-driver_encoderimage2.png" alt="微信截图_20160203150524" width="361" style="padding:5px 5px 12px 0px;">

### 概述

支持双通道直流编码器电机，编码电机包括了MCU和电机驱动芯片。MCU 内置 PID 算法，实现对电机速度和方向的精确控制。电机的 PID 参数可以通过 Makeblock 官方网站上提供的软件进行控制，以便在不同环境下都能达到最佳工作状态。该模块旨在轻松，快速，准确地控制电机。

如果你想更好地使用这个模块，例如自平衡车。您需要调整PID和一些参数，这就要求用户有相关的基础。该模块还可以应用于PID算法的学习，打开SPI和串口以便二次开发，所以它相当于一个小型电机开发板。它的红色端口表示它的输入电压为6〜12V，且应该连接到 Makeblock Orion 上带有红色的端口。

### 技术规格

- 电机通道: 2  
- 输入电压: 6V-12V DC  
- 单通道最大持续电流: 1.2A  
- 单通道峰值电流: 3.2A  
- MCU工作电压: 5V DC  
- 通讯口: I²C  
- 输出口: SPI – 串口  
- 电机接口: M+, M-, GND, 5V, ENC1, ENC2  
- 工作温度: -40\~85℃  
- 尺寸大小: 67.5 x 32 x 18 mm (长 x 宽 x 高)

### 功能特性

- 模块的白色区域是接触金属梁的起始区域  
- 精准地控制电机的位置，速度与方向  
- 提供实时的位置和速度反馈  
- 配置有效的基于H桥的电机驱动芯片  
- 过流保护及防反向保护  
- 支持mBlock图形化编程，适用全年龄  
- 采用便于连接的RJ25口  
- 配备反馈双通道电机前进/翻转指示灯的MCU reset键  
- 配有接头支持绝大多数Arduino系列主控板。

### 针脚定义

编码驱动模块有12个针脚的接头,每个针脚的功能如下表:

<img src="../../../en/electronic-modules/motor-drivers/images/me-encoder-motor-driver_微信截图_20160203150905.png" alt="微信截图_20160203150905" width="720" style="padding:5px 5px 12px 0px;">

### 接线方式

● **RJ25连接**  

由于编码电机驱动模块接口是红色色标，属于电机驱动。当使用 RJ25 接口时，需要连接到主控板上带有红色色标的接口。以 Makeblock
Orion 为例，可以连接到1、2号接口，如图:

<img src="../../../en/electronic-modules/motor-drivers/images/me-encoder-motor-driver_微信截图_20160203150949.png" alt="微信截图_20160203150949" width="358" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**  

当使用杜邦线连接到Arduino Uno主板时，它的 SCL 和 SDA 引脚应该连接I2C口，即是, A5 主 A4 口如下:

<img src="../../../en/electronic-modules/motor-drivers/images/me-encoder-motor-driver_微信截图_20160203151003.png" alt="微信截图_20160203151003" width="343" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino编程**  

如果使用Arduino编程，需要调用库 ``Makeblock-Library-master`` 来控制编码电机驱动模块。  

本程序通过Arduino编程让电机按需求转动。

<img src="../../../en/electronic-modules/motor-drivers/images/me-encoder-motor-driver_微信截图_20160203151124.png" alt="微信截图_20160203151124" width="456" style="padding:5px 5px 12px 0px;">

<img src="../../../en/electronic-modules/motor-drivers/images/me-encoder-motor-driver_微信截图_20160203151209.png" alt="微信截图_20160203151209" width="804" style="padding:5px 5px 12px 0px;">

> 需要 API 文档, 请访问: [API Documentation of Me Encoder Motor](http://learn.makeblock.com/cn/api-documentation-of-me-encoder-motor/)

### 原理解析

直流编码电机驱动的编码电机与普通直流电机区别之处在于编码电机包含了磁性编码器。采用磁场原理产生信号的优势是磁场信号不会受到灰尘，湿气，高温及振动的影响。在磁性编码器内部采用一个磁性转盘和一个霍尔传感器。磁性转盘的旋转会引起内部磁场强度的变化。当交变磁场经过时，霍尔传感器产生输出电压脉冲，脉冲的幅度是由激励磁场的场强决定的。磁盘每旋转一个磁化长度，磁场变化半周，信号输出则变化一个周期，磁盘上磁极对的数量和输出信号的周期数在旋转一周时是相等的。所以通过测定输出信号的周期就可知道磁盘的位置和转速。

### 原理图

<img src="../../../en/electronic-modules/motor-drivers/images/me-encoder-motor-driver_encode.png" alt="encode" width="1270" style="padding:5px 5px 12px 0px;">
