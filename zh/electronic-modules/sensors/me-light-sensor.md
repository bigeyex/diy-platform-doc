# 光线传感器


![](../../../en/electronic-modules/sensors/images/me-light-sensor_Me-Light-Sensor.jpg)

<img src="../../../en/electronic-modules/sensors/images/me-light-sensor_微信截图_20160129151455.png" alt="微信截图_20160129151455" width="245" style="padding:5px 5px 12px 0px;">

### 概述

光线传感器是基于半导体的光电效应原理所开发的，其可用来对周围环境光的强度进行检测，还可以被用来检测不同颜色表面的光线差别。用户能够用它来制作一些和光互动的项目，比如智能调光小灯，一个激光通信系统或者更酷的一些事情。本模块接口是黑色色标，说明是模拟信号接口，传感器模块连接主控板上带黑色色标的接口。

### 技术规格

- 工作电压：5V DC  
- 工作温度范围:-30°C到70°C  
- 模块尺寸：52 x 24 x 18 mm (长x宽x高)  
- 控制方式：单模拟口控制  
- 模拟输出值:暴露于日光(&gt; 500),晚上(0 \~ 100),室内照明情况(100 \~
500)

### 功能特性

- 仅对可见光敏感，不需要额外的过滤镜；  
- 模块的白色区域是与金属梁接触的参考区域；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持 Arduino IDE 编程,并且提供运行库来简化编程；  
- 支持 mBlock 图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有接头支持绝大多数 Arduino 系列主控板。

### 引脚定义

光线传感器模块有三个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">介绍 </th>
</tr>

<tr>
<td style="border: 1px solid black;"> 1 </td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">接地</td>
</tr>

<tr>
<td style="border: 1px solid black;">2 </td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;"> 接电源 </td>
</tr>

<tr>
<td style="border: 1px solid black;"> 3 </td>
<td style="border: 1px solid black;">AO</td>
<td style="border: 1px solid black;">模拟信号输出端</td>
</tr>

</table>

<br>

### 连线方式

● **RJ25连接**  

由于光线传感器模块接口是黑色色标，当使用RJ25接口时，需要连接到主控板上带有黑色色标的接口。

以Makeblock Orion为例，可以连接到6，7，8号接口，如图:

<img src="../../../en/electronic-modules/sensors/images/me-light-sensor_微信截图_20160129151717.png" alt="微信截图_20160129151717" width="383" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**  

当使用杜邦线连接到 Arduino Uno 主板时，模块AO引脚需要连接到ANALOG（模拟）口,如下图所示:

<img src="../../../en/electronic-modules/sensors/images/me-light-sensor_微信截图_20160129151746.png" alt="微信截图_20160129151746" width="390" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino编程** 

如果使用 Arduino 编程，需要调用库 ``Makeblock-Library-master`` 来控制光线传感器。本程序通过Arduino编程让光线传感器读取当前光线强度。

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">方程</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeLightSensor (uint8_t port</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">int read()</td>
<td style="border: 1px solid black;">读取光强度值</td>
</tr>

</table>

<br>

代码功能介绍：  
读取光强检测结果，将结果输出到 Arduino IDE 串口监视器您将看到运行结果如下:

<img src="../../../en/electronic-modules/sensors/images/me-light-sensor_微信截图_20160129152118.png" alt="微信截图_20160129152118" width="511" style="padding:5px 5px 12px 0px;">

● **mBlock编程**  

光线传感器模块支持mBlock编程环境，如下是该模块指令简介:

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<thead>
<tr>
<th>编程说明</th>
<th>介绍</th>
</tr>
</thead>
<tbody>
<tr>
<td style="border: 1px solid black;"><img src="images/me-light-sensor_12312423.png" alt="微信截图_20160129152118" width="261" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">参数： 选定接口；<br>
<br>
功能： 返回光线强度数值(范围： 0~980)</td>
</tr>
</tbody>
</table>

<br>

以下是如何使用mBlock控制光线传感器模块的例子：  
LED接收光线，M-Panda会左右移动并说I lovesunshine；遮住LED灯，M-Panda会停止移动并说I love night。

运行结果如下:

<img src="../../../en/electronic-modules/sensors/images/me-light-sensor_微信截图_20160129152226.png" alt="微信截图_20160129152226" width="659" style="padding:5px 5px 12px 0px;">

### 原理解析

本模块是基于半导体的光电效应原理所开发的光线传感器，主要部件为光电晶体管，其电阻随光的强度增加而减小，通过和另一电阻串联，输出电阻的分压值，便能将变化的光信号变换为变化的电气信号，并从模拟口输出。此模块反应灵敏，可以制作与光互动的项目，例如：制作可自我调节亮度的小台灯，来确保环境的光强在人体的舒适范围内。

### 原理图

<img src="../../../en/electronic-modules/sensors/images/me-light-sensor_lightsensor.png" alt="Me light sensor" width="1065" style="padding:5px 5px 12px 0px;">
