# 触摸传感器

![](../../../en/electronic-modules/sensors/images/me-touch-sensor_Me-Touch-Sensor.jpg)

<img src="../../../en/electronic-modules/sensors/images/me-touch-sensor_微信截图_20160129142421.png" alt="微信截图_20160129142421" width="204" style="padding:5px 5px 12px 0px;">

### 概 述

触摸模块的元件主要是1个触摸IC。触摸检测是为了用可变面积的区域取代传统的按钮键而设计的。当被触摸时，板上的蓝色LED灯会亮起,否则熄灭。它可以结合其他器件做成触摸控制的台灯。本模块接口是蓝色色标，说明是双数字接口，需要连接到主板上的蓝色标识的接口。

### 技术规格

- 工作电压： 5V DC  
- 响应时间：60-220ms  
- 芯片工作最大电流：7uA  
- 工作温度：-20°至+70°  
- 模块尺寸：51 x 24 x 18 mm (长x宽x高)

### 功能特性

- 始终进行自校准，当键没被触摸时，重校准周期约为4.0s；  
- 反应灵敏，延时小；  
- 模块的白色区域是与金属梁接触的参考区域；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持Arduino IDE编程, 并且提供运行库来简化编程；  
- 支持mBlock图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有接头支持绝大多数Arduino系列主控板。

### 引脚定义

触摸模块有四个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;"> 1 </td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">接地</td>
</tr>

<tr>
<td style="border: 1px solid black;">2 </td>
<td style="border: 1px solid black;">5V</td>
<td style="border: 1px solid black;"> 接电源 </td>
</tr>

<tr>
<td style="border: 1px solid black;"> 3 </td>
<td style="border: 1px solid black;">OT</td>
<td style="border: 1px solid black;">数字输出</td>
</tr>

<tr>
<td style="border: 1px solid black;"> 4 </td>
<td style="border: 1px solid black;">Touch</td>
<td style="border: 1px solid black;">传感器输入口</td>
</tr>

</table>

### 接线方式

● **RJ25连接** 

由于触摸模块接口是蓝色色标，当使用RJ25接口时，需要连接到主控板上带有蓝色色标的接口。以 Makeblock Orion 为例，可以连接到3，4，5，6号接口，如图：
  
<img src="../../../en/electronic-modules/sensors/images/me-touch-sensor_微信截图_20160129142638.png" alt="微信截图_20160129142638" width="338" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**

当使用杜邦线连接到Arduino Uno主板时，模块OUT引脚需要连接到DIGITAL(数字)口，TOUCH引脚是可以外接金属片以作触摸输入用，如图所示：  

<img src="../../../en/electronic-modules/sensors/images/me-touch-sensor_微信截图_20160129143127.png" alt="微信截图_20160129143127" width="315" style="padding:5px 5px 12px 0px;">

### 编程指南

● **Arduino 编程**  

如果使用 Arduino 编程，需要调用库 ``Makeblock-Library-master`` 来控制触摸模块。本程序通过 Arduino 编程来判断是否有被触摸。

<img src="../../../en/electronic-modules/sensors/images/me-touch-sensor_微信截图_20160129143212.png" alt="微信截图_20160129143212" width="538" style="padding:5px 5px 12px 0px;">


<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeTouchSensor (uint8_t)</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">bool touched()</td>
<td style="border: 1px solid black;">判断是否被触摸</td>
</tr>

</table>

代码功能介绍：当触摸模块被触摸时，串口输出“State:
DOWN.”当没有被触摸时，串口输出“State: UP.”

● **mBlock 编程**  

触摸模块支持mBlock编程环境，如下是该模块指令简介当按住触摸模块时，小熊猫左右移动并说：Nice to meet you\~，当松开触摸模块时，小熊猫停止移动并说：byebye，随后隐藏。

<img src="../../../en/electronic-modules/sensors/images/me-touch-sensor_微信截图_20160129143330.png" alt="微信截图_20160129143330" width="609" style="padding:5px 5px 12px 0px;">

### 原理解析

这是一个基于电容感应的触摸模块。用户可以通过RJ25口来设定模块的工作方式。RJ25接口上TOG引脚是用来控制工作模式的。当TOG为高电平时，为触发模式；当TOG为低电平时为直接模式。人体或金属在传感器金属面上的直接触碰会被感应到。除了与金属面的直接触摸，隔着一定厚度的塑料、玻璃等材料的接触也可以被感应到，感应灵敏度随接触面的大小和覆盖材料的厚度有关。

### 原理图

<img src="../../../en/electronic-modules/sensors/images/me-touch-sensor_Me-touch-sensor.png" alt="Me touch sensor" width="870" style="padding:5px 5px 12px 0px;">
