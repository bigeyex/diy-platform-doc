# 声音传感器


![](../../../en/electronic-modules/sensors/images/me-sound-sensor_Me-Sound-Sensor.jpg)

<img src="../../../en/electronic-modules/sensors/images/me-sound-sensor_微信截图_20160129145124.png" alt="微信截图_20160129145124" width="200" style="padding:5px 5px 12px 0px;">

### 概述

声音传感器以麦克风为基础，可用来对周围环境中的声音强度进行检测，主要部件为LM2904低功耗放大器。你可以用它来做一些交互性项目，例如声控开关，跟随舞蹈变动的机器人。本模块接口是黑色色标，说明是模拟口控制，需要连接到主板上带有黑色标识接口。

### 技术规格

- 工作电压： 5V DC  
- 麦克风灵敏度(1 khz):50-54dB  
- 麦克风阻抗:2.2 kΩ  
- 麦克风频率:16-20Khz  
- 麦克风信噪比:54 db  
- 返回值范围：0\~980  
- 控制方式：单模拟口输出  
- 最大电流：0.5mA  
- 基于LM2906功率放大器  
- 模块尺寸：51 x 24 x 18 mm (长x宽x高)

### 功能特性

- 板载LED亮度显示声音相对大小；  
- 对声音灵敏度高；  
- 模块的白色区域是与金属梁接触的参考区域；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持Arduino IDE编程, 并且提供运行库来简化编程；  
- 支持mBlock图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有接头支持绝大多数Arduino系列主控板。

| 声音环境 | 模拟输出值范围 |
|----------|----------------|
| 安静     | 约0-483        |
| 嘈杂     | 约483-980      |

<br>

### 引脚定义

声音传感器模块有三个针脚的接头,每个针脚的功能如下表:

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">介绍 </th>
</tr>

<tr>
<td style="border: 1px solid black;"> 1 </td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">接地</td>
</tr>

<tr>
<td style="border: 1px solid black;">2 </td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;"> 接电源 </td>
</tr>

<tr>
<td style="border: 1px solid black;"> 3 </td>
<td style="border: 1px solid black;">AO</td>
<td style="border: 1px solid black;">模拟输出</td>
</tr>

</table>

<br>

### 连线方式

● **RJ25连接** 

由于声音传感器模块接口是黑色色标，当使用RJ25接口时，需要连接到主控板上带有黑色色标的接口。以 Makeblock Orion 为例，可以连接到6，7，8号接口，如图:

<img src="../../../en/electronic-modules/sensors/images/me-sound-sensor_微信截图_20160129145447.png" alt="微信截图_20160129145447" width="364" style="padding:5px 5px 12px 0px;">

> 声音传感器 模块与 Makeblock Orion连接

● **杜邦线连接** 

当使用杜邦线连接到 Arduino Uno 主板时，模块A0引脚需要连接到ANALOG（模拟）口，如下图所示:

<img src="../../../en/electronic-modules/sensors/images/me-sound-sensor_微信截图_20160129145519.png" alt="微信截图_20160129145519" width="369" style="padding:5px 5px 12px 0px;">

> 声音传感器 与 Arduino UNO 连接图

### 编程指导

● **Arduino 编程** 

如果使用Arduino编程，需要调用库 ``Makeblock-Library-master`` 来控制声音传感器。 

本程序通过 Arduino 编程让模块检测周围环境声音大小。 

<img src="../../../en/electronic-modules/sensors/images/me-sound-sensor_微信截图_20160129145554.png" alt="微信截图_20160129145554" width="387" style="padding:5px 5px 12px 0px;">

| 函数                         | 功能         |
|------------------------------|--------------|
| MeSoundSensor(uint8\_t port) | 选定接口     |
| int strength()               | 测量声音强度 |

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeSoundSensor(uint8_t port</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">int strength()</td>
<td style="border: 1px solid black;">测量声音强度</td>
</tr>

</table>

<br>

从声音传感器读取测量结果,将结果输出到 Arduino IDE 串口监视器,
周期为100ms。 

上传代码到Makeblock主板点击 Arduino 串口监视器，将看到运行结果如下：

<img src="../../../en/electronic-modules/sensors/images/me-sound-sensor_微信截图_20160129145642.png" alt="微信截图_20160129145642" width="474" style="padding:5px 5px 12px 0px;">

● **mBlock 编程** 

声音传感器模块支持 mBlock 编程环境，如下是该模块指令简介

<table>
<thead>
<tr class="header">
<th>编程说明</th>
<th>介绍</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><img src="images/me-sound-sensor_123123123123134.png" alt="微信截图_20160129145642"  style="padding:5px 5px 12px 0px;"></td>
<td>参数: 选定接口;<br style="padding:5px 5px 12px 0px;">
功能: 返回声音强度数值;</td>
</tr>
</tbody>
</table>

<br>

以下是如何使用mBlock控制 声音传感器 模块的例子：  
本程序可以通过声音的变化控制画笔。画笔会把声波画出，以下是运行结果:  

<img src="images/me-sound-sensor_22323525252345234村.jpg" alt="Me Sound Sensor" width="707" style="padding:5px 5px 12px 0px;">

### 原理图

<img src="../../../en/electronic-modules/sensors/images/me-sound-sensor_Me-Sound-Sensor.png" alt="Me Sound Sensor" width="1507" style="padding:5px 5px 12px 0px;">
