# 火焰传感器

![](../../../en/electronic-modules/sensors/images/me-flame-sensor_Me-Flame-Sensor.jpg)

<img src="../../../en/electronic-modules/sensors/images/me-flame-sensor_微信截图_20160129135225.png" alt="微信截图_20160129135225" width="241" style="padding:5px 5px 12px 0px;">

### 概述

火焰传感器可用于检测波长范围为 760 nm至1100 nm 的火源或光源。检测角度可达60度，可调节检测精度。检测到火焰时，其蓝色指示灯将亮起。可应用于消防机器人，火焰报警等安全监控项目。该模块的端口有黑色ID，RJ25电缆可用于连接Makeblock Orion 上带有黑色 ID 的端口。

### Technical specifications

- 工作电压：5V DC
- 检测谱带：840-1200nm
- 检测角度：60°
- 反馈时间：15μs
- 控制模式：数字和模拟端口
- 模块尺寸：51 x 24 x 18mm（长x宽x高）

### 功能特性

- 火焰高度为5cm时，探测距离为1m
- 可根据灵敏度调节板载电位器
- 防反接保护 - 反向连接电源不会损坏IC
- 模块的白色区域是接触金属梁的参考区域
- 采用RJ25接口，连接方便
- 提供数字和模拟信号的输出端口
- 提供引脚型端口，支持包括Arduino系列在内的大多数开发板

### 引脚定义

火焰传感器有四个引脚的，其功能如下：

<img src="../../../en/electronic-modules/sensors/images/me-flame-sensor_微信截图_20160129135537.png" alt="微信截图_20160129135537" width="785" style="padding:5px 5px 12px 0px;">

### 接线方式

● **RJ25连接**  

由于火焰传感器模块接口是黑色色标，当使用RJ25接口时，需要连接到主控板上带有黑色色标的接口。以 Makeblock Orion 为例，可以连接到6，7，8号接口，如图：

<img src="../../../en/electronic-modules/sensors/images/me-flame-sensor_微信截图_20160129135616.png" alt="微信截图_20160129135616" width="360" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**

当使用杜邦线连接到 Arduino Uno 主板的时候，DO引脚应连接到数字端口，AO引脚应连接到模拟端口，如下图所示：

<img src="../../../en/electronic-modules/sensors/images/me-flame-sensor_微信截图_20160129135715.png" alt="微信截图_20160129135715" width="316" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino编程**

如果使用Arduino编程，需要调用库[`Makeblock-Library-master`](https://github.com/Makeblock-official/Makeblock-Libraries/tree/master/examples/Me_Gyro/MeGyroTest)来控制火焰传感器。

以下是通过 Arduino 编程来识别是否存在火焰：

<img src="../../../en/electronic-modules/sensors/images/me-flame-sensor_微信截图_20160129135754.png" alt="微信截图_20160129135754" width="456" style="padding:5px 5px 12px 0px;">

<img src="../../../en/electronic-modules/sensors/images/me-flame-sensor_微信截图_20160129135828.png" alt="微信截图_20160129135828" width="1096" style="padding:5px 5px 12px 0px;">

本程序通过火焰传感器读取检测结果，并将结果输出到 Arduino IDE 中的串行监视器。将代码段上传到 Makeblock Orion 并单击 Arduino 串口监视器，您将看到如下运行结果：

<img src="../../../en/electronic-modules/sensors/images/me-flame-sensor_微信截图_20160129135900.png" alt="微信截图_20160129135900" width="827" style="padding:5px 5px 12px 0px;">

### 原理分析

火焰由各种燃烧产物，中间体，高温气体，碳氢化合物和高温固体颗粒（主要是无机材料）组成。火焰的热辐射具有不连续光谱的气体发射和具有连续光谱的固体辐射。火焰的辐射强度和波长分布随着燃烧物体而变化。通常，火焰的近红外光具有很大的辐射强度。 火焰传感器就是基于此功能制作的。

火焰传感器可以检测波长为 700 nm至1200 nm 的红外光。当红外波长接近940nm时，它达到最高灵敏度。 Me火焰传感器的探头将周围红外光的强度转换为电流的变化，然后进行模数转换（ADC）以识别附近是否有火灾。

### 原理图

<img src="../../../en/electronic-modules/sensors/images/me-flame-sensor_Me-Flame.png" alt="Me Flame" width="1242" style="padding:5px 5px 12px 0px;">
