# 巡线传感器


![](../../../en/electronic-modules/sensors/images/me-line-follower_Me-Line-Follower.jpg)

<img src="../../../en/electronic-modules/sensors/images/me-line-follower_微信截图_20160129150749.png" alt="微信截图_20160129150749" width="241" style="padding:5px 5px 12px 0px;">

### 概述

巡线模块专为巡线机器人设计。它包含两个传感器，每个传感器有一个红外发射LED和一个红外感应光电晶体管，机器人能够沿着白色背景上的黑色线条移动，或是黑色背景上的白色线条移动，具有检测速度快，电路简单等优点。本模块接口是蓝色色标，说明是双数字口控制，需要连接到主板上带有蓝色标识接口。

### 技术规格

- 工作电压：5V DC  
- 工作温度：0到70℃  
- 检测范围：1到2厘米  
- 检测角度：120°范围内  
- 控制方式：双数字口控制  
- 模块尺寸：51 x 24 x 22 mm (长x宽x高)

### 功能特性

- 模块的白色区域是与金属梁接触的参考区域；  
- 具有两只LED指示灯用于巡线反馈；  
- 具有反接保护，电源反接不会损坏IC；  
- 注意本模块容易受自然光的影响，对环境光变化较大的场所有一定的局限性；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持mBlock图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有S1、S2、VCC、GND接头支持绝大多数 Arduino 系列主控板。

### 引脚定义

巡线模块有四个针脚的接头,每个针脚的功能如下表:

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能 </th>
</tr>

<tr>
<td style="border: 1px solid black;"> 1 </td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">接地</td>
</tr>

<tr>
<td style="border: 1px solid black;">2 </td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;"> 接电源 </td>
</tr>

<tr>
<td style="border: 1px solid black;"> 3 </td>
<td style="border: 1px solid black;">S1</td>
<td style="border: 1px solid black;">传感器1数据输出</td>
</tr>

<tr>
<td style="border: 1px solid black;"> 4 </td>
<td style="border: 1px solid black;">S2</td>
<td style="border: 1px solid black;">传感器2数据输出</td>
</tr>
</table>

<br>


### 连接方式

● **RJ25连接**  

由于巡线传感器接口是蓝色色标，当使用RJ25接口时，需要连接到主控板上带有蓝色色标的接口。以 Makeblock Orion 为例，可以连接到3，4，5，6号接口，如下图:

<img src="../../../en/electronic-modules/sensors/images/me-line-follower_微信截图_20160129151012.png" alt="微信截图_20160129151012" width="421" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**  

当使用杜邦线连接到 Arduino Uno 主板时，模块S1与S2引脚需要连接到
DIGITAL（数字）口，如下图所示：

<img src="../../../en/electronic-modules/sensors/images/me-line-follower_微信截图_20160129151050.png" alt="微信截图_20160129151050" width="426" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino 编程**  
如果使用Arduino编程，需要调用库`Makeblock-Library-master`
来控制巡线模块

<img src="../../../en/electronic-modules/sensors/images/me-line-follower_微信截图_20160129151123.png" alt="微信截图_20160129151123" width="519" style="padding:5px 5px 12px 0px;">


<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">方程</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeLineFollower(uint8_t port)</td>
<td style="border: 1px solid black;">定义连接端口</td>
</tr>

<tr>
<td style="border: 1px solid black;">uint8_t readSensors()</td>
<td style="border: 1px solid black;">读取传感器状态</td>
</tr>

</table>

<br>

本程序通过Arduino编程每200毫秒不断读取传感器的值,从串口监视器可以看出两个传感器是否在黑线之外:

<img src="../../../en/electronic-modules/sensors/images/me-line-follower_微信截图_20160129151218.png" alt="微信截图_20160129151218" width="468" style="padding:5px 5px 12px 0px;"> 

● **mBlock 编程**  

巡线模块支持mBlock编程环境，如下是该模块指令简介:

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">编程说明</th>
<th style="border: 1px solid black;">描述</th>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-line-follower_巡线.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">参数一：选择接口</td>
</tr>
</table>

<br>