# 限位开关


![](../../../en/electronic-modules/sensors/images/me-micro-switch-ab_Me-Micro-Switch-A.jpg)

<img src="../../../en/electronic-modules/sensors/images/me-micro-switch-ab_微信截图_20160126151-1.png" alt="微信截图_20160126151" width="237" style="padding:5px 5px 12px 0px;">

### 概述

限位开关是一款物理开关，可以与Makeblock零件完美配合使用。当它被触发时，它会发送一个信号给控制端。它可以通过电线和RJ25线连接到Orion开发板模块，并通过自包含的支架连接到Makeblock机械部件。

### 技术规格

- 额定电压: 250V AC  
- 额定电流: 1A  
- 接触电阻: ≤50 mΩ  
- 孤立电阻: ≥1 MΩ  
- 开关寿命: 10,000 times  
- 尺寸: 20 x 6 x 24 mm (L x W x H) 不包括线

### 功能特性

- 小巧易安装  
- 提供连接线避免用户再焊  
- 支持arduino库方便用户编程  
- 控制机械设备，并使用限位保护和开关电路元件  
- 机械寿命和电子寿命长

### 支架三维图 (unit: mm)

<img src="../../../en/electronic-modules/sensors/images/me-micro-switch-ab_微信截图_20160129121434.png" alt="微信截图_20160129121434" width="343" style="padding:5px 5px 12px 0px;">

### 连接图

<img src="../../../en/electronic-modules/sensors/images/me-micro-switch-ab_微信截图_20160129121515.png" alt="微信截图_20160129121515" width="484" style="padding:5px 5px 12px 0px;">

<img src="../../../en/electronic-modules/sensors/images/me-micro-switch-ab_微信截图_20160129121530.png" alt="微信截图_20160129121530" width="325" style="padding:5px 5px 12px 0px;">
