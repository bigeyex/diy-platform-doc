# 颜色传感器


<img src="../../../en/electronic-modules/sensors/images/color-sensor-2.jpg" alt="微信截图_20160129150749" width="241" style="padding:5px 5px 12px 0px;">

### 概述

颜色传感器是一款可识别多种颜色的颜色传感器。可识别黑、黄、红、蓝、绿、白六种颜色。本模块接口是蓝白色标，说明是I2C通信模式，需要连接到主板上带有蓝白色标识接口。

### 技术规格

- 工作电压：5V  
- 工作电流：&lt;5 mA  
- 工作温度：-20～60℃  
- 信号模式：I2C通信  
- 模块尺寸：48x24x18mm（长x宽x高）

### 功能特性

- 模块的白色区域是与金属梁接触的参考区域；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持 mBlock 图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列。

### 引脚定义

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">SCL</td>
<td style="border: 1px solid black;">I2C通讯（时钟管脚）</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">SDA</td>
<td style="border: 1px solid black;">I2C通讯（数据管脚）</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线</td>
</tr>

<tr>
<td style="border: 1px solid black;">5</td>
<td style="border: 1px solid black;">S1</td>
<td style="border: 1px solid black;">补光灯控制管脚 </td>
</tr>

</table>

<br>

### 连线模式

● **RJ25连接** 

由于颜色传感器模块接口是蓝白色色标，当使用RJ25接口时，需要连接到主控板上带有蓝白色色标的接口。

以 Makeblock MegaPiPro 为例，可以连接到6、7、8、9、10、11、12号接口，如图：

<img src="../../../en/electronic-modules/sensors/images/me-color-sensor-v1_Megapi.png" alt="微信截图_20160129151012" width="721" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**  

当使用杜邦线连接到 Arduino Mega2560 主板的时候，模块SCL、SDA引脚需要连接到I2C接口，S1、S2引脚需要连接到数字接口，即连接到A10、A11接口如下图所示：

<img src="../../../en/electronic-modules/sensors/images/arduino-mega.png" alt="微信截图_20160129151050" width="706" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino 编程**  

如果使用Arduino编程，需要调用库 ``Makeblock-Library-master`` 
来控制颜色传感器模块。

本程序通过 Arduino 编程，使用颜色传感器识别不同颜色时，我们可通过串口监视器查看颜色数据。

<img src="../../../en/electronic-modules/sensors/images/color-sensor-arduino-program.png" alt="微信截图_20160129151218" width="468" style="padding:5px 5px 12px 0px;">  

● **mBlock 编程** 

颜色传感器模块支持 mblock 编程环境，该模块指令使用简介如下：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">模块</th>
<th style="border: 1px solid black;">描述</th>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="../../../en/electronic-modules/sensors/images/color-sensor-mblock-program.png" alt="微信截图_20160129151218" width="400" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">参数一： 选择模块所连接的端口</td>
</tr>

</table>

<br>