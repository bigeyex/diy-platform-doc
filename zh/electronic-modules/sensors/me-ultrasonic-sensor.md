# 超声波模块


![](../../../en/electronic-modules/sensors/images/me-ultrasonic-sensor_Me-Ultrasonic-Sensor.jpg)

<img src="../../../en/electronic-modules/sensors/images/me-ultrasonic-sensor_微信截图_20160129143938.png" alt="微信截图_20160129143938" width="246" style="padding:5px 5px 12px 0px;">

### 超声波模块

超声波模块是一个用来测量距离的电子模块，测量范围是 3 cm 到 400
cm.。可以用来帮助小车避开障碍或加入其他有关测距的项目。本模块接口是黄色色标，说明是单数字接口，需要连接到主板上的黄色标识的接口。

### 技术规格

- 工作电压： 5V DC  
- 工作温度：-25～+80℃  
- 测量角度：30度范围内  
- 测量范围：3-400cm（误差小于1cm）  
- 超声波频率：42kHz  
- 控制方式：单数字口控制  
- 模块尺寸：56 x 36 x 31 mm (长x宽x高)

### 功能特性

- 具有反接保护，电源反接不会损坏IC；  
- 模块的白色区域是与金属梁接触的参考区域；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持 mBlock 图形化编程，适合全年龄用户；  
- 使用 RJ25 接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有接头支持绝大多数Arduino系列主控板。

### 引脚定义

超声波模块有三个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能 </th>
</tr>

<tr>
<td style="border: 1px solid black;"> 1 </td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">接地</td>
</tr>

<tr>
<td style="border: 1px solid black;">2 </td>
<td style="border: 1px solid black;">5V</td>
<td style="border: 1px solid black;"> 电源线 </td>
</tr>

<tr>
<td style="border: 1px solid black;"> 3 </td>
<td style="border: 1px solid black;">SIG</td>
<td style="border: 1px solid black;">距离信号输出引脚</td>
</tr>

</table>

<br>

### 接线方式

● **RJ25连接**

由于超声波模块接口是黄色色标，当使用RJ25接口时，需要连接到主控板上带有黄色色标的接口。以 Makeblock Orion 为例，可以连接到3，4，5，6，7，8 号接口，如图:  

<img src="../../../en/electronic-modules/sensors/images/me-ultrasonic-sensor_微信截图_20160129144158.png" alt="微信截图_20160129144158" width="376" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**  

当使用杜邦线连接到 Arduino Uno 主板时，模块SIG引脚需要连接到
DIGITAL（数字）口，如下图所示：

<img src="../../../en/electronic-modules/sensors/images/me-ultrasonic-sensor_微信截图_20160129144228.png" alt="微信截图_20160129144228" width="387" style="padding:5px 5px 12px 0px;">

### 编程指南

● **Arduino 编程**

如果使用 Arduino 编程，需要调用库 ``Makeblock-Library-master`` 来控制
超声波模块。本程序通过 Arduino 编程让模块测量距离。

<img src="../../../en/electronic-modules/sensors/images/me-ultrasonic-sensor_微信截图_20160129144319.png" alt="微信截图_20160129144319" width="511" style="padding:5px 5px 12px 0px;">

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">方程</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeUltrasonicSensor(uint8_t port)</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">long distanceCm()</td>
<td style="border: 1px solid black;">测量距离（厘米）</td>
</tr>

<tr>
<td style="border: 1px solid black;">long distanceInch()</td>
<td style="border: 1px solid black;">测量距离（英寸）</td>
</tr>

</table>

<br>


本代码的功能是读取测量距离（两种单位）输出结果到 Arduino IDE串口监视器，
周期为 100ms。

上传代码到Makeblock主板点击Arduino串口监视器，您将看到运行结果如下： 

<img src="../../../en/electronic-modules/sensors/images/me-ultrasonic-sensor_微信截图_20160129144417.png" alt="微信截图_20160129144417" width="471" style="padding:5px 5px 12px 0px;">

● **mBlock 编程**

超声波模块支持mBlock编程环境，如下是该模块指令简介：

<img src="../../../en/electronic-modules/sensors/images/me-ultrasonic-sensor_微信截图_20160129144500.png" alt="微信截图_20160129144500" width="790" style="padding:5px 5px 12px 0px;">

以下是如何使用mBlock控制超声波模块的例子  
本程序可以让小熊猫说出超声波检测的距离。运行结果如下： 

<img src="../../../en/electronic-modules/sensors/images/me-ultrasonic-sensor_微信截图_20160129144530.png" alt="微信截图_20160129144530" width="787" style="padding:5px 5px 12px 0px;">

### 原理解析

超声波模块中，超声波发射器向某一方向发射超声波，在发射的同时开始计时，超声波在空气中传播，途中碰到障碍物就立即返回来，超声波接收器收到反射波就立即停止计时。声波在空气中的传播速度为340m/s，根据计时器记录的时间t，就可以计算出发射点距障碍物的距离s，即：s=340m/s×t/2
。

### 原理图

<img src="../../../en/electronic-modules/sensors/images/me-ultrasonic-sensor_Me_Ultrasonic_V3.0.png" alt="Me_Ultrasonic_V3.0" width="1512" style="padding:5px 5px 12px 0px;">
