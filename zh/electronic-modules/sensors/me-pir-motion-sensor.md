# 人体红外传感器


![](../../../en/electronic-modules/sensors/images/me-pir-motion-sensor_Me-PIR-Motion-Sensor.jpg)

<img src="../../../en/electronic-modules/sensors/images/me-pir-motion-sensor_微信截图_20160129145803.png" alt="微信截图_20160129145803" width="171" style="padding:5px 5px 20px 0px;">

### 概述

人体红外传感器是用来检测人或动物身体上发出的红外辐射的模块，最大测量范围为6m。如果有人在量程内运动，DO引脚将会输出有效信号，板上的蓝色LED会被点亮。本模块接口是蓝色色标，说明是双数字接口，需要连接到主板上的蓝色标识的接口。

### 技术规格

-  工作电压： 5V DC  
-  工作温度: -20℃\~ + 70℃  
-  输出电压: 5 V /高电平，0 V /低电平  
-  触发信号: 5 V /高电平  
-  保持时间: 2秒  
-  检测角度: 120度  
-  检测距离: 最大6米  
-  尺寸: 51 x 24 x 18 mm (长x宽x高)

### 功能特性

-  模块上有电位器，可以调节灵敏度；  
-  内部的双向鉴幅器可有效抑制干扰；  
-  模块有两种工作模式，分为可重复触发和不可触发重复；  
-  模块的白色区域是与金属梁接触的参考区域；  
-  支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
-  支持 mBlock 图形化编程，适合全年龄用户；  
-  使用 RJ25 接口连线方便；  
-  模块化安装，兼容乐高系列；  
-  配有接头支持绝大多数 Arduino 系列主控板。

### 引脚定义

人体红外传感器模块有四个针脚的接头,每个针脚的功能如下表:


<table cellpadding="10px;" cellspacing="15px;" style="text-align:left;" padding="5px 10px 10px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">接地</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">5V</td>
<td style="border: 1px solid black;">接电源</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">mode</td>
<td style="border: 1px solid black;">检测模式设置引脚</td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">DO</td>
<td style="border: 1px solid black;">数字信号输出引脚</td>
</tr>

</table>

<br>

### 连线方式

● **RJ25连接**

由于人体红外传感器模块接口是蓝色色标，当使用 RJ25 接口时，需要连接到主控板上带有蓝色色标的接口。以 Makeblock Orion 为例，可以连接到3，4，5，6号接口，如图:

<img src="../../../en/electronic-modules/sensors/images/me-pir-motion-sensor_微信截图_20160129150011.png" alt="微信截图_20160129150011" width="381" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**

当使用杜邦线连接到 Arduino Uno 主板时，Mode 和 DO 引脚 需要连接到
DIGITAL（数字）引脚。如下图所示:

<img src="../../../en/electronic-modules/sensors/images/me-pir-motion-sensor_微信截图_20160129150042.png" alt="微信截图_20160129150042" width="414" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino编程**  

如果使用 Arduino 编程，需要调用库 ``Makeblock-Library-master`` 来控制人体红外传感器。本程序通过 Arduino 编程用模块来判断附近有无人在运动。

<img src="../../../en/electronic-modules/sensors/images/me-pir-motion-sensor_pir2.png" alt="微信截图_20160129150137" width="439" style="padding:5px 5px 12px 0px;">

<table cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding: 5px 5px 12px 0px;" >
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MePIRMotionSensor (uint8_t port)</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">isHumanDetected()</td>
<td style="border: 1px solid black;">检测是否有人走动</td>
</tr>

</table>

<br>

代码功能介绍：

读取 DO 的检测结果，并判断是否有人，有人输出 1，没人输 出0，并将结果输出到Arduino IDE 串口监视器， 周期为 100ms。

上传代码到 Makeblock 主板点击 Arduino 串口监视器，您将看到运行结果如下:

<img src="../../../en/electronic-modules/sensors/images/me-pir-motion-sensor_微信截图_20160129150321.png" alt="微信截图_20160129150321" width="602" style="padding:5px 5px 12px 0px;">

● **mBlock编程**

人体红外传感器模块支持 mBlock 编程环境. 如下是人体红外传感器控制模块简介:

- 参数：选定接口
- 功能：读取模块监测结果 (0 或 1)


以下是如何使用 mBlock 控制人体红外传感器模块的例子：  
当模块没有检测到有人移动时， 小熊猫会说 “Nobody here!”，反之，小熊猫会说“Somebody here!”。

运行结果如下:

<img src="images/me-pir-motion-sensor_123124135423.jpg" alt="微信截图_20160129150427" width="599" style="padding:5px 5px 12px 0px;">

### 原理解析

模块中运用了热释红外专用芯片 BISS0001 ，是一种能检测人或动物发射的红外线而输出电信号的传感器。它是由运算放大器、电压比较器、状态控制、延迟时间定时器以及封锁时间定时器等构成的数模混合专用集成电路。当人体辐射的红外线通过菲涅尔透镜被聚焦在热释电红外传感器的探测元上时，电路中的传感器将输出电压信号，从而检测出附近有人在移动。结构框图如下:

<img src="images/me-pir-motion-sensor_fasdfasdf.png" alt="微信截图_20160129150512" width="651" style="padding:5px 5px 12px 0px;">

此模块上有电位器，可以通过其来调节灵敏度。同时可以通过控制 Mode 引脚的电平来选择工作模式，当 Mode 引脚为高电平时，允许重复触发，即时感应。当 Mode 引脚为低电平时，为不可重复触发模式，当有人在量程内移动时，模块被触发并保持一段时间，期间有无人在量程内移动，状态都不会被干扰。

### 原理图

<img src="images/me-pir-motion-sensor_PIR-sensor.png" alt="PIR sensor" width="1542" style="padding:5px 5px 12px 0px;">
