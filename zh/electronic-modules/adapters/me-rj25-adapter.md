# RJ25转接板


![](../../../en/electronic-modules/adapters/images/me-rj25-adapter_Me-RJ25-Adapter.jpg)

<img src="../../../en/electronic-modules/adapters/images/me-rj25-adapter_微信截图_20160128162440.png" alt="微信截图_20160128162440" width="211" style="padding:5px 5px 12px 0px;">

### 概述

RJ25适配器将标准的RJ25接口转换为六个引脚（分别为VCC,GND,S1，S2，SDA,SCL），方便从MakeBlock接口引出来以兼容其他厂商的电子模块，例如温度传感器，舵机模块等。本模块需要连接到主板上带有黄或蓝或黑色标识接口。

### 技术规格

- 工作电压：5V DC  
- 最大电流：3A  
- 模块尺寸：51 x 24 x 18 mm (长x宽x高)

### 功能特性

- 红色LED为电源指示灯  
- 含有I2C接口和两个数字/模拟接口  
- 可以连接其他厂商的电子模块  
- 模块的白色区域是与金属梁接触的参考区域；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持mBlock图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；

### 引脚定义

RJ25适配器模块有六个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

</td>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">SCL </td>
<td style="border: 1px solid black;">I2C数据总线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">SDA</td>
<td style="border: 1px solid black;">I2C时钟总线</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">接地</td>
</tr>

<tr>
<td style="border: 1px solid black;">4  </td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">接电源</td>
</tr>

<tr>
<td style="border: 1px solid black;">5 </td>
<td style="border: 1px solid black;">S1</td>
<td style="border: 1px solid black;">数字、模拟口</td>
</tr>

<tr>
<td style="border: 1px solid black;">6 </td>
<td style="border: 1px solid black;">S2 </td>
<td style="border: 1px solid black;">数字、模拟口</td>
</tr>
</table>

<br>

### 接线方式

<img src="../../../en/electronic-modules/adapters/images/me-rj25-adapter_微信截图_20160128163016.png" alt="微信截图_20160128163016" width="469" style="padding:5px 5px 12px 0px;">

### 编程指南

● **Arduino编程** 

如果使用Arduino编程，需要调用库`Makeblock-Library-master` 来控制
RJ25适配器。

本程序通过 Arduino 编程读取Limit Switch状态, 输出结果到 Arduino IDE
串口监视器上。  

<img src="../../../en/electronic-modules/adapters/images/me-rj25-adapter_微信截图_20160128163321.png" alt="微信截图_20160128163321" width="568" style="padding:5px 5px 12px 0px;">

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数 </th>
<th style="border: 1px solid black;">功能 </th>
</tr>

<tr>
<td style="border: 1px solid black;">MePort(uint8_t port)</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">bool dRead1()</td>
<td style="border: 1px solid black;">从SLOT1读取数字信号 </td>
</tr>

<tr>
<td style="border: 1px solid black;">bool dRead2()</td>
<td style="border: 1px solid black;"> 从SLOT2读取数字信号</td>
</tr>

<tr>
<td style="border: 1px solid black;"> bool dWrite1()</td>
<td style="border: 1px solid black;">从SLOT1写入数字信号</td>
</tr>

<tr>
<td style="border: 1px solid black;">bool dWrite2()  </td>
<td style="border: 1px solid black;">从SLOT2写入数字信号</td>
</tr>

<tr>
<td style="border: 1px solid black;">bool aRead1() </td>
<td style="border: 1px solid black;">从SLOT1读取模拟信号</td>
</tr>

<tr>
<td style="border: 1px solid black;">bool aRead2()</td>
<td style="border: 1px solid black;">从SLOT2读取模拟信号 </td>
</tr>

<tr>
<td style="border: 1px solid black;">bool aWrite1() </td>
<td style="border: 1px solid black;">从SLOT1写入模拟信号 </td>
</tr>

<tr>
<td style="border: 1px solid black;">bool aWrite2() </td>
<td style="border: 1px solid black;">从SLOT2写入模拟信号 </td>
</tr>
</table>

<br>

代码功能介绍：读取Limit Switch状态，输出结果到串口监视器。上传代码到Makeblock主板点击串口监视器。

您将看到运行结果如下：  

<img src="../../../en/electronic-modules/adapters/images/me-rj25-adapter_微信截图_20160128163703.png" alt="微信截图_20160128163703" width="495" style="padding:5px 5px 12px 0px;">

● **mBlock编程** 
 
RJ25适配器模块支持 mBlock 编程环境. 如下是RJ25适配器控制模块简介： 
 
<img src="../../../en/electronic-modules/adapters/images/me-rj25-adapter_微信截图_20160128163747.png" alt="微信截图_20160128163747" width="799" style="padding:5px 5px 12px 0px;">

### 原理解析

本模块将主控板的IO口引出六个，分别为SDA, SCL, GND, VCC,
S1，S2。S1，S2口可以用来作数字/模拟的输入输出，SDA为I2C时钟线,SCL为I2C数据线,可以连接支持I2C总线的传感器。例如：将支持I2C协议的温度传感器串联连接在I2C总线上，可以进行组网测温。

### 原理图

<img src="images/me-rj25-adapter_adapter.png" alt="adapter" width="1027" style="padding:5px 5px 12px 0px;">
