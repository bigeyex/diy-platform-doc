# 树莓派转接板


![](images/me-shield-for-raspberry-pi_Me-Shield-for-Raspberry-Pi.jpg)

<img src="images/me-shield-for-raspberry-pi_微信截图_20160128161844.png" alt="微信截图_20160128161844" width="276" style="padding:5px 5px 12px 0px;">

### 描述

树莓派转接板是附着在树莓派板上给 baseboard 或 Orion 访问与硬件串行通信。

<img src="images/me-shield-for-raspberry-pi_微信截图_20160128162026.png" alt="微信截图_20160128162026" width="738" style="padding:5px 5px 12px 0px;">

### 特点

- 不需要电焊就可以使用
- 支持Python编程
- 兼容模型 A,B, B+
- 带有一个RJ25连接器可连接Baseboard 或Orion板
- 带有金属座，不需占有通用输入输出口

### 规格

- 尺寸: 45 x 35 x 27 mm (长 x 宽 x 高)

### 用户指导

#### 开始

<img src="images/me-shield-for-raspberry-pi_s4r_wiring.jpg" alt="s4r_wiring" width="990" style="padding:5px 5px 12px 0px;">

#### 建议阅读

[什么是树莓派?](http://www.raspberrypi.org/help/what-is-a-raspberry-pi/)

[如何在树莓派上安装操作系统映像?](http://www.raspberrypi.org/documentation/installation/installing-images/README.md)

**步骤 1 连线** 

确保你已经在树莓派上安装了操作系统映像，并通过以太网线将树莓派连接到互联网上。在你的树莓派上滑动我的转接板，并将转接板连接到 baseboard 的端口4 (Leonardo) 或 Makeblock Orion 端口5(Uno)。接下来，在baseboard上电源或 Makeblock Orion 与12V电源。(转接板已经为树莓派板提供了5V电压，所以不需要对树莓派板单独使用)

<img src="images/me-shield-for-raspberry-pi_s4r_wiring2.jpg" alt="s4r_wiring2" width="1000" style="padding:5px 5px 12px 0px;">

**步骤 2 登入树莓派**

如果你使用windows系统，那么你需要下载一个免费SSH软件（windows）[这里](https://it.wm.edu/software/public/ssh/sshsecureshellclient-3.2.9.exe)。

然后点击按纽“快速连接”来登录，输入IP,用户名，密码（用户名：pi，密码：raspberry）

<img src="images/me-shield-for-raspberry-pi_quickconnect.png" alt="quickconnect" width="595" style="padding:5px 5px 12px 0px;">

<img src="images/me-shield-for-raspberry-pi_login.png" alt="login" width="398" style="padding:5px 5px 12px 0px;">

<img src="images/me-shield-for-raspberry-pi_SSHlogin.png" alt="SSHlogin" width="605" style="padding:5px 5px 12px 0px;">


如果您在 Mac 或 linux 上，则在树莓派上打开一个带有samba服务的终端。在终端窗口中键入下面的命令。

`ssh pi@<your-rpi-ip>`

更多有用文档：<http://www.raspberrypi.org/documentation/remote-access/ssh/unix.md>.

**步骤 3禁用被占用硬件串行ttyAMA0。**

请确保在您的rip上安装了pyserial，如果不是，请键入下面的命令并等待过程完成。

`sudo apt-get install python-serial`

在树莓派上，禁用登录提示。

<img src="images/me-shield-for-raspberry-pi_serial.jpg" alt="serial" width="495" style="padding:5px 5px 12px 0px;">

**步骤 4 安装 Makeblock 的 Python 库**

输入以下命令来安装库：

`sudo pip install megapi`

Python原始代码:

```py
from megapi import *
bot = Megapi()
bot.start() # if using usb cable, need to call bot.start('/dev/ttyACM0')

python your code
```

**步骤 5 上传一个Arduino固件到 Baseboard 或 Orion**.

下载 Arduino 库：<https://github.com/Makeblock-official/Makeblock-Libraries/archive/master.zip>

将 Makeblock 文件夹复制到你的 Arduino 默认库。您的 Arduino 库文件夹现在应该是这样的:

- (On Windows):
    *\[x:\\Users\\XXX\\Documents\]\\Arduino\\libraries\\makeblock\\src*
- (On Mac OSX):
    *\[\\Users\\XXX\\Documents\]\\Arduino\\libraries\\makeblock\\src*

打开 Arduino IDE，从File&gt;example中选择固件;根据您的控制板类型(在本例中是Orion）。

<img src="images/me-shield-for-raspberry-pi_firmware.png" alt="firmware" width="842" style="padding:5px 5px 12px 0px;">

**步骤 6 连接一个电机到Orion板上的端口M1** .

确保那块板是由外部提供电源的。

<img src="images/me-shield-for-raspberry-pi_OrionMotor-266x300.png" alt="Orion&amp;Motor" width="450" style="padding:5px 5px 12px 0px;">

**步骤 7 在raspberry Pi运行演示脚本**

进入脚本的目录和运行 `motorRun.py` 文件

```
cd PythonForMegaPi

cd examples

python motorRun.py
```

### Python API

- Start
    - **MegaPi**()
    - **start**()
- GPIO
    - **digitalWrite**( pin, level )
    - **pwmWrite**( pin, pwm )
    - **digitalRead**( pin, **def** onResult )
    - **analogRead**( pin, **def** onResult )
- Motion
    - DC Motor
        - **motorRun**( port, speed )
        - **motorMove**( leftspeed, rightspeed )
    - Servo Motor
        - **servoRun**( port, slot, angle )
    - Encoder Motor
        - **encoderMotorRun**( port, speed )
        - **encoderMotorMove**( port, speed, distance, **def**
            onFinish )
        - **encoderMotorMoveTo**( port, speed, position, **def**
            onFinish )
    - Stepper Motor
        - **stepperMotorSetting**( port, microsteps, acceleration )
        - **stepperMotorRun**( port, speed )
        - **stepperMotorMove**( port, speed, distance, **def**
            onFinish )
        - **stepperMotorMoveTo**( port, speed, position, **def**
            onFinish )
- Sensors
    - Ultrasonic Sensor
        - **ultrasonicSensorRead** ( port, **def** onResult )
    - LineFollow Sensor
        - **lineFollowerRead** ( port, **def** onResult )
    - Light Sensor
        - **lightSensorRead** ( port, **def** onResult )
    - Sound Sensor
        - **soundSensorRead** ( port, **def** onResult )
    - Temperature Sensor
        - **temperatureRead** ( port, **def** onResult )
    - PIR Motion Sensor
        - **pirMotionSensorRead** ( port, **def** onResult )
    - Touch Sensor
        - **touchSensorRead** ( port, **def** onResult )
    - LimitSwitch
        - **limitSwitchRead** ( port, slot, **def** onResult )
    - Humiture Sensor
        - **humitureSensorRead** ( port, type, **def** onResult )
    - Gas Sensor
        - **gasSensorRead** ( port, **def** onResult )
    - Flame Sensor
        - **flameSensorRead** ( port, **def** onResult )
    - Button
        - **buttonRead** ( port, **def** onResult )
    - Potentiometer
        - **potentiometerRead** ( port, **def** onResult )
    - Joystick
        - **joystickRead** ( port, axis, **def** onResult )
    - 3-Axis Accelerometer and Gyro Sensor
        - **gyroRead** ( axis, **def** onResult )
    - Compass
        - **compassRead** ( **def** onResult )
- Display
    - RGB Led
        - **rgbLedSetColor** ( port, slot, index, r, g, b )
        - **rgbLedShow** ( port, slot )
        - **rgbLedDisplay** ( port, slot, index, r, g, b )
    - 7-segment Display
        - **sevenSegmentDisplay** ( port, value )
    - Led Matrix Display
        - **ledMatrixDisplayMessage** ( port, x, y, msg )
        - **ledMatrixDisplayRaw** ( port, buffer )
    - Serial LCD Display
        - **lcdDisplay** ( string )
- Others
    - DSLR Shutter
        - **shutterOn** ( port )
        - **shutterOff** ( port )
        - **focusOn** ( port )
        - **focusOff** ( port )

### 原理图

<img src="images/me-shield-for-raspberry-pi_shield-pi.png" alt="shield pi" width="993" style="padding:5px 5px 12px 0px;">
