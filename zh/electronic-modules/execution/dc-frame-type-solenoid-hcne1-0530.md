# 电磁铁模块


![](../../../en/electronic-modules/execution/images/dc-frame-type-solenoid-hcne1-0530_DC-Frame-Type-Solenoid-HCNE1-0530.jpg)

<img src="../../../en/electronic-modules/execution/images/dc-frame-type-solenoid-hcne1-0530_微信截图_20160128164127.png" alt="微信截图_20160128164127" width="215" style="padding:5px 5px 12px 0px;">

### 描述

电磁铁模块是一种基本的开放线圈式直线运动电磁阀。由于其简单的设计结构，成本较低，并提供各种尺寸和类型，以最大限度地满足您的应用需求。

### 技术规格

1.  电压:12VDC
2.  电阻:27 ohms
3.  行程:6mm
4.  扭力:50g
5.  通电率:25%

### 应用范围

1. 老虎机，硬币机等游戏机  
2. 发票机  
3. 办公设备  
4. 传送设备  
5. 本地应用  
6. 其他  


### 特征

- 易于修复和连续加载  
- 升温稳定，可长时间使用  
- E形环和橡胶垫使阀门不发热  
- 升降机使用没有摩擦长度  
- 简单与稳定的结构  
  
**\* 工作条件** 

- 操作温度-5到40℃时，阀门不会凝固  
- 操作湿度在45％至85％的实际湿度内不固化，  
- 储存温度不会在-40到75℃的温度下固化  
- 存储湿度在0％到95％的相对湿度下不固化，

**\* 注释**  

- 65℃以下升温（a级）升温：负荷超过100万  

**\* 测试温度**  

- 环境温度23±2℃  
- 可调湿度50±10％  
- 气压1013MPa  
- 500VDC绝缘测试时，绝缘电阻需要100MΩ的绝缘电阻  
- 绝缘强度的强度应该是600VAC / 1分钟
