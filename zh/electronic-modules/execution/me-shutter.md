# 快门线模块


![](../../../en/electronic-modules/execution/images/me-shutter_Me-Shutter.jpg)

<img src="../../../en/electronic-modules/execution/images/me-shutter_微信截图_20160128164616.png" alt="微信截图_20160128164616" width="256" style="padding:5px 5px 12px 0px;">

### 概述

快门线模块是一个特殊模块，旨在实现数码单反相机自动拍照的功能。用户可将其运用于拍摄高速照片，或通过定时曝光控制计以拍摄“延时”录像和照片。本模块接口是蓝色色标，说明是双数字接口，需要连接到主板上的蓝色标识的接口，并用专用线连接照相机。

### 技术规格

- 工作电压：5V  
- 控制方式：双数字口控制  
- 模块尺寸：52 x 24 x 18 mm (长x宽x高)

### 功能参数

- 可以控制相机快门和对焦；  
- 适用于不同的相机类型；  
- 延时小，性能好,抗干扰能力强；  
- 模块的白色区域是与金属梁接触的参考区域 ；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持 mBlock 图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有接头支持绝大多数Arduino系列主控板。

### 引脚定义

快门线模块有四个针脚的接头,每个针脚的功能如下表:

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

</td>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">S</td>
<td style="border: 1px solid black;">控制快门 </td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;"> F</td>
<td style="border: 1px solid black;">控制对焦 </td>
</tr>
</table>

<br>

### 连线模式

● **RJ25连接**

由于快门线模块接口是蓝色色标，当使用RJ25接口时，需要连接到主控板上带有蓝色色标的接口。

以 Makeblock Orion 为例，可以连接到3， 4， 5，6号接口，如图:

<img src="../../../en/electronic-modules/execution/images/me-shutter_微信截图_20160128165218.png" alt="微信截图_20160128165218" width="425" style="padding:5px 5px 12px 0px;"> 

● **杜邦线连接** 

当使用杜邦线连接到 Arduino Uno 主板时，数字S、F引脚需要连接到Digital（数字）口如下图所示：

<img src="../../../en/electronic-modules/execution/images/me-shutter_微信截图_20160128165333.png" alt="微信截图_20160128165333" width="396" style="padding:5px 5px 12px 0px;">

### 编程指南

● **Arduino编程** 

如果使用 Arduino 编程，需要调用库`Makeblock-Library-master` 来控制
快门线模块。

本程序通过Arduino编程让相机接收到信息时进行延时拍照。 
 
<img src="../../../en/electronic-modules/execution/images/me-shutter_微信截图_20160128165412.png" alt="微信截图_20160128165412" width="463" style="padding:5px 5px 12px 0px;">

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:50%">函数</th>
<th style="border: 1px solid black;width:50%">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeShutter(uint8_t port)</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">void shotOn()</td>
<td style="border: 1px solid black;">向照相机快门输出低电平</td>
</tr>

<tr>
<td style="border: 1px solid black;">void shotOff() </td>
<td style="border: 1px solid black;">向照相机快门输出高电平</td>
</tr>

<tr>
<td style="border: 1px solid black;">void focusOn()</td>
<td style="border: 1px solid black;">向照相机对焦输出低电平</td>
</tr>

<tr>
<td style="border: 1px solid black;">void focusOff() </td>
<td style="border: 1px solid black;">向照相机对焦输出高电平</td>
</tr>
</table>

<br>

代码功能介绍：通过对快门和对焦的控制，实现现相机的延时拍照功能。

### 原理图

<img src="../../../en/electronic-modules/execution/images/me-shutter_Shutter.png" alt="Shutter" width="1259" style="padding:5px 5px 12px 0px;">
