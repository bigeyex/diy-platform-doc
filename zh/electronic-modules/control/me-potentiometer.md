# 电位器模块


![](../../../en/electronic-modules/control/images/me-potentiometer_potentionmeter.jpg)

<img src="../../../en/electronic-modules/control/images/me-potentiometer_微信截图_20160128165613.png" alt="微信截图_20160128165613" width="223" />

### 概述

电位器模块含有最高电阻为 10K 的电位器。电位器是具有三个引出端，阻值可由旋钮旋转调节的电阻元件，它可以被用来调整电机转速，LED灯亮度等。本模块接口是黑色色标，说明是模拟信号接口，需要连接到主板上的黑色标识的接口。

### 技术规格

- 工作电压：5V DC  
- 最大电流：30mA  
- 额定功率：0.1W  
- 旋转角度：280度  
- 总电阻： 10KΩ  
- 信号类型：模拟信号(范围从0到980)  
- 模块大小： 51 x 24 x 22 mm (长x宽x高)

### 功能特性

- 模块上有蓝色LED灯，亮度变化代表当前模拟输出值的变化；  
- 模块的白色区域是与金属梁接触的参考区域；  
- 支持Arduino IDE编程, 并且提供运行库来简化编程；  
- 支持mBlock图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有接头支持绝大多数Arduino系列主控板。

### 引脚定义

电位器模块有三个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

</td>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;"> 2 </td>
<td style="border: 1px solid black;"> VCC</td>
<td style="border: 1px solid black;">电源线 </td>
</tr>

<tr>
<td style="border: 1px solid black;"> 3</td>
<td style="border: 1px solid black;">AO</td>
<td style="border: 1px solid black;">电位器模拟输出（电压范围：0~4.8V）</td>
</tr>
</table>

<br>

### 接线方式

● **RJ25连接** 

由于电位器模块接口是黑色色标，当使用RJ25接口时，需要连接到主控板上带有黑色色标的接口。

以 Makeblock Orion 为例，可以连接到6，7，8 号接口，如图：

<img src="../../../en/electronic-modules/control/images/me-potentiometer_微信截图_20160128170029.png" alt="微信截图_20160128170029" width="339" />

● **杜邦线连接**  

当使用杜邦线连接到 Arduino Uno主板时，模块AO引脚需要连接到ANALOG（模拟）口如下图所示：

<img src="../../../en/electronic-modules/control/images/me-potentiometer_微信截图_20160128170200.png" alt="微信截图_20160128170200" width="404" />

### 编程指南

● **Arduino 编程**  

如果使用Arduino编程，需要调用库`Makeblock-Library-master`
来控制并读取电位器当前输出数值。  

<img src="../../../en/electronic-modules/control/images/me-potentiometer_微信截图_20160128170249.png" alt="微信截图_20160128170249" width="403" />

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MePotentiometer(uint8t port)</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">int read() </td>
<td style="border: 1px solid black;">读取电位器输出数值(范围：0~980)</td>
</tr>
</table>

<br>

代码功能介绍：读取电位器参数， 将结果输出到 Arduino IDE 串口监视器，周期为100ms。

上传代码到 Makeblock 主板点击 Arduino 串口监视器。将看到运行结果如下：

<img src="../../../en/electronic-modules/control/images/me-potentiometer_微信截图_20160128170858.png" alt="微信截图_20160128170858" width="460" />

电位器数值范围为 0 ~ 980。  
当你逆时针旋转时，数值将减小，反之，数值将升高。

● **mBlock 编程**  

电位器模块支持 mBlock 编程环境。如下是电位器 控制模块简介：  

<img src="../../../en/electronic-modules/control/images/me-potentiometer_微信截图_20160128170944.png" alt="微信截图_20160128170944" width="790" />

如下是如何使用 mBlock 控制电位器模块的例子： 

本程序将会让小熊猫说出电位器输出的模拟值并且移动到相应的横坐标。范围是 0~980。运行结果如下：  

<img src="../../../en/electronic-modules/control/images/me-potentiometer_微信截图_20160128171021.png" alt="微信截图_20160128171021" width="790" />

### 原理解析

本模块主要元件为电位器。电位器是具有三个引出端、阻值可按某种变化规律调节的电阻元件，通常由电阻体和可移动的电刷组成。基于串联电阻，分压的原理当电刷沿电阻体移动时，在输出端即获得与位移量成一定关系的电阻值，从而从模拟口输出数值。此模块可以配合其他模块构建有趣的项目，例如配合直流电机制作可调速的玩具车，配合LED制作可调光的台灯等。

### 原理图

<img src="../../../en/electronic-modules/control/images/me-potentiometer_Petention.png" alt="Petention" width="1143" />
