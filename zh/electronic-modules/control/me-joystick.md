# 摇杆模块

![](../../../en/electronic-modules/control/images/me-joystick_Me-Joystick.jpg)

<img src="../../../en/electronic-modules/control/images/me-joystick_微信截图_20160128172254.png" alt="微信截图_20160128172254" width="252" style="padding:5px 5px 12px 0px;">

### 概述

摇杆模块包含一个十字摇杆，可以用在控制小车的移动方向及互动视频游戏等方面。本模块接口是黑色色标，是模拟接口，需要连接到主板上带有黑色标识接口。

### 技术规格

- 工作电压：5V DC  
- 信号模式：2轴模拟输出  
- 十字摇杆：由两个电位器及平衡环组成  
- 模块尺寸：52 x 24 x 32 mm (长x宽x高)

### 功能特性

- 模块的白色区域是与金属梁接触的参考区域；  
- 平衡环将摇杆位移分为水平移动(X)与竖直移动(Y)；  
- 模拟采集电位器电压来判断摇杆的推动位置；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持mBlock图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有VCC、GND、X、Y接头支持绝大多数 Arduino 系列主控板。

### 引脚定义

摇杆模块有四个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

</td>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2 </td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;"> X</td>
<td style="border: 1px solid black;">X轴模拟量输出</td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">Y</td>
<td style="border: 1px solid black;">Y轴模拟量输出</td>
</tr>
</table>

<br>


### 连接方式

● **RJ25连接** 

由于摇杆模块接口是黑色色标，当使用RJ25接口时，需要连接到主控板上带有黑色色标的接口。

以 Makeblock Orion 为例，可以连接到6，7，8号接口，如图：  

<img src="../../../en/electronic-modules/control/images/me-joystick_微信截图_20160128172516.png" alt="微信截图_20160128172516" width="331" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**  

当使用杜邦线连接到Arduino Uno主板时，模块X、Y引脚需要连接到
ANALOG（模拟）口，如下图所示：  

<img src="../../../en/electronic-modules/control/images/me-joystick_微信截图_20160128172549.png" alt="微信截图_20160128172549" width="336" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino 编程** 

如果使用Arduino编程，需要调用库`Makeblock-Library-master` 来控制摇杆模块。

<img src="../../../en/electronic-modules/control/images/me-joystick_微信截图_20160128172722.png" alt="微信截图_20160128172722" width="512" style="padding:5px 5px 12px 0px;"> 

**函数功能列表**

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:50%;">函数</th>
<th style="border: 1px solid black;width:50%">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeJoystick(uint8_t port)</td>
<td style="border: 1px solid black;">选定接口 </td>
</tr>

<tr>
<td style="border: 1px solid black;">int readX()</td>
<td style="border: 1px solid black;">读取X-轴模拟输出(范围：-490~490)</td>
</tr>

<tr>
<td style="border: 1px solid black;">int readY()</td>
<td style="border: 1px solid black;">读取Y-轴模拟输出(范围：-490~490) </td>
</tr>
</table>

<br>